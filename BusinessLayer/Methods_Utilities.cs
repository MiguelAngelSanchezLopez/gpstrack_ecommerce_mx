﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;

namespace BusinessLayer
{
  public class Methods_Utilities
  {
    private ModelEntities _context = new ModelEntities();

    public DateTime GetDate()
    {
      try
      {
        return (DateTime)_context.Track_GetDate().FirstOrDefault().Fecha;
      }
      catch (Exception)
      {
        return new DateTime();
      }
    }

    public string SessionExpirationStatus()
    {
      try
      {
        Configuracion _configuracion = (from c in _context.Configuracion where c.Clave == "SessionExpirationStatus" select c).FirstOrDefault();

        string _sessionExpirationStatus = _configuracion.Valor;

        return _sessionExpirationStatus;
      }
      catch (Exception e)
      {
        return "0";
      }
    }

  }
}
