﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Data.Objects;
using System.Configuration;
using CrossCutting;
using System.Data.SqlClient;
using System.Data;

namespace BusinessLayer
{
    public class Methods_Viajes
    {
        private ModelEntities _context = new ModelEntities();

        public List<Track_InforWindows_Result> InfoWindows(long nroTransporte)
        {
            try
            {
                return _context.Track_InforWindows(nroTransporte).ToList();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Track_InforWindows_Result>();
            }
        }

        public List<PoolPlaca> GetTransportistabyCedis(string cedis)
        {
            try
            {
                List<PoolPlaca> _listTransportistas = (from c in _context.PoolPlaca where (cedis.Contains(c.Cedis) || cedis == "Todos" || cedis == "Todas") select c).Distinct().ToList();
                return _listTransportistas;
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<PoolPlaca>();
            }
        }

        public List<PoolPlacaRemolque> GetAllPatentesPoolRemolques(string transportista, bool _all = false)
        {
            try
            {
                List<PoolPlacaRemolque> _listaPatentes = (from c in _context.PoolPlacaRemolque where (c.Carrier == transportista || transportista == "Todos" || transportista == "Todas" || transportista == "") select c).ToList();
                if (_all)
                {
                    PoolPlacaRemolque newItem = new PoolPlacaRemolque { Placa = "Todas" };
                    _listaPatentes.Insert(0, newItem);
                }

                return _listaPatentes;

            }
            catch (Exception)
            {
                return new List<PoolPlacaRemolque>();
            }
        }

        public List<PoolPlacaRemolque> GetAllCarrierPool(bool _all = false)
        {
            try
            {
                List<PoolPlacaRemolque> _listaCarrier = (from c in _context.PoolPlacaRemolque select c).Distinct().ToList();
                if (_all)
                {
                    PoolPlacaRemolque newItem = new PoolPlacaRemolque { Carrier = "Todos" };
                    _listaCarrier.Insert(0, newItem);
                }

                return _listaCarrier;

            }
            catch (Exception e)
            {
                return new List<PoolPlacaRemolque>();
            }
        }

        public List<Track_GetCamionesCercanosBackhaul_Result> GetCamionesCercanosBackhaul()
        {
            try
            {
                List<Track_GetCamionesCercanosBackhaul_Result> _lista = _context.Track_GetCamionesCercanosBackhaul().ToList();
                return _lista;

            }
            catch (Exception)
            {
                return new List<Track_GetCamionesCercanosBackhaul_Result>();
            }
        }

        public List<PoolPlaca> GetAllPatentesPool(string transportista, bool _all = false)
        {
            try
            {
                List<PoolPlaca> _listaPatentes = (from c in _context.PoolPlaca where (c.Transporte == transportista || transportista == "Todos" || transportista == "Todas" || transportista == "") select c).ToList();
                if (_all)
                {
                    PoolPlaca newItem = new PoolPlaca { Placa = "Todas" };
                    _listaPatentes.Insert(0, newItem);
                }

                return _listaPatentes;

            }
            catch (Exception)
            {
                return new List<PoolPlaca>();
            }
        }


        public List<PoolPlaca> GetAllCedisPool(string transportista, string userName, bool _all = false)
        {
            
            try
            {
                int idUsuario;
                string[] CEDISAsociado;

                List<PoolPlaca> _listaCedis = new List<PoolPlaca>();

                int perfil = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdRol).FirstOrDefault();

                if (perfil > 2)
                {
                    idUsuario = (int)(from c in _context.Track_Usuarios where (c.Usuario == userName) select c.IdUsuario).FirstOrDefault();

                    CEDISAsociado = (from c in _context.Track_UsuariosCEDIS where (c.IdUsuario == idUsuario) select c.DeterminanteCEDIS.ToString()).ToArray();

                    _listaCedis = (from c in _context.PoolPlaca where (CEDISAsociado.Contains(c.Determinante.ToString())) && (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
                }
                else
                {
                    _listaCedis = (from c in _context.PoolPlaca where (c.Transporte == transportista || transportista == "Todos" || transportista == "") select c).Distinct().ToList();
                }

                if (_all)
                {
                    PoolPlaca newItem = new PoolPlaca { Cedis = "Todas" };
                    _listaCedis.Insert(0, newItem);
                }

                return _listaCedis;

            }
            catch (Exception e)
            {
                return new List<PoolPlaca>();
            }
            
            //return new List<PoolPlaca>();
        }

        public List<Track_GetInformeViajes_Result> GetInformeViajes(DateTime desde, DateTime hasta, long nroTransporte, string patente, string transportista, string userName)
        {
            /*comentadas estas lineas por fallas en un parametro*/
            /*
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetInformeViajes_Result> _listViajes = _context.Track_GetInformeViajes(desde, hasta, transportista, patente, nroTransporte, userName).ToList();
                return _listViajes;

            }
            catch (Exception ex)
            {
                return new List<Track_GetInformeViajes_Result>();
            }
            */
            try
            {
                
                InicializarConexion();

                List<Track_GetInformeViajes_Result> _listViajes = new List<Track_GetInformeViajes_Result>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetInformeViajes");

                EstablecerParametroProcedimientoAlmacenado(desde, "datetime", "@desde");
                EstablecerParametroProcedimientoAlmacenado(hasta, "datetime", "@hasta");
                EstablecerParametroProcedimientoAlmacenado(transportista, "string", "@transportista");
                EstablecerParametroProcedimientoAlmacenado(patente, "string", "@patente");
                EstablecerParametroProcedimientoAlmacenado(nroTransporte, "int", "@nroTransporte");
                EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");

                LlenarLectorDatosSQL();

                _listViajes = LlenarListadoViajesInforme();

                conexionSQL.Close();

                return _listViajes;

            }
            catch (Exception ex)
            {
                return new List<Track_GetInformeViajes_Result>();
            }
        }


        /*
        public List<Track_GetProveedoresGPS_Result> GetProveedoresGPS(bool _all, string rutTransportista)
        {
            
            try
            {
                List<Track_GetProveedoresGPS_Result> _listProveedores = _context.Track_GetProveedoresGPS(rutTransportista).ToList();

                if (_all)
                {
                    Track_GetProveedoresGPS_Result item = new Track_GetProveedoresGPS_Result { ProveedorGPS = "Todos" };
                    _listProveedores.Insert(0, item);
                }

                return _listProveedores;

            }
            catch (Exception ex)
            {
                return new List<Track_GetProveedoresGPS_Result>();
            }
            
            //return new List<Track_GetProveedoresGPS_Result>();
        }

        */
        /*
        public List<Track_GetProveedoresGPS_Result> GetProveedoresGPS(bool _all, string rutTransportista)
        {
            try
            {
                List<Track_GetProveedoresGPS_Result> _listProveedores = _context.Track_GetProveedoresGPS(rutTransportista).ToList();

                if (_all)
                {
                    Track_GetProveedoresGPS_Result item = new Track_GetProveedoresGPS_Result { ProveedorGPS = "Todos" };
                    _listProveedores.Insert(0, item);
                }

                return _listProveedores;

            }
            catch (Exception ex)
            {
                return new List<Track_GetProveedoresGPS_Result>();
            }
        }
        */
        #region METODOS PRUEBA CONSUMO STORED PROCEDURES SIN ENTITY FRAMEWORKS
        protected SqlConnection conexionSQL;
        protected SqlCommand comandoSQL;
        protected SqlDataReader lectorDatosSQL;

        public string CADENA_CONEXION = ConfigurationManager.ConnectionStrings["ConexionPerfilamiento"].ConnectionString;

        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.ExecuteNonQuery();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public Track_GetProveedoresGPS_Result LlenarProveedorGPS()
        {
            Track_GetProveedoresGPS_Result proveedorGPSTemporal = new Track_GetProveedoresGPS_Result();
            proveedorGPSTemporal.ProveedorGPS = lectorDatosSQL["ProveedorGPS"].ToString();
            
            return proveedorGPSTemporal;
        }

        public List<Track_GetProveedoresGPS_Result> LlenarListadoProveedoresGPS(List<Track_GetProveedoresGPS_Result> listadoProveedoresGPS)
        {
            while (lectorDatosSQL.Read())
                listadoProveedoresGPS.Add(LlenarProveedorGPS());

            return listadoProveedoresGPS;
        }
        
        public List<Track_GetInformeViajes_Result> LlenarListadoViajesInforme()
        {
            List<Track_GetInformeViajes_Result> _listViajes = new List<Track_GetInformeViajes_Result>();

            while (lectorDatosSQL.Read())
                _listViajes.Add(LlenarElementoViajeInforme());

            return _listViajes;
        }

        public List<Track_GetViajesHistoricos_Result> LlenarListadoViajes() {
            List<Track_GetViajesHistoricos_Result> _listViajes = new List<Track_GetViajesHistoricos_Result>();

            while (lectorDatosSQL.Read())
                _listViajes.Add(LlenarElementoViaje());

            return _listViajes;
        }


        public Track_GetInformeViajes_Result LlenarElementoViajeInforme()
        {
            Track_GetInformeViajes_Result elementoViajeInforme = new Track_GetInformeViajes_Result();
            
            elementoViajeInforme.NroTransporte = Convert.ToInt32(lectorDatosSQL["NroTransporte"].ToString());
            elementoViajeInforme.PatenteTrailer = lectorDatosSQL["PatenteTrailer"].ToString();
            elementoViajeInforme.PatenteTracto = lectorDatosSQL["PatenteTracto"].ToString();
            elementoViajeInforme.Transportista = lectorDatosSQL["Transportista"].ToString();
            elementoViajeInforme.CodigoOrigen = Convert.ToInt32(lectorDatosSQL["CodigoOrigen"].ToString());
            elementoViajeInforme.NombreOrigen = lectorDatosSQL["NombreOrigen"].ToString();
            elementoViajeInforme.FHSalidaOrigen = Convert.ToDateTime(lectorDatosSQL["FHSalidaOrigen"].ToString());
            elementoViajeInforme.NombreConductor = lectorDatosSQL["NombreConductor"].ToString();
            elementoViajeInforme.Destinos = lectorDatosSQL["Destinos"].ToString();
            elementoViajeInforme.MontoReclamado = Convert.ToDouble(lectorDatosSQL["MontoReclamado"].ToString());
            elementoViajeInforme.RutConductor = lectorDatosSQL["RutConductor"].ToString();
            elementoViajeInforme.Score = lectorDatosSQL["Score"].ToString();
        
            return elementoViajeInforme;
        }

        public Track_GetViajesHistoricos_Result LlenarElementoViaje()
        {
            Track_GetViajesHistoricos_Result elementoViaje = new Track_GetViajesHistoricos_Result();
            
            elementoViaje.IdViaje= Convert.ToInt32( lectorDatosSQL["IdViaje"].ToString());
            elementoViaje.NroTransporte= Convert.ToInt32(lectorDatosSQL["NroTransporte"].ToString());
            elementoViaje.Fecha= Convert.ToDateTime(lectorDatosSQL["Fecha"].ToString());
            elementoViaje.SecuenciaDestino= Convert.ToInt32(lectorDatosSQL["SecuenciaDestino"].ToString());
            elementoViaje.PatenteTracto= lectorDatosSQL["PatenteTracto"].ToString();
            elementoViaje.PatenteTrailer= lectorDatosSQL["PatenteTrailer"].ToString();
            elementoViaje.Transportista= lectorDatosSQL["Transportista"].ToString();
            elementoViaje.FechaHoraCreacion= Convert.ToDateTime(lectorDatosSQL["FechaHoraCreacion"].ToString());
            elementoViaje.CodigoOrigen= Convert.ToInt32(lectorDatosSQL["CodigoOrigen"].ToString());
            elementoViaje.NombreOrigen= lectorDatosSQL["NombreOrigen"].ToString();
            elementoViaje.FHAsignacion= lectorDatosSQL["FHAsignacion"].ToString();
            elementoViaje.FHSalidaOrigen= lectorDatosSQL["FHSalidaOrigen"].ToString();
            elementoViaje.CodigoDestino= Convert.ToInt32(lectorDatosSQL["CodigoDestino"].ToString());
            elementoViaje.NombreDestino= lectorDatosSQL["NombreDestino"].ToString();
            elementoViaje.FHLlegadaDestino= lectorDatosSQL["FHLlegadaDestino"].ToString();

            try
            {
                elementoViaje.FHCierreSistema = Convert.ToDateTime(lectorDatosSQL["FHCierreSistema"].ToString());
            }
            catch (Exception) {
                elementoViaje.FHCierreSistema = null;
            }
            
            elementoViaje.TiempoViaje= Convert.ToInt32(lectorDatosSQL["TiempoViaje"].ToString());
            elementoViaje.CantidadAlertas= Convert.ToInt32(lectorDatosSQL["CantidadAlertas"].ToString());
            elementoViaje.CantidadAperturaPuerta= Convert.ToInt32(lectorDatosSQL["CantidadAperturaPuerta"].ToString());
            elementoViaje.CantidadDetencion= Convert.ToInt32(lectorDatosSQL["CantidadDetencion"].ToString());
            elementoViaje.CantidadPerdidaSenal= Convert.ToInt32(lectorDatosSQL["CantidadPerdidaSenal"].ToString());
            elementoViaje.CantidadTemperatura= Convert.ToInt32(lectorDatosSQL["CantidadTemperatura"].ToString());
            elementoViaje.FHSalidaDestino= lectorDatosSQL["FHSalidaDestino"].ToString();
            elementoViaje.EstadoViaje= lectorDatosSQL["EstadoViaje"].ToString();
            elementoViaje.EstadoLat= lectorDatosSQL["EstadoLat"].ToString();
            elementoViaje.EstadoLon= lectorDatosSQL["EstadoLon"].ToString();
            elementoViaje.DestinoLat= lectorDatosSQL["DestinoLat"].ToString();
            elementoViaje.DestinoLon= lectorDatosSQL["DestinoLon"].ToString();
        
            return elementoViaje;
        }

        #endregion METODOS PRUEBA CONSUMO STORED PROCEDURES SIN ENTITY FRAMEWORKS

        public List<Track_GetProveedoresGPS_Result> GetProveedoresGPS(bool _all, string rutTransportista)
        {
            try
            {
                InicializarConexion();

                List < Track_GetProveedoresGPS_Result > listadoProveedoresGPS = new List<Track_GetProveedoresGPS_Result>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetProveedoresGPS");

                EstablecerParametroProcedimientoAlmacenado(rutTransportista, "string", "@rutTransportista");

                LlenarLectorDatosSQL();

                listadoProveedoresGPS=LlenarListadoProveedoresGPS(listadoProveedoresGPS);

                conexionSQL.Close();

                return listadoProveedoresGPS;
                /*
                List<Track_GetProveedoresGPS_Result> _listProveedores = _context.Track_GetProveedoresGPS(rutTransportista).ToList();

                if (_all)
                {
                    Track_GetProveedoresGPS_Result item = new Track_GetProveedoresGPS_Result { ProveedorGPS = "Todos" };
                    _listProveedores.Insert(0, item);
                }

                return _listProveedores;
                */
            }
            catch (Exception ex)
            {
                return new List<Track_GetProveedoresGPS_Result>();
            }
        }

        public List<Track_GetMonitoreoOnline_Result> GetMonitoreoOnline(string patente, string transportista, int ignicion, string estadoViaje, string estadoGPS, string proveedorGPS, long nroTransporte, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetMonitoreoOnline_Result> _listaMonitoreoOnline = _context.Track_GetMonitoreoOnline(patente, transportista, ignicion, estadoViaje, estadoGPS, proveedorGPS, nroTransporte, userName).ToList();
                return _listaMonitoreoOnline;

            }
            catch (Exception ex)
            {
                return new List<Track_GetMonitoreoOnline_Result>();
            }
        }

        public List<Track_GetViajesDashboard_Result> GetViajesDashboardControl(DateTime desde, DateTime hasta, long nroTransporte, string patenteTracto, string patenteTrailer, string estadoViaje, string transportista, string alertas, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesDashboard_Result> _listViajes = _context.Track_GetViajesDashboard(desde, hasta, transportista, patenteTracto, patenteTrailer, estadoViaje, nroTransporte, alertas, userName).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesDashboard_Result>();
            }
        }


        public List<Track_GetDetalleTrayecto_Result> GetDetalleTrayecto(long nroTransporte)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetDetalleTrayecto_Result> _listaDetalleTrayecto = _context.Track_GetDetalleTrayecto(nroTransporte).ToList();
                return _listaDetalleTrayecto;

            }
            catch (Exception)
            {
                return new List<Track_GetDetalleTrayecto_Result>();
            }
        }

        public List<Track_GetPosicionesGPS_Viaje_Result> GetPosicionesViaje(long nroTransporte)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 300;

                List<Track_GetPosicionesGPS_Viaje_Result> _listaPosicionesRuta = _context.Track_GetPosicionesGPS_Viaje(nroTransporte).ToList();
                return _listaPosicionesRuta;

            }
            catch (Exception e)
            {

                return new List<Track_GetPosicionesGPS_Viaje_Result>();
            }
        }

        public List<Track_GetFlotaOnline_Result> GetFlotaOnline(string patente, string transportista, int ignicion, string estadoViaje, string estadoGPS, string proveedorGPS)
        {
            try
            {
                List<Track_GetFlotaOnline_Result> _listaFlotaOnline = _context.Track_GetFlotaOnline(patente, transportista, ignicion, estadoViaje, estadoGPS, proveedorGPS).ToList();
                return _listaFlotaOnline;

            }
            catch (Exception)
            {
                return new List<Track_GetFlotaOnline_Result>();
            }
        }


        public List<Track_GetViajesHistoricos_Result> GetViajesHistoricos(DateTime desde, DateTime hasta, long nroTransporte, string patente, string estadoViaje, string transportista, int codLocal, string tipoViaje, string userName)
        {
            try
            {
                /*comentadas estas lineas por fallas en un parametro*/
                //((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                //List<Track_GetViajesHistoricos_Result> _listViajes = _context.Track_GetViajesHistoricos(
                //desde, hasta, transportista, patente, estadoViaje, nroTransporte, codLocal, tipoViaje, userName).ToList();

                InicializarConexion();

                List<Track_GetViajesHistoricos_Result> _listViajes = new List<Track_GetViajesHistoricos_Result>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetViajesHistoricos");

                EstablecerParametroProcedimientoAlmacenado(desde, "datetime", "@desde");
                EstablecerParametroProcedimientoAlmacenado(hasta, "datetime", "@hasta");
                EstablecerParametroProcedimientoAlmacenado(transportista, "string", "@transportista");
                EstablecerParametroProcedimientoAlmacenado(patente, "string", "@patente");
                EstablecerParametroProcedimientoAlmacenado(estadoViaje, "string", "@estadoViaje");
                EstablecerParametroProcedimientoAlmacenado(nroTransporte, "int", "@nroTransporte");
                EstablecerParametroProcedimientoAlmacenado(codLocal, "int", "@codLocal");
                EstablecerParametroProcedimientoAlmacenado(tipoViaje, "string", "@tipoViaje");
                EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");
                
                LlenarLectorDatosSQL();

                _listViajes = LlenarListadoViajes();

                conexionSQL.Close();

                return _listViajes;

            }
            catch (Exception ex)
            {
                return new List<Track_GetViajesHistoricos_Result>();
            }
        }


        public List<Track_GetReporteEstadiaRemolques_Historico_Result> getReporteEstadiaRemolques_Historico(DateTime desde, DateTime hasta, string patente, string Cedis, string listCarrier, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaRemolques_Historico(desde, hasta, patente, Cedis, listCarrier, userName).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaRemolques_Historico_Result>();
            }
        }

        public List<Track_GetReporteEstadiaTractos_Historico_Result> getReporteEstadiaTractos_Historico(DateTime desde, DateTime hasta, string Transportista, string patente, string Cedis, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaTractos_Historico(desde, hasta, Transportista, patente, Cedis, userName).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaTractos_Historico_Result>();
            }
        }


        public List<Track_GetReporteEstadiaRemolques_Result> getReporteEstadiaRemolques(string patente, string Cedis, string listCarrier, string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaRemolques(patente, Cedis, listCarrier, userName).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaRemolques_Result>();
            }
        }

        public List<Track_GetReporteEstadiaTractos_Result> getReporteEstadiaTractos(string Transportista, string patente, string Cedis, string userName)
        {
            try
            {
                //PENDIENTE POR QUE NO TIENE NINGUN COINCIDIENTE LA TABLA TRACK_REPORTEESTADIATRACTOS

                //aqui debo vinular a mi sp Track_GetReporteEstadiaTractos_sin_cedis_050718
                //y si detecto que tiene un todas ese debe ser el unico a atender, no los demas cedis x,Todas,y = Todas

                List<Track_GetReporteEstadiaTractos_Result> listaReporteEstadiaTractos = new List<Track_GetReporteEstadiaTractos_Result>();

                //solo hace una busqueda de Todos los cedis si selecciono Todas en los cedis, ignora los demas elementos seleccionados
                if (Cedis.IndexOf("Todas") > -1)
                    Cedis = "Todas";

                List<string> listaIdsCedis = Cedis.Split(',').ToList<string>();

                foreach (string cedis in listaIdsCedis)
                {

                    InicializarConexion();
                    conexionSQL.Open();

                    EstablecerProcedimientoAlmacenado("Track_GetReporteEstadiaTractos_sin_cedis_050718");

                    EstablecerParametroProcedimientoAlmacenado(Transportista, "string", "@Transportista");
                    EstablecerParametroProcedimientoAlmacenado(patente, "string", "@patente");
                    EstablecerParametroProcedimientoAlmacenado(cedis, "string", "@idCedis");
                    EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");

                    EjecutarProcedimientoAlmacenado();

                    LlenarLectorDatosSQL();

                    listaReporteEstadiaTractos.AddRange(LlenarListadoReporteEstadiaTractos());

                    conexionSQL.Close();
                    
                }
                return listaReporteEstadiaTractos;


                //codigo comentado para probar de modo seguro el nuevo sp //050718
                /*
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteEstadiaTractos(Transportista, patente, Cedis, userName).ToList();
                */
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteEstadiaTractos_Result>();
            }
        }

        public List<Track_GetReporteEstadiaTractos_Result> LlenarListadoReporteEstadiaTractos()
        {
            List<Track_GetReporteEstadiaTractos_Result> listaReporteEstadiaTractos = new List<Track_GetReporteEstadiaTractos_Result>();

            while (lectorDatosSQL.Read())
                listaReporteEstadiaTractos.Add(LlenarElementoReporteEstadiaTracto());

            return listaReporteEstadiaTractos;
        }

        public Track_GetReporteEstadiaTractos_Result LlenarElementoReporteEstadiaTracto()
        {
            Track_GetReporteEstadiaTractos_Result elementoReporteEstadiaTracto = new Track_GetReporteEstadiaTractos_Result();

            elementoReporteEstadiaTracto.Placa = lectorDatosSQL["Placa"].ToString();
            elementoReporteEstadiaTracto.Transportista = lectorDatosSQL["Transportista"].ToString();
            elementoReporteEstadiaTracto.EstadoEventos = lectorDatosSQL["EstadoEventos"].ToString();
            elementoReporteEstadiaTracto.FechaEvento = lectorDatosSQL["FechaEvento"].ToString();
            elementoReporteEstadiaTracto.Antiguedad = lectorDatosSQL["Antiguedad"].ToString();
            elementoReporteEstadiaTracto.Velocidad = Convert.ToInt32(lectorDatosSQL["Velocidad"]);
            elementoReporteEstadiaTracto.Latitud = Convert.ToDecimal(lectorDatosSQL["Latitud"]);
            elementoReporteEstadiaTracto.Longitud = Convert.ToDecimal(lectorDatosSQL["Longitud"]);

            elementoReporteEstadiaTracto.Referencia = lectorDatosSQL["Referencia"].ToString();
            elementoReporteEstadiaTracto.Detenido = lectorDatosSQL["Detenido"].ToString();
            elementoReporteEstadiaTracto.TiempoDetenido = lectorDatosSQL["TiempoDetenido"].ToString();
            elementoReporteEstadiaTracto.FechaDetencion = lectorDatosSQL["FechaDetencion"].ToString();
            elementoReporteEstadiaTracto.UltimaEntrada = lectorDatosSQL["UltimaEntrada"].ToString();
            elementoReporteEstadiaTracto.UltimaSalida = lectorDatosSQL["UltimaSalida"].ToString();
            elementoReporteEstadiaTracto.EnZona = lectorDatosSQL["EnZona"].ToString();

            elementoReporteEstadiaTracto.Zona = Convert.ToInt32(lectorDatosSQL["Zona"]);

            elementoReporteEstadiaTracto.TiempoEnZona = lectorDatosSQL["TiempoEnZona"].ToString();

            elementoReporteEstadiaTracto.CodZonaCercana = Convert.ToInt32(lectorDatosSQL["CodZonaCercana"]);

            elementoReporteEstadiaTracto.NombreZonaCercana = lectorDatosSQL["NombreZonaCercana"].ToString();
            elementoReporteEstadiaTracto.DistanciaZonaCercana = lectorDatosSQL["DistanciaZonaCercana"].ToString();

            elementoReporteEstadiaTracto.CEDIS = Convert.ToInt32(lectorDatosSQL["CEDIS"]);

            elementoReporteEstadiaTracto.NombreCEDIS = lectorDatosSQL["NombreCEDIS"].ToString();
            elementoReporteEstadiaTracto.TipoUnidad = lectorDatosSQL["TipoUnidad"].ToString();
            elementoReporteEstadiaTracto.FechaReporte = lectorDatosSQL["FechaReporte"].ToString();

            elementoReporteEstadiaTracto.DesfaseReporte = Convert.ToInt32(lectorDatosSQL["DesfaseReporte"]);
            elementoReporteEstadiaTracto.HorasDetenido = Convert.ToInt32(lectorDatosSQL["HorasDetenido"]);

            elementoReporteEstadiaTracto.UltimaZona = lectorDatosSQL["UltimaZona"].ToString();
            elementoReporteEstadiaTracto.NombreZona = lectorDatosSQL["NombreZona"].ToString();
            elementoReporteEstadiaTracto.Formato = lectorDatosSQL["Formato"].ToString();
            elementoReporteEstadiaTracto.EstadoCarga = lectorDatosSQL["EstadoCarga"].ToString();
            elementoReporteEstadiaTracto.EstadoViaje = lectorDatosSQL["EstadoViaje"].ToString();
            elementoReporteEstadiaTracto.RegionZona = lectorDatosSQL["RegionZona"].ToString();
            elementoReporteEstadiaTracto.HorasDetenidoTxt = lectorDatosSQL["HorasDetenidoTxt"].ToString();

            return elementoReporteEstadiaTracto;
        }

        /*
        public List<Track_GetReporteComercial_Result> getReporteComercial(DateTime? fecDesde, DateTime? fecHasta, string Cedis)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteComercial(fecDesde, fecHasta, Cedis).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteComercial_Result>();
            }
        }
        */
        /*
        public List<Track_GetReporteComercial_Result> getReporteComercial(DateTime? fecDesde, DateTime? fecHasta, string Cedis)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteComercial(fecDesde, fecHasta, Cedis).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteComercial_Result>();
            }
        }
        */

        public List<Track_GetReporteComercial_Result> getReporteComercial(DateTime? fecDesde, DateTime? fecHasta, string Cedis)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;
                return _context.Track_GetReporteComercial(fecDesde, fecHasta, Cedis).ToList();
            }
            catch (Exception ex)
            {
                return new List<Track_GetReporteComercial_Result>();
            }
        }

        public List<Track_GetViajesRuta_Result> GetViajesRuta(int nroTransporte, string patente, string estadoViaje, string transportista)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetViajesRuta_Result> _listViajes = _context.Track_GetViajesRuta(nroTransporte, patente, estadoViaje, transportista).ToList();
                return _listViajes;

            }
            catch (Exception)
            {
                return new List<Track_GetViajesRuta_Result>();
            }
        }

        public List<Track_GetNroTransportesRuta_Result> GetNroTransportesRuta()
        {
            try
            {
                List<Track_GetNroTransportesRuta_Result> _listNroTransportes = _context.Track_GetNroTransportesRuta().ToList();
                return _listNroTransportes;

            }
            catch (Exception)
            {
                return new List<Track_GetNroTransportesRuta_Result>();
            }
        }

        public List<Track_GetPatentesRuta_Result> GetPatentesRuta()
        {
            try
            {
                List<Track_GetPatentesRuta_Result> _listPatentesRuta = _context.Track_GetPatentesRuta().ToList();
                return _listPatentesRuta;

            }
            catch (Exception)
            {
                return new List<Track_GetPatentesRuta_Result>();
            }
        }

        public List<Track_GetTransportistasRuta_Result> GetTransportistasRuta()
        {
            try
            {
                List<Track_GetTransportistasRuta_Result> _listTransportistasRuta = _context.Track_GetTransportistasRuta().ToList();
                return _listTransportistasRuta;

            }
            catch (Exception)
            {
                return new List<Track_GetTransportistasRuta_Result>();
            }
        }

        public List<Track_GetPosicionesRuta_Result> GetPosicionesRuta(string patenteTracto, string patenteTrailer, DateTime? fechaHoraCreacion, DateTime? fechaHoraSalidaOrigen, DateTime? fechaHoraLlegadaDestino, long nroTransporte, long idEmbarque, int destino, string estadoViaje)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetPosicionesRuta_Result> _listaPosicionesRuta = _context.Track_GetPosicionesRuta(patenteTracto, patenteTrailer, fechaHoraCreacion, fechaHoraSalidaOrigen, fechaHoraLlegadaDestino, nroTransporte, idEmbarque, destino, estadoViaje).ToList();
                return _listaPosicionesRuta;

            }
            catch (Exception e)
            {
                CommonUtilities.WriteInfo("Message: " + e.Message + " Inner Exception: " + e.InnerException + " StackTrace: " + e.StackTrace, CommonUtilities.EventType.Error, ConfigurationManager.AppSettings["LogPath"].ToString(), true);
                return new List<Track_GetPosicionesRuta_Result>();
            }
        }

        public List<Track_Movil> GetAllPatentes(string transportista, bool _all = false)
        {
            try
            {
                List<Track_Movil> _listaPatentes = (from c in _context.Track_Movil where (c.Transportista == transportista || transportista == "Todos" || transportista == "") select c).ToList();
                if (_all)
                {
                    Track_Movil newItem = new Track_Movil { Patente = "Todas" };
                    _listaPatentes.Insert(0, newItem);
                }

                return _listaPatentes;

            }
            catch (Exception)
            {
                return new List<Track_Movil>();
            }
        }

        public List<Track_Movil> GetAllTransportistas(bool _all = false)
        {
            try
            {
                List<Track_Movil> _listaTransportistas = (from c in _context.Track_Movil select c).ToList();
                if (_all)
                {
                    Track_Movil newItem = new Track_Movil { Transportista = "Todos" };
                    _listaTransportistas.Insert(0, newItem);
                }

                return _listaTransportistas;

            }
            catch (Exception)
            {
                return new List<Track_Movil>();
            }
        }
        /*
        public List<Track_GetViajesLocal_Result> GetViajesLocal(DateTime desde, DateTime hasta, string rutCliente, int nroTransporte, string transportista, string username, string tipoEtis, string estadoViaje)
        {
          try
          {
            ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

            List<Track_GetViajesLocal_Result> _listViajes = _context.Track_GetViajesLocal(desde, hasta, rutCliente, nroTransporte, transportista, username, tipoEtis, estadoViaje).ToList();
            return _listViajes;

          }
          catch (Exception)
          {
            return new List<Track_GetViajesLocal_Result>();
          }
        }
        */
        public List<Track_GetViajesControlLocal_Result> GetViajesControlLocal(DateTime desde, DateTime hasta, string rutCliente, int nroTransporte, string transportista, string username, string tipoEtis, string estadoViaje, string tipoCarga, string pod, string otif, string nroContenedor, string problema, bool filFH)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetViajesControlLocal_Result> _listViajes = _context.Track_GetViajesControlLocal(desde, hasta, rutCliente, nroTransporte, transportista, username, tipoEtis, estadoViaje, tipoCarga, pod, otif, nroContenedor, problema, filFH).ToList();
                return _listViajes;

            }
            catch (Exception e)
            {
                return new List<Track_GetViajesControlLocal_Result>();
            }
        }

        public List<Track_GetPosicionesGPS_Result> GetPosicionesGPS(DateTime fechaDesde, DateTime fechaHasta, string patente)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetPosicionesGPS_Result> _listaPosicionesGPS = _context.Track_GetPosicionesGPS(fechaDesde, fechaHasta, patente).ToList();
                return _listaPosicionesGPS;

            }
            catch (Exception)
            {
                return new List<Track_GetPosicionesGPS_Result>();
            }
        }

        public List<Track_GetPosicionesGPS_Ruta_Result> GetPosicionesGPS_Ruta(DateTime fechaDesde, DateTime fechaHasta, string patente)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetPosicionesGPS_Ruta_Result> _listaPosicionesGPS_Ruta = _context.Track_GetPosicionesGPS_Ruta(fechaDesde, fechaHasta, patente).ToList();
                return _listaPosicionesGPS_Ruta;

            }
            catch (Exception)
            {
                return new List<Track_GetPosicionesGPS_Ruta_Result>();
            }
        }
        /*
        public List<Track_GetInformeViajes_Result> GetInformeViajes(DateTime desde, DateTime hasta, int nroTransporte, string patente, string transportista)
        {
          try
          {
            List<Track_GetInformeViajes_Result> _listViajes = _context.Track_GetInformeViajes(desde, hasta, transportista, patente, nroTransporte).ToList();
            return _listViajes;

          }
          catch (Exception)
          {
            return new List<Track_GetInformeViajes_Result>();
          }
        }
        */
        public List<Track_GetDetalleTrayecto_Result> GetDetalleTrayecto(int nroTransporte)
        {
            try
            {
                List<Track_GetDetalleTrayecto_Result> _listaDetalleTrayecto = _context.Track_GetDetalleTrayecto(nroTransporte).ToList();
                return _listaDetalleTrayecto;

            }
            catch (Exception)
            {
                return new List<Track_GetDetalleTrayecto_Result>();
            }
        }

        public List<Track_GetNroTransportes_Result> GetNroTransportes(DateTime desde, DateTime hasta)
        {
            try
            {
                List<Track_GetNroTransportes_Result> _listNroTransportes = _context.Track_GetNroTransportes(desde, hasta).ToList();
                return _listNroTransportes;

            }
            catch (Exception)
            {
                return new List<Track_GetNroTransportes_Result>();
            }
        }

        public List<Track_GetTipoEtis_Result> GetTipoEtis(bool _all)
        {
            try
            {
                List<Track_GetTipoEtis_Result> _listEtis = _context.Track_GetTipoEtis().ToList();

                if (_all)
                {
                    Track_GetTipoEtis_Result item = new Track_GetTipoEtis_Result { ETIS = "Todas" };
                    _listEtis.Insert(0, item);
                }

                return _listEtis;

            }
            catch (Exception)
            {
                return new List<Track_GetTipoEtis_Result>();
            }
        }

        public List<Track_GetClientes_Result> GetClientes(bool _all)
        {
            try
            {
                List<Track_GetClientes_Result> _listClientes = _context.Track_GetClientes().ToList();

                if (_all)
                {
                    Track_GetClientes_Result item = new Track_GetClientes_Result { RutCliente = "Todos", NombreCliente = "Todos" };
                    _listClientes.Insert(0, item);
                }

                return _listClientes;

            }
            catch (Exception)
            {
                return new List<Track_GetClientes_Result>();
            }
        }

        public List<Track_GetClientesUsuario_Result> GetClientesUsuario(bool _all, string userName)
        {
            try
            {
                List<Track_GetClientesUsuario_Result> _listClientes = _context.Track_GetClientesUsuario(userName).ToList();

                if (_all)
                {
                    Track_GetClientesUsuario_Result item = new Track_GetClientesUsuario_Result { RutCliente = "Todos", NombreCliente = "Todos" };
                    _listClientes.Insert(0, item);
                }

                return _listClientes;

            }
            catch (Exception)
            {
                return new List<Track_GetClientesUsuario_Result>();
            }
        }

        public List<Track_GetRptCargaSuelta_Result> GetRptCargaSuelta(DateTime desde, DateTime hasta, string rutCliente, int nroTransporte, string transportista, string usuario, string tipoEtis, string estadoViaje)
        {
            try
            {
                List<Track_GetRptCargaSuelta_Result> _listViaje = _context.Track_GetRptCargaSuelta(desde, hasta, rutCliente, nroTransporte, transportista, usuario, tipoEtis, estadoViaje).ToList();
                return _listViaje;

            }
            catch (Exception e)
            {
                return new List<Track_GetRptCargaSuelta_Result>();
            }
        }

        public List<Track_GetFotografiasViaje_Result> GetFotografiasViaje(int nroTransporte)
        {
            try
            {
                List<Track_GetFotografiasViaje_Result> _listFotografias = _context.Track_GetFotografiasViaje(nroTransporte).ToList();

                for (int i = 0; i <_listFotografias.Count(); i++)
                {
                    int IdEntrega = _listFotografias[i].IdRegistro;
                    string IdImagen = _listFotografias[i].Referencia;

                    string url = UtilitiesLayer.Utilities.DevolverURLAmazon(IdEntrega, IdImagen);

                    _listFotografias[i].URL = url;
                }

                return _listFotografias;

            }
            catch (Exception e)
            {
                return new List<Track_GetFotografiasViaje_Result>();
            }
        }

        public List<Track_GetViajesControlLocalCliente_Result> GetViajesControlLocalCliente(DateTime desde, DateTime hasta, long nroTransporte, int codOrigen, string patente, string tipoViaje, string username)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetViajesControlLocalCliente_Result> _listViajes = _context.Track_GetViajesControlLocalCliente(desde, hasta, nroTransporte, codOrigen, patente, tipoViaje, username).ToList();
                return _listViajes;

            }
            catch (Exception e)
            {
                return new List<Track_GetViajesControlLocalCliente_Result>();
            }
        }

    }
}
