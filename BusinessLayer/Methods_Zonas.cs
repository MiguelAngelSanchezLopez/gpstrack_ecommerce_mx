﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Data.Objects;

namespace BusinessLayer
{
    public class Methods_Zonas
    {
        private ModelEntities _context = new ModelEntities();

        //Nota: si serializo el objeto  List<Track_Zonas>, me da el siguiente error: 
        //newtonsoft self referencing loop detected with type 'system.data.entity.dynamicproxies
        // Por eso se retorna un Object
        public List<Object> GetEstadosMexico()
        {
            try
            {
                List<Track_Zonas> _result = (from c in _context.Track_Zonas select c).Where(c => c.IdTipoZona == 17).ToList();
                List<Object> _object = new List<object>();
                for (int i = 0; i < _result.Count; i++)
                {
                    var json = new
                    {
                        IdZona = _result[i].IdZona,
                        NombreZona = _result[i].NombreZona
                    };

                    _object.Add(json);
                }

                return _object;
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Object>();
            }
        }


        public List<Track_GetLocalesBackhaul_Result> GetLocalesBackhaul(int idTipoZona, string nombreZona)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

                List<Track_GetLocalesBackhaul_Result> _listZonas = _context.Track_GetLocalesBackhaul(idTipoZona, nombreZona).ToList();

                return _listZonas;

            }
            catch (Exception)
            {
                return new List<Track_GetLocalesBackhaul_Result>();
            }
        }


        public List<Track_TipoZonas> GetTipoZonas(bool _all = false)
        {
            try
            {
                List<Track_TipoZonas> _listTipoZonas = (from c in _context.Track_TipoZonas select c).ToList();
                if (_all)
                {
                    Track_TipoZonas newItem = new Track_TipoZonas { IdTipoZona = 0, NombreTipoZona = "Todas" };
                    _listTipoZonas.Insert(0, newItem);
                }

                return _listTipoZonas;

            }
            catch (Exception)
            {
                return new List<Track_TipoZonas>();
            }
        }

        public List<Track_GetZonas_Result> GetZonas(int idTipoZona, string nombreZona)
        {
            try
            {
                List<Track_GetZonas_Result> _listZonas = _context.Track_GetZonas(idTipoZona, nombreZona, 0).ToList();
                return _listZonas;

            }
            catch (Exception)
            {
                return new List<Track_GetZonas_Result>();
            }
        }

        public string EliminaZona(int IdZona)
        {
            try
            {

                string _resp = _context.Track_EliminarZona(IdZona).FirstOrDefault().ToString();

                return _resp;
            }
            catch (Exception)
            {
                return "Error al intentar eliminar la geocerca.";
            }
        }

        public Track_Zonas GetZonaById(int idZona)
        {
            try
            {
                Track_Zonas _zona = _context.Track_Zonas.Where(Z => Z.IdZona == idZona).FirstOrDefault();

                return _zona;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<Track_Vertices> GetVerticesZona(int idZona)
        {
            try
            {
                List<Track_Vertices> _list = new List<Track_Vertices>();

                _list = _context.Track_Vertices.Where(C => C.IdZona == idZona).OrderBy(C => C.IdPunto).ToList();

                _list.RemoveAt(_list.Count - 1);

                return _list;
            }
            catch (Exception)
            {
                return new List<Track_Vertices>();
            }
        }

        public List<Track_Vertices> GetAllVerticesZona(int idZona)
        {
            try
            {
                List<Track_Vertices> _list = new List<Track_Vertices>();

                _list = _context.Track_Vertices.Where(C => C.IdZona == idZona).OrderBy(C => C.IdPunto).ToList();

                return _list;
            }
            catch (Exception)
            {
                return new List<Track_Vertices>();
            }
        }

        public bool ValidarIdZona(int IdZona)
        {
            try
            {
                Track_Zonas _existe = _context.Track_Zonas.Where(C => C.IdZona == IdZona).FirstOrDefault();

                if (_existe != null)
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string NuevaZona(int idZona, string nombreZona, int idTipoZona, List<Tuple<decimal, decimal>> vertices)
        {
            try
            {
                decimal promLat = 0;
                decimal promLon = 0;

                Track_Zonas _zona = _context.Track_Zonas.Where(C => C.IdZona == idZona).FirstOrDefault();

                if (_zona != null)
                {
                    return "El Id de Zona ingresado ya existe.";
                }

                _zona = new Track_Zonas();

                _zona.IdZona = idZona;
                _zona.NombreZona = nombreZona;
                _zona.IdTipoZona = idTipoZona;

                for (int i = 0; i < vertices.Count; i++)
                {
                    promLat += vertices[i].Item1;
                    promLon += vertices[i].Item2;

                    Track_Vertices _vertice = new Track_Vertices();
                    _vertice.IdZona = _zona.IdZona;
                    _vertice.IdPunto = (i + 1);
                    _vertice.Latitud = vertices[i].Item1;
                    _vertice.Longitud = vertices[i].Item2;

                    _context.Track_Vertices.Add(_vertice);
                }

                Track_Vertices _ultVertice = new Track_Vertices();
                _ultVertice.IdZona = _zona.IdZona;
                _ultVertice.IdPunto = vertices.Count + 1;
                _ultVertice.Latitud = vertices[0].Item1;
                _ultVertice.Longitud = vertices[0].Item2;

                _context.Track_Vertices.Add(_ultVertice);

                promLat = promLat / vertices.Count;
                promLon = promLon / vertices.Count;

                _zona.Latitud = promLat;
                _zona.Longitud = promLon;

                _context.Track_Zonas.Add(_zona);

                _context.SaveChanges();

                return "Zona agregada satisfactoriamente.";
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public string EditarZona(int idZona, string nombreZona, int idTipoZona, List<Tuple<decimal, decimal>> vertices)
        {
            try
            {
                decimal promLat = 0;
                decimal promLon = 0;

                Track_Zonas _zona = _context.Track_Zonas.Where(C => C.IdZona == idZona).FirstOrDefault();

                if (_zona == null)
                {
                    return "El Id de Zona no existe.";
                }

                _zona.NombreZona = nombreZona;
                _zona.IdTipoZona = idTipoZona;

                List<Track_Vertices> _vertices = _context.Track_Vertices.Where(C => C.IdZona == idZona).ToList();

                for (int i = 0; i < _vertices.Count; i++)
                {
                    _context.Track_Vertices.Remove(_vertices[i]);
                }

                for (int i = 0; i < vertices.Count; i++)
                {
                    promLat += vertices[i].Item1;
                    promLon += vertices[i].Item2;

                    Track_Vertices _vertice = new Track_Vertices();
                    _vertice.IdZona = _zona.IdZona;
                    _vertice.IdPunto = (i + 1);
                    _vertice.Latitud = vertices[i].Item1;
                    _vertice.Longitud = vertices[i].Item2;

                    _context.Track_Vertices.Add(_vertice);
                }

                Track_Vertices _ultVertice = new Track_Vertices();
                _ultVertice.IdZona = _zona.IdZona;
                _ultVertice.IdPunto = vertices.Count + 1;
                _ultVertice.Latitud = vertices[0].Item1;
                _ultVertice.Longitud = vertices[0].Item2;

                _context.Track_Vertices.Add(_ultVertice);

                promLat = promLat / vertices.Count;
                promLon = promLon / vertices.Count;

                _zona.Latitud = promLat;
                _zona.Longitud = promLon;

                _context.SaveChanges();

                return "Zona modificada satisfactoriamente.";
            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public List<Track_GetZonasToDraw_Result> GetZonasToDraw(DateTime fechaDesde, DateTime fechaHasta, string patente1, string patente2)
        {
            try
            {
                List<Track_GetZonasToDraw_Result> _listaZonasToDraw = _context.Track_GetZonasToDraw(fechaDesde, fechaHasta, patente1, patente2).ToList();
                return _listaZonasToDraw;

            }
            catch (Exception)
            {
                return new List<Track_GetZonasToDraw_Result>();
            }
        }

        public List<Track_Usuarios_Locales> GetLocales()
        {
            try
            {
                List<Track_Usuarios_Locales> _listLocales = (from c in _context.Track_Usuarios_Locales select c).ToList();
                Track_Usuarios_Locales newItem = new Track_Usuarios_Locales { Usuario = "Todos", CodLocal = 0, NombreLocal = "Todos" };
                _listLocales.Insert(0, newItem);

                return _listLocales;

            }
            catch (Exception)
            {
                return new List<Track_Usuarios_Locales>();
            }
        }

        public int ZonesInsideZone(int IdZona)
        {
            try
            {
                int _zonesInsideZone = (int)_context.Track_GetZonesInsideZone(IdZona).FirstOrDefault().ZonesInsideZone;

                return _zonesInsideZone;
            }
            catch (Exception)
            {
                return -1;
            }
        }

    }
}
