﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;
using System.Data.Objects;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;

namespace BusinessLayer
{
    public class Methods_User
    {
        private ModelEntities _context = new ModelEntities();

        public List<Track_GetUsuarios_Result> GetUsuarios(string userName)
        {
            try
            {
                ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 180;

                List<Track_GetUsuarios_Result> _listUsuarios = _context.Track_GetUsuarios(userName).ToList();
                return _listUsuarios;

            }
            catch (Exception)
            {
                return new List<Track_GetUsuarios_Result>();
            }
        }

        #region METODOS PRUEBA CONSUMO STORED PROCEDURES SIN ENTITY FRAMEWORKS
        protected SqlConnection conexionSQL;
        protected SqlCommand comandoSQL;
        protected SqlDataReader lectorDatosSQL;

        public string CADENA_CONEXION = ConfigurationManager.ConnectionStrings["ConexionPerfilamiento"].ConnectionString;

        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.ExecuteNonQuery();
        }

        public string EjecutarProcedimientoAlmacenadoEscalar()
        {
            return comandoSQL.ExecuteScalar().ToString();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        
        #endregion METODOS PRUEBA CONSUMO STORED PROCEDURES SIN ENTITY FRAMEWORKS

        public string GuardarUsuario(int idUsuario, string userName, string password, string nombre, string apellidos, string eMail, string agrupacion, int activo, string listCedisASociados, int idUsuarioConectado)
        {
            try
            {
                /*comentadas estas lineas por fallas en un parametro*/

                /*
                string _resp = _context.Track_GuardarUsuario(idUsuario, userName, password, nombre, apellidos, eMail, agrupacion, activo, listCedisASociados).FirstOrDefault().Response;

                Methods_User _user = new Methods_User();

                _user.guardarlog(idUsuarioConectado, "Creación / modificación de usuario : " + userName.ToString());

                return _resp;
                */

                InicializarConexion();

                //List<Track_GetInformeViajes_Result> _listViajes = new List<Track_GetInformeViajes_Result>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GuardarUsuario");

                EstablecerParametroProcedimientoAlmacenado(idUsuario, "int", "@idUsuario");
                EstablecerParametroProcedimientoAlmacenado(userName, "string", "@userName");
                EstablecerParametroProcedimientoAlmacenado(password, "string", "@password");
                EstablecerParametroProcedimientoAlmacenado(nombre, "string", "@nombre");
                EstablecerParametroProcedimientoAlmacenado(apellidos, "string", "@apellidos");
                EstablecerParametroProcedimientoAlmacenado(eMail, "string", "@eMail");
                EstablecerParametroProcedimientoAlmacenado(agrupacion, "string", "@agrupacion");
                EstablecerParametroProcedimientoAlmacenado(activo, "int", "@activo");
                EstablecerParametroProcedimientoAlmacenado(listCedisASociados, "string", "@listCedisASociados");


                //LlenarLectorDatosSQL();

                //_listViajes = LlenarListadoViajesInforme();
                string respuesta = EjecutarProcedimientoAlmacenadoEscalar();

                conexionSQL.Close();

                return respuesta;
            }
            catch (Exception ex)
            {
                return "Se ha producido un error.";
            }
        }

        public Track_Usuarios getUsuarioConectado(string pUsuario)
        {
            try
            {
                return (from a in _context.Track_Usuarios where a.Usuario == pUsuario select a).FirstOrDefault();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new Track_Usuarios();
            }
        }


        public int getPerfilUsuarioConectado(string usuario)
        {
            try
            {
                int perfil = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().IdRol;

                return perfil;

            }
            catch (Exception)
            {
                return 0;
            }
        }


        public string GetUserIP()
        {

            string ipList = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (!string.IsNullOrEmpty(ipList))
            {
                return ipList.Split(',')[0];
            }

            return System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
        }


        public void guardarlog(int idUsuario, string action)
        {
            try
            {
                string userIpAddress = GetUserIP();

                if (userIpAddress == "::1")
                {
                    userIpAddress = "127.0.0.1";
                }

                _context.Track_GuardarLog(idUsuario, userIpAddress, action);
            }
            catch (Exception ex)
            {

            }
        }

        public List<Track_Aplicacion_Modulos_Result> getModulos(int idPefil)
        {
            /*
            try
            {
                return _context.Track_Aplicacion_Modulos(idPefil).ToList();
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
                return new List<Track_Aplicacion_Modulos_Result>();
            }
            */
            return new List<Track_Aplicacion_Modulos_Result>();
        }

        public string getNameUsuarioConectado(string usuario)
        {
            try
            {
                string fulName = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Nombre + " " + (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Apellidos;

                return fulName;

            }
            catch (Exception)
            {
                return "";
            }
        }

        public string getTransportista(string usuario)
        {
            try
            {
                string transportista = "";

                int rolUsuario = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().IdRol;

                if (rolUsuario == 5) //Rol Transportista
                {
                    transportista = (from a in _context.Track_Usuarios where a.Usuario == usuario select a).FirstOrDefault().Transportista;
                }

                return transportista;

            }
            catch (Exception)
            {
                return "";
            }
        }


        public void CerrarSesion(string userName)
        {
            //_context.Track_CerrarSesion(userName);
        }

        public string ValidarUsuario(string usuario, string password)
        {
            try
            {
                string _result = _context.Track_ValidarUsuario(usuario, password, "").FirstOrDefault().Respuesta;
                return _result;

            }
            catch (Exception e)
            {
                return "Se ha producido un error.";
            }
        }

        public string ValidarUsuarioLocal(string local, string usuario, string password)
        {
            try
            {
                string _result = _context.Track_ValidarUsuarioLocal(local, usuario, password).FirstOrDefault().Respuesta;
                return _result;

            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
        }

        public Track_Usuarios GetUsuario(string userName)
        {
            try
            {
                Track_Usuarios _usuario = (from c in _context.Track_Usuarios where c.Usuario == userName select c).FirstOrDefault();

                return _usuario;

            }
            catch (Exception)
            {
                return new Track_Usuarios();
            }
        }

    }
}
