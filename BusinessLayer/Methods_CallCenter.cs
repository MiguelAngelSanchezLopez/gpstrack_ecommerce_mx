﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using ContextLayer.Model;

namespace BusinessLayer
{
  public class Methods_CallCenter
  {
    private ModelEntities _context = new ModelEntities();

    public List<TipoGeneral> GetExpliacionGestion()
    {
      try
      {
        List<TipoGeneral> _listExplicacion = (from c in _context.TipoGeneral where c.Tipo == "Explicacion" && c.Activo == true select c).ToList();

        return _listExplicacion;

      }
      catch (Exception)
      {
        return new List<TipoGeneral>();
      }
    }

    public List<Track_GetContactosGestion_Result> GetContactosGestion(int idAlerta)
    {
            /*
      try
      {
        List<Track_GetContactosGestion_Result> _listContactos = _context.Track_GetContactosGestion(idAlerta).ToList();
        return _listContactos;

      }
      catch (Exception)
      {
        return new List<Track_GetContactosGestion_Result>();
      }
      */
            return new List<Track_GetContactosGestion_Result>();
    }

    public List<Track_GetGestionCallCenter_Result> GetGestionCallCenter(long nroTransporte, long idEmbarque, int codLocal)
    {
      try
      {
        ((System.Data.Entity.Infrastructure.IObjectContextAdapter)_context).ObjectContext.CommandTimeout = 120;

        List<Track_GetGestionCallCenter_Result> _listAlertas = _context.Track_GetGestionCallCenter(nroTransporte, idEmbarque, codLocal).ToList();

        return _listAlertas;

      }
      catch (Exception)
      {
        return new List<Track_GetGestionCallCenter_Result>();
      }
    }
        /*
            public string GuardarGestionAlerta(int idAlerta, int idUsuario1, string explicacion1, string observacion1, int idUsuario2, string explicacion2, string observacion2, int idUsuario3, string explicacion3, string observacion3, int idUsuario4, string explicacion4, string observacion4, int idUsuario5, string explicacion5, string observacion5)
            {
              try
              {
                List<Track_GuardarGestionAlerta_Result> _result = _context.Track_GuardarGestionAlerta(idAlerta, idUsuario1, explicacion1, observacion1, idUsuario2, explicacion2, observacion2, idUsuario3, explicacion3, observacion3, idUsuario4, explicacion4, observacion4, idUsuario5, explicacion5, observacion5).ToList();
                return _result.FirstOrDefault().Respuesta;

              }
              catch (Exception)
              {
                return "Se ha producido un error.";
              }
            }
            */
        public string GuardarGestionAlerta(int idAlerta, int idUsuario1, string explicacion1, string observacion1, int idUsuario2, string explicacion2, string observacion2, int idUsuario3, string explicacion3, string observacion3, int idUsuario4, string explicacion4, string observacion4, int idUsuario5, string explicacion5, string observacion5)
        {
            /*
            try
            {
                List<Track_GuardarGestionAlerta_Result> _result = _context.Track_GuardarGestionAlerta(idAlerta, idUsuario1, explicacion1, observacion1, idUsuario2, explicacion2, observacion2, idUsuario3, explicacion3, observacion3, idUsuario4, explicacion4, observacion4, idUsuario5, explicacion5, observacion5).ToList();
                return _result.FirstOrDefault().Respuesta;

            }
            catch (Exception)
            {
                return "Se ha producido un error.";
            }
            */
            return "";
        }
    }
}
