﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BusinessEntities;
using BusinessLayer;
using ContextLayer.Model;

using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using iTextSharp.text;
using System.IO;
using System.Web;
using System.Net;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;


namespace BusinessLayer
{
    public class Methods_Export
    {
        private ModelEntities _context = new ModelEntities();

        public static string HTML_RPT_Comercial(List<Track_GetReporteComercial_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Top16</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Transporte</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placas_Pool</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placas_Integradas</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placas Esperadas</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placas Reales Reportando</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Integrado</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Reportando</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Id));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].CantidadPlacas));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PlacasIntegradas));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PlacasEsperadas));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PlacasRealesReportando));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Integrado));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Reportando));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_RPT_EstadiaRemolques_Historico(List<Track_GetReporteEstadiaRemolques_Historico_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            //reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Línea Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tipo unidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha GPS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha transmisión</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Desfase transmisión(min)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo última señal(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Latitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Referencia</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha detención</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo detenido(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Horas detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última entrada zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última salida zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">En Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Determinante zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Formato zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo en zona(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Distancia zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">CEDIS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Carga</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Viaje</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                //reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Fecha + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Placa));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TipoUnidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoEventos));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaEvento + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaReporte + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DesfaseReporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Antiguedad + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Latitud.ToString().Replace(",", ".") + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Longitud.ToString().Replace(",", ".") + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Referencia));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Detenido));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaDetencion + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoDetenido + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].HorasDetenidoTxt));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaEntrada + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaSalida + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EnZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Zona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Formato));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoEnZona + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DistanciaZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreCEDIS));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoCarga));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].RegionZona));

            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_RPT_EstadiaRemolques(List<Track_GetReporteEstadiaRemolques_Result> _list, bool habilitarCampoReferencia)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Línea Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tipo unidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha GPS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha transmisión</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Desfase transmisión(min)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo última señal(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Latitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");

            if (habilitarCampoReferencia)
                reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Referencia</span></td>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha detención</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo detenido(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Horas detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última entrada zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última salida zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">En Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Determinante zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Formato zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo en zona(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Distancia zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">CEDIS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Carga</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Viaje</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Placa));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TipoUnidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoEventos));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaEvento + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaReporte + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DesfaseReporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Antiguedad + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Latitud.ToString().Replace(",", ".") + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Longitud.ToString().Replace(",", ".") + "&#8203 "));

                if (habilitarCampoReferencia)
                    reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Referencia));

                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Detenido));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaDetencion + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoDetenido + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].HorasDetenidoTxt));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaEntrada + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaSalida + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EnZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Zona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Formato));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoEnZona + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DistanciaZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreCEDIS));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoCarga));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].RegionZona));

            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_Zonas(List<Track_GetZonas_Result> _list)
        {

            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Id Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tipo Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Latitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].IdZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreTipoZona));

                Methods_Zonas _obj = new Methods_Zonas();

                List<Track_Vertices> _vertices = _obj.GetVerticesZona(_list[i].IdZona);
                for (int j = 0; j < _vertices.Count; j++)
                {
                    reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", "&#8203 " + _vertices[j].Latitud.ToString().Replace(",", ".")));
                    reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", "&#8203 " + _vertices[j].Longitud.ToString().Replace(",", ".")));

                    if (j == _vertices.Count - 1)
                    {
                        reportBuilder.AppendLine("  <tr style = \"border: 0px\" ></tr>");
                        break;
                    }
                    else
                    {
                        reportBuilder.AppendLine("  </tr>");
                    }

                    reportBuilder.AppendLine("  <td style = \"border: 0px\" > </td>");
                    reportBuilder.AppendLine("  <td style = \"border: 0px\" > </td>");
                    reportBuilder.AppendLine("  <td style = \"border: 0px\" > </td>");
                }
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }


        public static string HTML_RPT_KmsRecorridos(List<Track_GetRpt_KmsRecorridos_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">IdMaster</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">IdEmbarque</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tracto</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Remolque</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Origen</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Operador</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Destinos</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Km Recorridos</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NroTransporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].IdEmbarque));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", Convert.ToDateTime(_list[i].FHSalidaOrigen).ToString("yyyy-MM-dd")));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PatenteTracto));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PatenteTrailer));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreOrigen));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreConductor));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Destinos));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].KmRecorridos.ToString().Replace(",", ".")));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_RPT_EstadiaTractos_Historico(List<Track_GetReporteEstadiaTractos_Historico_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            //reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Línea Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tipo unidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha GPS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha transmisión</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Desfase transmisión(min)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo última señal(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Latitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Referencia</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha detención</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo detenido(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Horas detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última entrada zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última salida zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">En Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Determinante zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Formato zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo en zona(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Distancia zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">CEDIS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Carga</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Viaje</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                //reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Fecha + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Placa));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TipoUnidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoEventos));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaEvento + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaReporte + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DesfaseReporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Antiguedad + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Latitud.ToString().Replace(",", ".") + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Longitud.ToString().Replace(",", ".") + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Referencia));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Detenido));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaDetencion + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoDetenido + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].HorasDetenidoTxt));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaEntrada + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaSalida + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EnZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Zona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Formato));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoEnZona + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DistanciaZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreCEDIS));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoCarga));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].RegionZona));

            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_RPT_EstadiaTractos(List<Track_GetReporteEstadiaTractos_Result> _list, bool habilitarCampoReferencia)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Línea Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tipo unidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha GPS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha transmisión</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Desfase transmisión(min)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo última señal(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Latitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");

            if (habilitarCampoReferencia)
                reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Referencia</span></td>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha detención</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo detenido(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Horas detenido</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última entrada zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última salida zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Última zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">En Zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Determinante zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Formato zona</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tiempo en zona(d-h-m)</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Distancia zona cercana</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">CEDIS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Carga</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Viaje</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Zona</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Placa));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TipoUnidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoEventos));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaEvento + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaReporte + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DesfaseReporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Antiguedad + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Latitud.ToString().Replace(",", ".") + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Longitud.ToString().Replace(",", ".") + "&#8203 "));

                if (habilitarCampoReferencia)
                    reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Referencia));

                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Detenido));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FechaDetencion + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoDetenido + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].HorasDetenidoTxt));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaEntrada + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaSalida + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltimaZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EnZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Zona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZona));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Formato));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TiempoEnZona + "&#8203 "));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].DistanciaZonaCercana));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreCEDIS));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoCarga));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].RegionZona));

            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_PosicionesGPS(List<Track_GetPosicionesGPS_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Patente</span></td>");
            //reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Laitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Fecha));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Patente));
                //reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", "&#8203 " + _list[i].Latitud.ToString().Replace(",", ".")));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", "&#8203 " + _list[i].Longitud.ToString().Replace(",", ".")));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_FlotaOnline(List<Track_GetFlotaOnline_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Id_Master</span></td>");
            //reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Id_Embarque</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Linea_Transporte</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            //reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Operador</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado GPS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Viaje</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Latitud</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Longitud</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].UltReporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NroTransporte));
                //reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].IdEmbarque));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Patente));
                //reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreConductor));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoGPS));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", "&#8203 " + _list[i].Latitud.ToString().Replace(",", ".")));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", "&#8203 " + _list[i].Longitud.ToString().Replace(",", ".")));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_ViajesHistoricos(List<Track_GetViajesHistoricos_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Transporte</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Trailer</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tracto</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Origen</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre Origen</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Destino</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nombre Destino</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Asignación</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Salida Origen</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Llegada Destino</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Salida Destino</span></td>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Alertas</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado</span></td>");
            //reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tipo Viaje</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NroTransporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PatenteTrailer));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PatenteTracto));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].CodigoOrigen));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreOrigen));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].CodigoDestino));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreDestino));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FHAsignacion));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FHSalidaOrigen));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FHLlegadaDestino));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].FHSalidaDestino));

                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].CantidadAlertas));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                //reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].TipoViaje));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_ViajesLocal(string nroTransporte, string fecha, string patenteTrailer, string patenteTracto, string transportista, string codigoDestino, string estado, List<Track_GetAlertasRuta_Result> _listAlertas)
        {
            StringBuilder reportBuilder = new StringBuilder();

            DateTime _fec = Convert.ToDateTime(fecha);

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Nro. Transporte</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Trailer</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Tracto</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Transportista</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Destino</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado Viaje</span></td>");
            reportBuilder.AppendLine("  </tr>");
            reportBuilder.AppendLine("  <tr>");
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", nroTransporte));
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _fec));
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", patenteTrailer));
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", patenteTracto));
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", transportista));
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", codigoDestino));
            reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", estado));
            reportBuilder.AppendLine("  </tr>");

            reportBuilder.AppendLine("</table>");

            reportBuilder.AppendLine("  <tr>");
            reportBuilder.AppendLine("  </tr>");

            reportBuilder.AppendLine("  <tr>");
            reportBuilder.AppendLine("      <td style=\"border: 0px\"><span style=\"font-size:14pt; font-weight:bold\">Alertas</span></td>");
            reportBuilder.AppendLine("  </tr>");

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha Inicio</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha Envío</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Descripción</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _listAlertas.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _listAlertas[i].FechaInicioAlerta));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _listAlertas[i].FechaHoraCreacion));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _listAlertas[i].DescripcionAlerta));
                reportBuilder.AppendLine("  </tr>");
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_RptViajesControlLocal(List<Track_GetViajesControlLocal_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Guía Despacho</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Patente</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Cliente</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">ETIS</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Contenedor</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">OTIF</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Delta</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Problema</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">EstadoViaje</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NroTransporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PatenteTracto));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreCliente));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Etis));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NumeroContenedor));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].OTIF));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Delta));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Problema));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_RptViajesControlLocalCliente(List<Track_GetViajesControlLocalCliente_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Order Number</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Origen</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Destino</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Hora_Salida</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Hora_Llegada</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Estado_Viaje</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Reportabilidad</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NroTransporte));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].PatenteTracto));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreOrigen));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].NombreDestino));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].SalidaOrigen));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].LlegadaCliente));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoViaje));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EstadoGPS));
            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static string HTML_LastPositions(List<Track_GetLastPositions_Result> _list)
        {
            StringBuilder reportBuilder = new StringBuilder();

            reportBuilder.AppendLine("<table border=\"0,5\" width=\"100%\">");

            reportBuilder.AppendLine("  <tr style=\"border: 0px\"><td>   </td></tr>");

            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Fecha</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Placa</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Línea Transporte</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Velocidad</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">En Tiempo</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Retraso</span></td>");
            reportBuilder.AppendLine("      <td style=\"border: 1px\"><span style=\"font-size:12pt; font-weight:bold\">Sin reporte + 12</span></td>");

            reportBuilder.AppendLine("  </tr>");

            for (int i = 0; i < _list.Count; i++)
            {
                reportBuilder.AppendLine("  <tr>");
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Fecha));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Patente));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Transportista));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Velocidad));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].EnTiempo));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].Retraso));
                reportBuilder.AppendLine(string.Format("      <td style=\"border: 0,5px\">{0}</td>", _list[i].SinReporte));

            }

            reportBuilder.AppendLine("</table>");

            return reportBuilder.ToString();
        }

        public static MemoryStream MS_InformeViajes(string nroTransporte, string transportista, string nombreOrigen, string destinos, string tracto, string trailer, string conductor, List<Track_GetDetalleTrayecto_Result> _detalleTrayecto, List<Track_GetAlertasInformeViaje_Result> _alertasInformeViaje, List<Track_GetPosicionesGPS_Viaje_Result> _listPosiciones)
        {
            //Disable SSL certificate security. It´s not working other way
            ServicePointManager.ServerCertificateValidationCallback = delegate (object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors)
            { return true; };

            Document document = new Document();

            MemoryStream memoryStream = new MemoryStream();
            PdfWriter pdfWriter = PdfWriter.GetInstance(document, memoryStream);

            PdfPTable pdfTableResumen = new PdfPTable(2);

            pdfTableResumen.WidthPercentage = 100;
            pdfTableResumen.DefaultCell.Border = Rectangle.NO_BORDER;
            pdfTableResumen.HorizontalAlignment = Element.ALIGN_LEFT;

            float[] widths = new float[] { 2f, 5f };
            pdfTableResumen.SetWidths(widths);

            pdfTableResumen.SpacingBefore = 30f;
            pdfTableResumen.SpacingAfter = 40f;

            pdfTableResumen.AddCell("Id. Master / Folio:");
            pdfTableResumen.AddCell(" " + nroTransporte);
            pdfTableResumen.AddCell("Transportista:");
            pdfTableResumen.AddCell(" " + transportista);
            pdfTableResumen.AddCell("Origen");
            pdfTableResumen.AddCell(" " + nombreOrigen);
            pdfTableResumen.AddCell("Destino/s:");
            pdfTableResumen.AddCell(" " + destinos);
            pdfTableResumen.AddCell("Tracto:");
            pdfTableResumen.AddCell(" " + tracto);
            pdfTableResumen.AddCell("Remolque:");
            pdfTableResumen.AddCell(" " + trailer);
            pdfTableResumen.AddCell("Operador:");
            pdfTableResumen.AddCell(" " + conductor);

            document.Open();

            PdfPTable pdfTableLogos = new PdfPTable(3);
            pdfTableLogos.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTableLogos.DefaultCell.Border = Rectangle.NO_BORDER;
            float[] widthsTableLogos = new float[] { 2f, 8f, 2f };
            pdfTableLogos.SetWidths(widthsTableLogos);

            var server = HttpContext.Current.Server;
            var imagePath = server.MapPath("Images");
            Image logoAltoTrack = Image.GetInstance(imagePath + "/logo_white_1500x789.png");
            Image logoWalmartMX = Image.GetInstance(imagePath + "/logo_walmartMX_1500x769.png");
            logoAltoTrack.ScalePercent(5f);
            logoWalmartMX.ScalePercent(7f);

            pdfTableLogos.AddCell(logoAltoTrack);
            pdfTableLogos.AddCell("");
            pdfTableLogos.AddCell(logoWalmartMX);

            document.Add(pdfTableLogos);

            var boldTitle = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 20);

            Paragraph title = new Paragraph("Informe de Viaje", boldTitle);
            title.Alignment = Element.ALIGN_CENTER;

            document.Add(title);

            document.Add(pdfTableResumen);

            var boldFont = FontFactory.GetFont(FontFactory.HELVETICA_BOLD, 12);

            var tituloDetalle = new Phrase();
            tituloDetalle.Add(new Chunk("Detalle de trayecto:", boldFont));
            document.Add(new Paragraph(tituloDetalle));

            PdfPTable pdfTableDetalleTrayecto = new PdfPTable(2);
            pdfTableDetalleTrayecto.WidthPercentage = 100;
            pdfTableDetalleTrayecto.HorizontalAlignment = Element.ALIGN_LEFT;
            pdfTableDetalleTrayecto.DefaultCell.Border = Rectangle.NO_BORDER;
            float[] widthsTableDetalleTrayecto = new float[] { 3f, 5f };
            pdfTableDetalleTrayecto.SetWidths(widthsTableDetalleTrayecto);
            pdfTableDetalleTrayecto.SpacingBefore = 10;
            pdfTableDetalleTrayecto.SpacingAfter = 20;

            for (int j = 0; j < _detalleTrayecto.Count; j++)
            {
                pdfTableDetalleTrayecto.AddCell(_detalleTrayecto[j].Fecha);
                pdfTableDetalleTrayecto.AddCell(_detalleTrayecto[j].Detalle);
            }
            document.Add(pdfTableDetalleTrayecto);

            var tituloPosicionesViaje = new Phrase();
            tituloPosicionesViaje.Add(new Chunk("Recorrido:", boldFont));

            document.Add(new Paragraph(tituloPosicionesViaje));

            if (_listPosiciones.Count > 0)
            {
                string centerPos = _listPosiciones[_listPosiciones.Count - 1].Latitud + "," + _listPosiciones[_listPosiciones.Count - 1].Longitud;

                //var stringUrlMapPositions = "https://maps.googleapis.com/maps/api/staticmap?zoom=9&maptype=roadmap&size=500x300&center=" + centerPos + "&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY";
                var stringUrlMapPositions = "https://maps.googleapis.com/maps/api/staticmap?zoom=9&maptype=roadmap&size=500x300&center=" + centerPos + "&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8";

                for (int k = 0; k < _listPosiciones.Count; k++)
                {
                    string coordinates = _listPosiciones[k].Latitud.ToString().Replace(",", ".") + ',' + _listPosiciones[k].Longitud.ToString().Replace(",", ".");

                    string urlIcon = "https://gpsmx.altotrack.com/Images/";

                    int dir = 0;

                    if (_listPosiciones[k].Velocidad > 0)
                    {
                        dir = (int)_listPosiciones[k].Direccion;

                        if ((dir >= 338) || (dir < 22))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/1_arrowcircle_blue_N_20x20.png";
                        }
                        if ((dir >= 22) && (dir < 67))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/2_arrowcircle_blue_NE_20x20.png";
                        }
                        if ((dir >= 67) && (dir < 112))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/3_arrowcircle_blue_E_20x20.png";
                        }
                        if ((dir >= 112) && (dir < 157))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/4_arrowcircle_blue_SE_20x20.png";
                        }
                        if ((dir >= 157) && (dir < 202))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/5_arrowcircle_blue_S_20x20.png";
                        }
                        if ((dir >= 202) && (dir < 247))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/6_arrowcircle_blue_SW_20x20.png";
                        }
                        if ((dir >= 247) && (dir < 292))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/7_arrowcircle_blue_W_20x20.png";
                        }
                        if ((dir >= 292) && (dir < 338))
                        {
                            urlIcon = urlIcon + "Circle_Arrow/8_arrowcircle_blue_NW_20x20.png";
                        }

                    }
                    else
                    {
                        urlIcon = urlIcon + "dot_red_16x16.png";
                    }

                    stringUrlMapPositions = stringUrlMapPositions + "&markers=icon:" + urlIcon + "|" + coordinates;

                }

                iTextSharp.text.Image mapImagePositions = iTextSharp.text.Image.GetInstance(stringUrlMapPositions);
                document.Add(mapImagePositions);
            }

            var tituloAlertas = new Phrase();
            tituloAlertas.Add(new Chunk("Alertas:", boldFont));

            var tituloObservaciones = new Phrase();
            tituloObservaciones.Add(new Chunk("Observaciones:", boldFont));

            if (_alertasInformeViaje.Count > 0)
            {
                document.NewPage();
                document.Add(new Paragraph(tituloAlertas));
            }

            int countMapPag = 0;

            for (int i = 0; i < _alertasInformeViaje.Count; i++)
            {
                PdfPTable pdfTableAlertas = new PdfPTable(4);
                pdfTableAlertas.WidthPercentage = 100;
                pdfTableAlertas.HorizontalAlignment = Element.ALIGN_LEFT;
                float[] widthsTableAlertas = new float[] { 2f, 1f, 3f, 1f };
                pdfTableAlertas.SetWidths(widthsTableAlertas);

                pdfTableAlertas.SpacingBefore = 30;
                pdfTableAlertas.SpacingAfter = 10;

                pdfTableAlertas.AddCell("Fecha Inicio:");
                pdfTableAlertas.AddCell("Tienda:");
                pdfTableAlertas.AddCell("Descripción");
                pdfTableAlertas.AddCell("Tiempo");
                pdfTableAlertas.AddCell(_alertasInformeViaje[i].FechaInicioAlerta.ToString());
                pdfTableAlertas.AddCell(_alertasInformeViaje[i].LocalDestino.ToString());
                pdfTableAlertas.AddCell(_alertasInformeViaje[i].DescripcionAlerta.ToString());
                //pdfTableAlertas.AddCell(_alertasInformeViaje[i].Tiempo);

                document.Add(pdfTableAlertas);
                /*
                if (_alertasInformeViaje[i].NombreZona != "")
                {
                    document.Add(new Paragraph("Zona: " + _alertasInformeViaje[i].NombreZona));
                }
                */
                string center = _alertasInformeViaje[i].Latitud + "," + _alertasInformeViaje[i].Longitud;
                string polygon;

                if (_alertasInformeViaje[i].ZoneLocation != -1)
                {
                    int _idZona;
                    _idZona = Convert.ToInt32(_alertasInformeViaje[i].ZoneLocation);

                    polygon = "&path=color:0x000000%7Cweight:2%7Cfillcolor:0x9966FF%7C";

                    Methods_Zonas _obj = new Methods_Zonas();
                    List<Track_Vertices> _vertices = _obj.GetAllVerticesZona(_idZona);
                    for (int k = 0; k < _vertices.Count; k++)
                    {
                        polygon = polygon + _vertices[k].Latitud.ToString().Replace(",", ".") + ',' + _vertices[k].Longitud.ToString().Replace(",", ".");

                        if (k < _vertices.Count - 1)
                        {
                            polygon = polygon + "%7C";
                        }
                    }

                }
                else
                {
                    polygon = "";
                }

                //var stringUrlMap = "https://maps.googleapis.com/maps/api/staticmap?zoom=13&maptype=roadmap&size=500x200&center=" + center + "&markers=color:red%7C" + center + "&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" + polygon;
                var stringUrlMap = "https://maps.googleapis.com/maps/api/staticmap?zoom=13&maptype=roadmap&size=500x200&center=" + center + "&markers=color:red%7C" + center + "&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" + polygon;

                iTextSharp.text.Image mapImage = iTextSharp.text.Image.GetInstance(stringUrlMap);
                document.Add(mapImage);

                document.Add(new Paragraph(tituloObservaciones));
                if (_alertasInformeViaje[i].Observaciones != "")
                {
                    document.Add(new Paragraph(_alertasInformeViaje[i].Observaciones));
                }
                else
                {
                    document.Add(new Paragraph("Sin observaciones."));
                }

                countMapPag = countMapPag + 1;

                if (countMapPag >= 2)
                {
                    document.NewPage();
                    countMapPag = 0;
                }

            }

            document.Close();

            return memoryStream;

        }
    }

}

