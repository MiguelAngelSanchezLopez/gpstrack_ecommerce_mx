﻿using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class TablerosPorUsuario : OperacionesTableroGeneral
    {
        ControladorTablerosPorUsuario controladorTablerosPorUsuario = new ControladorTablerosPorUsuario();
        ControladorUsuarios controladorUsuarios = new ControladorUsuarios();
        ControladorPerfiles controladorPerfiles = new ControladorPerfiles();
        ControladorTableros controladorTableros = new ControladorTableros();
        ControladorPerfilesPorUsuario controladorPerfilesPorUsuario = new ControladorPerfilesPorUsuario();

        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);
            ObtenerInformacionTodosJSON();
        }

        public override void ObtenerInformacionTodosJSON()
        {

            contenedorJSONTablerosPorUsuario.Value = controladorTablerosPorUsuario.ObtenerTodosRegistrosTablerosPorUsuario();
            contenedorJSONUsuarios.Value = controladorUsuarios.ObtenerTodosRegistrosUsuarios();
            contenedorJSONPerfiles.Value = controladorPerfiles.ObtenerTodosRegistrosPerfil();
            contenedorJSONTableros.Value = controladorTableros.ObtenerTodosRegistrosTableros();
            contenedorJSONPerfilesPorUsuario.Value = controladorPerfilesPorUsuario.ObtenerTodosRegistrosPerfilPorUsuario();
        }

        public void btnBorrarTablerosPorUsuario_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorUsuario.EliminarRegistroTablerosPorUsuarioPorId(Convert.ToInt32(hidden_idTemporalTableroPorUsuario.Value));
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }

        public void btnActualizarTablerosPorUsuario_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorUsuario.InsertarRegistroTablerosPerfilesUsuario(hidden_IdsTablerosSeleccionadas.Value, hidden_IdsPerfilesSeleccionados.Value, hidden_IdsUsuariosSeleccionados.Value);
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }

        public void btnInsertarTablerosPorUsuario_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorUsuario.InsertarRegistroTablerosPorUsuarioPorId(hidden_JSONTableroPorUsuarioTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }

        public void btnInsertarRegistrosTablerosPerfilesUsuario_Click(Object sender, EventArgs e)
        {
            controladorTablerosPorUsuario.InsertarRegistroTablerosPerfilesUsuario(hidden_IdsTablerosSeleccionadas.Value, hidden_IdsPerfilesSeleccionados.Value, hidden_IdsUsuariosSeleccionados.Value);
            Session["recargado"] = "TRUE";
            RecargarTableroActual();
        }

    }
}