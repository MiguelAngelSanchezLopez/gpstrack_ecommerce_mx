﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorPaginasPorUsuario: ControladorGeneral
    {
        private List<Track_PaginasPorUsuario> listadoPaginasPorUsuario;

        public Track_PaginasPorUsuario LlenarPaginaPorUsuario()
        {
            Track_PaginasPorUsuario paginaPorUsuarioTemporal = new Track_PaginasPorUsuario();
            paginaPorUsuarioTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            paginaPorUsuarioTemporal.IdUsuario = Convert.ToInt32(lectorDatosSQL["IdUsuario"]);
            paginaPorUsuarioTemporal.IdPagina = Convert.ToInt32(lectorDatosSQL["IdPagina"]);
            paginaPorUsuarioTemporal.FechaCreacion = Convert.ToDateTime(lectorDatosSQL["FechaCreacion"]);
            paginaPorUsuarioTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"]);

            //paginaPorUsuarioTemporal.PaginasPorUsuario = ObtenerNombrePaginasPorUsuario(paginaPorUsuarioTemporal.IdUsuario);
            string nombresPaginas = lectorDatosSQL["NombresPaginas"].ToString();
            paginaPorUsuarioTemporal.PaginasPorUsuario = nombresPaginas;

            //paginaPorUsuarioTemporal.IdsPaginasPorUsuario = ObtenerIdsPaginasPorUsuario(paginaPorUsuarioTemporal.IdUsuario);
            string IdsPaginasPorUsuario = lectorDatosSQL["IdsPaginas"].ToString();
            IdsPaginasPorUsuario = IdsPaginasPorUsuario.Substring(0, IdsPaginasPorUsuario.Length - 1);
            paginaPorUsuarioTemporal.IdsPaginasPorUsuario = new List<int>(IdsPaginasPorUsuario.Split(',').Select(s => int.Parse(s)));

            return paginaPorUsuarioTemporal;
        }
 
        private List<int> ObtenerIdsPaginasPorUsuario(int idUsuario)
        {
            List<int> IdsPaginasPorUsuario = new List<int>();
            try
            {
                SqlConnection conexionSQLInterna;
                conexionSQLInterna = new SqlConnection(CADENA_CONEXION);

                conexionSQLInterna.Open();

                SqlCommand comandoSQLInterno = new SqlCommand("Track_GetIdPaginasPorUsuario", conexionSQLInterna);

                comandoSQLInterno.CommandType = CommandType.StoredProcedure;
                comandoSQLInterno.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = idUsuario;

                IdsPaginasPorUsuario = ObtenerListadoIdsPaginasPorUsuario(comandoSQLInterno.ExecuteReader());

                conexionSQLInterna.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
            return IdsPaginasPorUsuario;
        }

        public List<int> ObtenerListadoIdsPaginasPorUsuario(SqlDataReader lectorDatos)
        {
            List<int> IdsPaginasPorUsuario = new List<int>();

            while (lectorDatos.Read())
                IdsPaginasPorUsuario.Add(Convert.ToInt32(lectorDatos["Id"]));

            return IdsPaginasPorUsuario;
        }

        private string ObtenerNombrePaginasPorUsuario(int idUsuario)
        {
            string nombresPaginas = "";
            try
            {
                SqlConnection conexionSQLInterna;
                conexionSQLInterna = new SqlConnection(CADENA_CONEXION);
                
                conexionSQLInterna.Open();
                
                SqlCommand comandoSQLInterno = new SqlCommand("Track_GetNombrePaginasPorUsuario", conexionSQLInterna);
                
                comandoSQLInterno.CommandType = CommandType.StoredProcedure;
                comandoSQLInterno.Parameters.Add("@IdUsuario", SqlDbType.Int).Value = idUsuario;
                
                nombresPaginas = ObtenerListadoNombresPaginasPorUsuario(comandoSQLInterno.ExecuteReader());

                conexionSQLInterna.Close();
            }
            catch (Exception ex)
            {
                nombresPaginas = "";
            }
            return nombresPaginas;
        }

        public string ObtenerListadoNombresPaginasPorUsuario(SqlDataReader lectorDatos) {

            StringBuilder listadoNombresPaginasPorUsuario = new StringBuilder();

            while (lectorDatos.Read())
                listadoNombresPaginasPorUsuario.Append(lectorDatos["Nombre"] + "<br>");

            string nombresPaginas = listadoNombresPaginasPorUsuario.ToString();

            return nombresPaginas.Substring(0, nombresPaginas.Length-1);
        }

        public void DesactivarPaginasPorUsuarioCoincidente(int IdUsuario) {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_DesactivarPaginasPorUsuario");

                EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AgregarNuevasPaginasPorUsuario(int IdUsuario, string IdsPaginasSeleccionadas) {
           
            List<int> arregloIdsPaginas = new List<int>();

            arregloIdsPaginas = IdsPaginasSeleccionadas.Split(',').Select(Int32.Parse).ToList();

            try
            {

                InicializarConexion();
                conexionSQL.Open();

                StringBuilder camposInsertar = new StringBuilder();

                for (int indice = 0; indice < arregloIdsPaginas.Count; indice++) 
                    camposInsertar.Append("("+ IdUsuario + ","+ arregloIdsPaginas[indice] + ",GETDATE(),1),");

                String camposInsertarTotales = camposInsertar.ToString();

                camposInsertarTotales = camposInsertarTotales.Substring(0, camposInsertarTotales.Length-1);

                EstablecerProcedimientoAlmacenado("Track_InsertarListaPaginasPorUsuario");
                EstablecerParametroProcedimientoAlmacenado(camposInsertarTotales, "string", "@CamposInsertar");
                
                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();

            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void DesactivarPerfilesPorUsuarioCoincidente(int IdUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_DesactivarPerfilesPorUsuario");

                EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void AgregarNuevosPerfilesPorUsuario(int IdUsuario, string IdsPerfilesSeleccionados)
        {
            List<int> arregloIdsPerfiles = new List<int>();

            arregloIdsPerfiles = IdsPerfilesSeleccionados.Split(',').Select(Int32.Parse).ToList();

            try
            {
                for (int indice = 0; indice < arregloIdsPerfiles.Count; indice++)
                {
                    InicializarConexion();
                    conexionSQL.Open();

                    EstablecerProcedimientoAlmacenado("Track_InsertarPerfilPorUsuario");

                    EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                    EstablecerParametroProcedimientoAlmacenado(arregloIdsPerfiles[indice], "int", "@IdPerfil");

                    EjecutarProcedimientoAlmacenado();
                    conexionSQL.Close();
                }
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroPaginasPerfilesUsuario(string IdsPaginasSeleccionadas, string IdsPerfilesSeleccionados, string IdUsuarioSeleccionado)
        {
                DesactivarPaginasPorUsuarioCoincidente( Convert.ToInt32(IdUsuarioSeleccionado));
                AgregarNuevasPaginasPorUsuario(Convert.ToInt32(IdUsuarioSeleccionado), IdsPaginasSeleccionadas);

                DesactivarPerfilesPorUsuarioCoincidente(Convert.ToInt32(IdUsuarioSeleccionado));
                AgregarNuevosPerfilesPorUsuario(Convert.ToInt32(IdUsuarioSeleccionado), IdsPerfilesSeleccionados);
        }

        public void LlenarListadoPaginasPorUsuario()
        {
            while (lectorDatosSQL.Read())
                listadoPaginasPorUsuario.Add(LlenarPaginaPorUsuario());
        }

        public string ObtenerTodosRegistrosPaginasPorUsuario()
        {
            try
            {
                InicializarConexion();

                listadoPaginasPorUsuario = new List<Track_PaginasPorUsuario>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPaginasPorUsuario");

                LlenarLectorDatosSQL();

                LlenarListadoPaginasPorUsuario();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoPaginasPorUsuario);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }
        
        public void EliminarRegistroPaginasPorUsuarioPorId(int IdUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();
                
                EstablecerProcedimientoAlmacenado("Track_EliminarPaginasPorUsuario_PorIdUsuario");
                EstablecerParametroProcedimientoAlmacenado(IdUsuario, "int", "@IdUsuario");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
        
        public void InsertarRegistroPaginasPorUsuarioPorId(string cadenaJSONPaginaPorUsuario)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarPaginasPorUsuario");
                Track_PaginasPorUsuario paginaPorUsuarioTemporal = JsonConvert.DeserializeObject<Track_PaginasPorUsuario>(cadenaJSONPaginaPorUsuario);

                EstablecerParametroProcedimientoAlmacenado(paginaPorUsuarioTemporal.IdPagina, "int", "@IdPagina");
                EstablecerParametroProcedimientoAlmacenado(paginaPorUsuarioTemporal.IdUsuario, "int", "@IdUsuario");
                EstablecerParametroProcedimientoAlmacenado(paginaPorUsuarioTemporal.FechaCreacion, "datetime", "@FechaCreacion");
                EstablecerParametroProcedimientoAlmacenado(paginaPorUsuarioTemporal.Activo, "bool", "@Activo");
                
                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}