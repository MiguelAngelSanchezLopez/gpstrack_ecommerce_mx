﻿using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorUsuarios : ControladorGeneral
    {
        private List<Track_Usuarios> listadoUsuarios;

        public Track_Usuarios LlenarUsuario()
        {
            Track_Usuarios usuarioTemporal = new Track_Usuarios();
            usuarioTemporal.IdUsuario = Convert.ToInt32(lectorDatosSQL["IdUsuario"]);
            usuarioTemporal.Nombre = lectorDatosSQL["Nombre"].ToString();
            usuarioTemporal.Apellidos = lectorDatosSQL["Apellidos"].ToString();

            return usuarioTemporal;
        }

        public void LlenarListadoUsuarios()
        {
            while (lectorDatosSQL.Read())
                listadoUsuarios.Add(LlenarUsuario());
        }

        public string ObtenerTodosRegistrosUsuarios()
        {
            try
            {
                InicializarConexion();

                listadoUsuarios = new List<Track_Usuarios>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetUsuarios_SoloIdsNombresApellidos");

                LlenarLectorDatosSQL();

                LlenarListadoUsuarios();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoUsuarios);
            }
            catch (Exception ex) {
                return ArmarJSONGenericoConError(ex.Message);
            }
        }

    }
}