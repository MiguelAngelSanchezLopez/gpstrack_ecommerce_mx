﻿using perfilamientoUsuarios.ModelosBD;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ControladoresBD
{
    public class ControladorPerfiles : ControladorGeneral
    {
        private List<Perfil> listadoPerfiles;

        public Perfil LlenarPerfil()
        {
            Perfil perfilTemporal = new Perfil();
            perfilTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            perfilTemporal.Nombre = lectorDatosSQL["Nombre"].ToString();
            perfilTemporal.Llave = lectorDatosSQL["Llave"].ToString();
            //IdsPaginasPorPerfil
            perfilTemporal.IdsPaginasPorPerfil = ObtenerIdsPaginasPorPerfil(Convert.ToInt32(lectorDatosSQL["Id"]));

            return perfilTemporal;
        }

        public void LlenarListadoPerfiles()
        {
            while (lectorDatosSQL.Read())
                listadoPerfiles.Add(LlenarPerfil());
        }

        public string ObtenerTodosRegistrosPerfil()
        {
            try
            {
                InicializarConexion();

                listadoPerfiles = new List<Perfil>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPerfiles");

                LlenarLectorDatosSQL();

                LlenarListadoPerfiles();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoPerfiles);
            }
            catch (Exception ex) {
                return ArmarJSONGenericoConError(ex.Message);
            }
        }

        private List<int> ObtenerIdsPaginasPorPerfil(int idPerfil)
        {
            List<int> IdsPaginasPorPerfil = new List<int>();
            
            try
            {
                SqlConnection conexionSQLInterna;
                conexionSQLInterna = new SqlConnection(CADENA_CONEXION);

                conexionSQLInterna.Open();

                SqlCommand comandoSQLInterno = new SqlCommand("Track_GetIdsPaginasPorPerfil", conexionSQLInterna);

                comandoSQLInterno.CommandType = CommandType.StoredProcedure;
                comandoSQLInterno.Parameters.Add("@IdPerfil", SqlDbType.Int).Value = idPerfil;

                SqlDataReader lectorDatos = comandoSQLInterno.ExecuteReader();

                while (lectorDatos.Read())
                    IdsPaginasPorPerfil.Add(Convert.ToInt32(lectorDatos["IdPagina"]));
                    
                conexionSQLInterna.Close();
            }
            catch (Exception ex)
            {
                return IdsPaginasPorPerfil;
            }
            return IdsPaginasPorPerfil;
        }
    }
}