﻿using Newtonsoft.Json;
using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Track_Web.ModelosBD;

namespace Track_Web.ControladoresBD
{
    public class ControladorTableros : ControladorGeneral
    {
        private List<Track_Tableros> listadoTableros;

        public Track_Tableros LlenarTablero()
        {
            Track_Tableros TableroTemporal = new Track_Tableros();
            TableroTemporal.Id = Convert.ToInt32(lectorDatosSQL["Id"]);
            TableroTemporal.NombreCorto = lectorDatosSQL["NombreCorto"].ToString();
            TableroTemporal.Descripcion = lectorDatosSQL["Descripcion"].ToString();
            TableroTemporal.Liga = lectorDatosSQL["Liga"].ToString();
            TableroTemporal.Activo = Convert.ToBoolean(lectorDatosSQL["Activo"].ToString());
            return TableroTemporal;
        }

        public void LlenarListadoTableros()
        {
            while (lectorDatosSQL.Read())
                listadoTableros.Add(LlenarTablero());
        }

        public string ObtenerTodosRegistrosTableros()
        {
            try
            {
                InicializarConexion();

                listadoTableros = new List<Track_Tableros>();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetTableros");

                LlenarLectorDatosSQL();

                LlenarListadoTableros();

                conexionSQL.Close();

                return ObtenerInformacionFormatoJSON(listadoTableros);
            }
            catch (Exception ex)
            {
                return ArmarJSONGenericoConError(ex.Message);
            }

        }

        public string ObtenerURLTableroPorId(int idTablero)
        {
            try
            {
                InicializarConexion();

                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetLigaTableroPorId");

                EstablecerParametroProcedimientoAlmacenado(idTablero, "int", "@IdTablero");

                String ligaTableroObtenida = EjecutarProcedimientoAlmacenadoEscalar();

                conexionSQL.Close();

                return ligaTableroObtenida;
            }
            catch (Exception ex)
            {
                return "Images/tablero_no_encontrado.jpg";
            }

        }


        public void EliminarRegistroTableroPorId(int id)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_EliminarTablero");
                EstablecerParametroProcedimientoAlmacenado(id, "int", "@Id");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void ActualizarRegistroTablero(string cadenaJSONTablero)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_ActualizarTablero");
                Track_Tableros TableroTemporal = JsonConvert.DeserializeObject<Track_Tableros>(cadenaJSONTablero);

                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.NombreCorto, "string", "@NombreCorto");
                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.Descripcion, "string", "@Descripcion");
                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.Liga, "string", "@Liga");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }

        public void InsertarRegistroTablero(string cadenaJSONTablero)
        {
            try
            {
                InicializarConexion();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_InsertarTablero");
                Track_Tableros TableroTemporal = JsonConvert.DeserializeObject<Track_Tableros>(cadenaJSONTablero);

                //EstablecerParametroProcedimientoAlmacenado(TableroTemporal.Id, "int", "@Id");
                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.NombreCorto, "string", "@NombreCorto");
                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.Descripcion, "string", "@Descripcion");
                EstablecerParametroProcedimientoAlmacenado(TableroTemporal.Liga, "string", "@Liga");

                EstablecerParametroProcedimientoAlmacenado(true, "bool", "@Activo");

                EjecutarProcedimientoAlmacenado();
                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}