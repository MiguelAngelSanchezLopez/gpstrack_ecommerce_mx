﻿using perfilamientoUsuarios.ControladoresBD;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class PaginasPorPerfil : OperacionesPaginaGeneral
    {
        ControladorPaginasPorPerfil controladorPaginasPorPerfil = new ControladorPaginasPorPerfil();
        ControladorUsuarios controladorUsuarios = new ControladorUsuarios();
        ControladorPerfiles controladorPerfiles = new ControladorPerfiles();
        ControladorPaginas controladorPaginas = new ControladorPaginas();

        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);
            ObtenerInformacionTodosJSON();
        }

        public void btnBorrarPaginaPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorPerfil.EliminarRegistroPaginasPorPerfilPorId(Convert.ToInt32(hidden_idTemporalPaginaPorPerfil.Value));
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnActualizarPaginaPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorPerfil.ActualizarRegistroPaginasPorPerfilPorId(hidden_JSONPaginaPorPerfilTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnInsertarPaginaPorPerfil_Click(Object sender, EventArgs e)
        {
            controladorPaginasPorPerfil.InsertarRegistroPaginasPorPerfilPorId(hidden_JSONPaginaPorPerfilTemporal.Value);
            Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public override void ObtenerInformacionTodosJSON()
        {
            contenedorJSONPaginasPorPerfil.Value = controladorPaginasPorPerfil.ObtenerTodosRegistrosPaginasPorPerfil();
            contenedorJSONUsuarios.Value = controladorUsuarios.ObtenerTodosRegistrosUsuarios();
            contenedorJSONPerfiles.Value = controladorPerfiles.ObtenerTodosRegistrosPerfil();
            contenedorJSONPaginas.Value = controladorPaginas.ObtenerTodosRegistrosPaginas();
        }
    }
}