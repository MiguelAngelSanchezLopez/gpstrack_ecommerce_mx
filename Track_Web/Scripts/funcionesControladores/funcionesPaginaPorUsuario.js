﻿var Columnas =
    {
        Id: 0,
        Usuario: 1,
        Pagina: 2,
        Perfil: 3,
        FechaCreacion: 4,
        Acciones: 5
    };

var elementoPaginaPorUsuarioTemporal = new Object();

var objetoJSONPaginasPorUsuario = new Object();

var objetoJSONPerfilesPorUsuario = new Object();

var objetoJSONPaginas = new Object();

var objetoJSONPerfiles = new Object();

var objetoJSONUsuarios = new Object();

var idRegistroOperacionTemporal = "";

var filtroId = "";

var filtroIdPagina = "";

var arregloCamposFiltros =
    [
        "txtId",
        "txtIdPagina",
        "txtIdPerfil",
        "txtIdUsuario"
    ];

var arregloCamposValidacion =
    [
        "txtIdPaginaAdm",
        "txtIdPerfilAdm",
        "txtIdUsuarioAdm"
    ];

var IdPerfilRemovido = 0;
var listadoArregloPaginasTemporal = [];
var arregloPaginasTemporal = [];

var IdsPaginasSeleccionadas = [];
var IdsPerfilesSeleccionados = [];
var IdsUsuariosSeleccionados = [];

function LimpiarElementoPaginaPorUsuarioTemporal() {
    elementoPaginaPorUsuarioTemporal = {
        "Id": "0",
        "IdUsuario": "0",
        "IdPagina": "0",
        "FechaCreacion": "01/01/0001",
        "Activo": "False"
    };
}

function LlenarElementoPaginaPorUsuarioTemporal() {
    ValidarCamposInformacion();
    
    elementoPaginaPorUsuarioTemporal = {
        "Id": idRegistroOperacionTemporal,
        "IdUsuario": $("#txtIdUsuarioAdm").val(),
        "IdPagina": $("#txtIdPaginaAdm").val(),
        "FechaCreacion": convertirFechaAFormatoMMDDYYYY("01/01/0001"),
        "Activo": "True"
    };

    $("#" + CONTENEDOR_ASPNET + "hidden_JSONPaginaPorUsuarioTemporal").val(JSON.stringify(elementoPaginaPorUsuarioTemporal));
}

function LlenarSelectUsuarios() {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        var ApellidosTemporal = "";
        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                IdTemporal = value;

            if (key == "Nombre")
                NombreTemporal = value;

            if (key == "Apellidos")
                ApellidosTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + " " + ApellidosTemporal + "</option>");

    });

    $("#usuariosSelect").html(elementosSelectGeneral.join(""));
    $("#usuariosSelectAdm").html(elementosSelectGeneral.join(""));
}

function LlenarSelectPaginas() {
    LlenarSelectGeneral(objetoJSONPaginas, "paginasSelect");
    LlenarSelectGeneral(objetoJSONPaginas, "paginasSelectAdm");
}

function LlenarSelectPerfiles() {
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelect");
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelectAdm");
}

function ObtenerNombrePaginaPorId(idPaginaBuscado) {
    return ObtieneDatoPorId(idPaginaBuscado, objetoJSONPaginas, "item.Nombre", "Id");
}

function ObtenerNombrePerfilPorId(idPerfilBuscado) {
    return ObtieneDatoPorId(idPerfilBuscado, objetoJSONPerfiles, "item.Nombre", "Id");
}

function ObtenerNombreCompletoUsuarioPorId(idUsuarioBuscado) {
    return ObtieneDatoPorId(idUsuarioBuscado, objetoJSONUsuarios, "item.Nombre +' ' + item.Apellidos", "IdUsuario");
}

function ObtenerDatosPorId(idRegistroBuscado) {
    $.each(objetoJSONPaginasPorUsuario, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    ObtenerRegistroCompleto(item);

        });
    });
}

function LimpiarCamposAdministracion() {

    $("#txtIdAdm").val("");
    $("#txtIdPaginaAdm").val("");
    $("#txtIdPerfilAdm").val("");
    $("#txtIdUsuarioAdm").val("");

    ResetearSelects();
}

function ObtenerRegistroCompleto(elemento) {
    idRegistroOperacionTemporal = elemento.Id;

    $("#txtIdAdm").val(elemento.Id);
    $("#txtIdPaginaAdm").val(elemento.IdPagina);
    
    $("#txtIdPerfilAdm").val(elemento.IdPerfil);
    
    $("#txtIdUsuarioAdm").val(elemento.IdUsuario);

    $('#usuariosSelectAdm').val(elemento.IdUsuario).trigger('change');

    $("#txtActivoAdm").val(elemento.Activo);

    $("#cbActivoAdm").prop('checked', ObtenerBooleanoDeCadena(elemento.Activo));
}

function AsignarComportamientoChangePerfilesSelectAdm() {
    
    $("#perfilesSelectAdm").on("select2:selecting", function (e) {
        var IdPerfilAgregado = e.params.args.data.id;
        var IdsPaginasPorPerfilEncontrados = ObtenerIdsPaginasPorIdPerfil(IdPerfilAgregado);

        console.log("IdPerfilAgregado:" + IdPerfilAgregado);
        console.log("IdsPaginasPorPerfilEncontrados:" + IdsPaginasPorPerfilEncontrados);

        SeleccionarAutomaticamentePaginasRelacionadasAlPerfil(IdsPaginasPorPerfilEncontrados);
    });

    $("#perfilesSelectAdm").on("change.select2", function () {
        var IdsPaginasPorPerfilEncontrados = ObtenerIdsPaginasPorIdPerfil(this.value);
        SeleccionarAutomaticamentePaginasRelacionadasAlPerfil(IdsPaginasPorPerfilEncontrados);
    });
}

function RemoverPaginasRelacionadasAlPerfilRemovido(IdPerfilRemovido) {
    var IdsPaginasARemover = ObtenerIdsPaginasPorIdPerfil(IdPerfilRemovido);
    
    for (var indice = 0; indice < IdsPaginasARemover.length; indice++) {
        var opcionBuscada = $('#paginasSelectAdm option[value="' + IdsPaginasARemover[indice] + '"]');
        opcionBuscada.prop('selected', false);
    }
    $('#paginasSelectAdm').trigger('change.select2');
    
}

function AsignarComportamientoQuitarPerfilesSelectAdm() {

    $('#perfilesSelectAdm').on("select2:unselecting", function (e) {
        IdPerfilRemovido = e.params.args.data.id;
    }).trigger('change');

    $('#perfilesSelectAdm').on("select2:unselect", function (e) {
        RemoverPaginasRelacionadasAlPerfilRemovido(IdPerfilRemovido);
    }).trigger('change');
    
}

function EliminarPaginasConIndexCero(IdsPaginasPorPerfil) {
    var nuevoArreglo = [];

    for (var indice = 0; indice < IdsPaginasPorPerfil.length; indice++)
        if (IdsPaginasPorPerfil[indice] != 0)
            nuevoArreglo.push(IdsPaginasPorPerfil[indice]);

    return nuevoArreglo;
}

function SeleccionarAutomaticamentePaginasRelacionadasAlPerfil(IdsPaginasPorPerfilEncontrados) {
    var paginasSeleccionadasAnteriormente = $('#paginasSelectAdm').val();

    var todosIdsPaginasPorPerfil = [];

    todosIdsPaginasPorPerfil = paginasSeleccionadasAnteriormente.concat(IdsPaginasPorPerfilEncontrados);
    
    todosIdsPaginasPorPerfil = EliminarPaginasConIndexCero(todosIdsPaginasPorPerfil);
    
    $('#paginasSelectAdm').val(todosIdsPaginasPorPerfil).trigger('change');
}

function ObtenerIdsPaginasPorIdPerfil(idPerfilBuscado) {
    var nombrePerfilEncontrado = "";
    var IdsPaginasPorPerfilEncontrados = "";
    var perfilEncontrado = false;

    //console.log("objetoJSONPerfiles");
    //console.log(JSON.stringify(objetoJSONPerfiles));
    
    $.each(objetoJSONPerfiles, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idPerfilBuscado) {
                    nombrePerfilEncontrado = item.Nombre;
                    perfilEncontrado = true;
                }
                    

            if (perfilEncontrado)
                if (key == "IdsPaginasPorPerfil") {
                    //console.log("item.IdsPaginasPorPerfil -->" + item.IdsPaginasPorPerfil);
                    IdsPaginasPorPerfilEncontrados = item.IdsPaginasPorPerfil;
                    perfilEncontrado = false;
                }
              
        });
    });

    return IdsPaginasPorPerfilEncontrados;
}

function ObtenerTodosPerfiles(IdUsuarioTemporal) {
    var arregloPerfiles = [];

    $.each(objetoJSONPerfilesPorUsuario, function (key, value) {
        item = value;
        
        var usuarioCoincidente = false;
        
        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                if (value == IdUsuarioTemporal)
                    usuarioCoincidente = true;

            if (key == "IdPerfil")
                if (usuarioCoincidente)
                    arregloPerfiles.push(ObtenerNombrePerfilPorId(value));
            
        });
    });
    return arregloPerfiles.join(",");
}

function ObtenerTodosIdsPerfiles(IdUsuarioTemporal) {
    var arregloPerfiles = [];

    $.each(objetoJSONPerfilesPorUsuario, function (key, value) {
        item = value;

        var usuarioCoincidente = false;

        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                if (value == IdUsuarioTemporal)
                    usuarioCoincidente = true;

            if (key == "IdPerfil")
                if (usuarioCoincidente)
                    arregloPerfiles.push(value);

        });
    });
    return arregloPerfiles.join(",");
}

function ConsumirObjetoJSON() {

    var elementosTabla = [];

    var usuarioRepetido = false;
    var idUsuarioRepetido = 0;

    var IdTemporal = "";
    var IdUsuarioTemporal = "";
    var IdPaginaTemporal = "";
    var FechaCreacionTemporal = "";
    var PerfilesTemporal = "";
    var PaginasPorUsuario = "";
    var NombresPaginasPorUsuarioTemporal = "";

    $.each(objetoJSONPaginasPorUsuario, function (key, value) {
        item = value;
        
        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "IdPagina")
                IdPaginaTemporal = value;

            if (key == "FechaCreacion")
                FechaCreacionTemporal = CrearFechaFormatoDDMMYYYY(value);

            if (key == "IdUsuario") {
                IdUsuarioTemporal = value;

                //arregloPaginasTemporal.push(ObtenerNombrePaginaPorId(IdPaginaTemporal));

                if (idUsuarioRepetido != IdUsuarioTemporal) {
                    //arregloPaginasTemporal = [];

                    //listadoArregloPaginasTemporal.push(arregloPaginasTemporal.join(","));
                    
                    idUsuarioRepetido = IdUsuarioTemporal;
                    usuarioRepetido = false;
                } else {
                    usuarioRepetido = true;
                }
                

                PerfilesTemporal = ObtenerTodosPerfiles(IdUsuarioTemporal);
            }

            if (key == "PaginasPorUsuario")
                NombresPaginasPorUsuarioTemporal = value;

            if (key == "IdsPaginasPorUsuario")
                IdsPaginasPorUsuarioTemporal = value;

        });

        if (!usuarioRepetido) {
            elementosTabla.push("<tr>");
            elementosTabla.push("<td>" + IdTemporal + "</td>");
            elementosTabla.push("<td>" + ObtenerNombreCompletoUsuarioPorId(IdUsuarioTemporal) + "</td>");

            elementosTabla.push("<td>" + "<a class='enlaceListadoPaginas btn btn-primary ' href='#' data-listadoPaginas='" + NombresPaginasPorUsuarioTemporal + "'  ><i class='fa fa-eye'></i>&nbsp;&nbsp;ver páginas</a>" + "<span style='display:none;' >" + NombresPaginasPorUsuarioTemporal + "</span>" + "</td>");//IdUsuarioTemporal
            elementosTabla.push("<td>" + PerfilesTemporal + "</td>");
            elementosTabla.push("<td>" + FechaCreacionTemporal + "</td>");

            elementosTabla.push("<td>");
            elementosTabla.push("<a data-IdUsuario='" + IdUsuarioTemporal + "' data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEliminarRegistro' href='#' role='button'><i class='fa fa-trash'></i></a>");
            elementosTabla.push("&nbsp;&nbsp;&nbsp;");
            elementosTabla.push("<a data-IdsPerfiles='" + ObtenerTodosIdsPerfiles(IdUsuarioTemporal) + "' data-listadoIdsPaginas='" + IdsPaginasPorUsuarioTemporal + "' data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEditarRegistro' href='#' role='button'><i class='fa fa-pencil'></i></a>");
            elementosTabla.push("</td>");
            elementosTabla.push("</tr>");
        }
        
    });

    return elementosTabla.join("");
}

function AsignarComportamientoBotonAgregarRegistro() {
    $("#btnAgregar").on("click", function () {
        LimpiarCamposAdministracion();

        $('#perfilesSelectAdm').val(null).trigger('change');
        
        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        $("#usuariosSelectAdm").removeAttr("disabled");

        $(".OcultarPorInsercion").css("visibility", "hidden");

        MostrarBotonInsercionNuevoRegistro(true);

    });
}

function MostrarBotonInsercionNuevoRegistro(mostrar) {
    $("#btnGuardar").css("display", ObtenerArgumentoDisplayDeBoolean(!mostrar));
    $("#btnInsertar").css("display", ObtenerArgumentoDisplayDeBoolean(mostrar));
}

function AsignarComportamientoEnlaceListadoPaginas() {
    $('#table_id tbody').on('click', '.enlaceListadoPaginas', function () {
        swal({
            title: '<b>Páginas Asignadas</b>',
            type: 'info',
            html: "<b>" + $(this).attr("data-listadoPaginas") + "</b>",
            showCloseButton: true,
            focusConfirm: false,
            
        });
    });
}

function SeleccionarUnoPorUnoLosPerfilesParaAgregarPaginasCorrespondientes(arregloPerfiles) {
    for (var indice = 0; indice < arregloPerfiles.length; indice++) 
        $('#perfilesSelectAdm').val(arregloPerfiles[indice]).trigger('change');
}

function SeleccionarTodosLosPerfilesALaVezParaMostrarlos(arregloPerfiles) {
    $('#perfilesSelectAdm').val(arregloPerfiles).trigger('change');
}

function AsignarComportamientoBotonEditarRegistro() {
    $('#table_id tbody').on('click', '.btnEditarRegistro', function () {

        var cadenaPerfiles = $(this).attr("data-IdsPerfiles");
        var arregloPerfiles = cadenaPerfiles.split(",");

        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        //MostrarOcultarPanelesDeAcordeNecesidad();
        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        MostrarBotonInsercionNuevoRegistro(false);

        $(".OcultarPorInsercion").css("visibility", "visible");

        SeleccionarUnoPorUnoLosPerfilesParaAgregarPaginasCorrespondientes(arregloPerfiles);

        SeleccionarTodosLosPerfilesALaVezParaMostrarlos(arregloPerfiles);

        var listadoX = $(this).attr("data-listadoIdsPaginas");

        setTimeout(
            function () {
                $('#paginasSelectAdm').val(null).trigger('change');
                console.log("listadoX : " + listadoX);
                var listadoPaginasTemporal = [];
                eval("listadoPaginasTemporal=[" + listadoX + "];");
                $('#paginasSelectAdm').val(listadoPaginasTemporal).trigger('change');
            }, 250
        );
        
    });
}

function LimpiarCamposFiltros() {
    $("#txtId").val("");
    $("#txtIdPagina").val("");
    $("#txtIdPerfil").val("");
    $("#txtIdUsuario").val("");

    ResetearSelects();
}

function AsignarComportamientoCambioSelectPaginas() {
    console.log("478");
    AsignarComportamientoCambioSelectGeneralTexto("paginasSelect", "txtIdPagina");
    AsignarComportamientoCambioSelectGeneralId("paginasSelectAdm", "txtIdPaginaAdm");
}

function AsignarComportamientoCambioSelectPerfiles() {
    console.log("484");
    AsignarComportamientoCambioSelectGeneralTexto("perfilesSelect", "txtIdPerfil");
    AsignarComportamientoCambioSelectGeneralId("perfilesSelectAdm", "txtIdPerfilAdm");
}

function AsignarComportamientoCambioSelectUsuarios() {
    console.log("490");
    AsignarComportamientoCambioSelectGeneralTexto("usuariosSelect", "txtIdUsuario");
    AsignarComportamientoCambioSelectGeneralId("usuariosSelectAdm", "txtIdUsuarioAdm");
}

function EstablecerObjetoJSONPerfilesPorUsuario() {
    EstablecerObjetoJSONGeneral("contenedorJSONPerfilesPorUsuario", "objetoJSONPerfilesPorUsuario");
}

function EstablecerObjetoJSONPaginasPorUsuario() {
    EstablecerObjetoJSONGeneral("contenedorJSONPaginasPorUsuario", "objetoJSONPaginasPorUsuario");
}

function EstablecerObjetoJSONUsuarios() {
    EstablecerObjetoJSONGeneral("contenedorJSONUsuarios", "objetoJSONUsuarios");
}

function EstablecerObjetoJSONPerfiles() {
    EstablecerObjetoJSONGeneral("contenedorJSONPerfiles", "objetoJSONPerfiles");
}

function EstablecerObjetoJSONPaginas() {
    EstablecerObjetoJSONGeneral("contenedorJSONPaginas", "objetoJSONPaginas");
}

function BorrarRegistroSeleccionado() {
    $("#" + CONTENEDOR_ASPNET + "hidden_idTemporalPaginaPorUsuario").val(idRegistroOperacionTemporal);
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnBorrarPaginasPorUsuario").click();
        }, 500
    );
}

function ActualizarRegistroSeleccionado() {
    LlenarElementoPaginaPorUsuarioTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnActualizarPaginasPorUsuario").click();
        }, 500
    );
}

function InsertarRegistroSeleccionado() {
    idRegistroOperacionTemporal = 0;
    LlenarElementoPaginaPorUsuarioTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnInsertarPaginasPorUsuario").click();
        }, 500
    );
}

function InicializarTabla() {
    
    $('#table_id').DataTable({
        "columnDefs": [
            {
                "targets": [Columnas.Id, Columnas.FechaCreacion],
                "visible": false
            }
        ],
        "order": [[Columnas.Usuario, "asc"]]
    });
}

function ObtenerIdUsuarioParaBorrarSusRegistros(idRegistroBuscado) {
    $.each(objetoJSONPaginasPorUsuario, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    idRegistroOperacionTemporal = item.IdUsuario;

        });
    });
}

AsignarComportamientoBotonEliminarRegistro = function () {
    $('#table_id tbody').on('click', '.btnEliminarRegistro', function () {
        var idRegistroBuscado = $(this).attr("data-IdRegistro");
        ObtenerIdUsuarioParaBorrarSusRegistros(idRegistroBuscado);
        MostrarMensajeBorrado();
    });
}

function ResetearArreglosIDsSeleccionados() {
    IdsPaginasSeleccionadas = [];
    IdsPerfilesSeleccionados = [];
    IdsUsuariosSeleccionados = [];
}

function ObtenerIdsPaginasSeleccionadas() {
    console.log("ObtenerIdsPaginasSeleccionadas: " + $('#paginasSelectAdm').val());
    IdsPaginasSeleccionadas = $('#paginasSelectAdm').val();
}

function ObtenerIdsPerfilesSeleccionados() {
    console.log("ObtenerIdsPerfilesSeleccionados: " + $('#perfilesSelectAdm').val());
    IdsPerfilesSeleccionados = $('#perfilesSelectAdm').val();
}

function ObtenerIdsUsuariosSeleccionados() {
    console.log("ObtenerIdsUsuariosSeleccionados: " + $('#usuariosSelectAdm').val());
    IdsUsuariosSeleccionados = $('#usuariosSelectAdm').val();
}

function InsertarRegistrosPaginasPerfilesUsuario() {
    $("#" + CONTENEDOR_ASPNET + "hidden_IdsPaginasSeleccionadas").val(IdsPaginasSeleccionadas);
    $("#" + CONTENEDOR_ASPNET + "hidden_IdsPerfilesSeleccionados").val(IdsPerfilesSeleccionados);
    $("#" + CONTENEDOR_ASPNET + "hidden_IdsUsuariosSeleccionados").val(IdsUsuariosSeleccionados);

    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnInsertarRegistrosPaginasPerfilesUsuario").click();
        }, 500
    );
}

function ActualizarGuardarRegistros() {
    ValidarCamposInformacion();

    ResetearArreglosIDsSeleccionados();
    
    ObtenerIdsPaginasSeleccionadas();
    ObtenerIdsPerfilesSeleccionados();
    ObtenerIdsUsuariosSeleccionados();
    
    InsertarRegistrosPaginasPerfilesUsuario();
    
    MostrarAlertaExitosa();
}

AsignarComportamientoBotonInsertarRegistro = function () {
    $("#btnInsertar").on("click", function () {
        ActualizarGuardarRegistros();
    });
}

AsignarComportamientoBotonGuardarRegistro = function () {
    $("#btnGuardar").on("click", function () {
        ActualizarGuardarRegistros();
    });
}

function ProcesosUnaSolaVezPorPantalla() {
    
    InicializarBusquedaAjenaTabla();
    LlenarSelectPaginas();
    LlenarSelectPerfiles();
    LlenarSelectUsuarios();
    InicializarSelect();

    AsignarComportamientoCambioSelectPaginas();
    AsignarComportamientoCambioSelectPerfiles();
    AsignarComportamientoCambioSelectUsuarios();

    LimpiarElementoPaginaPorUsuarioTemporal();

    

    AsignarComportamientoChangePerfilesSelectAdm();
    AsignarComportamientoQuitarPerfilesSelectAdm();
}

function TodosProcesosPorPantalla() {
    EstablecerObjetoJSONPaginasPorUsuario();
    EstablecerObjetoJSONPerfilesPorUsuario();
    EstablecerObjetoJSONUsuarios();
    EstablecerObjetoJSONPerfiles();
    EstablecerObjetoJSONPaginas();

    ReiniciarTablaDatos();

    LlenarDatosTabla();
    InicializarTabla();
    
    OcultarFiltradorTabla();
    MostrarOcultarSeccionAdministraDatos();

    AsegurarMuestroCorrectoSeccionAdministraDatos();

    AsignarComportamientoEnlaceListadoPaginas();
    AsignarComportamientoBotonAgregarRegistro();
    AsignarComportamientoBotonCancelarAgregacionRegistro();
    AsignarComportamientoBotonGuardarRegistro();
    AsignarComportamientoBotonInsertarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
    AsignarComportamientoBotonEditarRegistro();
    AsignarComportamientoBotonLimpiarFiltros();
}

$(document).ready(function () {
    TodosProcesosPorPantalla();
    ProcesosUnaSolaVezPorPantalla();
});