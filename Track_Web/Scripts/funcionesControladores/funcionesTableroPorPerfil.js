﻿var Columnas =
    {
        Id: 0,
        Tablero: 1,
        Perfil: 2,
        Fecha_Creacion: 3,
        Usuario_Creo: 4,
        Activo: 5,
        Acciones: 6
    };

var elementoTableroPorPerfilTemporal = new Object();

var objetoJSONTablerosPorPerfil = new Object();

var objetoJSONTableros = new Object();

var objetoJSONPerfiles = new Object();

var objetoJSONUsuarios = new Object();

var idRegistroOperacionTemporal = "";

var filtroId = "";

var filtroIdTablero = "";

var arregloCamposFiltros =
    [
        "txtId",
        "txtIdTablero",
        "txtIdPerfil",
        "txtFechaCreacion",
        "txtIdUsuarioCreo"
    ];

var arregloCamposValidacion =
    [
        "txtIdTableroAdm",
        "txtIdPerfilAdm",
        "txtFechaCreacionAdm",
        "txtIdUsuarioCreoAdm",
        "txtActivoAdm",
    ];

function LimpiarElementoTableroPorPerfilTemporal() {
    elementoTableroPorPerfilTemporal = {
        "Id": "0",
        "IdTablero": "0",
        "IdPerfil": "0",
        "FechaCreacion": "01/01/0001",
        "IdUsuarioCreo": "0",
        "Activo": "False"
    };
}
var arregloImagenesTablero = [];

function LlenarArregloImagenesTablero() {
    /*
    arregloImagenesTablero.push("https://picsum.photos/600/350/?random");
    arregloImagenesTablero.push("https://picsum.photos/601/351/?random");
    arregloImagenesTablero.push("https://picsum.photos/602/352/?random");
    
    */
    arregloImagenesTablero.push("Images/panel1.jpg");
    arregloImagenesTablero.push("Images/panel2.jpg");
    //arregloImagenesTablero.push("Images/alert_black_24x24.png");
}

function LlenarArregloImagenesTablero2() {
    
    arregloImagenesTablero.push("https://picsum.photos/600/350/?random");
    arregloImagenesTablero.push("https://picsum.photos/601/351/?random");
    arregloImagenesTablero.push("https://picsum.photos/602/352/?random");
   
}

function ObtenerURLImagenTablero(indice) {
    return arregloImagenesTablero[indice];
}

function CrearContenedorImagenTablero(indice) {
    var imagenTablerosHTML = [];

    imagenTablerosHTML.push('<div>');
    imagenTablerosHTML.push('<img data-u="image" src="' + ObtenerURLImagenTablero(indice) + '" />');
    imagenTablerosHTML.push('</div>');

    return imagenTablerosHTML.join("");
}

function LlenarContenedorImagenesTableros() {
    var imagenesTablerosHTML = [];

    for (var indice = 0; indice < arregloImagenesTablero.length; indice++)
        imagenesTablerosHTML.push(CrearContenedorImagenTablero(indice));

    $("#contenedorImagenesTableros").html(imagenesTablerosHTML.join(""));

    //inicializarGaleria();
}

function LlenarContenedorImagenesTableros2() {
    $("#contenedorImagenesTableros").html("");

    arregloImagenesTablero = [];
    LlenarArregloImagenesTablero2();

    var imagenesTablerosHTML = [];

    for (var indice = 0; indice < arregloImagenesTablero.length; indice++)
        imagenesTablerosHTML.push(CrearContenedorImagenTablero(indice));

    $("#contenedorImagenesTableros").html(imagenesTablerosHTML.join(""));
}

function EstablecerTituloTablero() {
    $("#tituloTablero").html('Tablero 1 a');
}

function EstablecerEnlaceTablero() {
    $("#enlaceTablero").html('https://espanol.yahoo.com/a');
}

function InicializarMuestrarioTablero() {
    //LlenarContenedorImagenesTableros();
    EstablecerTituloTablero();
    EstablecerEnlaceTablero();
}

function LlenarElementoTableroPorPerfilTemporal() {
    console.log("txtIdTableroAdm: " + $("#txtIdTableroAdm").val());
    console.log("txtIdPerfilAdm: " + $("#txtIdPerfilAdm").val());

    $("#txtFechaCreacionAdm").val($("#DatePickerFechaCreacionAdm").val());

    console.log("txtFechaCreacionAdm: " + $("#txtFechaCreacionAdm").val());
    console.log("txtIdUsuarioCreoAdm: " + $("#txtIdUsuarioCreoAdm").val());
    console.log("txtActivoAdm: " + $("#txtActivoAdm").val());
    /*
     "txtIdTableroAdm",
        "txtIdPerfilAdm",
        "txtFechaCreacionAdm",
        "txtIdUsuarioCreoAdm",
        "txtActivoAdm"
     */


    ValidarCamposInformacion();

    elementoTableroPorPerfilTemporal = {
        "Id": idRegistroOperacionTemporal,
        "IdTablero": $("#txtIdTableroAdm").val(),
        "IdPerfil": $("#txtIdPerfilAdm").val(),
        "FechaCreacion": convertirFechaAFormatoMMDDYYYY($("#txtFechaCreacionAdm").val()),
        "IdUsuarioCreo": $("#txtIdUsuarioCreoAdm").val(),
        "Activo": $("#txtActivoAdm").val()
    };

    $("#" + CONTENEDOR_ASPNET + "hidden_JSONTableroPorPerfilTemporal").val(JSON.stringify(elementoTableroPorPerfilTemporal));
}

function LlenarSelectUsuarios() {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        var ApellidosTemporal = "";
        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                IdTemporal = value;

            if (key == "Nombre")
                NombreTemporal = value;

            if (key == "Apellidos")
                ApellidosTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + " " + ApellidosTemporal + "</option>");

    });

    $("#usuariosSelect").html(elementosSelectGeneral.join(""));
    $("#usuariosSelectAdm").html(elementosSelectGeneral.join(""));
}

function LlenarSelectTableros() {
    LlenarSelectTablerosGeneral(objetoJSONTableros, "TablerosSelect");
    LlenarSelectTablerosGeneral(objetoJSONTableros, "TablerosSelectAdm");
}

function LlenarSelectTablerosGeneral(fuenteJSON, nombreSelect) {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(fuenteJSON, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "NombreCorto")
                NombreTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + "</option>");

    });

    $("#" + nombreSelect).html(elementosSelectGeneral.join(""));
}

function LlenarSelectPerfiles() {
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelect");
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelectAdm");
}

function ObtenerNombreTableroPorId(idTableroBuscado) {
    return ObtieneDatoPorId(idTableroBuscado, objetoJSONTableros, "item.NombreCorto", "Id");
}

function ObtenerNombrePerfilPorId(idPerfilBuscado) {
    return ObtieneDatoPorId(idPerfilBuscado, objetoJSONPerfiles, "item.Nombre", "Id");
}

function ObtenerNombreCompletoUsuarioPorId(idUsuarioBuscado) {
    return ObtieneDatoPorId(idUsuarioBuscado, objetoJSONUsuarios, "item.Nombre +' ' + item.Apellidos", "IdUsuario");
}

function ObtenerDatosPorId(idRegistroBuscado) {
    $.each(objetoJSONTablerosPorPerfil, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    ObtenerRegistroCompleto(item);

        });
    });
}

function LimpiarCamposAdministracion() {

    $("#txtIdAdm").val("");
    $("#txtIdTableroAdm").val("");
    $("#txtIdPerfilAdm").val("");
    $("#txtFechaCreacionAdm").val("");
    $("#txtIdUsuarioCreoAdm").val("");
    $("#txtActivoAdm").val("");

    $("#DatePickerFechaCreacionAdm").val("");

    ResetearSelects();
}

function ObtenerRegistroCompleto(elemento) {
    idRegistroOperacionTemporal = elemento.Id;

    $("#txtIdAdm").val(elemento.Id);
    $("#txtIdTableroAdm").val(elemento.IdTablero);

    $('#TablerosSelectAdm').val(elemento.IdTablero).trigger('change');

    $("#txtIdPerfilAdm").val(elemento.IdPerfil);

    $('#perfilesSelectAdm').val(elemento.IdPerfil).trigger('change');

    $("#txtFechaCreacionAdm").val(elemento.FechaCreacion);
    $("#DatePickerFechaCreacionAdm").val(elemento.FechaCreacion);
    $("#txtIdUsuarioCreoAdm").val(elemento.IdUsuarioCreo);

    $('#usuariosSelectAdm').val(elemento.IdUsuarioCreo).trigger('change');

    $("#txtActivoAdm").val(elemento.Activo);

    $("#cbActivoAdm").prop('checked', ObtenerBooleanoDeCadena(elemento.Activo));
}

function ConsumirObjetoJSON() {

    var elementosTabla = [];

    $.each(objetoJSONTablerosPorPerfil, function (key, value) {
        item = value;

        elementosTabla.push("<tr>");

        var IdTemporal = "";
        var IdTableroTemporal = "";
        var IdPerfilTemporal = "";
        var FechaCreacionTemporal = "";
        var IdUsuarioCreoTemporal = "";
        var ActivoTemporal = "";

        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "IdTablero")
                IdTableroTemporal = value;

            if (key == "IdPerfil")
                IdPerfilTemporal = value;

            if (key == "FechaCreacion")
                FechaCreacionTemporal = CrearFechaFormatoDDMMYYYY(value);

            if (key == "IdUsuarioCreo")
                IdUsuarioCreoTemporal = value;

            if (key == "Activo")
                ActivoTemporal = value;

        });

        console.log("IdTableroTemporal: " + IdTableroTemporal );

        elementosTabla.push("<td>" + IdTemporal + "</td>");
        elementosTabla.push("<td>" + ObtenerNombreTableroPorId(IdTableroTemporal) + "</td>");
        elementosTabla.push("<td>" + ObtenerNombrePerfilPorId(IdPerfilTemporal) + "</td>");
        elementosTabla.push("<td>" + FechaCreacionTemporal + "</td>");
        elementosTabla.push("<td>" + ObtenerNombreCompletoUsuarioPorId(IdUsuarioCreoTemporal) + "</td>");
        elementosTabla.push("<td>" + ActivoTemporal + "</td>");
        elementosTabla.push("<td>");
        elementosTabla.push("<a data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEliminarRegistro' href='#' role='button'><i class='fa fa-trash'></i></a>");
        elementosTabla.push("&nbsp;&nbsp;&nbsp;");
        elementosTabla.push("<a data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEditarRegistro' href='#' role='button'><i class='fa fa-pencil'></i></a>");
        elementosTabla.push("</td>");
        elementosTabla.push("</tr>");

    });

    return elementosTabla.join("");
}

function asignarFechaPorDefectoHoy() {
    $("#DatePickerFechaCreacionAdm").datepicker("setDate", new Date());

    console.log("convertirFechaAFormatoMMDDYYYY: " + convertirFechaAFormatoMMDDYYYY(new Date()));

    $("#txtFechaCreacionAdm").val(convertirFechaAFormatoMMDDYYYY(new Date()));
}

function AsignarComportamientoBotonAgregarRegistro() {
    $("#btnAgregar").on("click", function () {
        LimpiarCamposAdministracion();

        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        $("#perfilesSelectAdm").removeAttr("disabled");
        $("#DatePickerFechaCreacionAdm").removeAttr("disabled");

        $(".OcultarPorInsercion").css("visibility", "hidden");

        asignarFechaPorDefectoHoy();

        MostrarBotonInsercionNuevoRegistro(true);

    });
}

function AsignarComportamientoBotonEditarRegistro() {

    $('#table_id tbody').on('click', '.btnEditarRegistro', function () {
        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        //MostrarOcultarPanelesDeAcordeNecesidad();
        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        MostrarBotonInsercionNuevoRegistro(false);
        $("#perfilesSelectAdm").attr("disabled", "disabled");
        $("#DatePickerFechaCreacionAdm").attr("disabled", "disabled");
        $(".OcultarPorInsercion").css("visibility", "visible");
    });
}

function LimpiarCamposFiltros() {
    $("#txtId").val("");
    $("#txtIdTablero").val("");
    $("#txtIdPerfil").val("");
    $("#txtFechaCreacion").val("");
    $("#txtIdUsuarioCreo").val("");
    $("#txtActivo").val("");

    $("#DatePickerFechaCreacion").val("");

    ResetearSelects();
}

function AsignarComportamientoCambioSelectTableros() {
    AsignarComportamientoCambioSelectGeneralTexto("TablerosSelect", "txtIdTablero");
    AsignarComportamientoCambioSelectGeneralId("TablerosSelectAdm", "txtIdTableroAdm");

    AsignarComportamientoCambioSelectGeneralHTML("TablerosSelectAdm", "tituloTablero", "text");
    AsignarComportamientoCambioSelectGeneralHTML("TablerosSelectAdm", "enlaceTablero", "url");

    AsignarComportamientoCambioSelectGaleria("TablerosSelectAdm", "contenedorImagenesTableros");
}

function AsignarComportamientoCambioSelectPerfiles() {
    AsignarComportamientoCambioSelectGeneralTexto("perfilesSelect", "txtIdPerfil");
    AsignarComportamientoCambioSelectGeneralId("perfilesSelectAdm", "txtIdPerfilAdm");
}

function AsignarComportamientoCambioSelectUsuarios() {
    AsignarComportamientoCambioSelectGeneralTexto("usuariosSelect", "txtIdUsuarioCreo");
    AsignarComportamientoCambioSelectGeneralId("usuariosSelectAdm", "txtIdUsuarioCreoAdm");
}

function AsignarComportamientoCambioCheckBoxGeneral(nombreCampo) {
    $('#' + nombreCampo).on('change', function () {
        $("#txtActivoAdm").val(ObtenerValorActivo($(this)));
    });
}

function EstablecerObjetoJSONTablerosPorPerfil() {
    EstablecerObjetoJSONGeneral("contenedorJSONTablerosPorPerfil", "objetoJSONTablerosPorPerfil");
}

function EstablecerObjetoJSONUsuarios() {
    EstablecerObjetoJSONGeneral("contenedorJSONUsuarios", "objetoJSONUsuarios");
}

function EstablecerObjetoJSONPerfiles() {
    EstablecerObjetoJSONGeneral("contenedorJSONPerfiles", "objetoJSONPerfiles");
}

function EstablecerObjetoJSONTableros() {
    EstablecerObjetoJSONGeneral("contenedorJSONTableros", "objetoJSONTableros");
}
//funciones ejecutan procesos
function BorrarRegistroSeleccionado() {
    $("#" + CONTENEDOR_ASPNET + "hidden_idTemporalTableroPorPerfil").val(idRegistroOperacionTemporal);
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnBorrarTableroPorPerfil").click();
        }, 500
    );

}

function ActualizarRegistroSeleccionado() {
    LlenarElementoTableroPorPerfilTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnActualizarTableroPorPerfil").click();
        }, 500
    );
}

function InsertarRegistroSeleccionado() {
    idRegistroOperacionTemporal = 0;
    $("#txtIdUsuarioCreoAdm").val(idUsuarioLogueado);//poner el id del usuario de la sesion actual
    $("#txtActivoAdm").val("True");
    LlenarElementoTableroPorPerfilTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnInsertarTableroPorPerfil").click();
        }, 500
    );
}
//fin de funciones ejecutan procesos

function ProcesosUnaSolaVezPorPantalla() {


    InicializarBusquedaAjenaTabla();
    LlenarSelectTableros();
    LlenarSelectPerfiles();
    LlenarSelectUsuarios();
    InicializarSelect();

    AsignarComportamientoCambioSelectTableros();
    AsignarComportamientoCambioSelectPerfiles();
    AsignarComportamientoCambioSelectUsuarios();

    InicializarSelectoresFecha();

    LimpiarElementoTableroPorPerfilTemporal();
}

function TodosProcesosPorPantalla() {
    EstablecerObjetoJSONTablerosPorPerfil();
    EstablecerObjetoJSONUsuarios();
    EstablecerObjetoJSONPerfiles();
    EstablecerObjetoJSONTableros();

    AsignarComportamientoCambioCheckBoxGeneral("cbActivoAdm");

    ReiniciarTablaDatos();

    LlenarDatosTabla();
    InicializarTabla();



    OcultarFiltradorTabla();
    MostrarOcultarSeccionAdministraDatos();

    AsegurarMuestroCorrectoSeccionAdministraDatos();

    AsignarComportamientoBotonAgregarRegistro();
    AsignarComportamientoBotonCancelarAgregacionRegistro();
    AsignarComportamientoBotonGuardarRegistro();
    AsignarComportamientoBotonInsertarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
    AsignarComportamientoBotonEditarRegistro();
    AsignarComportamientoBotonLimpiarFiltros();

    LlenarArregloImagenesTablero();
    InicializarMuestrarioTablero();
}

var arregloImagenesCookie = [];
var arregloImagenesJSON = null;

function AlimentarArregloImagenesCookie(arregloImagenesRecibido) {
    arregloImagenesCookie = arregloImagenesRecibido;
}

function CreacionArregloImagenesJSON() {
    arregloImagenesJSON = JSON.stringify(arregloImagenesCookie);
}

function AsignacionCookie() {
    $.cookie('arregloImagenesCookie', arregloImagenesJSON);
}

function CargarIframeImagenesPorTablero() {
    $("#IframeImagenesPorTablero").attr("src", "ImagenesPorTablero.aspx");
}

var AlimentarCargarImagenesPorTablero = function (arregloImagenesRecibido) {
    AlimentarArregloImagenesCookie(arregloImagenesRecibido);
    CreacionArregloImagenesJSON();
    AsignacionCookie();
    CargarIframeImagenesPorTablero();
}

$(document).ready(function () {
    TodosProcesosPorPantalla();
    ProcesosUnaSolaVezPorPantalla();
});