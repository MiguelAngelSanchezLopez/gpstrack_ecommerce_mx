﻿var Columnas =
    {
        Id: 0,
        Pagina: 1,
        Perfil: 2,
        Fecha_Creacion: 3,
        Usuario_Creo: 4,
        Activo: 5,
        Acciones: 6
    };

var elementoPaginaPorPerfilTemporal = new Object();

var objetoJSONPaginasPorPerfil = new Object();

var objetoJSONPaginas = new Object();

var objetoJSONPerfiles = new Object();

var objetoJSONUsuarios = new Object();

var idRegistroOperacionTemporal = "";

var filtroId = "";

var filtroIdPagina = "";

var arregloCamposFiltros =
    [
        "txtId",
        "txtIdPagina",
        "txtIdPerfil",
        "txtFechaCreacion",
        "txtIdUsuarioCreo"
    ];

var arregloCamposValidacion =
    [
        "txtIdPaginaAdm",
        "txtIdPerfilAdm",
        "txtFechaCreacionAdm",
        "txtIdUsuarioCreoAdm",
        "txtActivoAdm",
    ];

function LimpiarElementoPaginaPorPerfilTemporal() {
    elementoPaginaPorPerfilTemporal = {
        "Id": "0",
        "IdPagina": "0",
        "IdPerfil": "0",
        "FechaCreacion": "01/01/0001",
        "IdUsuarioCreo": "0",
        "Activo": "False"
    };
}

function LlenarElementoPaginaPorPerfilTemporal() {
    ValidarCamposInformacion();

    elementoPaginaPorPerfilTemporal = {
        "Id": idRegistroOperacionTemporal,
        "IdPagina": $("#txtIdPaginaAdm").val(),
        "IdPerfil": $("#txtIdPerfilAdm").val(),
        "FechaCreacion": convertirFechaAFormatoMMDDYYYY($("#txtFechaCreacionAdm").val()),
        "IdUsuarioCreo": $("#txtIdUsuarioCreoAdm").val(),
        "Activo": $("#txtActivoAdm").val()
    };

    $("#" + CONTENEDOR_ASPNET + "hidden_JSONPaginaPorPerfilTemporal").val(JSON.stringify(elementoPaginaPorPerfilTemporal));
}

function LlenarSelectUsuarios() {
    var elementosSelectGeneral = [];

    elementosSelectGeneral.push("<option value='0'>SELECCIONE UN VALOR</option>");

    $.each(objetoJSONUsuarios, function (key, value) {
        item = value;

        elementosSelectGeneral.push("<option");
        var IdTemporal = "";
        var NombreTemporal = "";
        var ApellidosTemporal = "";
        $.each(item, function (key, value) {

            if (key == "IdUsuario")
                IdTemporal = value;

            if (key == "Nombre")
                NombreTemporal = value;

            if (key == "Apellidos")
                ApellidosTemporal = value;

        });

        elementosSelectGeneral.push(" value='" + IdTemporal + "'>" + NombreTemporal + " " + ApellidosTemporal + "</option>");

    });

    $("#usuariosSelect").html(elementosSelectGeneral.join(""));
    $("#usuariosSelectAdm").html(elementosSelectGeneral.join(""));
}

function LlenarSelectPaginas() {
    LlenarSelectGeneral(objetoJSONPaginas, "paginasSelect");
    LlenarSelectGeneral(objetoJSONPaginas, "paginasSelectAdm");
}

function LlenarSelectPerfiles() {
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelect");
    LlenarSelectGeneral(objetoJSONPerfiles, "perfilesSelectAdm");
}

function ObtenerNombrePaginaPorId(idPaginaBuscado) {
    return ObtieneDatoPorId(idPaginaBuscado, objetoJSONPaginas, "item.Nombre", "Id");
}

function ObtenerNombrePerfilPorId(idPerfilBuscado) {
    return ObtieneDatoPorId(idPerfilBuscado, objetoJSONPerfiles, "item.Nombre","Id");
}

function ObtenerNombreCompletoUsuarioPorId(idUsuarioBuscado) {
    return ObtieneDatoPorId(idUsuarioBuscado, objetoJSONUsuarios, "item.Nombre +' ' + item.Apellidos", "IdUsuario");
}

function ObtenerDatosPorId(idRegistroBuscado) {
    $.each(objetoJSONPaginasPorPerfil, function (key, value) {
        item = value;
        $.each(item, function (key, value) {

            if (key == "Id")
                if (value == idRegistroBuscado)
                    ObtenerRegistroCompleto(item);

        });
    });
}

function LimpiarCamposAdministracion() {
    
    $("#txtIdAdm").val("");
    $("#txtIdPaginaAdm").val("");
    $("#txtIdPerfilAdm").val("");
    $("#txtFechaCreacionAdm").val("");
    $("#txtIdUsuarioCreoAdm").val("");
    $("#txtActivoAdm").val("");

    $("#DatePickerFechaCreacionAdm").val("");
    
    ResetearSelects();
}

function ObtenerRegistroCompleto(elemento) {
    idRegistroOperacionTemporal = elemento.Id;

    $("#txtIdAdm").val(elemento.Id);
    $("#txtIdPaginaAdm").val(elemento.IdPagina);
    
    $('#paginasSelectAdm').val(elemento.IdPagina).trigger('change');

    $("#txtIdPerfilAdm").val(elemento.IdPerfil);

    $('#perfilesSelectAdm').val(elemento.IdPerfil).trigger('change');

    $("#txtFechaCreacionAdm").val(elemento.FechaCreacion);
    $("#DatePickerFechaCreacionAdm").val(elemento.FechaCreacion);
    $("#txtIdUsuarioCreoAdm").val(elemento.IdUsuarioCreo);

    $('#usuariosSelectAdm').val(elemento.IdUsuarioCreo).trigger('change');

    $("#txtActivoAdm").val(elemento.Activo);
    
    $("#cbActivoAdm").prop('checked', ObtenerBooleanoDeCadena(elemento.Activo));
}

function ConsumirObjetoJSON() {

    var elementosTabla = [];

    $.each(objetoJSONPaginasPorPerfil, function (key, value) {
        item = value;

        elementosTabla.push("<tr>");

        var IdTemporal = "";
        var IdPaginaTemporal = "";
        var IdPerfilTemporal = "";
        var FechaCreacionTemporal = "";
        var IdUsuarioCreoTemporal = "";
        var ActivoTemporal = "";
        
        $.each(item, function (key, value) {

            if (key == "Id")
                IdTemporal = value;

            if (key == "IdPagina")
                IdPaginaTemporal = value;

            if (key == "IdPerfil")
                IdPerfilTemporal = value;

            if (key == "FechaCreacion")
                FechaCreacionTemporal = CrearFechaFormatoDDMMYYYY(value);

            if (key == "IdUsuarioCreo")
                IdUsuarioCreoTemporal = value;

            if (key == "Activo")
                ActivoTemporal = value;

        });

        elementosTabla.push("<td>" + IdTemporal + "</td>");
        elementosTabla.push("<td>" + ObtenerNombrePaginaPorId(IdPaginaTemporal)  + "</td>");
        elementosTabla.push("<td>" + ObtenerNombrePerfilPorId(IdPerfilTemporal) + "</td>");
        elementosTabla.push("<td>" + FechaCreacionTemporal + "</td>");
        elementosTabla.push("<td>" + ObtenerNombreCompletoUsuarioPorId(IdUsuarioCreoTemporal) + "</td>");
        elementosTabla.push("<td>" + ActivoTemporal + "</td>");
        elementosTabla.push("<td>");
        elementosTabla.push("<a data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEliminarRegistro' href='#' role='button'><i class='fa fa-trash'></i></a>");
        elementosTabla.push("&nbsp;&nbsp;&nbsp;");
        elementosTabla.push("<a data-IdRegistro='" + IdTemporal + "' class='btn btn-primary btnEditarRegistro' href='#' role='button'><i class='fa fa-pencil'></i></a>");
        elementosTabla.push("</td>");
        elementosTabla.push("</tr>");

    });

    return elementosTabla.join("");
}

function AsignarComportamientoBotonAgregarRegistro() {
    $("#btnAgregar").on("click", function () {
        LimpiarCamposAdministracion();

        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        $("#perfilesSelectAdm").removeAttr("disabled");
        $("#DatePickerFechaCreacionAdm").removeAttr("disabled");

        $(".OcultarPorInsercion").css("visibility", "hidden");

        MostrarBotonInsercionNuevoRegistro(true);

    });
}

function AsignarComportamientoBotonEditarRegistro() {

    $('#table_id tbody').on('click', '.btnEditarRegistro', function () {
        ObtenerDatosPorId($(this).attr("data-IdRegistro"));
        //MostrarOcultarPanelesDeAcordeNecesidad();
        $("#seccionAdministraDatos").show();
        $("#seccionFiltros").hide();
        $("#seccionConsultaDatos").hide();

        MostrarBotonInsercionNuevoRegistro(false);
        $("#perfilesSelectAdm").attr("disabled", "disabled");
        $("#DatePickerFechaCreacionAdm").attr("disabled", "disabled");
        $(".OcultarPorInsercion").css("visibility", "visible");
    });
}

function LimpiarCamposFiltros() {
    $("#txtId").val("");
    $("#txtIdPagina").val("");
    $("#txtIdPerfil").val("");
    $("#txtFechaCreacion").val("");
    $("#txtIdUsuarioCreo").val("");
    $("#txtActivo").val("");

    $("#DatePickerFechaCreacion").val("");

    ResetearSelects();
}

function AsignarComportamientoCambioSelectPaginas() {
    AsignarComportamientoCambioSelectGeneralTexto("paginasSelect", "txtIdPagina");
    AsignarComportamientoCambioSelectGeneralId("paginasSelectAdm", "txtIdPaginaAdm");
}

function AsignarComportamientoCambioSelectPerfiles() {
    AsignarComportamientoCambioSelectGeneralTexto("perfilesSelect", "txtIdPerfil");
    AsignarComportamientoCambioSelectGeneralId("perfilesSelectAdm", "txtIdPerfilAdm");
}

function AsignarComportamientoCambioSelectUsuarios() {
    AsignarComportamientoCambioSelectGeneralTexto("usuariosSelect", "txtIdUsuarioCreo");
    AsignarComportamientoCambioSelectGeneralId("usuariosSelectAdm", "txtIdUsuarioCreoAdm");
}

function AsignarComportamientoCambioCheckBoxGeneral(nombreCampo) {
    $('#' + nombreCampo).on('change', function () {
        $("#txtActivoAdm").val(ObtenerValorActivo($(this)));
    });
}

function EstablecerObjetoJSONPaginasPorPerfil() {
    EstablecerObjetoJSONGeneral("contenedorJSONPaginasPorPerfil", "objetoJSONPaginasPorPerfil");
}

function EstablecerObjetoJSONUsuarios() {
    EstablecerObjetoJSONGeneral("contenedorJSONUsuarios", "objetoJSONUsuarios");
}

function EstablecerObjetoJSONPerfiles() {
    EstablecerObjetoJSONGeneral("contenedorJSONPerfiles", "objetoJSONPerfiles");
}

function EstablecerObjetoJSONPaginas() {
    EstablecerObjetoJSONGeneral("contenedorJSONPaginas", "objetoJSONPaginas");
}
//funciones ejecutan procesos
function BorrarRegistroSeleccionado() {
    $("#" + CONTENEDOR_ASPNET + "hidden_idTemporalPaginaPorPerfil").val(idRegistroOperacionTemporal);
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnBorrarPaginaPorPerfil").click();
        }, 500
    );

}

function ActualizarRegistroSeleccionado() {
    LlenarElementoPaginaPorPerfilTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnActualizarPaginaPorPerfil").click();
        }, 500
    );
}

function InsertarRegistroSeleccionado() {
    idRegistroOperacionTemporal = 0;
    $("#txtIdUsuarioCreoAdm").val(idUsuarioLogueado);//poner el id del usuario de la sesion actual
    $("#txtActivoAdm").val("True");
    LlenarElementoPaginaPorPerfilTemporal();
    setTimeout(
        function () {
            $("#" + CONTENEDOR_ASPNET + "btnInsertarPaginaPorPerfil").click();
        }, 500
    );
}
//fin de funciones ejecutan procesos

function ProcesosUnaSolaVezPorPantalla() {

    
    InicializarBusquedaAjenaTabla();
    LlenarSelectPaginas();
    LlenarSelectPerfiles();
    LlenarSelectUsuarios();
    InicializarSelect();

    AsignarComportamientoCambioSelectPaginas();
    AsignarComportamientoCambioSelectPerfiles();
    AsignarComportamientoCambioSelectUsuarios();

    InicializarSelectoresFecha();

    LimpiarElementoPaginaPorPerfilTemporal();
}

function TodosProcesosPorPantalla() {
    EstablecerObjetoJSONPaginasPorPerfil();
    EstablecerObjetoJSONUsuarios();
    EstablecerObjetoJSONPerfiles();
    EstablecerObjetoJSONPaginas();

    AsignarComportamientoCambioCheckBoxGeneral("cbActivoAdm");

    ReiniciarTablaDatos();

    LlenarDatosTabla();
    InicializarTabla();

    

    OcultarFiltradorTabla();
    MostrarOcultarSeccionAdministraDatos();

    AsegurarMuestroCorrectoSeccionAdministraDatos();

    AsignarComportamientoBotonAgregarRegistro();
    AsignarComportamientoBotonCancelarAgregacionRegistro();
    AsignarComportamientoBotonGuardarRegistro();
    AsignarComportamientoBotonInsertarRegistro();
    AsignarComportamientoBotonEliminarRegistro();
    AsignarComportamientoBotonEditarRegistro();
    AsignarComportamientoBotonLimpiarFiltros();

}

$(document).ready(function () {
    TodosProcesosPorPantalla();
    ProcesosUnaSolaVezPorPantalla();
});