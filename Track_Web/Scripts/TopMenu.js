﻿var imageMenu = {
    id: 'imageMenu',
    xtype: 'image',
    src: 'Images/logo_transparent_80x37.png',
    height: 37,
    width: 80
};

var imgCliente = {
    id: 'imgCliente',
    xtype: 'image',
    src: 'Images/logo_98x31.png',
    height: 31,
    width: 135
}

var menuViajes = Ext.create('Ext.menu.Menu', {
    id: 'menuViajes'
});

var agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = null;

var btnMenuViajes = {
    id: 'btnMenuViajes',
    xtype: 'button',
    text: 'Viajes',
    iconAlign: 'left',
    icon: 'Images/truck_gray_24x24.png',
    height: 35,
    width: 100,
    menu: menuViajes
};

var menuGPS = Ext.create('Ext.menu.Menu', {
    id: 'menuGPS'
});

var btnMenuGPS = {
    id: 'btnMenuGPS',
    xtype: 'button',
    text: 'GPS',
    iconAlign: 'left',
    icon: 'Images/gps_gray_24x24.png',
    height: 35,
    width: 90,
    menu: menuGPS
};

var menuReportes = Ext.create('Ext.menu.Menu', {
    id: 'menuReportes'
});

var btnMenuReportes = {
    id: 'btnMenuReportes',
    xtype: 'button',
    text: 'Reportes',
    iconAlign: 'left',
    icon: 'Images/reports_gray_24x24.png',
    height: 35,
    width: 100,
    menu: menuReportes
};

var menuConfiguracion = Ext.create('Ext.menu.Menu', {
    id: 'menuConfiguracion'
});

var btnMenuConfig = {
    id: 'btnMenuConfig',
    xtype: 'button',
    text: 'Configuración',
    iconAlign: 'left',
    icon: 'Images/gear_white_24x24.png',
    height: 35,
    width: 120,
    menu: menuConfiguracion
};

var btnLogout = {
    id: 'btnLogout',
    xtype: 'button',
    text: 'Cerrar Sesión',
    iconAlign: 'left',
    height: 35,
    width: 120,
    icon: 'Images/logout_grey_24x24.png',
    handler: function () {
        window.location = "Login.aspx";
    }
};

var menuPaneles = Ext.create('Ext.menu.Menu', {
    id: 'menuPaneles'
});

agregacionTablerosDinamica();

//fin de hacer esta seccion dinamica
var btnMenuPaneles = {
    id: 'btnMenuPaneles',
    xtype: 'button',
    text: 'Paneles',
    iconAlign: 'left',
    icon: 'Images/control_black_24x24.png',
    height: 35,
    width: 120,
    menu: menuPaneles
};

var imageContacto = {
    id: 'imageContacto',
    xtype: 'image',
    src: 'Images/contacto.png',
    height: 37,
    width: 230
};

var viewWidth = Ext.getBody().getViewSize().width;

var listaPaginasBtnVisualizador = [
    "Visualizador.aspx"
];

var listaPaginasBtnMenuViajes = [
    "DashboardControl.aspx",
    "ViajesRuta.aspx",
    "ViajesBackhaul.aspx",
    "ConfigViajes.aspx",
    "ConfigViajes_v2.aspx"
];

var listaPaginasBtnMenuGPS = [
    "MonitoreoOnline.aspx",
    "PosicionesGPS.aspx",
    "ReportabilidadGPSTrack.aspx",
    "LastPositions.aspx"
];

var listaPaginasBtnMenuReportes = [
    "InformeViajes.aspx",
    "Rpt_KmsRecorridos.aspx",
    "Rpt_Alertas.aspx",
    "ReporteEstadiaTractos.aspx",
    "ReporteEstadiaTractos_Historico.aspx",
    "ReporteEstadiaRemolques.aspx",
    "ReporteEstadiaRemolques_Historico.aspx",
    "ReporteComercial.aspx"
];

var listaPaginasBtnMenuConfig = [
    "ConfigZonas.aspx",
    "ConfigUsuarios.aspx",
    "PaginasPorPerfil.aspx",
    "PaginasPorUsuario.aspx"
];

var listaPaginasBtnMenuPaneles = [
    "PanelExterno.aspx"
];

var btnPageHome = {
    id: 'btnPageHome',
    xtype: 'button',
    text: 'Inicio',
    iconAlign: 'left',
    icon: 'Images/home_white_24x24.png',
    height: 35,
    width: 80,
    handler: function () {
        window.location = "ViajesLocalCliente.aspx";//"Default.aspx";
    }
};

var btnVisualizador = {
    id: 'btnVisualizador',
    xtype: 'button',
    text: 'Vista General',
    iconAlign: 'left',
    icon: 'Images/earth_20x20.png',
    height: 35,
    width: 110,
    handler: function () {
        window.location = "Visualizador.aspx";
    }
};

var botonesToolBarMenu = [];

function RevisarSiSeAgregaMenu(paginaParaRevisar) {
    for (var indice = 0; indice < listadoPaginasPermitidasAlUsuario.length; indice++)
        if (listadoPaginasPermitidasAlUsuario[indice] == paginaParaRevisar)
            return true;
    return false;
}

function ProcesoAgregacionMenu(paginaParaRevisar, funcionAgregacionMenu) {
    if (RevisarSiSeAgregaMenu(paginaParaRevisar))
        funcionAgregacionMenu();
}

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        text: 'Dashboard de Control',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "DashboardControl.aspx";
        }
    })
};
ProcesoAgregacionMenu("DashboardControl.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        text: 'Control de Viajes',
        icon: 'Images/historico_gray_24x24.png',
        handler: function () {
            window.location = "ViajesRuta.aspx";
        }
    })
};
ProcesoAgregacionMenu("ViajesRuta.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        text: 'BackHaul',
        icon: 'Images/store_black_24x24.png',
        handler: function () {
            window.location = "ViajesBackhaul.aspx";
        }
    })
};
ProcesoAgregacionMenu("ViajesBackhaul.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({  
        id: 'menuViajesConfigViajes',
        text: 'Creación de Viajes',
        hidden: true,
        icon: 'Images/edittrip_black_26x26.png',
        handler: function () {
            window.location = "ConfigViajes.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigViajes.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

var existeMenuViajesConfigViajes_v2 = false;

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuViajes.add({
        id: 'menuViajesConfigViajes_v2',
        text: 'Creación de Viajes',
        hidden: true,
        icon: 'Images/edittrip_black_26x26.png',
        handler: function () {
            window.location = "ConfigViajes_v2.aspx";
        }
    });
    existeMenuViajesConfigViajes_v2 = true;
};
ProcesoAgregacionMenu("ConfigViajes_v2.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Visualización de Flota',
        icon: 'Images/monitoreoOnline_black_24x24.png',
        handler: function () {
            window.location = "MonitoreoOnline.aspx";
        }
    })
};
ProcesoAgregacionMenu("MonitoreoOnline.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Posiciones GPS',
        icon: 'Images/broadcast_black_24x24.png',
        handler: function () {
            window.location = "PosicionesGPS.aspx";
        }
    })
};
ProcesoAgregacionMenu("PosicionesGPS.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Reportabilidad',
        icon: 'Images/fronttruck_black_24x24.png',
        handler: function () {
            window.location = "ReportabilidadGPSTrack.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReportabilidadGPSTrack.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuGPS.add({
        text: 'Estado Flota',
        icon: 'Images/fronttruck_black_24x24.png',
        handler: function () {
            window.location = "LastPositions.aspx";
        }
    })
};
ProcesoAgregacionMenu("LastPositions.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Informe de Viajes',
        icon: 'Images/informeViajes_white_24x24.png',
        handler: function () {
            window.location = "InformeViajes.aspx";
        }
    })
};
ProcesoAgregacionMenu("InformeViajes.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Kms recorridos',
        icon: 'Images/road_black_24x25.png',
        handler: function () {
            window.location = "Rpt_KmsRecorridos.aspx";
        }
    })
};
ProcesoAgregacionMenu("Rpt_KmsRecorridos.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Temático de alertas',
        icon: 'Images/flag_black_24x24.png',
        handler: function () {
            window.location = "Rpt_Alertas.aspx";
        }
    })
};
ProcesoAgregacionMenu("Rpt_Alertas.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Tractos',
        icon: 'Images/truck_black_24x24.png',
        handler: function () {
            window.location = "ReporteEstadiaTractos.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaTractos.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Tractos Histórico',
        icon: 'Images/truck_black_24x24.png',
        handler: function () {
            window.location = "ReporteEstadiaTractos_Historico.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaTractos_Historico.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Remolques',
        icon: 'Images/trailer_black_26x26.png',
        handler: function () {
            window.location = "ReporteEstadiaRemolques.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaRemolques.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Estadía Remolques Histórico',
        icon: 'Images/trailer_black_26x26.png',
        handler: function () {
            window.location = "ReporteEstadiaRemolques_Historico.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteEstadiaRemolques_Historico.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuReportes.add({
        text: 'Reporte Comercial',
        icon: 'Images/informeViajes_white_24x24.png',
        handler: function () {
            window.location = "ReporteComercial.aspx";
        }
    })
};
ProcesoAgregacionMenu("ReporteComercial.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Zonas',
        icon: 'Images/zona_gray_22x22.png',
        handler: function () {
            window.location = "ConfigZonas.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigZonas.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Usuarios',
        icon: 'Images/user_black_24x24.png',
        handler: function () {
            window.location = "ConfigUsuarios.aspx";
        }
    })
};
ProcesoAgregacionMenu("ConfigUsuarios.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Páginas por perfil',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "PaginasPorPerfil.aspx";
        }
    })
};
ProcesoAgregacionMenu("PaginasPorPerfil.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Páginas por usuario',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "PaginasPorUsuario.aspx";
        }
    })
};
ProcesoAgregacionMenu("PaginasPorUsuario.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);


agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Tableros Por Perfil',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "TablerosPorPerfil.aspx";
        }
    })
};
ProcesoAgregacionMenu("TablerosPorPerfil.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Tableros Por Usuario',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "TablerosPorUsuario.aspx";
        }
    })
};
ProcesoAgregacionMenu("TablerosPorUsuario.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);

agregarMenuEnCasoDeSerPermitidoParaEsteUsuario = function () {
    menuConfiguracion.add({
        text: 'Administrar Tableros',
        icon: 'Images/control_black_24x24.png',
        handler: function () {
            window.location = "Tableros.aspx";
        }
    })
};
ProcesoAgregacionMenu("Tableros.aspx", agregarMenuEnCasoDeSerPermitidoParaEsteUsuario);


function armarBotonesToolBarMenu() {
    botonesToolBarMenu.push(imageMenu);
    botonesToolBarMenu.push({ xtype: 'tbseparator' });
    botonesToolBarMenu.push(imgCliente);
    botonesToolBarMenu.push({ xtype: 'tbseparator' });
    botonesToolBarMenu.push(btnPageHome);
    botonesToolBarMenu.push({ xtype: 'tbseparator' });
    
    RevisarSiSeAgregaElBoton(btnVisualizador, listaPaginasBtnVisualizador);
    RevisarSiSeAgregaElSeparador(btnVisualizador, listaPaginasBtnVisualizador);

    RevisarSiSeAgregaElBoton(btnMenuViajes, listaPaginasBtnMenuViajes);
    RevisarSiSeAgregaElSeparador(btnMenuViajes, listaPaginasBtnMenuViajes);
    RevisarSiSeAgregaElBoton(btnMenuGPS, listaPaginasBtnMenuGPS);
    RevisarSiSeAgregaElSeparador(btnMenuGPS, listaPaginasBtnMenuGPS);
    RevisarSiSeAgregaElBoton(btnMenuReportes, listaPaginasBtnMenuReportes);
    RevisarSiSeAgregaElSeparador(btnMenuReportes, listaPaginasBtnMenuReportes);

    //nuevas lineas para menu paneles
    RevisarSiSeAgregaElBoton(btnMenuPaneles, listaPaginasBtnMenuPaneles);
    RevisarSiSeAgregaElSeparador(btnMenuPaneles, listaPaginasBtnMenuPaneles);
    //fin nuevas lineas para menu paneles

    RevisarSiSeAgregaElBoton(btnMenuConfig, listaPaginasBtnMenuConfig);
    RevisarSiSeAgregaElSeparador(btnMenuConfig, listaPaginasBtnMenuConfig);
    botonesToolBarMenu.push(btnLogout);
    botonesToolBarMenu.push({ xtype: 'tbspacer', width: viewWidth - 1220 });
    botonesToolBarMenu.push(imageContacto);

    setTimeout(function () {
        ReubicarImagenContacto();
    }, 500);
}
armarBotonesToolBarMenu();

function ReubicarImagenContacto() {
    var anchuraImagenContacto = 230;
    var idBuscado = $("[src='Images/contacto.png']").attr('id');
    var windowWidth = $(window).width();
    var pocisionIdeal = windowWidth - anchuraImagenContacto;

    $("#" + idBuscado).parent().css({ position: 'relative' });
    $("#" + idBuscado).css({ left: pocisionIdeal, position: 'absolute' });
}

function RevisarSiSeAgregaElBoton(botonParaAgregar, listaPaginasSubMenu) {
    for (var indice = 0; indice < listadoPaginasPermitidasAlUsuario.length; indice++)
        for (var indiceListaPaginas = 0; indiceListaPaginas < listaPaginasSubMenu.length; indiceListaPaginas++)
            if (listadoPaginasPermitidasAlUsuario[indice] == listaPaginasSubMenu[indiceListaPaginas]) {
                botonesToolBarMenu.push(botonParaAgregar);
                return;
            }
}

function RevisarSiSeAgregaElSeparador(botonParaAgregar, listaPaginasSubMenu) {
    var seDebeAgregarSeparador = false;

    for (var indice = 0; indice < listadoPaginasPermitidasAlUsuario.length; indice++)
        for (var indiceListaPaginas = 0; indiceListaPaginas < listaPaginasSubMenu.length; indiceListaPaginas++)
            if (listadoPaginasPermitidasAlUsuario[indice] == listaPaginasSubMenu[indiceListaPaginas])
                seDebeAgregarSeparador = true;

    if (seDebeAgregarSeparador)
        botonesToolBarMenu.push({ xtype: 'tbseparator' });
};

var toolbarMenu = Ext.create('Ext.toolbar.Toolbar', {
    id: 'toolbarMenu',
    height: 40,
    items: [
        botonesToolBarMenu
    ]
});

var topMenu = new Ext.FormPanel({
    id: 'topMenu',
    region: 'north',
    border: true,
    height: 40,
    tbar: toolbarMenu
});

//CODIGO PARA EVITAR QUE EL MENU DE VIAJES SE OCULTE
$(document).ready(function () {
    /*
    $("#btnMenuViajes").on("click mouseover", function () {
        var x = $("#btnMenuViajes").position();
        $("#menuViajes").attr("style", "visibility: visible; position: absolute; left: " + x.left + "px; top: 39px;  z-index: 19011;  width: 150px;height: 110px;");
        
        $("#menuViajes").children().attr("style", "height: 181px;");
        $('#menuViajes').find('.x-vertical-box-overflow-body:first').attr("style", "height: 181px;");
        
        $("#menuViajesConfigViajes_v2").attr("style", "margin: 0px; width: 144px; height: 27px; left: 0px; z-index: 99999 !important; top:79px;   position: relative!important;");
        $("#menuViajesConfigViajes_v2").children().attr("style", "height: 181px;");
    });
    */
    setTimeout(function () {
        $("#btnMenuViajes").on("click mouseover", function () {//
            if (existeMenuViajesConfigViajes_v2) {
                var x = $("#btnMenuViajes").position();
                $("#menuViajes").attr("style", "visibility: visible; position: absolute; left: " + x.left + "px; top: 39px; z-index: 19011; width: 150px;height: 110px;");

                $("#menuViajesConfigViajes_v2").attr("style", "margin: 0px; width: 144px; height: 27px; left: 0px; top: 58px;");

                $("#menuViajes").children().attr("style", "height: 101px;");
                $('#menuViajes').find('.x-vertical-box-overflow-body:first').attr("style", "height: 161px;");

                //$("#menuViajesConfigViajes_v2").children().attr("style", "height: 181px;");
            }
        });
    }, 500);
    //$("#btnMenuViajes").on("mouseout", function () {
    //    $("#menuViajes").attr("style", "visibility: hidden;");
    //});

});
//ESTO OCURRE PORQUE LOS ESTILOS CSS QUE CARGA ESTA SECCION ENTRAN EN CONFLICTO CON LOS DEL MENU