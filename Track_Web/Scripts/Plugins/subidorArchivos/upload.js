﻿$(function () {
    var listaImagenesCreada = [];

    $.cookie('idTableroTemporal', '1');

    function OcultarBotonAsignarImgsTableroActual() {
        $("#AsignarImgsTableroActual").css("display", "none");
    }

    function MostrarBotonAsignarImgsTableroActual() {
        $("#AsignarImgsTableroActual").css("display", "block");
    }

    OcultarBotonAsignarImgsTableroActual();

    function RevisarSiHayImagenesEnListado() {
        var listaImagenes = $("#contenedorListaImagenes").html();
        if (listaImagenes.indexOf("img src=") > -1) {
            //hay imagenes en el listado
        } else {
            OcultarBotonAsignarImgsTableroActual();
        }
    }

    function MostrarAlertaGeneralImagen(titulo, descripcion, tipo) {
        swal(
            titulo, descripcion, tipo
        );
    }

    function RemoverImagenARelacionarConElTablero(idImagen) {

        for (var indice = listaImagenesCreada.length - 1; indice >= 0; indice--) 
            if (listaImagenesCreada[indice] === idImagen) 
                listaImagenesCreada.splice(indice, 1);
            
    }
        

    function ProcesarConfirmacionMensajeBorradoImagen(resultado, elementoABorrar) {
        if (resultado.value) {
            var idImagen = elementoABorrar.attr("nuevoidimagen");
            
            RemoverImagenARelacionarConElTablero(idImagen);

            elementoABorrar.remove();
            
            RevisarSiHayImagenesEnListado();
            MostrarAlertaGeneralImagen('Imagen Borrada', 'La Imagen ha sido borrada.', 'success');
        }
    }

    function MostrarMensajeBorradoImagen(elementoABorrar) {
        swal({
            title: 'Esta Segur@ de querer eliminar esta imagen?',
            text: "No podra deshacer esta acción",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Si, Eliminar',
            cancelButtonText: 'Cancelar'
        }).then((result) => {
            ProcesarConfirmacionMensajeBorradoImagen(result, elementoABorrar);
        })
    }

    function AsignarListenerBorrarImagen() {
        $(".retirarImg").on("click", function () {
            var elementoABorrar = $(this).parent();
            MostrarMensajeBorradoImagen(elementoABorrar) 
        });
    }

    function ObtenerJSONImagenesPorTablero() {
        var idTableroTemporal = $.cookie('idTableroTemporal');
        
        var cadenaJSONImagenesPorTablero = [];

        cadenaJSONImagenesPorTablero.push("[");

        var elementosImagenesPorTablero = [];

        for (var indice = 0; indice < listaImagenesCreada.length; indice++)
            elementosImagenesPorTablero.push("{'Id' :'1','IdImagen':'" + listaImagenesCreada[indice]+"','IdTablero':'" + idTableroTemporal + "','Activo':'true' },");

        var cadenaResultanteElemento = elementosImagenesPorTablero.join("");

        cadenaResultanteElemento = cadenaResultanteElemento.substring(0, cadenaResultanteElemento.length - 1);

        cadenaJSONImagenesPorTablero.push(cadenaResultanteElemento);

        cadenaJSONImagenesPorTablero.push("]");
        
        return cadenaJSONImagenesPorTablero.join("");
    }

    function AsignarListenerAsignarImagenes() {
        $("#AsignarImgsTableroActual").on("click", function () {
            
            SoloMostrarPanelBotoneraOpcionesGaleria();;

            MostrarseccionConsultaDatos($.cookie('idTableroTemporal'));

            //LlenarContenedorListaImagenesActual();
            
            var cadenaJSONImagenesPorTablero = ObtenerJSONImagenesPorTablero();
            
            $.ajax({
                url: "FileUploadHandler.ashx?OtroProceso=enviarJSON",
                data: cadenaJSONImagenesPorTablero,   
                dataType: "json",
                async: false,
                cache: false,
                contentType: "application/json; charset=utf-8"
            }).then(function (data) {
                //console.log("data: " + JSON.stringify(data));
            });
            MostrarAlertaGeneralImagen('Imagenes Asignadas', 'Las imagenes han sido asignadas a esta galeria.', 'success');
            $("#contenedorListaImagenes").html("");
        });
    }

    function CrearIdTemporalNuevaImagen(idNuevaImagen) {
        //$.cookie('idTemporalNuevaImagen', idNuevaImagen);
        listaImagenesCreada.push(idNuevaImagen);
    }

    function CrearVariableTemporalNuevaImagen(nuevaImagen, nuevoIdImagen) {
        $.cookie('variableTemporalNuevaImagen', nuevaImagen);
        LlenarContenedorListaImagenes(nuevaImagen, nuevoIdImagen);
    }

    function LlenarContenedorListaImagenes(nuevaImagen, nuevoIdImagen) {
        var contenidoListaImagenes = [];
        var listaImagenes = $("#contenedorListaImagenes").html();
        contenidoListaImagenes.push(listaImagenes);
        contenidoListaImagenes.push("<div nuevoidimagen='" + nuevoIdImagen + "'>");
        contenidoListaImagenes.push("<br>");
        contenidoListaImagenes.push("<br>");
        contenidoListaImagenes.push('<img src="' + nuevaImagen + '" width="150" height="87">');
        contenidoListaImagenes.push("&nbsp;&nbsp;&nbsp;");
        contenidoListaImagenes.push("<a class='retirarImg' href='#'>Quitar esta imagen</a>");
        contenidoListaImagenes.push("</div>");
        $("#contenedorListaImagenes").html(contenidoListaImagenes.join(""));
        AsignarListenerBorrarImagen();
        MostrarBotonAsignarImgsTableroActual();
    }

    $('#btnFileUpload').fileupload({
        url: 'FileUploadHandler.ashx?upload=start',
        add: function (e, data) {
            $('#progressbar').show();
            data.submit();
        },
        progress: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progressbar').show();
            $('#progressbar div').css('width', progress + '%');
        },
        success: function (response, status) {
            $("#mensajeTexto").html("Cargando Archivo ...");
            
            var respuesta = response;

            var arregloElementosRespuesta = respuesta.split(",");
            var rutaCompletaImagen = arregloElementosRespuesta[0];
            var nuevoIdImagen = arregloElementosRespuesta[1];

            CrearVariableTemporalNuevaImagen(rutaCompletaImagen, nuevoIdImagen);
            CrearIdTemporalNuevaImagen(nuevoIdImagen);

            setTimeout(function () {
                console.log("exito");
                $('#progressbar').hide();
            }, 2000);
        },
        error: function (error) {
            $("#mensajeTexto").html(error);
            setTimeout(function () {
                console.log("error");
                $('#progressbar').hide();
            }, 2000);
        }
    });

    AsignarListenerAsignarImagenes();
});