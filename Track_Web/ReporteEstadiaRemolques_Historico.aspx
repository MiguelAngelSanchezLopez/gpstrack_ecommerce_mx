﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ReporteEstadiaRemolques_Historico.aspx.cs" Inherits="Track_Web.ReporteEstadiaRemolques_Historico" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
  AltoTrack Platform 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
  <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
  <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
  <script src="Scripts/TopMenu.js" type="text/javascript"></script>
  <script src="Scripts/LabelMarker.js" type="text/javascript"></script>

  <script type="text/javascript">

    Ext.onReady(function () {

        Ext.QuickTips.init();
        Ext.Ajax.timeout = 3600000;
        Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
        Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
        Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

        Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

        //Verifica si se debe controlar tiempo de expiración de sesión
        Ext.Ajax.request({
            url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
            success: function (data, success) {
                if (data != null) {
                    data = Ext.decode(data.responseText);

                    if (data > 0) {
                        Ext.ns('App');

                        //Session timeout in secons     
                        App.SESSION_TIMEOUT = data;

                        // Helper that converts minutes to milliseconds.
                        App.toMilliseconds = function (minutes) {
                            return minutes * 60 * 1000;
                        }

                        // Notifies user that her session has timed out.
                        App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                            Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                            Ext.MessageBox.show({
                                title: "Sesión expirada.",
                                msg: "Su sesión ha expirado.",
                                icon: Ext.MessageBox.WARNING,
                                buttons: Ext.MessageBox.OK,
                                fn: function () {
                                    window.location = "Login.aspx";
                                }
                            });

                        });

                        // Starts the session timeout workflow after an AJAX request completes.
                        Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                            // Reset the client-side session timeout timers.
                            App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                        });

                    }
                }
            }
        })

      var dateDesde = new Ext.form.DateField({
          id: 'dateDesde',
          fieldLabel: 'Desde',
          labelWidth: 100,
          allowBlank: false,
          anchor: '99%',
          format: 'd-m-Y',
          editable: false,
          value: new Date(),
          maxValue: new Date(),
          style: {
              marginLeft: '5px'
          },
          disabled: false
      });

      var dateHasta = new Ext.form.DateField({
          id: 'dateHasta',
          fieldLabel: 'Hasta',
          labelWidth: 100,
          allowBlank: false,
          anchor: '99%',
          format: 'd-m-Y',
          editable: false,
          value: new Date(),
          minValue: Ext.getCmp('dateDesde').getValue(),
          maxValue: new Date(),
          style: {
              marginLeft: '5px'
          },
          disabled: false
      });

      dateDesde.on('change', function () {
          var _desde = Ext.getCmp('dateDesde');
          var _hasta = Ext.getCmp('dateHasta');

          _hasta.setMinValue(_desde.getValue());
          _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
          _hasta.validate();
          //FiltrarViajes();
      });

      dateHasta.on('change', function () {
          var _desde = Ext.getCmp('dateDesde');
          var _hasta = Ext.getCmp('dateHasta');

          _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
          _desde.validate();
      });

      Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
      Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

      var storeFiltroPatente = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['Patente'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentesPoolRemolques&Todas=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroPatente = new Ext.form.field.ComboBox({
        id: 'comboFiltroPatente',
        fieldLabel: 'Placa',
        labelWidth: 100,
        store: storeFiltroPatente,
        valueField: 'Patente',
        displayField: 'Patente',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        style: {
          marginLeft: '5px'
        }
      });

      Ext.getCmp('comboFiltroPatente').store.load({
          params: {
              transportista: ""
          },
          callback: function (r, options, success) {
              if (success) {

                  var firstPatente = Ext.getCmp("comboFiltroPatente").store.getAt(0).get("Placa");
                  Ext.getCmp("comboFiltroPatente").setValue(firstPatente);
              }
          }
      });

      var storeFiltroCedis = new Ext.data.JsonStore({
        autoLoad: false,
        fields: ['Cedis'],
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllCedisPool&Todas=True',
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var comboFiltroCedis = new Ext.form.field.ComboBox({
        id: 'comboFiltroCedis',
        fieldLabel: 'Cedis',
        labelWidth: 100,
        store: storeFiltroCedis,
        valueField: 'Cedis',
        displayField: 'Cedis',
        queryMode: 'local',
        anchor: '99%',
        emptyText: 'Seleccione...',
        enableKeyEvents: true,
        editable: true,
        multiSelect: true,
        style: {
          marginLeft: '5px'
        }
      });

      Ext.getCmp('comboFiltroCedis').store.load({
        param: {
          transportista: 'Todos'
        },
        callback: function (r, options, success) {
          if (success) {

            var firstceids = Ext.getCmp("comboFiltroCedis").store.getAt(1).get("Cedis");
            Ext.getCmp("comboFiltroCedis").setValue(firstceids);
          }
        }
      })

      var storeFiltroCarrier = new Ext.data.JsonStore({
          autoLoad: false,
          fields: ['Carrier'],
          proxy: new Ext.data.HttpProxy({
              url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllCarrierPool&Todos=True',
              headers: {
                  'Content-type': 'application/json'
              }
          })
      });

      var comboFiltroCarrier = new Ext.form.field.ComboBox({
          id: 'comboFiltroCarrier',
          fieldLabel: 'Carrier',
          labelWidth: 100,
          store: storeFiltroCarrier,
          valueField: 'Carrier',
          displayField: 'Carrier',
          queryMode: 'local',
          anchor: '99%',
          emptyText: 'Seleccione...',
          enableKeyEvents: true,
          editable: true,
          multiSelect: true,
          style: {
              marginLeft: '5px'
          },
          listeners: {
              change: function (field, newVal) {
                  FiltrarPatentes();
              }
          }
      });

      Ext.getCmp('comboFiltroCarrier').store.load({
          callback: function (r, options, success) {
              if (success) {

                  var firstcarrier = Ext.getCmp("comboFiltroCarrier").store.getAt(0).get("Carrier");
                  Ext.getCmp("comboFiltroCarrier").setValue(firstcarrier);
              }
          }
      })

      var btnBuscar = {
        id: 'btnBuscar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/searchreport_black_20x20.png',
        text: 'Buscar',
        width: 90,
        height: 26,
        handler: function () {
          Buscar();
        }
      };

      var btnExportar = {
        id: 'btnExportar',
        xtype: 'button',
        iconAlign: 'left',
        icon: 'Images/export_black_20x20.png',
        text: 'Exportar',
        width: 90,
        height: 26,
        listeners: {
          click: {
            element: 'el',
            fn: function () {

              var mapForm = document.createElement("form");
              mapForm.target = "ToExcel";
              mapForm.method = "POST"; // or "post" if appropriate
              mapForm.action = 'ReporteEstadiaRemolques_Historico.aspx?Metodo=ExportExcel';

              document.body.appendChild(mapForm);
              mapForm.submit();

            }
          }
        }
      };

      var displayEstado = new Ext.form.field.Display({
        id: 'displayEstado',
        labelWidth: 150,
        value: '',
        width: 500,
        style: {
          marginTop: '4px',
          marginLeft: '5px'
        },
        labelStyle: 'font-size: large'
      });

      var panelFilters = new Ext.FormPanel({
        id: 'panelFilters',
        title: 'Filtros Reporte',
        anchor: '100% 100%',
        bodyStyle: 'padding: 5px;',
        layout: 'anchor',
        items: [{
          xtype: 'container',
          layout: 'anchor',
          columnWidth: 1,
          items: [dateDesde, dateHasta, comboFiltroCedis, comboFiltroCarrier, comboFiltroPatente, displayEstado]
        }],
        buttons: [btnExportar, btnBuscar]
      });

      var storeReporte = new Ext.data.JsonStore({
        autoLoad: false,
        fields: [
                  { name: 'Fecha', type: 'date', dateFormat: 'c' },
                  'Placa',
                  'Transportista',
                  'TipoUnidad',
                  'EstadoEventos',
                  'FechaEvento',
                  'FechaReporte',
                  'DesfaseReporte',
                  'Antiguedad',
                  'Velocidad',
                  'Latitud',
                  'Longitud',
                  'Referencia',
                  'Detenido',
                  'TiempoDetenido',
                  'FechaDetencion',
                  'HorasDetenido',
                  'HorasDetenidoTxt',
                  'UltimaEntrada',
                  'UltimaSalida',
                  'UltimaZona',
                  'EnZona',
                  'Zona',
                  'NombreZona',
                  'Formato',
                  'TiempoEnZona',
                  'CodZonaCercana',
                  'NombreZonaCercana',
                  'DistanciaZonaCercana',
                  'CEDIS',
                  'NombreCEDIS',
                  'EstadoCarga',
                  'EstadoViaje'
        ],
        groupField: 'Transportista',
        proxy: new Ext.data.HttpProxy({
          url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetReporteEstadiaRemolques_Historico',
          reader: { type: 'json', root: 'Zonas' },
          headers: {
            'Content-type': 'application/json'
          }
        })
      });

      var gridPanelReporte = Ext.create('Ext.grid.Panel', {
        id: 'gridPanelReporte',
        title: 'Reporte Estadia Remolques',
        store: storeReporte,
        anchor: '100% 100%',
        columnLines: true,
        scroll: true,
        viewConfig: {
          style: { overflow: 'auto', overflowX: 'hidden' }
        },
        features: [{
          ftype: 'groupingsummary',
          groupHeaderTpl: '{name}'
        }],
        columns: [
                    { text: 'Fecha', sortable: true, width: 60, dataIndex: 'Fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y') },
                    { text: 'Placa', sortable: true, width: 60, dataIndex: 'Placa' },
                    { text: 'Línea T.', sortable: true, width: 80, dataIndex: 'Transportista' },
                    { text: 'Tipo Unidad', sortable: true, width: 80, dataIndex: 'Transportista' },
                    { text: 'Estado', sortable: true, width: 60, dataIndex: 'EstadoEventos' },
                    { text: 'Fecha GPS', sortable: true, width: 60, dataIndex: 'FechaEvento'},
                    { text: 'Fecha Transmisión', sortable: true, width: 60, dataIndex: 'FechaReporte'},
                    { text: 'Desfase(min)', sortable: true, width: 60, dataIndex: 'DesfaseReporte' },
                    { text: 'T. última señal', sortable: true, width: 105, dataIndex: 'Antiguedad' },
                    { text: 'Vel.', sortable: true, width: 30, dataIndex: 'Velocidad' },
                    { text: 'Latitud', sortable: true, width: 60, dataIndex: 'Latitud' },
                    { text: 'Longitud', sortable: true, width: 60, dataIndex: 'Longitud' },
                    { text: 'Referencia', sortable: true, flex: 1, dataIndex: 'Referencia' },
                    { text: 'Detenido', sortable: true, width: 60, dataIndex: 'Detenido' },
                    { text: 'Fec. Detencion', sortable: true, width: 85, dataIndex: 'FechaDetencion' },
                    { text: 'T. Detenido', sortable: true, width: 75, dataIndex: 'TiempoDetenido' },
                    { text: 'Ult. Entrada', sortable: true, width: 80, dataIndex: 'UltimaEntrada' },
                    { text: 'Ult. Salida', sortable: true, width: 80, dataIndex: 'UltimaSalida' },
                    { text: 'Ult. Zona', sortable: true, width: 80, dataIndex: 'UltimaZona' },
                    { text: 'En Zona', sortable: true, width: 60, dataIndex: 'EnZona' },
                    { text: 'Zona', sortable: true, width: 100, dataIndex: 'NombreZona' },
                    { text: 'T. En Zona', sortable: true, width: 70, dataIndex: 'TiempoEnZona' },
                    { text: 'Zona cercana', sortable: true, width: 80, dataIndex: 'NombreZonaCercana' },
                    { text: 'Dist. zona cercana', sortable: true, width: 90, dataIndex: 'DistanciaZonaCercana' },
                    { text: 'CEDIS', sortable: true, width: 90, dataIndex: 'NombreCEDIS' }
        ]

      });

      var leftPanel = new Ext.FormPanel({
        id: 'leftPanel',
        region: 'west',
        border: true,
        margins: '0 0 3 3',
        width: 300,
        minWidth: 200,
        maxWidth: 400,
        layout: 'anchor',
        collapsible: true,
        titleCollapsed: false,
        split: true,
        items: [panelFilters]
      });

      var centerPanel = new Ext.FormPanel({
        id: 'centerPanel',
        region: 'center',
        border: true,
        margins: '0 3 3 0',
        anchor: '100% 100%',
        items: [gridPanelReporte]
      });

      var viewport = Ext.create('Ext.container.Viewport', {
        layout: 'border',
        items: [topMenu, leftPanel, centerPanel]
      });
    });

  </script>


  <script type="text/javascript">

    function Buscar() {

      var desde = Ext.getCmp('dateDesde').getValue();
      var hasta = Ext.getCmp('dateHasta').getValue();
      var patente = Ext.getCmp('comboFiltroPatente').getValue();
      var idCedis = Ext.getCmp('comboFiltroCedis').getValue();
      var listCarrier = Ext.getCmp('comboFiltroCarrier').getValue();

      if (idCedis === null || idCedis === "") {
        idCedis = ""
      }

      if (listCarrier === null || listCarrier === "") {
          listCarrier = ""
      }

      var store = Ext.getCmp('gridPanelReporte').store;
      store.load({
          params: {
          desde: desde,
          hasta: hasta,
          patente: patente,
          idCedis: idCedis,
          listCarrier: listCarrier
        },
        callback: function (r, options, success) {
          if (!success) {
            Ext.MessageBox.show({
              title: 'Error',
              msg: 'Se ha producido un error.',
              buttons: Ext.MessageBox.OK
            });
          }
        }
      });
    }

    function FiltrarCedis() {

      var store = Ext.getCmp('comboFiltroCedis').store;
      store.load({
        params: {
          transportista: "Todas"
          },
        callback: function (r, options, success) {
            if (success) {
                var firstceids = Ext.getCmp("comboFiltroCedis").store.getAt(1).get("Cedis");
                Ext.getCmp("comboFiltroCedis").setValue(firstceids);
            }
        }
      });
    }

      function FiltrarPatentes() {

          var transportista = Ext.getCmp('comboFiltroCarrier').getValue();

          var store = Ext.getCmp('comboFiltroPatente').store;
          store.load({
              params: {
                  transportista: transportista
              },
              callback: function (r, options, success) {
                  if (success) {
                      Ext.getCmp('comboFiltroPatente').store.insert(0, [{ Placa: "Todas" }]);
                      Ext.getCmp("comboFiltroPatente").setValue("Todas");
                  }
              }
          });
    }

  </script>
</asp:Content>