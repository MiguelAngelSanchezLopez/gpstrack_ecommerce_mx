﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;
using System.Diagnostics;
using Microsoft.VisualBasic;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

using System.IO;
using SimpleImpersonation;
using System.Globalization;
using Ionic.Zip;
using System.Net;

namespace Track_Web
{
    public partial class ViajesLocalClienteCodelco : System.Web.UI.Page
    {
        IFormatProvider culture = new CultureInfo("en-US", true);
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel(Request.Form["desde"].ToString(), Request.Form["hasta"].ToString(), Request.Form["rutCliente"].ToString(), Request.Form["nroTransporte"].ToString(), Request.Form["transportista"].ToString(), Request.Form["tipoEtis"].ToString(), Request.Form["estadoViaje"].ToString(), Request.Form["pod"].ToString(), Request.Form["tipoCarga"].ToString(), Request.Form["nroContenedor"].ToString(), Request.Form["otif"].ToString(), Request.Form["problema"].ToString());
                    return;
                case "DescargarImagenes":
                    DescargarImagenes(Request.Form["URLImagenes"].Split(';'));
                    return;
                default:
                    break;
            }
        }

        public void ExportExcel(string desde, string hasta, string rutCliente, string nroTransporte, string transportista, string tipoEtis, string estadoViaje, string pod, string tipoCarga, string nroContenedor, string otif, string problema)
        {
            string username = Session["userName"].ToString();

            string now = DateTime.Now.ToString();
            now = now.Replace(" ", "_");
            now = now.Replace("-", "");
            now = now.Replace(":", "");

            int _nroTransporte;
            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                int.TryParse(nroTransporte, out _nroTransporte);
            }

            DateTime _desde;
            DateTime _hasta;

            //DateTime.TryParse(desde, out _desde);
            //DateTime.TryParse(hasta, out _hasta);
            if (DateTime.TryParseExact(desde.Replace("-", "/").Substring(0, 10), "dd/MM/yyyy", culture, DateTimeStyles.None, out _desde))
            {
            }
            else
            {
                DateTime.TryParseExact(desde.Replace("T", " ").Substring(0, 10), "yyyy-MM-dd", culture, DateTimeStyles.None, out _desde);
            }

            if (DateTime.TryParseExact(hasta.Replace("-", "/").Substring(0, 10), "dd/MM/yyyy", culture, DateTimeStyles.None, out _hasta))
            {
            }
            else
            {
                DateTime.TryParseExact(hasta.Replace("T", " ").Substring(0, 10), "yyyy-MM-dd", culture, DateTimeStyles.None, out _hasta);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            List<Track_GetViajesControlLocal_Result> _listViajes = _objMethosViajes.GetViajesControlLocal(_desde, _hasta, rutCliente, _nroTransporte, transportista, username, tipoEtis, estadoViaje, tipoCarga, pod, otif, nroContenedor, problema, false);

            StringBuilder reportBuilder = new StringBuilder();

            Response.Clear();
            Response.Buffer = true;
            Response.BufferOutput = true;
            //Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment;filename=RptViajesLocal_" + now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(Methods_Export.HTML_RptViajesControlLocal(_listViajes.ToList()));
            Response.End();
        }

        public void DescargarImagenes(string[] urlImagenes)
        {
            string now = DateTime.Now.ToString();
            now = now.Replace(" ", "_");
            now = now.Replace("-", "");
            now = now.Replace(":", "");

            string rutaSitio = Utilities.GetExePath();
            string fileExtension = "png";

            if (!(Directory.Exists(rutaSitio + "FileTemp")))
            {
                Directory.CreateDirectory(rutaSitio + "FileTemp");
            }

            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                zip.AddDirectoryByName("POD");

                for (int i = 0; i < urlImagenes.Count(); i++)
                {
                    string Fechafile = DateTime.Now.ToString("ddMMyyyyHHmmss");
                    var localPath = @"" + rutaSitio + "FileTemp\\tmpImage" + i.ToString() + "_" + Fechafile + "." + fileExtension;

                    WebClient webClient = new WebClient();
                    webClient.DownloadFile(urlImagenes[i], localPath);

                    zip.AddFile(localPath, "POD");

                }

                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("Zip_{0}.zip", DateTime.Now.ToString("yyyy-MMM-dd-HHmmss"));
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);

                Array.ForEach(Directory.GetFiles(@"" + rutaSitio + "FileTemp"), File.Delete);

                Response.End();


            }

        }

    }
}