﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using ContextLayer;

namespace Track_Web
{
    public partial class PrintPage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            string idRegistro = Request.QueryString["IdRegistro"].ToString();
            string referencia = Request.QueryString["Referencia"].ToString();

            string url = UtilitiesLayer.Utilities.DevolverURLAmazon(Convert.ToInt32(idRegistro), referencia);

            if (Request.QueryString["Form"].ToString().ToLower() == "print")
            {
                PrintImage.Src = url;
                Page.ClientScript.RegisterStartupScript(this.GetType(), "Script", "myFunction();", true);
            }
            else
                DescargarFoto(Convert.ToInt32(idRegistro), referencia);
        }

        public void DescargarFoto(int idRegistro, string referencia)
        {

            try
            {
                string url = UtilitiesLayer.Utilities.DevolverURLAmazon(Convert.ToInt32(idRegistro), referencia);

                string remoteUri = url;
                string fileName = referencia;
                System.Net.WebClient client = new System.Net.WebClient();

                byte[] myByteArray = client.DownloadData(remoteUri);


                
                Response.Buffer = true;
                Response.Clear();                
                Response.AppendHeader("Content-Disposition", "attachment;filename=" + fileName);
                Response.ContentType = "image/png";
                Response.BinaryWrite(myByteArray);
                Response.End();

               
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
            }

        }

    }
}