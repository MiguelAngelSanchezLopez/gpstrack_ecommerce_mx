﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Tableros.aspx.cs" Inherits="Track_Web.Tableros" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <script src="Scripts/TopMenu.js" type="text/javascript"></script>

    <script type="text/javascript">

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.form.Field.prototype.msgTarget = 'side';
            if (Ext.isIE) { Ext.enableGarbageCollector = false; }

            Ext.Ajax.timeout = 3600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data > 0) {
                            Ext.ns('App');

                            //Session timeout in secons     
                            App.SESSION_TIMEOUT = data;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                                /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                 toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 5 0 0',
                anchor: '100% 100%',
                html: ''//'<img src="Images/background_gray_1366x768.png" style="height:100%; width:100%"/>'
            });

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, centerPanel]
            });

        });

    </script>


    <script type="text/javascript" src="Scripts/jquery-3.3.1.js"></script>

    <script type="text/javascript" src="Scripts/bootstrap.js"></script>
    <script type="text/javascript" src="Scripts/Plugins/js/jquery.dataTables.js"></script>

    <script type="text/javascript" src="Scripts/Plugins/js/sweetalert2.all.js"></script>

    <!--<link href="Scripts/bootstrap.css" rel="stylesheet" />-->
    <link href="Scripts/Plugins/css/jquery.dataTables.css" rel="stylesheet" />
    <!--<link href="Scripts/Plugins/css/dataTables.bootstrap.css" rel="stylesheet" />-->
    <link rel="stylesheet" href="Scripts/Plugins/css/font-awesome.min.css">
    <link rel="stylesheet" href="Scripts/Plugins/css/font-awesome.min.css">

    <link href="Scripts/Plugins/css/select2.min.css" rel="stylesheet" />
    <script type="text/javascript" src="Scripts/Plugins/js/select2.min.js"></script>

    <link rel="stylesheet" href="Scripts/Plugins/css/jquery-ui.css" />

    <script type="text/javascript" src="Scripts/Plugins/js/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/funcionesControladores/Utilidades.js"></script>
    <script type="text/javascript" src="Scripts/funcionesControladores/funcionesGeneralesAdministrador.js"></script>
    <script type="text/javascript" src="Scripts/Plugins/jquery.cookie.js"></script>

    <input type="text" runat="server" id="contenedorJSONTableros" disabled="disabled" style="display: none;" />
    <input type="text" runat="server" id="hidden_idTablero" value="1" disabled="disabled" style="display: none;" />
    <input type="text" runat="server" id="hidden_JSONTableroTemporal"  /><!--disabled="disabled" style="display: none;"-->

    <asp:Button ID="btnBorrarTablero" OnClick="btnBorrarTablero_Click" runat="server" Text="Borrar" Style="display: none;" />
    <asp:Button ID="btnActualizarTablero" OnClick="btnActualizarTablero_Click" runat="server" Text="Actualizar" Style="display: none;" />
    <asp:Button ID="btnInsertarTablero" OnClick="btnInsertarTablero_Click" runat="server" Text="Insertar" Style="display: none;" />
    <br />
    <br />
    <br />

    <div id="seccionConsultaDatos">
        <div class="row">

            <div class="col-sm-1">
            </div>
            <div class="col-sm-10">
                <table id="table_id" class="display">
                    <thead>
                        <tr>
                            <th>Id</th>
                            <th>Nombre Corto</th>
                            <th>Descripción</th>
                            <th>Liga</th>
                            <th>Activo</th>
                            <th>Acciones</th>
                        </tr>
                    </thead>
                    <tbody id="datosTabla">
                    </tbody>
                </table>
            </div>
            <div class="col-sm-1">
            </div>

        </div>
        <div class="row">
            <div class="col-sm-12">
                <br />
            </div>
        </div>
        <div class="row">

            <div class="col-sm-1">
            </div>

            <div class="col-sm-8">
            </div>

            <div class="col-sm-2">
                <input type="button" id="btnAgregar" value="Agregar Registro" />
            </div>

            <div class="col-sm-1">
            </div>

        </div>
    </div>

    <div id="seccionAdministraDatos">
        <br />
        <table style="width: 100%">
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Nombre Corto:</b>
                </td>
                <td>
                    <input id="txtNombreCortoAdm" type="text" style="width: 100%;" />
                </td>
                <td>&nbsp;&nbsp;
                </td>

            </tr>

            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Descripción:</b>
                </td>
                <td>
                    <textarea id="txtDescripcionAdm" style="width: 100%;"></textarea>
                </td>
                <td>&nbsp;&nbsp;
                </td>

            </tr>

            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <b>Liga:</b>
                </td>
                <td>
                    <input id="txtLigaAdm" type="text" style="width: 100%;" />
                </td>
                <td>&nbsp;&nbsp;
                </td>

            </tr>


            <!--INICIO GALERIA IMAGENES-->
            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td colspan="2">
                    <br />
                    <center>
                        <iframe id="IframeObtenedorImagenesPorTablero" width="1px" height="1px" scrolling="no" frameborder="0"></iframe>

                        <table align="center">
                            <tr>
                                <td valign="top">
                                    <center>
                                        <h1><b>Imagenes asignadas a este tablero</b></h1>
                                    </center>
                                    <br />
                                    <iframe id="IframeImagenesPorTablero" width="600px" height="350px" scrolling="no" frameborder="0"></iframe>
                                </td>
                                <td valign="top">
                                    <div id="botoneraOpcionesGaleria">
                                        <a href="#" id="enlaceSubirImagenes"><b>Asignar nuevas imagenes a la galería</b></a>
                                        <br />
                                        <a href="#" id="enlaceEliminarImagenes"><b>Eliminar imagenes de la galería</b></a>
                                    </div>

                                    <div id="AsignadorImagenesPorTablero">

                                        <table align="center">
                                            <tr>
                                                <td>
                                                    <center>
                                                        <h1><b>Cargador de imagenes para la galería</b></h1>
                                                        <br />
                                                        <a href="#" class="cancelarOperacionGaleriaImagenes"><b>Cancelar</b></a>
                                                    </center>
                                                    <br />
                                                    <input type="file" name="file" id="btnFileUpload" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="progressbar" style="width: 100px; display: none;">
                                                        <div id="mensajeTexto">
                                                            Cargando Archivo ...
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <div id="contenedorListaImagenes" style="max-height: 320px; overflow-y: scroll; width: 500px;"></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <center>
                                                        <a id='AsignarImgsTableroActual' href='#'>Agregar imagenes al tablero</a>
                                                    </center>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                    <div id="EliminadorImagenesPorTablero">
                                        <table align="center">
                                            <tr>
                                                <td valign="top">
                                                    <center>
                                                        <h1><b>Eliminar imagenes asignadas a este tablero</b></h1>
                                                        <br />
                                                        <a href="#" class="cancelarOperacionGaleriaImagenes"><b>Cancelar</b></a>
                                                    </center>
                                                    <br />
                                                    <div>

                                                        <table align="center">
                                                            <tr>
                                                                <td>
                                                                    <div id="contenedorListaImagenesAEliminar" style="max-height: 320px; overflow-y: scroll; width: 500px;">
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </table>


                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>




                    </center>


                </td>
                <td>&nbsp;&nbsp;
                </td>

            </tr>
            <!--FIN GALERIA IMAGENES-->

            <tr>
                <td>&nbsp;&nbsp;
                </td>
                <td>
                    <input type="button" id="btnCancelar" value="Cancelar" />
                </td>
                <td>
                    <input type="button" id="btnGuardar" value="Actualizar" />
                    <input type="button" id="btnInsertar" value="Insertar" />
                </td>
                <td>&nbsp;&nbsp;
                </td>

            </tr>
        </table>

        <!--no se ve-->
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-3">
                <b>Fecha de creación:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtFechaCreacionAdm" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <!--no se ve-->
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>Id:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>
        <!--no se ve-->
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>IdTablero:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdTableroAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <!-- no se ve-->
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>IdPerfil:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdPerfilAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <!--no se ve-->
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>Activo:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtActivoAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

        <!--no se ve-->
        <div class="row" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-2">
                <b>IdUsuarioCreo:</b>
            </div>
            <div class="col-sm-3">
                <input type="text" id="txtIdUsuarioCreoAdm" disabled="disabled" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>


        <div class="row OcultarPorInsercion" style="display: none;">
            <div class="col-sm-1">
            </div>

            <div class="col-sm-3">
                <b>Activo:</b>
            </div>
            <div class="col-sm-3">
                <input type="checkbox" id="cbActivoAdm" campotexto="txtActivoAdm" />
            </div>
            <div class="col-sm-1">
            </div>
        </div>

    </div>


    <script src="Scripts/funcionesControladores/funcionesTablero.js"></script>

    <style type="text/css">
        .x-box-inner {
            position: static;
            height: 62px;
        }

        #centerPanel {
            display: none;
        }
    </style>

    <!--nuevos scripts subidor imagenes-->
    <!--<script src="Scripts/jquery-3.3.1.js" type="text/javascript"></script>-->
    <script src="Scripts/Plugins/js/jquery-ui.js" type="text/javascript"></script>
    <script src="Scripts/Plugins/subidorArchivos/js/jquery.iframe-transport.js" type="text/javascript"></script>
    <script src="Scripts/Plugins/subidorArchivos/js/jquery.fileupload.js" type="text/javascript"></script>
    
    <script src="Scripts/Plugins/subidorArchivos/upload.js" type="text/javascript"></script>
    <!--fin nuevos scripts subidor imagenes-->

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
</asp:Content>
