﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

namespace Track_Web
{
    public partial class Rpt_KmsRecorridos : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel(Request.Form["desde"].ToString(), Request.Form["hasta"].ToString(), Request.Form["transportista"].ToString(), Request.Form["patente"].ToString());
                    return;
                default:
                    break;
            }
        }

        public void ExportExcel(string desde, string hasta, string transportista, string patente)
        {
            desde = desde.Split('-')[2].ToString() + "/" + desde.Split('-')[1].ToString() + "/" + desde.Split('-')[0].ToString();
            hasta = hasta.Split('-')[2].ToString() + "/" + hasta.Split('-')[1].ToString() + "/" + hasta.Split('-')[0].ToString();
            DateTime _desde = Utilities.convertDateIc(desde);
            DateTime _hasta = Utilities.convertDateIc(hasta);

            string now = DateTime.Now.ToString();
            now = now.Replace(" ", "_");
            now = now.Replace("-", "");
            now = now.Replace(":", "");

            Methods_Reportes _objMethosReportes = new Methods_Reportes();

            List<Track_GetRpt_KmsRecorridos_Result> _viajes = _objMethosReportes.GetRpt_KmsRecorridos(_desde, _hasta, transportista, patente);

            Response.Clear();
            Response.Buffer = true;
            //Response.ContentType = "application/vnd.ms-excel";
            Response.ContentType = "text/csv";
            Response.AppendHeader("Content-Disposition", "attachment;filename=Reporte_KmsRecorridos_" + now + ".xls");
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Response.Write(Methods_Export.HTML_RPT_KmsRecorridos(_viajes.ToList()));
            Response.End();
        }
    }
}