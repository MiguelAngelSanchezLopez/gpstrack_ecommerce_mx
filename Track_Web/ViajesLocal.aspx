﻿<%@ Page Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ViajesLocal.aspx.cs" Inherits="Track_Web.ViajesLocal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
    <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>
    <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
    <script src="Scripts/RowExpander.js" type="text/javascript"></script>

    <style type="text/css">
        .slider_background .x-panel-body {
            background-image: url('Images/slider.jpg') !important;
        }

        .x-slider-horz .x-slider-thumb {
            width: 30px;
            height: 30px;
            border: none;
            background-image: url('Images/slider_truck.png') !important;
        }

        .Legend-label label {
            color: #FAF0E6;
        }
    </style>

    <script type="text/javascript">

        var geoLayer = new Array();
        var arrayPositions = new Array();
        var arrayAlerts = new Array();
        var infowindow = new google.maps.InfoWindow();
        var arrayAlerts = new Array();
        var trafficLayer = new google.maps.TrafficLayer();
        var stackedZones = 0;

        var markerLastPosition = new Object();
        markerLastPosition.marker = null;

        var tiempoViaje = 0;
        var valSlider = 0;

        var identifierCarga = 0;
        var identifierRecepcion = 0;

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.Ajax.timeout = 660000; //11 minutes
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.proxy.Ajax, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationStatus',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data == 1) {
                            Ext.ns('App');

                            // 1 min. before Ajax.timeout
                            App.SESSION_TIMEOUT = Ext.Ajax.timeout / 60000 - 1;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            var chkNroTransporte = new Ext.form.Checkbox({
                id: 'chkNroTransporte',
                labelSeparator: '',
                hideLabel: true,
                checked: false,
                style: {
                    marginTop: '5px',
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            Ext.getCmp("textFiltroNroTransporte").setDisabled(false);
                            Ext.getCmp("dateDesde").setDisabled(true);
                            Ext.getCmp("dateHasta").setDisabled(true);
                            Ext.getCmp("comboFiltroClientes").setDisabled(true);
                            Ext.getCmp("comboFiltroTipoEtis").setDisabled(true);
                            Ext.getCmp("comboFiltroEstadoViaje").setDisabled(true);
                            Ext.getCmp("comboFiltroPOD").setDisabled(true);
                            Ext.getCmp("comboFiltroTipoCarga").setDisabled(true);
                            Ext.getCmp("textFiltroNroContenedor").setDisabled(true);
                            Ext.getCmp("comboFiltroProblema").setDisabled(true);
                            Ext.getCmp("comboFiltroOTIF").setDisabled(true);
                            Ext.getCmp("chkFueraHorario").setDisabled(true);
                            Ext.getCmp("lblFueraHorario").setDisabled(true);

                        }
                        else {
                            Ext.getCmp("textFiltroNroTransporte").setDisabled(true);
                            Ext.getCmp('textFiltroNroTransporte').reset();
                            Ext.getCmp("dateDesde").setDisabled(false);
                            Ext.getCmp("dateHasta").setDisabled(false);
                            Ext.getCmp("comboFiltroClientes").setDisabled(false);
                            Ext.getCmp("comboFiltroTipoEtis").setDisabled(false);
                            Ext.getCmp("comboFiltroEstadoViaje").setDisabled(false);
                            Ext.getCmp("comboFiltroPOD").setDisabled(false);
                            Ext.getCmp("comboFiltroTipoCarga").setDisabled(false);
                            Ext.getCmp("textFiltroNroContenedor").setDisabled(false);
                            Ext.getCmp("comboFiltroProblema").setDisabled(false);
                            Ext.getCmp("comboFiltroOTIF").setDisabled(false);
                            Ext.getCmp("chkFueraHorario").setDisabled(false);
                            Ext.getCmp("lblFueraHorario").setDisabled(false);

                        }
                    }
                }
            });

            var dateDesde = new Ext.form.DateField({
                id: 'dateDesde',
                fieldLabel: 'Desde',
                labelWidth: 70,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                maxValue: new Date(),
                style: {
                    marginTop: '3px',
                    marginLeft: '5px'
                }
            });

            var dateHasta = new Ext.form.DateField({
                id: 'dateHasta',
                fieldLabel: 'Hasta',
                labelWidth: 70,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                minValue: Ext.getCmp('dateDesde').getValue(),
                maxValue: new Date(),
                style: {
                    marginTop: '3px',
                    marginLeft: '5px'
                }
            });

            dateDesde.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _hasta.setMinValue(_desde.getValue());
                _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
                _hasta.validate();
                //FiltrarViajes();
            });

            dateHasta.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
                _desde.validate();
                //FiltrarViajes();
            });

            Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
            Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

            var textFiltroNroTransporte = new Ext.form.TextField({
                id: 'textFiltroNroTransporte',
                fieldLabel: 'Nro. Viaje',
                labelWidth: 70,
                allowBlank: true,
                anchor: '99%',
                maxLength: 20,
                style: {
                    marginTop: '3px',
                    marginLeft: '5px'
                },
                disabled: true
            });

            var textFiltroNroContenedor = new Ext.form.TextField({
                id: 'textFiltroNroContenedor',
                fieldLabel: 'Contenedor',
                labelWidth: 70,
                allowBlank: true,
                anchor: '99%',
                maxLength: 20,
                style: {
                    marginTop: '3px',
                    marginLeft: '5px'
                },
                disabled: false
            });

            var storeFiltroEstadoViaje = new Ext.data.JsonStore({
                fields: ['EstadoViaje'],
                data: [{ "EstadoViaje": "Todos" },
                        { "EstadoViaje": "Programado" },
                        { "EstadoViaje": "Asignado" },
                        { "EstadoViaje": "En Ruta" },
                        { "EstadoViaje": "En Destino" },
                        { "EstadoViaje": "Finalizado" },
                        { "EstadoViaje": "Cerrado por Sistema" },
                        { "EstadoViaje": "No Integrado" },
                ]
            });

            var comboFiltroEstadoViaje = new Ext.form.field.ComboBox({
                id: 'comboFiltroEstadoViaje',
                fieldLabel: 'Estado',
                store: storeFiltroEstadoViaje,
                valueField: 'EstadoViaje',
                displayField: 'EstadoViaje',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 70,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroEstadoViaje').setValue('Todos');

            var storeFiltroPOD = new Ext.data.JsonStore({
                fields: ['Id', 'POD'],
                data: [{ Id: 0, POD: "Todos" },
                        { Id: 1, POD: "Con POD" },
                        { Id: 2, POD: "Sin POD" }
                ]
            });

            var comboFiltroPOD = new Ext.form.field.ComboBox({
                id: 'comboFiltroPOD',
                fieldLabel: 'POD',
                store: storeFiltroPOD,
                valueField: 'Id',
                displayField: 'POD',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 70,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroPOD').setValue(0);

            var storeFiltroTipoCarga = new Ext.data.JsonStore({
                fields: ['Id', 'TipoCarga'],
                data: [{ Id: 0, TipoCarga: "Todos" },
                        { Id: 1, TipoCarga: "Carga suelta" },
                        { Id: 2, TipoCarga: "Full" },
                        { Id: 3, TipoCarga: "Vacío" }
                ]
            });

            var comboFiltroTipoCarga = new Ext.form.field.ComboBox({
                id: 'comboFiltroTipoCarga',
                fieldLabel: 'Tipo Carga',
                store: storeFiltroTipoCarga,
                valueField: 'Id',
                displayField: 'TipoCarga',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 70,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroTipoCarga').setValue(0);

            var storeFiltroProblema = new Ext.data.JsonStore({
                fields: ['Id', 'Problema'],
                data: [{ Id: 0, Problema: "Todos" },
                        { Id: 1, Problema: "Integrados" },
                        { Id: 2, Problema: "No integrados" }
                ]
            });

            var comboFiltroProblema = new Ext.form.field.ComboBox({
                id: 'comboFiltroProblema',
                fieldLabel: 'Problema',
                store: storeFiltroProblema,
                valueField: 'Id',
                displayField: 'Problema',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 70,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroProblema').setValue(0);

            var storeFiltroOTIF = new Ext.data.JsonStore({
                fields: ['Id', 'OTIF'],
                data: [{ Id: 0, OTIF: "Todos" },
                        { Id: 1, OTIF: "ON Time" },
                        { Id: 2, OTIF: "Delay" }
                ]
            });

            var comboFiltroOTIF = new Ext.form.field.ComboBox({
                id: 'comboFiltroOTIF',
                fieldLabel: 'OTIF',
                store: storeFiltroOTIF,
                valueField: 'Id',
                displayField: 'OTIF',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 70,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                disabled: false
            });

            Ext.getCmp('comboFiltroOTIF').setValue(0);

            var chkMostrarTrafico = new Ext.form.Checkbox({
                id: 'chkMostrarTrafico',
                fieldLabel: 'Mostrar tráfico',
                labelWidth: 90,
                style: {
                    marginRight: '20px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            trafficLayer.setMap(map);
                        }
                        else {
                            trafficLayer.setMap(null);
                        }
                    }
                }
            });

            var btnCargaSuelta = {
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/download_black_18x18.png',
                text: 'Exportar',
                width: 70,
                height: 24,
                listeners: {
                    click: {
                        element: 'el',
                        fn: function () {

                            var desde = Ext.getCmp('dateDesde').getRawValue();
                            var hasta = Ext.getCmp('dateHasta').getRawValue();
                            var rutCliente = Ext.getCmp('comboFiltroClientes').getValue();
                            var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();
                            var transportista = 'Todos';
                            var tipoEtis = Ext.getCmp('comboFiltroTipoEtis').getValue();
                            var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
                            var pod = Ext.getCmp('comboFiltroPOD').getRawValue();
                            var tipoCarga = Ext.getCmp('comboFiltroTipoCarga').getRawValue();
                            var nroContenedor = Ext.getCmp('textFiltroNroContenedor').getValue();
                            var otif = Ext.getCmp('comboFiltroOTIF').getRawValue();
                            var problema = Ext.getCmp('comboFiltroProblema').getRawValue();

                            switch (estadoViaje) {
                                case "Finalizado":
                                    estadoViaje = "EnLocal-P";
                                    break;
                                case "En Destino":
                                    estadoViaje = "EnLocal-R";
                                    break;
                                case "En Ruta":
                                    estadoViaje = "RUTA";
                                    break;
                                case "Asignado":
                                    estadoViaje = "ASIGNADO";
                                    break;
                                case "Programado":
                                    estadoViaje = "PREASIGNADO";
                                    break;
                                case "Cerrado por Sistema":
                                    estadoViaje = "Cerrado por Sistema";
                                    break;
                                case "Todos":
                                    estadoViaje = "Todos";
                                    break;
                                default:
                                    estadoViaje = estadoViaje;
                            }

                            var mapForm = document.createElement("form");
                            mapForm.target = "ToExcel";
                            mapForm.method = "POST"; // or "post" if appropriate
                            mapForm.action = 'ViajesLocal.aspx?Metodo=ExportExcel';

                            //
                            var _desde = document.createElement("input");
                            _desde.type = "text";
                            _desde.name = "desde";
                            _desde.value = desde;
                            mapForm.appendChild(_desde);

                            var _hasta = document.createElement("input");
                            _hasta.type = "text";
                            _hasta.name = "hasta";
                            _hasta.value = hasta;
                            mapForm.appendChild(_hasta);

                            var _rutCliente = document.createElement("input");
                            _rutCliente.type = "text";
                            _rutCliente.name = "rutCliente";
                            _rutCliente.value = rutCliente;
                            mapForm.appendChild(_rutCliente);

                            var _nroTransporte = document.createElement("input");
                            _nroTransporte.type = "text";
                            _nroTransporte.name = "nroTransporte";
                            _nroTransporte.value = nroTransporte;
                            mapForm.appendChild(_nroTransporte);

                            var _transportista = document.createElement("input");
                            _transportista.type = "text";
                            _transportista.name = "transportista";
                            _transportista.value = transportista;
                            mapForm.appendChild(_transportista);

                            var _tipoEtis = document.createElement("input");
                            _tipoEtis.type = "text";
                            _tipoEtis.name = "tipoEtis";
                            _tipoEtis.value = tipoEtis;
                            mapForm.appendChild(_tipoEtis);

                            var _estadoViaje = document.createElement("input");
                            _estadoViaje.type = "text";
                            _estadoViaje.name = "estadoViaje";
                            _estadoViaje.value = estadoViaje;
                            mapForm.appendChild(_estadoViaje);

                            var _pod = document.createElement("input");
                            _pod.type = "text";
                            _pod.name = "pod";
                            _pod.value = pod;
                            mapForm.appendChild(_pod);

                            var _tipoCarga = document.createElement("input");
                            _tipoCarga.type = "text";
                            _tipoCarga.name = "tipoCarga";
                            _tipoCarga.value = tipoCarga;
                            mapForm.appendChild(_tipoCarga);

                            var _nroContenedor = document.createElement("input");
                            _nroContenedor.type = "text";
                            _nroContenedor.name = "nroContenedor";
                            _nroContenedor.value = nroContenedor;
                            mapForm.appendChild(_nroContenedor);

                            var _otif = document.createElement("input");
                            _otif.type = "text";
                            _otif.name = "otif";
                            _otif.value = otif;
                            mapForm.appendChild(_otif);

                            var _problema = document.createElement("input");
                            _problema.type = "text";
                            _problema.name = "problema";
                            _problema.value = problema;
                            mapForm.appendChild(_problema);

                            document.body.appendChild(mapForm);
                            mapForm.submit();

                        }
                    }
                }
            };

            var btnActualizar = {
                id: 'btnActualizar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/refresh_gray_20x20.png',
                text: 'Actualizar',
                width: 80,
                height: 24,
                handler: function () {
                    FiltrarViajes();
                }
            };

            var storeFiltroClientes = new Ext.data.JsonStore({
                fields: ['RutCliente', 'NombreCliente'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetClientesUsuario&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroClientes = new Ext.form.field.ComboBox({
                id: 'comboFiltroClientes',
                fieldLabel: 'Cliente',
                forceSelection: true,
                store: storeFiltroClientes,
                valueField: 'RutCliente',
                displayField: 'NombreCliente',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 70,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                multiSelect: true,
                forceSelection: true
            });

            var storeFiltroTipoEtis = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['ETIS'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetTipoEtis&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroTipoEtis = new Ext.form.field.ComboBox({
                id: 'comboFiltroTipoEtis',
                fieldLabel: 'ETIS',
                labelWidth: 70,
                forceSelection: true,
                store: storeFiltroTipoEtis,
                valueField: 'ETIS',
                displayField: 'ETIS',
                queryMode: 'local',
                anchor: '99%',
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: false,
                forceSelection: true,
                multiSelect: true,
                style: {
                    marginLeft: '5px'
                }
            });

            storeFiltroClientes.load({
                callback: function (r, options, success) {
                    if (success) {
                        comboFiltroClientes.setValue("Todos");

                        storeFiltroTipoEtis.load({
                            callback: function (r, options, success) {
                                if (success) {
                                    comboFiltroTipoEtis.setValue('Todas');
                                }
                            }
                        })

                    }
                }
            });


            var toolbarViajes = Ext.create('Ext.toolbar.Toolbar', {
                id: 'toolbarViajes',
                height: 120,
                layout: 'column',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.05,
                    items: [chkNroTransporte, {
                        xtype: 'checkboxfield',
                        id: 'chkFueraHorario',
                        hideLabel: true,
                        style: {
                            marginTop: '65px',
                            marginLeft: '5px'
                        }
                    }]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.30,
                    items: [textFiltroNroTransporte, comboFiltroClientes, textFiltroNroContenedor, {
                        xtype: 'label',
                        id: 'lblFueraHorario',
                        text: 'Programados Fuera De Horario'

                    }]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.30,
                    items: [dateDesde, comboFiltroTipoEtis, comboFiltroPOD, comboFiltroProblema]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.30,
                    items: [dateHasta, comboFiltroEstadoViaje, comboFiltroTipoCarga, comboFiltroOTIF]
                }
                ]
            });

            var displayCantidadRegistros = new Ext.form.DisplayField({
                id: 'displayCantidadRegistros',
                fieldLabel: 'Cant. Reg.',
                labelWidth: 80,
                width: 100,
                anchor: '99%',
                value: '',
                style: {
                    //marginTop: '5px',
                    marginLeft: '15px',
                    marginRight: '15px',
                }
            });

            var displayPorcientoOnTime = new Ext.form.DisplayField({
                id: 'displayPorcientoOnTime',
                fieldLabel: '% ON Time',
                labelWidth: 80,
                width: 140,
                anchor: '99%',
                value: '',
                style: {
                    //marginTop: '5px',
                    marginLeft: '15px',
                    marginRight: '15px',
                }
            });

            var displayLegendFH = new Ext.form.DisplayField({
                id: 'displayLegendFH',
                text: '',
                labelWidth: 100,
                width: 270,
                anchor: '99%',
                value: '(Nota: Las filas de color Indican Fuera de Horario)',
                style: {
                    //marginTop: '5px',
                    marginLeft: '15px',
                    marginRight: '15px',
                    cls: 'linen-row'
                }
            });

            Ext.getCmp('displayLegendFH').setFieldStyle('color: #DC9752');

            var storeViajesRuta = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['NroTransporte',
                          'NumeroOrdenServicio',
                          'NumeroContenedor',
                          'FechaHoraPresentacion',
                          'Fecha',
                          'SecuenciaDestino',
                          'PatenteTracto',
                          'PatenteTrailer',
                          'Transportista',
                          { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                          'CodigoOrigen',
                          'NombreOrigen',
                          { name: 'FHoraSalidaOrigen', type: 'date', dateFormat: 'c' },
                          'FHAsignacion',
                          'FHSalidaOrigen',
                          'CodigoDestino',
                          'NombreDestino',
                          'FHLlegadaDestino',
                          { name: 'FHCierreSistema', type: 'date', dateFormat: 'c' },
                          'TiempoViaje',
                          'FHSalidaDestino',
                          'EstadoViaje',
                          'EstadoLat',
                          'EstadoLon',
                          'DestinoLat',
                          'DestinoLon',
                          'CantidadAlertas',
                          'CantidadAperturaPuerta',
                          'CantidadDetencion',
                          'CantidadPerdidaSenal',
                          'CantidadTemperatura',
                          'CantidadSobrestadia',
                          'CurEstadoLat',
                          'CurEstadoLon',
                          'Etis',
                          'NombreCliente',
                          'LlegadaCliente',
                          'SalidaCliente',
                          'DireccionCliente',
                          'POD',
                          'RutConductor',
                          'NombreConductor',
                          'OTIF',
                          'OTIF_AUX',
                          'Delta',
                          'TipoCarga',
                          'EstadiaCliente',
                          'Problema',
                          'PorcientoOTIF',
                          'CantDestinos'],
                //groupField: 'EstadoViaje',
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetViajesControlLocal',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });
            /*
            var groupingFeature = Ext.create('Ext.grid.feature.Grouping', {
                groupHeaderTpl: '{name} ({rows.length})'
            });
            */
            var gridPanelViajesRuta = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelViajesRuta',
                title: 'Histórico de Viajes',
                store: storeViajesRuta,
                anchor: '100% 65%',
                columnLines: true,
                tbar: toolbarViajes,
                //features: [groupingFeature],
                buttons: [displayLegendFH, displayCantidadRegistros, displayPorcientoOnTime, chkMostrarTrafico, btnCargaSuelta, btnActualizar],
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' },
                    getRowClass: function (record) {
                        return record.get('OTIF_AUX') == 'FH' ? 'linen-row' : '';
                    }

                },
                columns: [
                { text: 'Guia', sortable: true, width: 65, dataIndex: 'NroTransporte' },
                //{ text: 'Fecha', sortable: true, width: 70, dataIndex: 'Fecha', renderer: Ext.util.Format.dateRenderer('d-m-Y') },
                { text: 'Patente', sortable: true, width: 60, dataIndex: 'PatenteTracto' },
                { text: 'Cliente', sortable: true, flex: 1, dataIndex: 'NombreCliente' },
                { text: 'ETIS', sortable: true, flex: 1, dataIndex: 'Etis' },
                { text: 'Contenedor', sortable: true, width: 70, dataIndex: 'NumeroContenedor' },
                { text: 'Alertas', sortable: true, width: 50, dataIndex: 'CantidadAlertas', renderer: renderCantidadAlertas },
                { text: 'OTIF', sortable: true, width: 55, dataIndex: 'OTIF', renderer: renderOTIF },
                { text: 'Delta', sortable: true, width: 40, dataIndex: 'Delta', renderer: renderDelta },
                { text: 'Problema', sortable: true, width: 80, dataIndex: 'Problema', renderer: renderProblema },
                { text: 'Estado', sortable: true, width: 80, dataIndex: 'EstadoViaje', renderer: renderEstadoViaje },
                  {
                      xtype: 'actioncolumn',
                      renderer: function (grid, metadata, record) {
                          if (record.raw.POD == "Con POD") {
                              gridPanelViajesRuta.columns[gridPanelViajesRuta.columns.length - 1].items[0].icon = 'Images/photo_green_20x20.png';
                              gridPanelViajesRuta.columns[gridPanelViajesRuta.columns.length - 1].items[0].tooltip = "Ver POD";
                          } else {
                              gridPanelViajesRuta.columns[gridPanelViajesRuta.columns.length - 1].items[0].icon = 'Images/photo_red_20x20.png';
                              gridPanelViajesRuta.columns[gridPanelViajesRuta.columns.length - 1].items[0].tooltip = "Sin POD";
                          }
                      },
                      width: 24,
                      items: [{
                          handler: function (grid, rowIndex, colIndex) {
                              var row = grid.getStore().getAt(rowIndex);

                              ShowPanelFotografias(row);
                              Ext.getCmp("winFotografias").show();

                          }
                      }]
                  }
                ],
                listeners: {
                    select: function (sm, row, rec) {

                        var nroTransporte = row.data.NroTransporte;
                        var estadoViaje = row.data.EstadoViaje;
                        if (estadoViaje == "No Integrado") {
                            return;
                        }

                        Ext.Msg.wait('Espere por favor...', 'Generando ruta');

                        var nroOrdenServicio = row.data.NumeroOrdenServicio;
                        var nroContenedor = row.data.NumeroContenedor;
                        var fechaPresentacion = row.data.FechaHoraPresentacion.replace("T", " ");
                        var origen = row.data.CodigoOrigen;
                        var destino = row.data.CodigoDestino;
                        var direccionCliente = row.data.DireccionCliente;
                        var patenteTracto = row.data.PatenteTracto;
                        var patenteTrailer = row.data.PatenteTrailer;
                        var FechaHoraCreacion = row.data.FechaHoraCreacion;
                        var nombreConductor = row.data.NombreConductor;

                        var nombreOrigen = row.data.NombreOrigen;
                        var FHLlegadaOrigen = row.data.FHAsignacion;
                        var FHSalidaOrigen = row.data.FHSalidaOrigen;

                        var nombreCliente = row.data.NombreCliente;
                        var FHLlegadaCliente = row.data.LlegadaCliente;
                        var FHSalidaCliente = row.data.SalidaCliente;

                        var nombreDestino = row.data.NombreDestino;
                        var FHLlegadaDestino = row.data.FHLlegadaDestino;
                        var FHSalidaDestino = row.data.FHSalidaDestino;

                        var FHoraSalidaOrigen = row.data.FHoraSalidaOrigen;
                        var FHCierreSistema = row.data.FHCierreSistema;

                        var estadoLat = row.data.EstadoLat;
                        var estadoLon = row.data.EstadoLon;
                        var destinoLat = row.data.DestinoLat;
                        var destinoLon = row.data.DestinoLon;

                        var curEstadoLat = row.data.CurEstadoLat;
                        var curEstadoLon = row.data.CurEstadoLon;

                        var cantDestinos = row.data.CantDestinos;

                        Ext.getCmp('displayNroTransporte').setValue(nroTransporte);
                        Ext.getCmp('displayNroOrdenServicio').setValue(nroOrdenServicio);
                        //Ext.getCmp('displayNroContenedor').setValue(nroContenedor);
                        Ext.getCmp('displayFechaPresentacion').setValue(fechaPresentacion);
                        Ext.getCmp('displayNombreConductor').setValue(nombreConductor);

                        Ext.getCmp('displayEstado').setValue(estadoViaje);
                        Ext.getCmp('displayDireccionCliente').setValue(direccionCliente);

                        //ShowGestionView(nroTransporte, destino);
                        //SearchIdentifiers(nroTransporte, destino);

                        Ext.getCmp('txtOrigen').setValue(nombreOrigen);
                        Ext.getCmp('txtFHLlegadaOrigen').setValue(FHLlegadaOrigen);
                        Ext.getCmp('txtFHSalidaOrigen').setValue(FHSalidaOrigen);

                        Ext.getCmp('txtCliente').setValue(nombreCliente);
                        Ext.getCmp('txtFHLlegadaCliente').setValue(FHLlegadaCliente);
                        Ext.getCmp('txtFHSalidaCliente').setValue(FHSalidaCliente);

                        if (cantDestinos > 1) {
                            Ext.getCmp('txtDestino').show();
                            Ext.getCmp('txtFHLlegadaDestino').show();
                            Ext.getCmp('txtFHSalidaDestino').show();

                            Ext.getCmp('txtDestino').setValue(nombreDestino);
                            Ext.getCmp('txtFHLlegadaDestino').setValue(FHLlegadaDestino);
                            Ext.getCmp('txtFHSalidaDestino').setValue(FHSalidaDestino);
                        }
                        else {
                            Ext.getCmp('txtDestino').hide();
                            Ext.getCmp('txtFHLlegadaDestino').hide();
                            Ext.getCmp('txtFHSalidaDestino').hide();
                        }

                        var estadiaCliente = row.data.EstadiaCliente;

                        if (estadiaCliente > 240) {
                            Ext.getCmp('txtEstadiaCliente').setFieldStyle('color: #d63b3b');
                        }
                        if (estadiaCliente <= 240) {
                            Ext.getCmp('txtEstadiaCliente').setFieldStyle('color: #19c663');
                        }

                        var hrs = Math.floor(estadiaCliente / 60);
                        estadiaCliente = estadiaCliente % 60;
                        if (estadiaCliente < 10) estadiaCliente = "0" + estadiaCliente;
                        estadiaCliente = hrs + ":" + estadiaCliente + ' hrs';

                        Ext.getCmp('txtEstadiaCliente').setValue(estadiaCliente);

                        switch (row.data.OTIF) {
                            case "On Time":
                                Ext.getCmp('txtCliente').setFieldStyle('color: #19c663');
                                Ext.getCmp('txtFHLlegadaCliente').setFieldStyle('color: #19c663');
                                //Ext.getCmp('txtFHSalidaCliente').setFieldStyle('color: #19c663');
                                //Ext.getCmp('txtEstadiaCliente').setFieldStyle('color: #19c663');
                                break;
                            case "Delay":
                                Ext.getCmp('txtCliente').setFieldStyle('color: #d63b3b');
                                Ext.getCmp('txtFHLlegadaCliente').setFieldStyle('color: #d63b3b');
                                //Ext.getCmp('txtFHSalidaCliente').setFieldStyle('color: #d63b3b');
                                //Ext.getCmp('txtEstadiaCliente').setFieldStyle('color: #d63b3b');
                                break;
                            default:
                                Ext.getCmp('txtCliente').setFieldStyle('color: Black');
                                Ext.getCmp('txtFHLlegadaCliente').setFieldStyle('color: Black');
                                //Ext.getCmp('txtFHSalidaCliente').setFieldStyle('color: Black');
                                //Ext.getCmp('txtEstadiaCliente').setFieldStyle('color: Black');
                        }

                        switch (estadoViaje) {
                            case "Asignado":
                            case "Programado":
                                valSlider = 0;
                                Ext.getCmp('sliderStatus').setValue(valSlider);
                                Ext.getCmp('displayETA').setValue('');

                                break;
                            case "En Ruta Cliente":
                            case "En Ruta ":
                                valSlider = 25;
                                Ext.getCmp('sliderStatus').setValue(valSlider);

                                Ext.getCmp('displayETA').setVisible(true);

                                var now = new Date();
                                var salidaOrigen = new Date(FHoraSalidaOrigen)
                                var diffMs = (now - salidaOrigen); // milliseconds
                                tiempoViaje = Math.round(diffMs / 60000);

                                CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon, tiempoViaje);
                                break;
                            case "En Destino Cliente":
                            case "En Destino ":
                                valSlider = 50;
                                Ext.getCmp('sliderStatus').setValue(valSlider);
                                Ext.getCmp('displayETA').setValue('En Destino');

                                break;
                            case "En Destino Terminal":
                                valSlider = 100;
                                Ext.getCmp('sliderStatus').setValue(valSlider);
                                Ext.getCmp('displayETA').setValue('En Destino');

                                break;
                            case "En Ruta Deposito":
                            case "En Ruta Terminal":
                                valSlider = 75;
                                Ext.getCmp('sliderStatus').setValue(75);
                                Ext.getCmp('displayETA').setVisible(true);

                                var now = new Date();
                                var salidaOrigen = new Date(FHoraSalidaOrigen)
                                var diffMs = (now - salidaOrigen); // milliseconds
                                tiempoViaje = Math.round(diffMs / 60000);

                                CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon, tiempoViaje);
                                break;
                            case "Finalizado":
                            case "Cerrado por Sistema":
                            case "Cerrado manual":
                                valSlider = 100;
                                Ext.getCmp('sliderStatus').setValue(valSlider);
                                Ext.getCmp('displayETA').setValue('');

                                break;
                            default:
                                valSlider = 0;
                                Ext.getCmp('sliderStatus').setValue(valSlider);
                                Ext.getCmp('displayETA').setValue('');

                        }

                        ClearMap();

                        arrayPositions.splice(0, arrayPositions.length);
                        arrayAlerts.splice(0, arrayAlerts.length);

                        GetPosiciones(origen, destino, patenteTracto, patenteTrailer, FechaHoraCreacion, FHSalidaOrigen, FHLlegadaDestino, FHCierreSistema, nroTransporte, destino, estadoViaje)
                        GetAlertasRuta(nroTransporte, destino, estadoViaje);
                        MuestraUltimaPosicion(estadoLat, estadoLon, 90);

                        Ext.Ajax.request({
                            url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
                            success: function (response, opts) {

                                var task = new Ext.util.DelayedTask(function () {
                                    Ext.Msg.hide();
                                });

                                task.delay(100);

                            },
                            failure: function (response, opts) {
                                Ext.Msg.hide();
                            }
                        });

                    }
                }

            });

            var ImageModel = Ext.define('ImageModel', {
                extend: 'Ext.data.Model',
                fields: [
                    { name: 'Id' },
                    { name: 'NroTransporte' },
                    { name: 'URL' },
                    { name: 'Bloque' },

                ]
            });

            var storeImages = new Ext.data.Store({
                storeId: 'storeImages',
                model: 'ImageModel',
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetFotografiasViaje',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var storeImg = new Ext.data.Store({
                storeId: 'storeImg',
                fields: [
                    'Id',
                    'NroTransporte',
                    'URL',
                    'Bloque'
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetFotografiasViaje',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var panelImg = new Ext.create('Ext.grid.Panel', {
                id: 'panelImg',
                title: 'Listado de Imagenes',
                store: storeImg,
                height: 300,
                width: 300,
                //      anchor: '100% 90%',                
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                    {
                        text: 'Imagen', width: 300, dataIndex: 'URL',
                        renderer: function (value, metadata, record, rowIndex, colIndex, store, row) {
                            return "<center><table>" +
                                        "<tr>" +
                                            "<td colspan=2>" +
                                                "<img src='" + value + "' height=100 width=100 /> " +
                                          "</td>" +
                                        "</tr>" +
                                        "<tr>" +
                                            "<td align=center>" +
                                                "<img src='Images/download_black_18x18.png' onclick=DownloadFile('" + value + "');  height=20  width=20 />" +
                                            "</td>" +
                                            "<td align=center>" +
                                                "<img src='Images/printer_20_20.png' onclick=openPrint('" + value + "');   height=20 width=20 />" +
                                            "</td>" +
                                        "</tr>" +
                                    "</table></center>";
                        }
                    },

                ]
            });

            var panelImages = new Ext.FormPanel({
                id: 'panelImages',
                layout: 'anchor',
                border: true,
                width: 500,
                height: 320,
                autoScroll: true,
                frame: true,
                items: [panelImg],



            });

            var btnSalir = {
                id: 'btnSalir',
                xtype: 'button',
                width: 60,
                height: 24,
                iconAlign: 'left',
                icon: 'Images/back_black_20x20.png',
                text: 'Salir',
                handler: function () {
                    Ext.getCmp("winFotografias").hide();
                    Ext.getCmp("panelImages").removeAll();
                }
            };

            var winFotografias = new Ext.Window({
                id: 'winFotografias',
                title: 'Fotografías',
                width: 320,
                height: 400,
                closeAction: 'hide',
                modal: true,
                items: [panelImg],
                resizable: false,
                border: false,
                constrain: true,
                buttons: [btnSalir]
            });

            var viewWidth = Ext.getBody().getViewSize().width;
            var viewHeight = Ext.getBody().getViewSize().height;

            var toolbarEstadoViaje = Ext.create('Ext.toolbar.Toolbar', {
                id: 'toolbarEstadoViaje',
                height: 100,
                layout: 'column',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: []
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: []
                }]
            });

            var sliderStatus = Ext.create('Ext.slider.Single', {
                id: 'sliderStatus',
                width: 564,
                height: 30,
                value: 0,
                minValue: 0,
                maxValue: 100,
                style: 'padding-top:45px;padding-left:60px;padding-bottom:5px',
                renderTo: Ext.getBody(),
                listeners: {
                    drag: function (slider, e, eOpts) {
                        Ext.getCmp('sliderStatus').setValue(valSlider);
                    }

                }
            });

            var displayNroTransporte = new Ext.form.DisplayField({
                id: 'displayNroTransporte',
                fieldLabel: 'Nro. Guía Despacho',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px'
                }
            });

            var displayNroOrdenServicio = new Ext.form.DisplayField({
                id: 'displayNroOrdenServicio',
                fieldLabel: 'Nro. Orden Servicio',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px'
                }
            });

            var displayETA = new Ext.form.DisplayField({
                id: 'displayETA',
                fieldLabel: 'ETA',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px'
                }
            });

            var displayNombreConductor = new Ext.form.DisplayField({
                id: 'displayNombreConductor',
                fieldLabel: 'Conductor',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px'
                }
            });

            var displayFechaPresentacion = new Ext.form.DisplayField({
                id: 'displayFechaPresentacion',
                fieldLabel: 'Fecha / Hora Programada',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px'
                }
            });

            var displayEstado = new Ext.form.DisplayField({
                id: 'displayEstado',
                fieldLabel: 'Estado',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px',
                    marginBottom: '15px'

                }
            });

            var displayDireccionCliente = new Ext.form.DisplayField({
                id: 'displayDireccionCliente',
                fieldLabel: 'Dirección Cliente',
                labelWidth: 120,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px'
                }
            });

            var txtOrigen = new Ext.form.DisplayField({
                id: 'txtOrigen',
                fieldLabel: 'Origen',
                labelWidth: 50,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '10px'
                }
            });

            var txtFHLlegadaOrigen = new Ext.form.DisplayField({
                id: 'txtFHLlegadaOrigen',
                fieldLabel: 'Hora Llegada',
                labelWidth: 80,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '10px'
                }
            });

            var txtFHSalidaOrigen = new Ext.form.DisplayField({
                id: 'txtFHSalidaOrigen',
                fieldLabel: 'Hora Salida',
                labelWidth: 80,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '10px'
                }
            });

            var txtCliente = new Ext.form.DisplayField({
                id: 'txtCliente',
                fieldLabel: 'Cliente',
                labelWidth: 50,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtFHLlegadaCliente = new Ext.form.DisplayField({
                id: 'txtFHLlegadaCliente',
                fieldLabel: 'Hora Llegada',
                labelWidth: 80,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtFHSalidaCliente = new Ext.form.DisplayField({
                id: 'txtFHSalidaCliente',
                fieldLabel: 'Hora Salida',
                labelWidth: 80,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtDestino = new Ext.form.DisplayField({
                id: 'txtDestino',
                fieldLabel: 'Destino',
                labelWidth: 50,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtFHLlegadaDestino = new Ext.form.DisplayField({
                id: 'txtFHLlegadaDestino',
                fieldLabel: 'Hora Llegada',
                labelWidth: 80,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtFHSalidaDestino = new Ext.form.DisplayField({
                id: 'txtFHSalidaDestino',
                fieldLabel: 'Hora Salida',
                labelWidth: 80,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtEstadiaCliente = new Ext.form.DisplayField({
                id: 'txtEstadiaCliente',
                fieldLabel: 'Estadía en BC',
                labelWidth: 80,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var txtEstadiaCliente = new Ext.form.DisplayField({
                id: 'txtEstadiaCliente',
                fieldLabel: 'Estadía en BC',
                labelWidth: 80,
                readOnly: true,
                anchor: '100%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '20px'
                }
            });

            var displayTiempoViaje = new Ext.form.DisplayField({
                id: 'displayTiempoViaje',
                value: '',
                style: {
                    marginTop: '5px',
                }

            });

            var displayTiempoLocal = new Ext.form.DisplayField({
                id: 'displayTiempoLocal',
                fieldLabel: 'Tiempo hasta destino',
                labelWidth: 140,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px',
                    marginBottom: '10px'
                }
            });

            var displayDistanciaLocal = new Ext.form.DisplayField({
                id: 'displayDistanciaLocal',
                fieldLabel: 'Distancia hasta destino',
                labelWidth: 140,
                anchor: '99%',
                value: '',
                style: {
                    marginTop: '5px',
                    marginLeft: '15px',
                    marginBottom: '30px'
                }
            });

            var sliderPanel = new Ext.FormPanel({
                id: 'sliderPanel',
                frame: false,
                layout: 'column',
                anchor: '100% 100%',
                renderTo: Ext.getBody(),
                cls: 'slider_background',
                style: {
                    backgroundImage: 'url(\'Images/slider.jpg\') !important',
                    border: '0px'
                },
                items: [
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.4,
                    items: [displayNroTransporte, displayFechaPresentacion, displayEstado]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.6,
                    items: [displayNroOrdenServicio, displayDireccionCliente, displayETA, displayNombreConductor]
                },
                 {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 1,
                     items: [sliderStatus]
                 }, {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 0.32,
                     items: [txtOrigen, txtFHLlegadaOrigen, txtFHSalidaOrigen]
                 }, {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 0.34,
                     items: [txtCliente, txtFHLlegadaCliente, txtFHSalidaCliente, txtEstadiaCliente]
                 }, {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 0.34,
                     items: [txtDestino, txtFHLlegadaDestino, txtFHSalidaDestino]
                 }, {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 1,
                     items: [displayTiempoViaje]
                 }]
            });

            var numberNroTransporte = new Ext.form.NumberField({
                fieldLabel: 'Nro. Transporte',
                id: 'numberNroTransporte',
                anchor: '99%',
                labelWidth: 140,
                hideTrigger: true
            });

            var numberLocalDestino = new Ext.form.NumberField({
                fieldLabel: 'Local',
                id: 'numberLocalDestino',
                anchor: '99%',
                labelWidth: 140,
                hideTrigger: true,
                style: {
                    marginLeft: '20px'
                },
            });

            var textFechaAsignacion = new Ext.form.TextField({
                fieldLabel: 'Fecha asignación',
                id: 'textFechaAsignacion',
                anchor: '99%',
                labelWidth: 140
            });

            var textFechaSalidaOrigen = new Ext.form.TextField({
                fieldLabel: 'Fecha salida origen',
                id: 'textFechaSalidaOrigen',
                anchor: '99%',
                labelWidth: 140,
                style: {
                    marginLeft: '20px'
                },
            });

            var textFechaLlegadaDestino = new Ext.form.TextField({
                fieldLabel: 'Fecha llegada destino',
                id: 'textFechaLlegadaDestino',
                anchor: '99%',
                labelWidth: 140
            });

            var textFechaSalidaDestino = new Ext.form.TextField({
                fieldLabel: 'Fecha salida destino',
                id: 'textFechaSalidaDestino',
                anchor: '99%',
                labelWidth: 140,
                style: {
                    marginLeft: '20px'
                },
            });

            var storeGestionCallCenter = new Ext.data.JsonStore({
                autoLoad: false,
                fields: [{ name: 'FechaCreacion', type: 'date', dateFormat: 'c' },
                        'NombreAlerta',
                        'AtendidoPor',
                        'NombreContacto',
                        'Explicacion',
                        'Observacion'
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxAlertas.aspx?Metodo=GetGestionCallCenter',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelGestionCallCenter = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelGestionCallCenter',
                store: storeGestionCallCenter,
                anchor: '100% 100%',
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                            { text: 'Fecha Creación', sortable: true, width: 105, dataIndex: 'FechaCreacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                            { text: 'Nombre alerta', sortable: true, flex: 1, dataIndex: 'NombreAlerta' },
                            { text: 'Atendido por', sortable: true, flex: 1, dataIndex: 'AtendidoPor' },
                            { text: 'Contacto', sortable: true, flex: 1, dataIndex: 'NombreContacto' },
                            { text: 'Explicación', sortable: true, flex: 1, dataIndex: 'Explicacion' },
                            { text: 'Observación', sortable: true, width: 300, dataIndex: 'Observacion' }
                ]
            });

            var storeAlertasPorGestionarCallCenter = new Ext.data.JsonStore({
                autoLoad: false,
                fields: [{ name: 'FechaCreacion', type: 'date', dateFormat: 'c' },
                          'IdAlerta',
                          'NombreAlerta',
                          'NroTransporte',
                          'LocalDestinoCodigo',
                          'NombreTransportista',
                          'Clasificacion',
                          'Prioridad'
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxAlertas.aspx?Metodo=GetAlertasPorGestionarCallCenter',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelAlertasPorGestionarCallCenter = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelAlertasPorGestionarCallCenter',
                store: storeAlertasPorGestionarCallCenter,
                anchor: '100% 100%',
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                            { text: 'Fecha Creación', sortable: true, width: 105, dataIndex: 'FechaCreacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                            { text: 'Nombre alerta', sortable: true, flex: 1, dataIndex: 'NombreAlerta' },
                            { text: 'Transportista', sortable: true, flex: 1, dataIndex: 'NombreTransportista' },
                            { text: 'Estado', sortable: true, flex: 1, dataIndex: 'Clasificacion' },
                            { text: 'Prioridad', sortable: true, width: 60, dataIndex: 'Prioridad' }
                ]
            });

            var tabPanelGestionCallCenter = Ext.create('Ext.tab.Panel', {
                id: 'tabPanelAlertasCallCenter',
                activeTab: 0,
                width: 980,
                height: 210,
                plain: true,
                items: [
                {
                    title: 'Alertas por gestionar',
                    layout: 'anchor',
                    items: [gridPanelAlertasPorGestionarCallCenter]
                }, {
                    title: 'Alertas gestionadas',
                    layout: 'anchor',
                    items: [gridPanelGestionCallCenter]
                }]
            });

            var formDetalleViaje = new Ext.FormPanel({
                id: 'formDetalleViaje',
                border: false,
                frame: true,
                height: 300,
                layout: 'column',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: [numberNroTransporte, textFechaAsignacion, textFechaLlegadaDestino]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.5,
                    items: [numberLocalDestino, textFechaSalidaOrigen, textFechaSalidaDestino]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [tabPanelGestionCallCenter]
                }]
            });

            var btnMostarEnMapa = {
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/showmap_gray_16x16.png',
                text: 'Ver en Mapa',
                width: 90,
                height: 24,
                handler: function () {
                    ShowMapViaje(Ext.getCmp("gridPanelViajesRuta").getStore().getAt(lastSelected));

                }
            };

            var btnSalirDetalleViaje = {
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/back_black_16x16.png',
                text: 'Salir',
                width: 70,
                height: 24,
                handler: function () {
                    winDetalleViaje.close();

                    Ext.getCmp('textFechaAsignacion').reset();
                    Ext.getCmp('textFechaSalidaOrigen').reset();
                    Ext.getCmp('textFechaLlegadaDestino').reset();
                    Ext.getCmp('textFechaSalidaDestino').reset();
                    Ext.getCmp('gridPanelGestionCallCenter').store.removeAll();
                }
            };

            var winDetalleViaje = new Ext.Window({
                id: 'winDetalleViaje',
                title: 'Detalle Viaje',
                width: 1000,
                closeAction: 'hide',
                modal: true,
                items: formDetalleViaje,
                resizable: false,
                border: false,
                constrain: true,
                buttons: [btnSalirDetalleViaje]
            });

            var panelMap = new Ext.FormPanel({
                id: 'panelMap',
                border: true,
                height: viewHeight - Ext.getCmp('sliderPanel').getHeight() - 58,
                contentEl: 'dvMap'
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                title: 'Estado del Viaje',
                region: 'center',
                margins: '1 0 3 3',
                border: true,
                width: 650,
                minWidth: 650,
                maxWidth: 650,
                hideCollapseTool: true,
                anchor: '100%',
                layout: 'column',
                items: [
                 {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 1,
                     items: [sliderPanel]
                 }, {
                     xtype: 'container',
                     layout: 'anchor',
                     columnWidth: 1,
                     items: [panelMap]
                 }]
            });


            Ext.EventManager.onWindowResize(function (w, h) {
                Ext.getCmp("panelMap").setHeight(Ext.getBody().getViewSize().height - Ext.getCmp('sliderPanel').getHeight() - 70);
                google.maps.event.trigger(map, "resize");
            });

            var storeZonasToDraw = new Ext.data.JsonStore({
                id: 'storeZonasToDraw',
                autoLoad: false,
                fields: ['IdZona'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonasToDraw',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridZonasToDraw = Ext.create('Ext.grid.Panel', {
                id: 'gridZonasToDraw',
                store: storeZonasToDraw,
                columns: [
                        { text: 'IdZona', flex: 1, dataIndex: 'IdZona' }
                ]

            });

            var storePosicionesRuta = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Patente',
                        'IdTipoMovil',
                        'NombreTipoMovil',
                        { name: 'Fecha', type: 'date', dateFormat: 'c' },
                        'Latitud',
                        'Longitud',
                        'Velocidad',
                        'Direccion',
                        'Ignicion',
                        'Puerta1',
                        'Temperatura1'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetPosicionesRuta',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPosicionesRuta = Ext.create('Ext.grid.Panel', {
                id: 'gridPosicionesRuta',
                store: storePosicionesRuta,
                columns: [
                            { text: 'Patente', dataIndex: 'Patente', hidden: true },
                            { text: 'IdTipoMovil', dataIndex: 'IdTipoMovil', hidden: true },
                            { text: 'NombreTipoMovil', dataIndex: 'NombreTipoMovil', hidden: true },
                            { text: 'Fecha', dataIndex: 'Fecha', hidden: true, renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                            { text: 'Latitud', dataIndex: 'Latitud', hidden: true },
                            { text: 'Longitud', dataIndex: 'Longitud', hidden: true },
                            { text: 'Velocidad', dataIndex: 'Velocidad', hidden: true },
                            { text: 'Direccion', dataIndex: 'Direccion', hidden: true },
                            { text: 'Ignicion', dataIndex: 'Ignicion', hidden: true }
                ]
            });

            var storeAlertasRuta = new Ext.data.JsonStore({
                autoLoad: false,
                fields: [{ name: 'FechaInicioAlerta', type: 'date', dateFormat: 'c' },
                       { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                        'PatenteTracto',
                        'TextFechaCreacion',
                        'PatenteTrailer',
                        'Velocidad',
                        'Latitud',
                        'Longitud',
                        'DescripcionAlerta',
                        'Ocurrencia',
                        'Puerta1',
                        'Temp1'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxAlertas.aspx?Metodo=GetAlertasRuta',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelAlertasRuta = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelAlertasRuta',
                title: 'Alertas',
                store: storeAlertasRuta,
                anchor: '100% 35%',
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                            { text: 'Fecha Inicio', sortable: true, width: 105, dataIndex: 'FechaInicioAlerta', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                            { text: 'Fecha Envío', sortable: true, width: 105, dataIndex: 'FechaHoraCreacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                            { text: 'Descripción', sortable: true, flex: 1, dataIndex: 'DescripcionAlerta' },
                ],
                listeners: {
                    select: function (sm, row, rec) {

                        var date = Ext.getCmp('gridPanelAlertasRuta').getStore().data.items[rec].raw.FechaHoraCreacion.toString();

                        for (var i = 0; i < markers.length; i++) {
                            if (markers[i].labelText == date) {
                                markers[i].setAnimation(google.maps.Animation.BOUNCE);
                                setTimeout('markers[' + i + '].setAnimation(null);', 800);

                                var contentString =

                                  '<br>' +
                                      '<table>' +
                                        '<tr>' +
                                            '       <td><b>Fecha</b></td>' +
                                            '       <td><pre>     </pre></td>' +
                                            '       <td>' + row.data.TextFechaCreacion + '</td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '        <td><b>Velocidad:</b></td>' +
                                            '       <td><pre>     </pre></td>' +
                                            '        <td>' + row.data.Velocidad + ' Km/h </td>' +
                                        '</tr>' +
                                        '<tr>' +
                                            '        <td><b>Descripción:</b></td>' +
                                            '       <td><pre>     </pre></td>' +
                                            '        <td>' + row.data.DescripcionAlerta + '</td>' +
                                        '</tr>' +

                                      '</table>' +
                                    '<br>';

                                infowindow.setContent(contentString);
                                infowindow.open(map, markers[i]);

                                break;
                            }
                        }

                        map.setCenter(new google.maps.LatLng(row.data.Latitud, row.data.Longitud));

                    }
                }
            });

            var leftPanel = new Ext.FormPanel({
                id: 'leftPanel',
                region: 'west',
                margins: '0 0 3 3',
                border: true,
                width: '100%',
                hideCollapseTool: true,
                items: [gridPanelViajesRuta, gridPanelAlertasRuta]
            });

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, leftPanel, centerPanel]
            });
        });

    </script>

    <script type="text/javascript">

        Ext.onReady(function () {
            GeneraMapa("dvMap", true);
        });

        function FiltrarViajes() {

            if (markerLastPosition.marker != null) {
                if (markerLastPosition.marker.map != null) {
                    markerLastPosition.marker.setMap(null);
                }
            }

            Ext.getCmp('displayNroTransporte').setValue('');
            Ext.getCmp('displayNroOrdenServicio').setValue('');
            Ext.getCmp('displayETA').setValue('');
            Ext.getCmp('displayNombreConductor').setValue('');
            Ext.getCmp('displayFechaPresentacion').setValue('');
            Ext.getCmp('displayEstado').setValue('');
            Ext.getCmp('displayDireccionCliente').setValue('');

            Ext.getCmp('txtOrigen').setValue('');
            Ext.getCmp('txtFHLlegadaOrigen').setValue('');
            Ext.getCmp('txtFHSalidaOrigen').setValue('');

            Ext.getCmp('txtCliente').setValue('');
            Ext.getCmp('txtFHLlegadaCliente').setValue('');
            Ext.getCmp('txtFHSalidaCliente').setValue('');

            Ext.getCmp('txtDestino').setValue('');
            Ext.getCmp('txtFHLlegadaDestino').setValue('');
            Ext.getCmp('txtFHSalidaDestino').setValue('');
            Ext.getCmp('txtEstadiaCliente').setValue('');

            Ext.getCmp('txtDestino').show();
            Ext.getCmp('txtFHLlegadaDestino').show();
            Ext.getCmp('txtFHSalidaDestino').show();

            Ext.getCmp('sliderStatus').setValue(0);

            Ext.getCmp("gridPanelViajesRuta").getStore().removeAll();

            Ext.getCmp('displayCantidadRegistros').setValue('');
            Ext.getCmp('displayPorcientoOnTime').setValue('');

            var desde = Ext.getCmp('dateDesde').getValue();
            var hasta = Ext.getCmp('dateHasta').getValue();
            var rutCliente = Ext.getCmp('comboFiltroClientes').getValue();
            var nroTransporte = Ext.getCmp('textFiltroNroTransporte').getValue();
            var transportista = 'Todos';
            var tipoEtis = Ext.getCmp('comboFiltroTipoEtis').getValue();
            var estadoViaje = Ext.getCmp('comboFiltroEstadoViaje').getValue();
            var pod = Ext.getCmp('comboFiltroPOD').getRawValue();
            var tipoCarga = Ext.getCmp('comboFiltroTipoCarga').getRawValue();
            var nroContenedor = Ext.getCmp('textFiltroNroContenedor').getValue();
            var otif = Ext.getCmp('comboFiltroOTIF').getRawValue();
            var problema = Ext.getCmp('comboFiltroProblema').getRawValue();
            var _filFh = Ext.getCmp('chkFueraHorario').getRawValue();

            switch (estadoViaje) {
                case "Finalizado":
                    estadoViaje = "EnLocal-P";
                    break;
                case "En Destino":
                    estadoViaje = "EnLocal-R";
                    break;
                case "En Ruta":
                    estadoViaje = "RUTA";
                    break;
                case "Asignado":
                    estadoViaje = "ASIGNADO";
                    break;
                case "Programado":
                    estadoViaje = "PREASIGNADO";
                    break;
                case "Cerrado por Sistema":
                    estadoViaje = "Cerrado por Sistema";
                    break;
                case "Todos":
                    estadoViaje = "Todos";
                    break;
                default:
                    estadoViaje = estadoViaje;
            }

            var storeViajes = Ext.getCmp('gridPanelViajesRuta').store;
            storeViajes.load({
                params: {
                    desde: desde,
                    hasta: hasta,
                    rutCliente: rutCliente,
                    nroTransporte: nroTransporte,
                    transportista: transportista,
                    tipoEtis: tipoEtis,
                    estadoViaje: estadoViaje,
                    tipoCarga: tipoCarga,
                    pod: pod,
                    otif: otif,
                    nroContenedor: nroContenedor,
                    problema: problema,
                    FilFH : _filFh
                },
                callback: function (r, options, success) {
                    if (Ext.getCmp('gridPanelViajesRuta').getStore().count() > 0) {
                        Ext.getCmp('displayCantidadRegistros').setValue(Ext.getCmp('gridPanelViajesRuta').getStore().count());
                        Ext.getCmp('displayPorcientoOnTime').setValue(Ext.getCmp('gridPanelViajesRuta').store.data.items[0].data.PorcientoOTIF);
                    }
                }
            });
        }

        function CalculateDistanceTime(estadoLat, estadoLon, destinoLat, destinoLon) {

            var service = new google.maps.DistanceMatrixService();
            var origen = new google.maps.LatLng(estadoLat, estadoLon);
            var destino = new google.maps.LatLng(destinoLat, destinoLon);

            service.getDistanceMatrix(
            {
                origins: [origen],
                destinations: [destino],
                travelMode: google.maps.TravelMode.DRIVING,
                unitSystem: google.maps.UnitSystem.METRIC,
                avoidHighways: false,
                avoidTolls: false
            }, callbackDistanceMatrix);
        }

        function callbackDistanceMatrix(response, status) {
            if (status == google.maps.DistanceMatrixStatus.OK) {

                var distance = response.rows[0].elements[0].distance.text;
                var time = response.rows[0].elements[0].duration.value / 60;

                var tiempoTotal = tiempoViaje + (time);
                var sliderValue = ((tiempoViaje * 100) / tiempoTotal) / 2 + 24

                var tiempoViajeHM = ((tiempoViaje - (tiempoViaje % 60)) / 60).toString() + ":" + ((tiempoViaje % 60) < 10 ? "0" : "") + (tiempoViaje % 60).toString();
                var tiempoLocalHM = ((Math.round(time) - (Math.round(time) % 60)) / 60).toString() + ":" + ((Math.round(time) % 60) < 10 ? "0" : "") + (Math.round(time) % 60).toString();

                var fechaEstimadaLlegada = new Date();
                fechaEstimadaLlegada.setMinutes(fechaEstimadaLlegada.getMinutes() + time - 180);

                Ext.getCmp('displayETA').setValue(fechaEstimadaLlegada.toISOString().slice(0, 16).replace('T', ' '));

            }
        }

        var renderEstadoViaje = function (value, meta) {

            switch (value) {
                case "Cerrado por Sistema":
                case "No Integrado":
                    meta.tdCls = 'red-cell';
                    return value;
                    break;
                case "Cerrado manual":
                    meta.tdCls = 'red-cell';
                    return value;
                    break;
                case "Finalizado":
                    meta.tdCls = 'green-cell';
                    return value;
                    break;
                case "Programado":
                    return value;
                    break;
                default:
                    meta.tdCls = 'blue-cell';
                    return value;
                    break;
            }


        };

        var renderProblema = function (value, meta) {

            if (value == "Integrado") {
                meta.tdCls = 'green-cell';
                return value;
            }
            else {
                meta.tdCls = 'red-cell';
                return value;
            }
        };

        var renderOTIF = function (value, meta) {

            if (value == "On Time") {
                meta.tdCls = 'green-cell';
                return value;
            }
            else {
                meta.tdCls = 'red-cell';
                return value;
            }
        };

        var renderDelta = function (value, meta) {
            {
                if (value >= 15) {
                    meta.tdCls = 'red-cell';
                    return value;
                }
                else {
                    meta.tdCls = 'green-cell';
                    return value;
                }
            }
        };

        var renderCantidadAlertas = function (value, meta) {
            {
                if (value >= 1) {
                    meta.tdCls = 'red-cell';
                    return value;
                }
                else {
                    return value;
                }
            }
        };

        function ShowDetalleViaje(row) {

            var nroTransporte = row.data.NroTransporte;
            var destino = row.data.CodigoDestino;

            var fecLlegadaOrigen = row.data.FHAsignacion;
            var fecSalidaOrigen = row.data.FHSalidaOrigen;
            var fecLlegadaDestino = row.data.FHLlegadaDestino;
            var fecSalidaDestino = row.data.FHSalidaDestino;

            Ext.getCmp('numberNroTransporte').setValue(nroTransporte);
            Ext.getCmp('numberLocalDestino').setValue(destino);
            Ext.getCmp('textFechaAsignacion').setValue(fecLlegadaOrigen);
            Ext.getCmp('textFechaSalidaOrigen').setValue(fecSalidaOrigen);
            Ext.getCmp('textFechaLlegadaDestino').setValue(fecLlegadaDestino);
            Ext.getCmp('textFechaSalidaDestino').setValue(fecSalidaDestino);

            var storeGestionCallCenter = Ext.getCmp('gridPanelGestionCallCenter').store;
            var storeAlertasPorGestionarCallCenter = Ext.getCmp('gridPanelAlertasPorGestionarCallCenter').store;

            storeGestionCallCenter.load({
                params: {
                    nroTransporte: nroTransporte,
                    codLocal: destino
                },
                callback: function (r, options, success) {
                    if (success) {
                        storeAlertasPorGestionarCallCenter.load({
                            params: {
                                nroTransporte: nroTransporte,
                                codLocal: destino
                            }
                        });
                    }
                }
            });

            Ext.getCmp("winDetalleViaje").show();

        }

        function GetAlertasRuta(nroTransporte, destino, estadoViaje) {

            var store = Ext.getCmp('gridPanelAlertasRuta').store;
            store.load({
                params: {
                    nroTransporte: nroTransporte,
                    destino: destino,
                    estadoViaje: estadoViaje
                },
                callback: function (r, options, success) {

                    MuestraAlertasViaje();

                }
            });
        }

        function GetPosiciones(origen, destino, patenteTracto, patenteTrailer, fechaHoraCreacion, fechaHoraSalidaOrigen, fechaHoraLlegadaDestino, fechaHoraCierreSistema, nroTransporte, destino, estadoViaje) {

            Ext.getCmp('gridPosicionesRuta').store.removeAll();

            var store = Ext.getCmp('gridPosicionesRuta').store;
            var storeZone = Ext.getCmp('gridZonasToDraw').store;

            var fec;

            if (estadoViaje == 'Finalizado') {
                fec = fechaHoraLlegadaDestino;
            }
            if (estadoViaje == 'Cerrado por Sistema') {
                fec = fechaHoraCierreSistema;
            }
            if (fec == null) {
                fec = new Date();
            }

            store.load({
                params: {
                    patenteTracto: patenteTracto,
                    patenteTrailer: patenteTrailer,
                    fechaHoraCreacion: fechaHoraCreacion,
                    fechaHoraSalidaOrigen: fechaHoraSalidaOrigen,
                    fechaHoraLlegadaDestino: fechaHoraLlegadaDestino,
                    nroTRansporte: nroTransporte,
                    destino: destino,
                    estadoViaje: estadoViaje
                },
                callback: function (r, options, success) {
                    if (success) {

                        storeZone.load({
                            params: {
                                fechaDesde: fechaHoraCreacion,
                                fechaHasta: fec,
                                patente1: patenteTrailer,
                                patente2: patenteTracto
                            },
                            callback: function (r, options, success) {
                                if (success) {

                                    MuestraRutaViaje();

                                    var store = Ext.getCmp('gridZonasToDraw').getStore();
                                    for (var i = 0; i < store.count() ; i++) {
                                        DrawZone(store.getAt(i).data.IdZona);
                                    }

                                    DrawZone(origen);
                                    var storeViajes = Ext.getCmp('gridPanelViajesRuta').store;

                                    for (var i = 0; i < storeViajes.count() ; i++) {
                                        if (storeViajes.getAt(i).data.NroTransporte == nroTransporte) {
                                            DrawZone(storeViajes.getAt(i).data.CodigoDestino);
                                        }
                                    }

                                }
                            }

                        });

                    }

                }
            });

        }

        function MuestraRutaViaje() {

            var store = Ext.getCmp('gridPosicionesRuta').getStore();
            var rowCount = store.count();
            var iterRow = 0;

            while (iterRow < rowCount) {

                var dir = parseInt(store.data.items[iterRow].raw.Direccion);

                var lat = store.data.items[iterRow].raw.Latitud;
                var lon = store.data.items[iterRow].raw.Longitud;

                var Latlng = new google.maps.LatLng(lat, lon);

                arrayPositions.push({
                    Fecha: store.data.items[iterRow].raw.Fecha.toString(),
                    Velocidad: store.data.items[iterRow].raw.Velocidad,
                    Latitud: lat,
                    Longitud: lon,
                    LatLng: Latlng
                });

                if (store.data.items[iterRow].raw.Velocidad > 0) {

                    switch (true) {
                        case ((dir >= 338) || (dir < 22)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/1_arrowcircle_blue_N_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 22) && (dir < 67)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/2_arrowcircle_blue_NE_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 67) && (dir < 112)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/3_arrowcircle_blue_E_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 112) && (dir < 157)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/4_arrowcircle_blue_SE_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 157) && (dir < 202)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/5_arrowcircle_blue_S_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 202) && (dir < 247)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/6_arrowcircle_blue_SW_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 247) && (dir < 292)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/7_arrowcircle_blue_W_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                        case ((dir >= 292) && (dir < 338)):
                            marker = new google.maps.Marker({
                                position: Latlng,
                                icon: 'Images/Circle_Arrow/8_arrowcircle_blue_NW_20x20.png',
                                map: map,
                                labelText: store.data.items[iterRow].raw.Fecha.toString()
                            });
                            break;
                    }
                }
                else {
                    marker = new google.maps.Marker({
                        position: Latlng,
                        icon: 'Images/dot_red_16x16.png',
                        map: map,
                        labelText: store.data.items[iterRow].raw.Fecha.toString()
                    });
                }

                var label = new Label({
                    map: null
                });
                label.bindTo('position', marker, 'position');
                label.bindTo('text', marker, 'labelText');

                google.maps.event.addListener(marker, 'click', function () {
                    var latLng = this.position;
                    var fec = this.labelText;

                    for (i = 0; i < arrayPositions.length; i++) {
                        if (arrayPositions[i].Fecha.toString() == fec.toString() & arrayPositions[i].LatLng.toString() == latLng.toString()) {

                            var Lat = arrayPositions[i].Latitud;
                            var Lon = arrayPositions[i].Longitud;

                            var contentString =

                                '<br>' +
                                    '<table>' +
                                      '<tr>' +
                                          '       <td><b>Fecha</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '       <td>' + (arrayPositions[i].Fecha.toString()).replace("T", " ") + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Velocidad:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayPositions[i].Velocidad + ' Km/h </td>' +
                                      '</tr>' +
                                    '</table>' +
                                  '<br>';

                            infowindow.setContent(contentString);
                            infowindow.open(map, this);

                            break;
                        }
                    }

                });

                markers.push(marker);
                labels.push(label);


                iterRow++;
            }

            if (rowCount > 0) {
                map.setCenter(markers[markers.length - 1].position);
            }

        }

        function MuestraAlertasViaje() {

            var store = Ext.getCmp('gridPanelAlertasRuta').getStore();
            var rowCount = store.count();
            var iterRow = 0;

            while (iterRow < rowCount) {
                var descrip = store.data.items[iterRow].raw.DescripcionAlerta;

                var lat = store.data.items[iterRow].raw.Latitud;
                var lon = store.data.items[iterRow].raw.Longitud;

                var Latlng = new google.maps.LatLng(lat, lon);

                arrayAlerts.push({
                    Fecha: store.data.items[iterRow].raw.FechaHoraCreacion.toString(),
                    TextFechaCreacion: store.data.items[iterRow].raw.TextFechaCreacion,
                    Velocidad: store.data.items[iterRow].raw.Velocidad,
                    Latitud: lat,
                    Longitud: lon,
                    LatLng: Latlng,
                    Descripcion: store.data.items[iterRow].raw.DescripcionAlerta
                });

                switch (true) {
                    case (descrip == 'LLEGADA A DESTINO'):
                        marker = new google.maps.Marker({
                            position: Latlng,
                            icon: 'Images/finishflag_24x24.png',
                            map: map,
                            labelText: store.data.items[iterRow].raw.FechaHoraCreacion.toString()
                        });
                        break;
                    default:
                        marker = new google.maps.Marker({
                            position: Latlng,
                            icon: 'Images/alert_orange_22x22.png',
                            map: map,
                            labelText: store.data.items[iterRow].raw.FechaHoraCreacion.toString()
                        });
                        break;
                }

                var label = new Label({
                    map: null
                });
                label.bindTo('position', marker, 'position');
                label.bindTo('text', marker, 'labelText');

                google.maps.event.addListener(marker, 'click', function () {

                    var latLng = this.position;
                    var fec = this.labelText;

                    for (i = 0; i < arrayAlerts.length; i++) {
                        if (arrayAlerts[i].Fecha.toString() == fec.toString() & arrayAlerts[i].LatLng.toString() == latLng.toString()) {

                            var contentString =

                                '<br>' +
                                    '<table>' +
                                      '<tr>' +
                                          '       <td><b>Fecha</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '       <td>' + arrayAlerts[i].TextFechaCreacion + '</td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Velocidad:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayAlerts[i].Velocidad + ' Km/h </td>' +
                                      '</tr>' +
                                      '<tr>' +
                                          '        <td><b>Descripción:</b></td>' +
                                          '       <td><pre>     </pre></td>' +
                                          '        <td>' + arrayAlerts[i].Descripcion + '</td>' +
                                      '</tr>' +

                                    '</table>' +
                                  '<br>';

                            infowindow.setContent(contentString);
                            infowindow.open(map, this);

                            break;
                        }
                    }

                });

                markers.push(marker);
                labels.push(label);

                iterRow++;
            }

        }

        function DrawZone(idZona) {

            for (var i = 0; i < geoLayer.length; i++) {
                geoLayer[i].layer.setMap(null);
                geoLayer[i].label.setMap(null);
                geoLayer.splice(i, 1);
            }

            var colorZone = "#7f7fff";
            var arrayColors = ['#0000ff', '#66cd00', '#ff4040', '#98f5ff', '#bf3eff', '#ff7f24', '#6495ed', '#ff1493', '#76ee00', '#caff70',
                               '#c1ffc1', '#97ffff', '#ffb90f', '#228b22', '#ffbbff', '#40e0d0', '#ffe7ba', '#ffff00', '#cd8c95', '#bdb76b']

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
                params: {
                    IdZona: idZona
                },
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        if (data.Vertices.length > 1) { //Polygon
                            var polygonGrid = new Object();
                            polygonGrid.IdZona = data.IdZona;

                            var arr = new Array();
                            for (var i = 0; i < data.Vertices.length; i++) {
                                arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
                            }

                            Ext.Ajax.request({
                                url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonesInsideZona',
                                params: {
                                    IdZona: idZona
                                },
                                success: function (data2, success) {
                                    if (data2 != null) {
                                        data2 = Ext.decode(data2.responseText);

                                        var colorZone = "#7f7fff";

                                        if (data.idTipoZona == 3) {
                                            colorZone = "#FF0000";
                                        }

                                        if (data2 >= 1) {

                                            colorZone = arrayColors[stackedZones];

                                            stackedZones = stackedZones + 1;
                                            if (stackedZones >= 20) {
                                                stackedZones = 0;
                                            }
                                        }

                                        polygonGrid.layer = new google.maps.Polygon({
                                            paths: arr,
                                            strokeColor: "#000000",
                                            strokeWeight: 1,
                                            strokeOpacity: 0.9,
                                            fillColor: colorZone,
                                            fillOpacity: 0.3,
                                            labelText: data.NombreZona
                                        });
                                        polygonGrid.label = new Label({
                                            position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                            //map: viewLabel ? map : null
                                            map: map
                                        });
                                        polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
                                        polygonGrid.layer.setMap(map);

                                        //if (centrarMapa == true || centrarMapa == null) {
                                        //    map.setCenter(new google.maps.LatLng(data.Latitud, data.Longitud));
                                        //}
                                        geoLayer.push(polygonGrid);
                                    }
                                }
                            })

                            //polygonGrid.layer = new google.maps.Polygon({
                            //    paths: arr,
                            //    strokeColor: "#000000",
                            //    strokeWeight: 1,
                            //    strokeOpacity: 0.9,
                            //    fillColor: colorZone,
                            //    fillOpacity: 0.3,
                            //    labelText: data.NombreZona
                            //});
                            //polygonGrid.label = new Label({
                            //    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                            //    map: map
                            //});
                            //polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
                            //polygonGrid.layer.setMap(map);
                            //geoLayer.push(polygonGrid);
                        }
                        else
                            if (data.Vertices.length = 1) { //Point
                                var Point = new Object();
                                Point.IdZona = data.IdZona;

                                var image = new google.maps.MarkerImage("Images/greymarker_32x32.png");

                                Point.layer = new google.maps.Marker({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    icon: image,
                                    labelText: data.NombreZona,
                                    map: map
                                });

                                Point.label = new Label({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    map: map
                                });

                                Point.label.bindTo('text', Point.layer, 'labelText');
                                Point.layer.setMap(map);
                                geoLayer.push(Point);
                            }

                    }
                }
            });
        }

        function ShowPanelFotografias(row) {
            Ext.getCmp("panelImages").removeAll();

            var store = Ext.data.StoreManager.lookup('storeImages');
            var store2 = Ext.data.StoreManager.lookup('storeImg');

            store2.load({
                params: {
                    nroTransporte: row.data.NroTransporte
                },
            });


            //store.load({
            //    params: {
            //        nroTransporte: row.data.NroTransporte
            //    },
            //    callback: function (r, options, success) {
            //        if (!success) {
            //            Ext.MessageBox.show({
            //                title: 'Error',
            //                msg: 'Se ha producido un error.',
            //                buttons: Ext.MessageBox.OK
            //            });
            //        }
            //        else {

            //            if (store.count() == 0) {
            //                Ext.getCmp("panelImages").add(Ext.create('Ext.Img', {
            //                    src: 'Images/SinFotografias_250x70.png',
            //                    width: 229,
            //                    height: 68,
            //                    style: {
            //                        marginTop: '3px',
            //                        marginLeft: '5px'
            //                    }
            //                }));
            //            }

            //            for (var i = 0; i < store.count() ; i++) {
            //                var url = store.data.items[i].data.URL;

            //                Ext.getCmp("panelImages").add(Ext.create('Ext.Img', {
            //                    src: url,
            //                    height: 150,
            //                    width: 150,
            //                    style: {
            //                        marginTop: '3px',
            //                        marginLeft: '5px'
            //                    }
            //                }));

            //            }
            //        }
            //    }
            //});
        }

        function renderImage(value, metaData, record, rowIndex, colIndex, store) {
            metaData.attr = 'style="cursor: pointer"';
            return '<img class="imageZoomCls" src="' + value + '" width="100" height="100" />';
        }

        function MuestraUltimaPosicion(estadoLat, estadoLon, dir) {

            if (markerLastPosition.marker != null) {
                if (markerLastPosition.marker.map != null) {
                    markerLastPosition.marker.setMap(null);
                }
            }

            var Latlng = new google.maps.LatLng(estadoLat, estadoLon);

            var marker = new google.maps.Marker({
                position: Latlng,
                icon: 'Images/Truck_Loaded/3_E_30x22.png',
                map: map,
                title: 'Última posición'
            });

            markerLastPosition.marker = marker;
        }

        function openPrint(source) {
            window.open("PrintPage.aspx?Url=" + source + "&Form=Print");
        }

        function DownloadFile(pathfile) {
            window.open("PrintPage.aspx?Url=" + pathfile + "&Form=Down");
        }



    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
    <div id="dvMap"></div>
</asp:Content>

