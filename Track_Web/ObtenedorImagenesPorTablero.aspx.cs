﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;

namespace Track_Web
{
    public partial class ObtenedorImagenesPorTablero : System.Web.UI.Page
    {
        ControladorTablerosPorPerfil controladorTablerosPorPerfil = new ControladorTablerosPorPerfil();

        private List<string> listadoImagenesPorTablero;

        protected void Page_Load(object sender, EventArgs e)
        {
            ArmarJSDespliegaGaleriaImagenesTablero();
        }

        public void ArmarJSDespliegaGaleriaImagenesTablero()
        {
            StringBuilder scriptJSGenerado = new StringBuilder();
            try
            {
                listadoImagenesPorTablero = controladorTablerosPorPerfil.ObtenerImagenesPorIdTablero(Convert.ToInt32(Request.QueryString["IdTablero"]));

                StringBuilder cadenaListadoImagenes = new StringBuilder();

                foreach (string imagen in listadoImagenesPorTablero)
                    cadenaListadoImagenes.Append("'" + imagen + "',");

                string listadoImagenesCadena = cadenaListadoImagenes.ToString();
                listadoImagenesCadena = listadoImagenesCadena.ToString().Substring(0, listadoImagenesCadena.Length - 1);

                scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" >\n");

                scriptJSGenerado.Append("var arregloImagenesRecibido = [");
                scriptJSGenerado.Append(listadoImagenesCadena);
                scriptJSGenerado.Append("];");
                scriptJSGenerado.Append("$(document).ready(function () {");

                scriptJSGenerado.Append("var arregloImagenesJSON = JSON.stringify(arregloImagenesRecibido);");
                scriptJSGenerado.Append("$.cookie('arregloImagenesCookie', arregloImagenesJSON);");

                scriptJSGenerado.Append("});");
                scriptJSGenerado.Append("\n\n </script>");
            }
            catch (Exception ex)
            {
                scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" >\n");

                scriptJSGenerado.Append("var arregloImagenesRecibido = [");
                scriptJSGenerado.Append("'Images/sinImagenes.jpg'");
                scriptJSGenerado.Append("];");
                scriptJSGenerado.Append("$(document).ready(function () {");

                scriptJSGenerado.Append("var arregloImagenesJSON = JSON.stringify(arregloImagenesRecibido);");
                scriptJSGenerado.Append("$.cookie('arregloImagenesCookie', arregloImagenesJSON);");

                scriptJSGenerado.Append("});");
                scriptJSGenerado.Append("\n\n </script>");
            }


            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptDespliegaGaleriaImagenesTablero", scriptJSGenerado.ToString(), false);

        }

    }
}