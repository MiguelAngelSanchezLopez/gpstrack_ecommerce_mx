﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace perfilamientoUsuarios.ModelosBD
{
    public class Track_Paginas
    {
        public int Id { get; set; }
        public string Nombre { get; set; }
        public string NombreArchivo { get; set; }
        public bool Activo { get; set; }
    }
}