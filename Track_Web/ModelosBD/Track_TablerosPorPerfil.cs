﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Track_Web.ModelosBD
{
    public class Track_TablerosPorPerfil
    {
        public int Id { get; set; }
        public int IdTablero { get; set; }
        public int IdPerfil { get; set; }
        public DateTime FechaCreacion { get; set; }
        public int IdUsuarioCreo { get; set; }
        public Boolean Activo { get; set; }
    }
}