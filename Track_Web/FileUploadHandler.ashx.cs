﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using Track_Web.ControladoresBD;
using Track_Web.ModelosBD;

namespace Track_Web
{
    /// <summary>
    /// Descripción breve de FileUploadHandler
    public class FileUploadHandler : IHttpHandler
    {
        ControladorImagenesParaTablero controladorImagenesParaTableros = new ControladorImagenesParaTablero();

        public void ProcessRequest(HttpContext context)
        {
            string rutaCarpeta = "Images";

            try
            {
                if (context.Request.QueryString["OtroProceso"] != null) {
                    OtroProceso(context);
                }

                if (context.Request.QueryString["ObtenerListaImagenesAsignadasAlTablero"] != null)
                    ObtenerListaImagenesAsignadasAlTablero(context);

                if (context.Request.QueryString["EliminarRegistroImagenPorTableroPorId"] != null)
                    EliminarRegistroImagenPorTableroPorId(context);

                if (context.Request.QueryString["upload"] != null)
                {
                    string pathrefer = context.Request.UrlReferrer.ToString();
                    string Serverpath = HttpContext.Current.Server.MapPath(rutaCarpeta);

                    var postedFile = context.Request.Files[0];

                    string file;

                    //For IE to get file name
                    if (HttpContext.Current.Request.Browser.Browser.ToUpper() == "IE")
                    {
                        string[] files = postedFile.FileName.Split(new char[] { '\\' });
                        file = files[files.Length - 1];
                    }
                    else
                    {
                        file = postedFile.FileName;
                    }


                    if (!Directory.Exists(Serverpath))
                        Directory.CreateDirectory(Serverpath);

                    string fileDirectory = Serverpath;
                    if (context.Request.QueryString["fileName"] != null)
                    {
                        file = context.Request.QueryString["fileName"];
                        if (File.Exists(fileDirectory + "\\" + file))
                        {
                            File.Delete(fileDirectory + "\\" + file);
                        }
                    }

                    string ext = Path.GetExtension(fileDirectory + "\\" + file);

                    file = Guid.NewGuid() + ext;

                    fileDirectory = Serverpath + "\\" + file;

                    postedFile.SaveAs(fileDirectory);

                    context.Response.AddHeader("Vary", "Accept");
                    try
                    {
                        if (context.Request["HTTP_ACCEPT"].Contains("application/json"))
                            context.Response.ContentType = "application/json";
                        else
                            context.Response.ContentType = "text/plain";
                    }
                    catch
                    {
                        context.Response.ContentType = "text/plain";
                    }

                    string rutaCompletaImagen = (rutaCarpeta + "/" + file);

                    int nuevoIdImagen = InsertarNuevaImagenParaTablero(rutaCompletaImagen);

                    string respuestaArmada = rutaCompletaImagen + "," + nuevoIdImagen;

                    context.Response.Write(respuestaArmada); //"Success"
                }
            }
            catch (Exception exp)
            {
                context.Response.Write(exp.Message);
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }

        public int InsertarNuevaImagenParaTablero(string rutaCompletaImagen){
            //controladorImagenesParaTableros.InsertarRegistroImagenParaTablero(rutaCompletaImagen);
            return controladorImagenesParaTableros.InsertarRegistroImagenParaTableroObtieneIdImagen(rutaCompletaImagen);
        }

        public void OtroProceso(HttpContext context) {
            
            string delimitador = "enviarJSON&";

            string cadenaRecibida = context.Request.Url.ToString();

            cadenaRecibida = cadenaRecibida.Substring((cadenaRecibida.IndexOf(delimitador) + delimitador.Length));
            cadenaRecibida = cadenaRecibida.Substring(0, cadenaRecibida.IndexOf("&"));
            
            List<Track_ImagenesPorTablero> ListaImagenesPorTableroTemporal = JsonConvert.DeserializeObject<List<Track_ImagenesPorTablero>>(cadenaRecibida);

            ControladorImagenesPorTablero controladorImagenesPorTablero = new ControladorImagenesPorTablero();

            foreach (Track_ImagenesPorTablero imagenParaTablero in ListaImagenesPorTableroTemporal)
                controladorImagenesPorTablero.InsertarRegistroImagenPorTablero(imagenParaTablero.IdImagen,imagenParaTablero.IdTablero);
            
            //Console.WriteLine("se hace otro proceso");
            context.Response.Write("se hizo otro proceso");
        }

        public void ObtenerListaImagenesAsignadasAlTablero(HttpContext context)
        {

            string delimitador = "ObtenerListaImagenesAsignadasAlTablero=";

            string cadenaRecibida = context.Request.Url.ToString();

            cadenaRecibida = cadenaRecibida.Substring((cadenaRecibida.IndexOf(delimitador) + delimitador.Length));
            //cadenaRecibida = cadenaRecibida.Substring(0, cadenaRecibida.IndexOf("&"));

            //List<Track_ImagenesPorTablero> ListaImagenesPorTableroTemporal = JsonConvert.DeserializeObject<List<Track_ImagenesPorTablero>>(cadenaRecibida);

            ControladorImagenesPorTablero controladorImagenesPorTablero = new ControladorImagenesPorTablero();

            context.Response.ContentType = "application/json";

            context.Response.Write(
                controladorImagenesPorTablero.ObtenerTodasImagenesYRelacionesPorTableros(
                    Convert.ToInt32(
                        cadenaRecibida
                    )
                )
            );

            //foreach (Track_ImagenesPorTablero imagenParaTablero in ListaImagenesPorTableroTemporal)
            //    controladorImagenesPorTablero.InsertarRegistroImagenPorTablero(imagenParaTablero.IdImagen, imagenParaTablero.IdTablero);

            //Console.WriteLine("se hace otro proceso");
            //context.Response.Write("se hizo otro proceso");
        }

        public void EliminarRegistroImagenPorTableroPorId(HttpContext context) {
            string delimitador = "EliminarRegistroImagenPorTableroPorId=";

            string cadenaRecibida = context.Request.Url.ToString();

            cadenaRecibida = cadenaRecibida.Substring((cadenaRecibida.IndexOf(delimitador) + delimitador.Length));

            ControladorImagenesPorTablero controladorImagenesPorTablero = new ControladorImagenesPorTablero();

            controladorImagenesPorTablero.EliminarRegistroImagenPorTableroPorId(
                Convert.ToInt32(
                    cadenaRecibida
                )
            );
        }
    }
}