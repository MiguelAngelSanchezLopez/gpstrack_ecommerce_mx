﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="PrintPage.aspx.cs" Inherits="Track_Web.PrintPage" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<script>
    function myFunction() {
        window.print();
        setTimeout(function () { window.close(); }, 1000);
    }
</script>
<body>
    <form id="form1" runat="server">
        <div>
            <img id="PrintImage" runat="server" />
        </div>
    </form>
</body>
</html>
