﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Rpt_Alertas.aspx.cs" Inherits="Track_Web.Rpt_Alertas" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Title" runat="server">
    AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
    <!--<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=visualization&key=AIzaSyDFhE-5S6P5dI1Q1mFjpgGKKmcbTiM0GbY" type="text/javascript"></script>-->
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=visualization&key=AIzaSyDKLevfrbLESV7ebpmVxb9P7XRRKE1ypq8" type="text/javascript"></script>
    <script src="Scripts/MapFunctions.js" type="text/javascript"></script>
    <script src="Scripts/markerclusterer.js" type="text/javascript"></script>
    <script src="Scripts/TopMenu.js" type="text/javascript"></script>
    <script src="Scripts/LabelMarker.js" type="text/javascript"></script>
    <script src="Scripts/OverlappingMarkerSpiderfier.min.js" type="text/javascript"></script>

    <script type="text/javascript">

        var checkedZones = new Array();
        var heatMapData = new Array();
        var heatMap;
        var markerCluster;
        var oms;
        var geoLayer = new Array();
        var arrayAlerts = new Array();
        var infowindow = new google.maps.InfoWindow();
        var markersVertices = new Array();

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.Ajax.timeout = 3600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                               /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data > 0) {
                            Ext.ns('App');

                            //Session timeout in secons     
                            App.SESSION_TIMEOUT = data;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            var dateDesde = new Ext.form.DateField({
                id: 'dateDesde',
                fieldLabel: 'Desde',
                labelWidth: 100,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                maxValue: new Date(),
                style: {
                    marginLeft: '5px'
                }
            });

            var hourDesde = {
                xtype: 'timefield',
                id: 'hourDesde',
                allowBlank: false,
                format: 'H:i',
                minValue: '00:00',
                maxValue: '23:59',
                increment: 10,
                anchor: '100%',
                editable: true,
                value: '00:00'
            };

            var dateHasta = new Ext.form.DateField({
                id: 'dateHasta',
                fieldLabel: 'Hasta',
                labelWidth: 100,
                allowBlank: false,
                anchor: '99%',
                format: 'd-m-Y',
                editable: false,
                value: new Date(),
                minValue: Ext.getCmp('dateDesde').getValue(),
                maxValue: new Date(),
                style: {
                    marginLeft: '5px'
                }
            });

            var hourHasta = {
                xtype: 'timefield',
                id: 'hourHasta',
                allowBlank: false,
                format: 'H:i',
                minValue: '00:00',
                maxValue: '23:59',
                increment: 10,
                anchor: '100%',
                editable: true,
                value: new Date()
            };

            dateDesde.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _hasta.setMinValue(_desde.getValue());
                _hasta.setMaxValue(Ext.Date.add(_desde.getValue(), Ext.Date.DAY, 60));
                _hasta.validate();
            });

            dateHasta.on('change', function () {
                var _desde = Ext.getCmp('dateDesde');
                var _hasta = Ext.getCmp('dateHasta');

                _desde.setMinValue(Ext.Date.add(_hasta.getValue(), Ext.Date.DAY, -60));
                _desde.validate();
            });

            Ext.getCmp('dateDesde').setMinValue(Ext.Date.add(Ext.getCmp('dateHasta').getValue(), Ext.Date.DAY, -60));
            Ext.getCmp('dateHasta').setMaxValue(Ext.Date.add(Ext.getCmp('dateDesde').getValue(), Ext.Date.DAY, 60));

            var storeFiltroTransportista = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Transportista'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllTransportistas&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroTransportista = new Ext.form.field.ComboBox({
                id: 'comboFiltroTransportista',
                fieldLabel: 'Línea Transporte',
                labelWidth: 100,
                forceSelection: true,
                store: storeFiltroTransportista,
                valueField: 'Transportista',
                displayField: 'Transportista',
                queryMode: 'local',
                anchor: '99%',
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                multiSelect: true,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (field, newVal) {
                        if (newVal != null) {
                            FiltrarPatentes();
                            FiltrarProveedoresGPS();
                        }
                        Ext.getCmp("comboFiltroPatente").setValue("Todas");

                        Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
                    }
                }
            });

            var storeFiltroProveedorGPS = new Ext.data.JsonStore({
                fields: ['ProveedorGPS'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetProveedoresGPS&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroProveedorGPS = new Ext.form.field.ComboBox({
                id: 'comboFiltroProveedorGPS',
                fieldLabel: 'Proveedor GPS',
                forceSelection: true,
                store: storeFiltroProveedorGPS,
                valueField: 'ProveedorGPS',
                displayField: 'ProveedorGPS',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                multiSelect: true

            });

            storeFiltroProveedorGPS.load({
                callback: function (r, options, success) {
                    if (success) {
                        FiltrarProveedoresGPS();
                        Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
                    }
                }
            })

            var storeFiltroPatente = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Patente'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxViajes.aspx?Metodo=GetAllPatentes&Todas=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroPatente = new Ext.form.field.ComboBox({
                id: 'comboFiltroPatente',
                fieldLabel: 'Placa',
                labelWidth: 100,
                store: storeFiltroPatente,
                valueField: 'Patente',
                displayField: 'Patente',
                queryMode: 'local',
                anchor: '99%',
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: true,
                forceSelection: true,
                allowBlank: false,
                style: {
                    marginLeft: '5px'
                }
            });

            storeFiltroTransportista.load({
                callback: function (r, options, success) {
                    if (success) {

                        var firstTransportista = Ext.getCmp("comboFiltroTransportista").store.getAt(0).get("Transportista");
                        Ext.getCmp("comboFiltroTransportista").setValue(firstTransportista);

                        storeFiltroPatente.load({
                            callback: function (r, options, success) {
                                if (success) {
                                    Ext.getCmp("comboFiltroPatente").setValue("Todas");
                                }
                            }
                        })

                        storeFiltroProveedorGPS.load({
                            params: {
                                Transportista: firstTransportista
                            },
                            callback: function (r, options, success) {
                                if (success) {
                                    Ext.getCmp("comboFiltroProveedorGPS").setValue("Todos");
                                }
                            }
                        })
                    }
                }
            })

            var storeFiltroScoreConductor = new Ext.data.JsonStore({
                fields: ['Score'],
                data: [{ Score: 'Todos' },
                         { Score: 'Blanco' },
                         { Score: 'Verde' },
                         { Score: 'Amarillo' },
                         { Score: 'Rojo' }
                ]
            });

            var comboFiltroScoreConductor = new Ext.form.field.ComboBox({
                id: 'comboFiltroScoreConductor',
                fieldLabel: 'Score conductor',
                store: storeFiltroScoreConductor,
                valueField: 'Score',
                displayField: 'Score',
                emptyText: 'Seleccione...',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
            });

            Ext.getCmp('comboFiltroScoreConductor').setValue('Todos');

            var storeFiltroConductores = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['RutConductor', 'NombreConductor'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetConductores&Todos=True',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroConductores = new Ext.form.field.ComboBox({
                id: 'comboFiltroConductores',
                fieldLabel: 'Conductor',
                allowBlank: false,
                store: storeFiltroConductores,
                valueField: 'RutConductor',
                displayField: 'NombreConductor',
                queryMode: 'local',
                anchor: '99%',
                forceSelection: true,
                enableKeyEvents: true,
                editable: true,
                labelWidth: 100,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                listConfig: {
                    loadingText: 'Buscando...',
                    getInnerTpl: function () {
                        return '<a class="search-item">' +
                                        '<span>Rut: {RutConductor}</span><br />' +
                                        '<span>Nombre: {NombreConductor}</span>' +
                                    '</a>';
                    }
                }
            });

            storeFiltroConductores.load({
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp("comboFiltroConductores").setValue("Todos");
                    }
                }
            });

            var storeFiltroAlertas = new Ext.data.JsonStore({
                fields: ['NombreTipoAlerta'],
                data: [{ NombreTipoAlerta: 'RUTA' },
                          { NombreTipoAlerta: 'OPERACIONAL' },
                          { NombreTipoAlerta: 'TIENDAS' },
                ]
            });

            var comboFiltroAlertas = new Ext.form.field.ComboBox({
                id: 'comboFiltroAlertas',
                fieldLabel: 'Tipo alerta',
                store: storeFiltroAlertas,
                valueField: 'NombreTipoAlerta',
                displayField: 'NombreTipoAlerta',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                editable: false,
                multiSelect: true,
                allowBlank: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true
            });

            Ext.getCmp("comboFiltroAlertas").setValue("RUTA");

            var storeFiltroDescripcionAlerta = new Ext.data.JsonStore({
                fields: ['DescripcionAlerta'],
                data: [     { DescripcionAlerta: 'Todas' },
                            { DescripcionAlerta: 'DETENCION NO AUTORIZADA' },
                            { DescripcionAlerta: 'DETENCION PROLONGADA ZONA AUTORIZADA' },
                            { DescripcionAlerta: 'DETENCION PROLONGADA ZONA NO AUTORIZADA' },
                            { DescripcionAlerta: 'EXCESO DE VELOCIDAD' },
                            { DescripcionAlerta: 'MOVIMIENTO FUERA DE HORARIO' },
                            { DescripcionAlerta: 'PERDIDA SEÑAL' },
                            { DescripcionAlerta: 'SOBRETIEMPO EN DESCARGA DROP' }
                ]
            });

            var comboFiltroDescripcionAlerta = new Ext.form.field.ComboBox({
                id: 'comboFiltroDescripcionAlerta',
                fieldLabel: 'Descripción alerta',
                store: storeFiltroDescripcionAlerta,
                valueField: 'DescripcionAlerta',
                displayField: 'DescripcionAlerta',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                editable: false,
                allowBlank: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true
            });

            Ext.getCmp("comboFiltroDescripcionAlerta").setValue("Todas");

            var storeFiltroFormatos = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['Id', 'Nombre'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetFormatos&Todos=True',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroFormatos = new Ext.form.field.ComboBox({
                id: 'comboFiltroFormatos',
                fieldLabel: 'Formato',
                allowBlank: false,
                store: storeFiltroFormatos,
                valueField: 'Id',
                displayField: 'Nombre',
                queryMode: 'local',
                anchor: '99%',
                forceSelection: true,
                enableKeyEvents: true,
                editable: true,
                labelWidth: 100,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                listeners: {
                    change: function (field, newVal) {
                        if (newVal != null) {
                            FiltrarLocales();
                        }
                    }
                }
            });

            storeFiltroFormatos.load({
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp("comboFiltroFormatos").setValue(0);
                    }
                }
            });

            var storeFiltroLocales = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['CodigoInterno', 'IdFormato', 'NumeroLocal'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetLocales',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroLocales = new Ext.form.field.ComboBox({
                id: 'comboFiltroLocales',
                fieldLabel: 'Tienda',
                store: storeFiltroLocales,
                valueField: 'CodigoInterno',
                displayField: 'NumeroLocal',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                allowBlank: false,
                editable: true,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
                listeners: {
                    select: function () {
                        //FiltrarFlota();
                    }
                }
            });

            storeFiltroLocales.load({
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp("comboFiltroLocales").setValue(0);
                    }
                }
            });

            var storeFiltroPermiso = new Ext.data.JsonStore({
                fields: ['Permiso'],
                data: [{ Permiso: 'Todas' },
                         { Permiso: 'AUTORIZADA' },
                         { Permiso: 'NO AUTORIZADA' }
                ]
            });

            var comboFiltroPermiso = new Ext.form.field.ComboBox({
                id: 'comboFiltroPermiso',
                fieldLabel: 'Permiso',
                store: storeFiltroPermiso,
                valueField: 'Permiso',
                displayField: 'Permiso',
                emptyText: 'Seleccione...',
                queryMode: 'local',
                anchor: '99%',
                labelWidth: 100,
                editable: false,
                style: {
                    marginLeft: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                forceSelection: true,
            });

            Ext.getCmp('comboFiltroPermiso').setValue('Todas');

            var radioGroupViewMapType = new Ext.form.RadioGroup({
                id: 'radioGroupViewMapType',
                fieldLabel: 'Tipo de mapa',
                labelWidth: 100,
                // Arrange radio buttons into three columns, distributed vertically
                columns: 1,
                style: {
                    marginLeft: '5px'
                },
                vertical: true,
                items: [
                    { boxLabel: 'Mapa de calor', name: 'mt', inputValue: '1', checked: true },
                    { boxLabel: 'Concentración', name: 'mt', inputValue: '2' }
                ]
            });

            var chkMostrarZonas = new Ext.form.Checkbox({
                id: 'chkMostrarZonas',
                fieldLabel: 'Mostrar Zonas',
                labelWidth: 100,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            MostrarZonas();
                        }
                        else {
                            eraseAllZones();
                        }
                    }
                }
            });

            var chkMostrarLabels = new Ext.form.Checkbox({
                id: 'chkMostrarLabels',
                fieldLabel: 'Mostrar Etiquetas',
                labelWidth: 100,
                style: {
                    marginLeft: '5px'
                },
                listeners: {
                    change: function (cb, checked) {
                        if (checked == true) {
                            for (var i = 0; i < zoneLabels.length; i++) {
                                zoneLabels[i].setMap(map);
                            }
                        }
                        else {
                            for (var i = 0; i < zoneLabels.length; i++) {
                                zoneLabels[i].setMap(null);
                            }
                        }
                    }
                }
            });

            var storeFiltroTipoZona = new Ext.data.JsonStore({
                fields: ['IdTipoZona', 'NombreTipoZona'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetTipoZonas&Todos=True',
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroTipoZona = new Ext.form.field.ComboBox({
                id: 'comboFiltroTipoZona',
                fieldLabel: 'Filtrar Tipo',
                labelWidth: 85,
                forceSelection: true,
                store: storeFiltroTipoZona,
                valueField: 'IdTipoZona',
                displayField: 'NombreTipoZona',
                queryMode: 'local',
                anchor: '99%',
                style: {
                    marginTop: '5px'
                },
                emptyText: 'Seleccione...',
                enableKeyEvents: true,
                editable: false,
                listeners: {
                    change: function (field, newVal) {
                        if (newVal != null) {
                            FiltrarZonas();
                        }
                    }
                }
            });

            /********************************************************/

            storeFiltroTipoZona.load({
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp('comboFiltroTipoZona').select(1);
                    }
                }
            })

            var textFiltroNombre = new Ext.form.TextField({
                fieldLabel: 'Filtrar Nombre',
                id: 'textFiltroNombre',
                allowBlank: true,
                labelWidth: 85,
                anchor: '99%',
                style: {
                    marginTop: '5px'
                },
                maxLength: 20,
                enableKeyEvents: true,
                listeners: {
                    'change': {
                        fn: function (a, b, c) {
                            FiltrarZonas();
                        }
                    }
                }
            });

            var toolbarZona = Ext.create('Ext.toolbar.Toolbar', {
                id: 'toolbarZona',
                height: 65,
                layout: 'anchor',
                items: [comboFiltroTipoZona, textFiltroNombre]
            });

            var storeZonas = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboZonas = new Ext.form.field.ComboBox({
                id: 'comboZonas',
                store: storeZonas
            });

            storeZonas.load({
                params: {
                    idTipoZona: 1,
                    nombreZona: '',
                    EstadoMx: 0
                }
            });
            


            var btnMostrarTodas = {
                id: 'btnMostrarTodas',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/showall_black_20x20.png',
                width: 105,
                height: 26,
                text: 'Mostrar Todas',
                handler: function () {
                    MostrarTodas();
                }
            };

            var btnCancelar = {
                id: 'btnCancelar',
                xtype: 'button',
                width: 100,
                height: 26,
                iconAlign: 'left',
                icon: 'Images/eraser_20x20.png',
                text: 'Borrar zonas',
                handler: function () {

                    var _ckId = new Array();
                    for (var i = 0; i < checkedZones.length; i++) {
                        document.getElementById('viewCheck' + checkedZones[i].toString()).checked = false;
                        _ckId.push(checkedZones[i].toString());
                    }
                    for (var i = 0; i < _ckId.length; i++) {
                        var _check = new Object();
                        _check.checked = false;
                        CheckRow(_check, _ckId[i], true);
                    }

                }
            };

            var gridPanelZonas = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelZonas',
                title: 'Zonas',
                store: storeZonas,
                height: 450,
                columnLines: true,
                tbar: toolbarZona,
                buttons: [btnMostrarTodas, btnCancelar],
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                            {
                                text: 'Ver', width: 27, dataIndex: 'IdZona',
                                renderer: function (value, cell) {
                                    ClearPoints();
                                    var _ck = false;
                                    for (var i = 0; i < checkedZones.length; i++) {
                                        if (checkedZones[i] == value) {
                                            _ck = true;
                                        }
                                    }
                                    return '<input id="viewCheck' + value.toString() + '" type="checkbox" onclick="CheckRow(this, \'' + value.toString() + '\' );" ' + (_ck ? 'checked' : '') + '/>';
                                }
                            },
                              { text: 'Id Zona', sortable: true, width: 50, dataIndex: 'IdZona' },
                              { text: 'Nombre', sortable: true, flex: 1, dataIndex: 'NombreZona' },
                              { text: 'IdTipoZona', sortable: true, flex: 1, dataIndex: 'IdTipoZona', hidden: true },
                              { text: 'NombreTipoZona', sortable: true, flex: 1, dataIndex: 'NombreTipoZona', hidden: true }
                              
                ],
                listeners: {
                    select: function (sm, row, rec) {
                        viewIdZona = row.data.IdZona;
                        viewIdTipoZona = row.data.IdTpoZona;

                    }
                }
            });
            
            var progBar = Ext.create('Ext.MessageBox', {
                id: 'progBar',
                title: 'Espere por favor'
            });

            var btnBuscar = {
                id: 'btnBuscar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/searchreport_black_20x20.png',
                text: 'Buscar',
                width: 90,
                height: 26,
                handler: function () {
                    Buscar();
                }
            };

            var panelFilters = new Ext.FormPanel({
                id: 'panelFilters',
                title: 'Filtros',
                anchor: '100% 100%',
                bodyStyle: 'padding: 5px;',
                layout: 'column',
                items: [{
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.75,
                    items: [dateDesde, dateHasta]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 0.24,
                    items: [hourDesde, hourHasta]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroTransportista]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroProveedorGPS]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroPatente]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroScoreConductor]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroConductores]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroFormatos]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroLocales]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroAlertas]
                },
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroDescripcionAlerta]
                },
                {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [comboFiltroPermiso]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [radioGroupViewMapType]
                }, {
                    xtype: 'container',
                    layout: 'anchor',
                    columnWidth: 1,
                    items: [gridPanelZonas]
                }],
                buttons: [btnBuscar]
            });

            var storeAlertas = new Ext.data.JsonStore({
                autoLoad: false,
                fields: [{ name: 'FechaInicioAlerta', type: 'date', dateFormat: 'c' },
                            { name: 'FechaHoraCreacion', type: 'date', dateFormat: 'c' },
                            'TextFechaCreacion',
                            'NroTransporte',
                            'IdEmbarque',
                            'CodigoDestino',
                            'NombreDestino',
                            'TipoAlerta',
                            'DescripcionAlerta',
                            'PatenteTracto',
                            'PatenteTrailer',
                            'Transportista',
                            'Velocidad',
                            'Latitud',
                            'Longitud',
                            'Ocurrencia',
                            'Temp1',
                            'Permiso',
                            'Formato',
                            'EstadoViaje',
                            'NombreConductor',
                            'ScoreConductor',
                            'Discrepancia'
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetRpt_Alertas',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelAlertas = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelAlertas',
                title: 'Alertas',
                store: storeAlertas,
                anchor: '100% 100%',
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                          { text: 'Fecha Inicio', sortable: true, width: 110, dataIndex: 'FechaInicioAlerta', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                          { text: 'Fecha Envío', sortable: true, width: 110, dataIndex: 'FechaHoraCreacion', renderer: Ext.util.Format.dateRenderer('d-m-Y H:i') },
                          { text: 'Id Master', sortable: true, dataIndex: 'NroTransporte' },
                          { text: 'Id Embarque', sortable: true, dataIndex: 'IdEmbarque' },
                          { text: 'Destino', sortable: true, dataIndex: 'NombreDestino' },
                          { text: 'Tracto', sortable: true, dataIndex: 'PatenteTracto' },
                          { text: 'Remolque', sortable: true, dataIndex: 'PatenteTrailer' },
                          { text: 'Velocidad', sortable: true, dataIndex: 'Velocidad' },
                          { text: 'Descripción', sortable: true, flex: 1, dataIndex: 'DescripcionAlerta' }
                ]
            });

            var viewWidth = Ext.getBody().getViewSize().width;
            var viewHeight = Ext.getBody().getViewSize().height;

            var leftPanel = new Ext.FormPanel({
                id: 'leftPanel',
                region: 'west',
                border: true,
                margins: '0 0 3 3',
                width: 300,
                minWidth: 200,
                maxWidth: 400,
                layout: 'anchor',
                anchor: '100% 100%',
                collapsible: true,
                titleCollapsed: true,
                split: true,
                items: [panelFilters]
            });

            leftPanel.on('collapse', function () {
                google.maps.event.trigger(map, "resize");
                google.maps.event.trigger(map2, "resize");
                Ext.getCmp('rightPanel').setWidth(viewWidth / 2 - 50);
            });

            leftPanel.on('expand', function () {
                google.maps.event.trigger(map, "resize");
                google.maps.event.trigger(map2, "resize");
                Ext.getCmp('rightPanel').setWidth(viewWidth / 2.7);
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 0 3 0',
                anchor: '100% 100%',
                contentEl: 'dvMap1'
            });
            
            var imageStatusTracto = Ext.create('Ext.Img', {
              id: 'imageSenalizacion',
              height: '100%',
              width: '100%',
              src: 'Images/senalizaciones.png'
            });

            var winSenalizacion= new Ext.Window({
              id: 'winSenalizacion',
              title: 'Señalizaciones',
              width: 210,
              height: 132,
              closable: false,
              collapsible: true,
              modal: false,
              initCenter: false,
              x: viewWidth - 220,
              y: 50,
              items: [{
                xtype: 'container',
                layout: 'anchor',
                //style: 'padding-top:3px;padding-left:5px;',
                items: [imageStatusTracto]
              }],
              resizable: false,
              border: true,
              draggable: false
            }).show();

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, leftPanel, centerPanel]
            });

            viewport.on('resize', function () {
              google.maps.event.trigger(map, "resize");
              Ext.getCmp('winSenalizacion').setPosition(Ext.getBody().getViewSize().width - 220, 50, true)
            });

        });

    </script>


    <script type="text/javascript">

        var zoneLabels = new Array();

        Ext.onReady(function () {
            GeneraMapa("dvMap1", true);

            heatMap = new google.maps.visualization.HeatmapLayer({
                data: heatMapData
            });

        });

        function FiltrarPatentes() {
            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

            var store = Ext.getCmp('comboFiltroPatente').store;
            store.load({
                params: {
                    transportista: transportista
                }
            });
        }

        function FiltrarProveedoresGPS() {
            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();

            var store = Ext.getCmp('comboFiltroProveedorGPS').store;
            store.load({
                params: {
                    Transportista: transportista
                }
            });
        }

        function Buscar() {

            if (!Ext.getCmp('leftPanel').getForm().isValid()) {
                return;
            }

            oms = new OverlappingMarkerSpiderfier(map, { markersWontMove: true, markersWontHide: true, keepSpiderfied: true });
            oms.addListener('click', function (marker, event) {

                infowindow.setContent(marker.labelText);
                infowindow.open(map, marker);

            });

            ClearMap();
            arrayAlerts.splice(0, arrayAlerts.length);

            Ext.Msg.wait('Espere por favor...', 'Generando');

            heatMapData = [];
            heatMap.setMap(null);

            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null)
            }
            markers = [];

            if (markerCluster != null) {
                markerCluster.clearMarkers();
            }

            var desde = Ext.getCmp('dateDesde').getValue();
            var hasta = Ext.getCmp('dateHasta').getValue();
            var horaDesde = Ext.getCmp('hourDesde').getRawValue();
            var horaHasta = Ext.getCmp('hourHasta').getRawValue();

            var transportista = Ext.getCmp('comboFiltroTransportista').getValue();
            var proveedorGPS = Ext.getCmp('comboFiltroProveedorGPS').getValue();
            var patente = Ext.getCmp('comboFiltroPatente').getValue();
            var rutConductor = Ext.getCmp('comboFiltroConductores').getValue();
            var tipoAlerta = Ext.getCmp('comboFiltroAlertas').getValue();
            var descripcionAlerta = Ext.getCmp('comboFiltroDescripcionAlerta').getValue();
            var idFormato = Ext.getCmp('comboFiltroFormatos').getValue();
            var codigoLocal = Ext.getCmp('comboFiltroLocales').getValue();
            var permiso = Ext.getCmp('comboFiltroPermiso').getValue();
            var estadoViaje = "Todos";
            var scoreConductor = Ext.getCmp('comboFiltroScoreConductor').getValue();

            var store = Ext.getCmp('gridPanelAlertas').store;
            store.load({
                params: {
                    desde: desde,
                    hasta: hasta,
                    HoraDesde: horaDesde,
                    HoraHasta: horaHasta,
                    transportista: transportista,
                    proveedorGPS: proveedorGPS,
                    patente: patente,
                    scoreConductor: scoreConductor,
                    rutConductor: rutConductor,
                    tipoAlerta: tipoAlerta,
                    descripcionAlerta: descripcionAlerta,
                    idFormato: idFormato,
                    codigoLocal: codigoLocal,
                    permiso: permiso,
                    estadoViaje: estadoViaje
                },
                callback: function (r, options, success) {
                    if (!success) {
                        Ext.MessageBox.show({
                            title: 'Error',
                            msg: 'Se ha producido un error.',
                            buttons: Ext.MessageBox.OK
                        });
                    }
                    else {

                        for (var i = 0; i < store.count() ; i++) {
                            var lat = store.data.items[i].raw.Latitud;
                            var lon = store.data.items[i].raw.Longitud;
                            var latLng = new google.maps.LatLng(store.getAt(i).data.Latitud, store.getAt(i).data.Longitud);
                            heatMapData.push(latLng);

                            var nroTransporte = store.data.items[i].raw.NroTransporte.toString();
                            var idEmbarque = store.data.items[i].raw.IdEmbarque.toString();
                            var fecha = store.data.items[i].raw.FechaHoraCreacion.toString();
                            var textFechaCreacion = store.data.items[i].raw.TextFechaCreacion;
                            var transportista = store.data.items[i].raw.Transportista;
                            var proveedorGPS = store.data.items[i].raw.ProveedorGPS;
                            var velocidad = store.data.items[i].raw.Velocidad;
                            var temperatura = store.data.items[i].raw.Temp1;
                            var descripcion = store.data.items[i].raw.DescripcionAlerta;
                            var permiso = store.data.items[i].raw.Permiso;
                            var formato = store.data.items[i].raw.Formato;
                            var codigoLocal = store.data.items[i].raw.CodigoDestino;
                            var tipoAlerta = store.data.items[i].raw.TipoAlerta;
                            var nombreConductor = store.data.items[i].raw.NombreConductor;
                            var scoreConductor = store.data.items[i].raw.ScoreConductor;
                            var discrepancia = store.data.items[i].raw.Discrepancia;

                            arrayAlerts.push({
                                NroTransporte: nroTransporte,
                                IdEmbarque: idEmbarque,
                                Fecha: fecha,
                                TextFechaCreacion: textFechaCreacion,
                                Transportista: transportista,
                                ProveedorGPS: proveedorGPS,
                                Velocidad: velocidad,
                                Latitud: lat,
                                Longitud: lon,
                                LatLng: latLng,
                                Temperatura: temperatura,
                                Descripcion: descripcion,
                                Permiso: permiso,
                                Formato: formato,
                                CodigoLocal: codigoLocal,
                                NombreConductor: nombreConductor,
                                ScoreConductor: scoreConductor,
                                Discrepancia: discrepancia,
                            });

                            var contentString =
                                                  '<br>' +
                                                      '<table>' +
                                                      '<tr>' +
                                                          '       <td><b>Id. Master</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '       <td>' + nroTransporte + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '       <td><b>Id. Embarque</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '       <td>' + idEmbarque + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '       <td><b>Fecha</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '       <td>' + textFechaCreacion + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Transportista:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + transportista + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Proveedor GPS:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + proveedorGPS + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Formato:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + formato + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Local:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + codigoLocal + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Descripción:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + descripcion + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Permiso:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + permiso + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Conductor:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + nombreConductor + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Score conductor:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + scoreConductor + '</td>' +
                                                      '</tr>' +
                                                      '<tr>' +
                                                          '        <td><b>Coordenadas:</b></td>' +
                                                          '       <td><pre>     </pre></td>' +
                                                          '        <td>' + lat + ',' + lon + '</td>' +
                                                      '</tr>' +

                                                      '</table>' +
                                                  '<br>';

                            switch (descripcion) {
                                case "PERDIDA SEÑAL":
                                    marker = new google.maps.Marker({
                                        position: latLng,
                                        icon: 'Images/Alertas/signal_purple_23x23.png',
                                        labelText: contentString
                                    });
                                    break;

                                case "APERTURA PUERTA":
                                    marker = new google.maps.Marker({
                                        position: latLng,
                                        icon: 'Images/Alertas/unlock_blue_23x23.png',
                                        labelText: contentString
                                    });
                                    break;

                                case "DETENCION NO AUTORIZADA":
                                case "DETENCION PATIO LINEA TRANSPORTE":
                                case "DETENCION PROLONGADA ZONA AUTORIZADA":
                                case "DETENCION PROLONGADA ZONA NO AUTORIZADA":
                                case "SOBRETIEMPO EN DESCARGA DROP":
                                    marker = new google.maps.Marker({
                                        position: latLng,
                                        icon: 'Images/Alertas/dot_stop_red_23x23.png',
                                        labelText: contentString
                                    });
                                    break;

                                default:
                                    marker = new google.maps.Marker({
                                        position: latLng,
                                        icon: 'Images/Alertas/alert_orange_22x22.png',
                                        labelText: contentString
                                    });
                            }

                            markers.push(marker);
                            oms.addMarker(marker);

                        }

                        var viewMapType = Ext.getCmp("radioGroupViewMapType").getValue().mt;

                        if (viewMapType == '1') {
                            heatMap = new google.maps.visualization.HeatmapLayer({
                                data: heatMapData,
                                radius: 30,
                                opacity: 0.75,
                                dissipating: true,
                                map: map
                            });

                            heatMap2 = new google.maps.visualization.HeatmapLayer({
                                data: heatMapData,
                                radius: 30,
                                opacity: 0.75,
                                dissipating: true,
                                map: map2
                            });
                        }
                        else if (viewMapType == '2') {
                            markerCluster = new MarkerClusterer(map, markers);
                            markerCluster.setMaxZoom(16);

                            markerCluster2 = new MarkerClusterer(map2, markers);
                            markerCluster2.setMaxZoom(16);
                        }

                        Ext.Msg.hide();
                    }

                }
            });
        }

        function MostrarZonas() {

            Ext.Msg.wait('Espere por favor...', 'Generando zonas');

            var countZonas = Ext.getCmp('comboZonas').store.count()

            for (i = 0; i < countZonas; i++) {
                var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
                var idTipoZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdTipoZona;

                DrawZone(idZona, idTipoZona);
            }

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
                success: function (response, opts) {

                    var task = new Ext.util.DelayedTask(function () {
                        Ext.Msg.hide();
                    });

                    task.delay(1500);

                },
                failure: function (response, opts) {
                    Ext.Msg.hide();
                }
            });
        }

        function containsZone(a, obj) {
            var i = a.length;
            while (i--) {
                if (a[i].IdZona === obj) {
                    return true;
                }
            }
            return false;
        }

        function containsLabel(a, obj) {
            var i = a.length;
            while (i--) {//GetDetalleAreaSeleccionada
                if (a[i].text === obj) {
                    return true;
                }
            }
            return false;
        }

        function eraseAllZones() {
            var countZonas = Ext.getCmp('comboZonas').store.count()

            for (i = 0; i < countZonas; i++) {
                var idZona = Ext.getCmp("comboZonas").store.data.getAt(i).data.IdZona;
                EraseZone(idZona);
            }

            zoneLabels = [];
        }

        function EraseZone(idZona) {
            for (var i = 0; i < geoLayer.length; i++) {
                if (idZona == geoLayer[i].IdZona) {
                    geoLayer[i].layer.setMap(null);
                    geoLayer[i].label.setMap(null);
                    geoLayer.splice(i, 1);
                }
            }

            for (var i = 0; i < zoneLabels.length; i++) {
                if (zoneLabels[i].text == idZona) {
                    zoneLabels[i].setMap(null);
                    zoneLabels.splice(i, 1);

                }
            }

        }

        function FiltrarLocales() {
            var idFormato = Ext.getCmp('comboFiltroFormatos').getValue();

            var store = Ext.getCmp('comboFiltroLocales').store;
            store.load({
                params: {
                    IdFormato: idFormato,
                },
                callback: function (r, options, success) {
                    if (!success) {
                        Ext.MessageBox.show({
                            title: 'Error',
                            msg: 'Se ha producido un error.',
                            buttons: Ext.MessageBox.OK
                        });
                    }
                    else {
                        Ext.getCmp("comboFiltroLocales").setValue(0);
                    }

                }
            });
        }


        function CheckRow(check, id, centrarMapa) {
            if (check.checked) {

                checkedZones.push(id);
                Ext.Ajax.request({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetVerticesZona',
                    params: {
                        IdZona: id
                    },
                    success: function (data, success) {
                        if (data != null) {
                            data = Ext.decode(data.responseText);
                            if (data.Vertices.length > 1) { //Polygon
                                var polygonGrid = new Object();
                                polygonGrid.IdZona = data.IdZona;

                                var arr = new Array();
                                for (var i = 0; i < data.Vertices.length; i++) {
                                    arr.push(new google.maps.LatLng(data.Vertices[i].Latitud, data.Vertices[i].Longitud));
                                }

                                if (data.idTipoZona == 3) {
                                    var colorZone = "#FF0000";
                                }
                                else {
                                    var colorZone = "#7f7fff";
                                }

                                polygonGrid.layer = new google.maps.Polygon({
                                    paths: arr,
                                    strokeColor: "#000000",
                                    strokeWeight: 1,
                                    strokeOpacity: 0.9,
                                    fillColor: colorZone,
                                    fillOpacity: 0.3,
                                    labelText: data.NombreZona
                                });
                                polygonGrid.label = new Label({
                                    position: new google.maps.LatLng(data.Latitud, data.Longitud),
                                    //map: viewLabel ? map : null
                                    map: map
                                });
                                polygonGrid.label.bindTo('text', polygonGrid.layer, 'labelText');
                                polygonGrid.layer.setMap(map);

                                if (centrarMapa == true || centrarMapa == null) {
                                    map.setCenter(new google.maps.LatLng(data.Latitud, data.Longitud));
                                }
                                geoLayer.push(polygonGrid);
                            }
                        }
                    },
                    failure: function (msg) {
                        alert('Se ha producido un error.');
                    }
                });

            }
            else {
                for (var i = 0; i < geoLayer.length; i++) {
                    if (id == geoLayer[i].IdZona) {
                        geoLayer[i].layer.setMap(null);
                        geoLayer[i].label.setMap(null);
                        geoLayer.splice(i, 1);
                    }
                }
                for (var i = 0; i < checkedZones.length; i++) {
                    if (id == checkedZones[i]) {
                        checkedZones.splice(i, 1);
                    }
                }

            }
        }


        function FiltrarZonas() {
            var idTipoZona = Ext.getCmp('comboFiltroTipoZona').getValue();
            var nombreZona = Ext.getCmp('textFiltroNombre').getValue();
            var _estadoMx = 0

            var store = Ext.getCmp('gridPanelZonas').store;
            store.load({
                params: {
                    idTipoZona: idTipoZona,
                    nombreZona: nombreZona,
                    EstadoMx: _estadoMx
                }
            });
        }

        function MostrarTodas() {

            Ext.Msg.wait('Espere por favor...', 'Generando zonas');

            var _storeZonas = Ext.getCmp('gridPanelZonas').getStore();

            var countZonas = Ext.getCmp('gridPanelZonas').getStore().count()

            for (i = 0; i < countZonas; i++) {
                var idZona = _storeZonas.data.items[i].data.IdZona
                var objCheck = document.getElementById("viewCheck" + idZona);
                objCheck.checked = true; // Ext.getCmp('chkVerMapa').getValue();
                if (checkedZones.indexOf(idZona) < 0) {
                    CheckRow(objCheck, idZona, false);
                }
            }

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxFunctions.aspx?Metodo=ProgressBarCall',
                success: function (response, opts) {

                    var task = new Ext.util.DelayedTask(function () {
                        Ext.Msg.hide();
                    });

                    task.delay(1100);

                },
                failure: function (response, opts) {
                    Ext.Msg.hide();
                }
            });


        }

    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Body" runat="server">
    <div id="dvMap1"></div>
</asp:Content>
