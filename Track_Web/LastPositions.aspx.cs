﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

namespace Track_Web
{
    public partial class LastPositions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            Utilities.VerifyLoginStatus(Session, Response);

            switch (Request.QueryString["Metodo"])
            {
                case "ExportExcel":
                    ExportExcel();
                    return;
                default:
                    break;
            }
        }

        public void ExportExcel()
        {
            try
            {
                if (Session["ReporteEstadoFlota"] != null)
                {
                    string ob = Session["ReporteEstadoFlota"].ToString();
                    List<Track_GetLastPositions_Result> _object = JsonConvert.DeserializeObject<List<Track_GetLastPositions_Result>>(Session["ReporteEstadoFlota"].ToString());

                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "text/csv";
                    Response.AppendHeader("Content-Disposition", "attachment;filename=Reporte_EstadoFlota_" + DateTime.Now.ToString("ddMMyyyy_HHmm") + ".xls");
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Response.Write(Methods_Export.HTML_LastPositions(_object));
                    Response.End();
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();
            }


        }
    }
}