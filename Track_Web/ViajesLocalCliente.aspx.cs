﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using UtilitiesLayer;
using System.Text;
using System.Diagnostics;
using Microsoft.VisualBasic;

using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;

using System.IO;
using SimpleImpersonation;
using System.Globalization;

namespace Track_Web
{
  public partial class ViajesLocalCliente : System.Web.UI.Page
  {
    IFormatProvider culture = new CultureInfo("en-US", true);
    protected void Page_Load(object sender, EventArgs e)
    {
      Utilities.VerifyLoginStatus(Session, Response);

      switch (Request.QueryString["Metodo"])
      {
        case "ExportExcelCliente":
          ExportExcelCliente(Request.Form["desde"].ToString(), Request.Form["hasta"].ToString(), Request.Form["nroTransporte"].ToString(), Request.Form["codOrigen"].ToString(), Request.Form["patente"].ToString(), Request.Form["tipoViaje"].ToString());
          return;
        default:
          break;
      }
    }

    public void ExportExcelCliente(string desde, string hasta, string nroTransporte, string codOrigen, string patente, string tipoViaje)
    {
      string username = Session["userName"].ToString();

      string now = DateTime.Now.ToString();
      now = now.Replace(" ", "_");
      now = now.Replace("-", "");
      now = now.Replace(":", "");

      DateTime _desde;
      DateTime _hasta;

            int _codOrigen;
            long _nroTransporte;

            if (nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                long.TryParse(nroTransporte, out _nroTransporte);
            }

            int.TryParse(codOrigen, out _codOrigen);

            //DateTime.TryParse(desde, out _desde);
            //DateTime.TryParse(hasta, out _hasta);
            if (DateTime.TryParseExact(desde.Replace("-", "/").Substring(0, 10), "dd/MM/yyyy", culture, DateTimeStyles.None, out _desde))
      {
      }
      else
      {
        DateTime.TryParseExact(desde.Replace("T", " ").Substring(0, 10), "yyyy-MM-dd", culture, DateTimeStyles.None, out _desde);
      }

      if (DateTime.TryParseExact(hasta.Replace("-", "/").Substring(0, 10), "dd/MM/yyyy", culture, DateTimeStyles.None, out _hasta))
      {
      }
      else
      {
        DateTime.TryParseExact(hasta.Replace("T", " ").Substring(0, 10), "yyyy-MM-dd", culture, DateTimeStyles.None, out _hasta);
      }

      Methods_Viajes _objMethosViajes = new Methods_Viajes();

      List<Track_GetViajesControlLocalCliente_Result> _listViajes = _objMethosViajes.GetViajesControlLocalCliente(_desde, _hasta, _nroTransporte, _codOrigen, patente, tipoViaje, username);

      StringBuilder reportBuilder = new StringBuilder();

      Response.Clear();
      Response.Buffer = true;
      Response.BufferOutput = true;
      //Response.ContentType = "application/vnd.ms-excel";
      Response.ContentType = "text/csv";
      Response.AppendHeader("Content-Disposition", "attachment;filename=RptViajesLocalCliente_" + now + ".xls");
      Response.Charset = "UTF-8";
      Response.ContentEncoding = Encoding.Default;
      Response.Write(Methods_Export.HTML_RptViajesControlLocalCliente(_listViajes.ToList()));
      Response.End();
    }

  }
}