﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Track_Web.ControladoresBD;
using UtilitiesLayer;

namespace Track_Web
{
    public partial class Tableros : OperacionesTableroGeneral
    {

        ControladorTableros controladorTableros = new ControladorTableros();

        private List<string> listadoImagenesPorTablero;

        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Utilities.VerifyLoginStatus(Session, Response);
                ObtenerInformacionTodosJSON();
            }


            listadoImagenesPorTablero = PruebaObtenerImagenesPorTablero();
            ArmarJSDespliegaGaleriaImagenesTablero();
        }
        /*
        public void btnCargarImagenesPorTablero_Click(Object sender, EventArgs e) {
            listadoImagenesPorTablero = controladorTablerosPorPerfil.ObtenerImagenesPorIdTablero(Convert.ToInt32(hidden_idTablero.Value));
            ArmarJSDespliegaGaleriaImagenesTablero();
        }
        */
        public void btnBorrarTablero_Click(Object sender, EventArgs e)
        {
            controladorTableros.EliminarRegistroTableroPorId(Convert.ToInt32(hidden_idTablero.Value));
            //Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnActualizarTablero_Click(Object sender, EventArgs e)
        {
            controladorTableros.ActualizarRegistroTablero(hidden_JSONTableroTemporal.Value);
            //Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public void btnInsertarTablero_Click(Object sender, EventArgs e)
        {
            controladorTableros.InsertarRegistroTablero(hidden_JSONTableroTemporal.Value);
            //Session["recargado"] = "TRUE";
            RecargarPaginaActual();
        }

        public List<string> PruebaObtenerImagenesPorTablero()
        {
            List<string> listaVacia = new List<string>();
            listaVacia.Add("Images/sinImagenes.jpg");

            return listaVacia;
        }

        public void ArmarJSDespliegaGaleriaImagenesTablero()
        {
            StringBuilder cadenaListadoImagenes = new StringBuilder();

            foreach (string imagen in listadoImagenesPorTablero)
                cadenaListadoImagenes.Append("'" + imagen + "',");

            string listadoImagenesCadena = cadenaListadoImagenes.ToString();
            listadoImagenesCadena = listadoImagenesCadena.ToString().Substring(0, listadoImagenesCadena.Length - 1);

            StringBuilder scriptJSGenerado = new StringBuilder();

            scriptJSGenerado.Append("\n<script type=\"text/javascript\" language=\"Javascript\" >\n");

            scriptJSGenerado.Append("var arregloImagenesRecibido = [");
            scriptJSGenerado.Append(listadoImagenesCadena);
            scriptJSGenerado.Append("];");
            scriptJSGenerado.Append("$(document).ready(function () {");
            scriptJSGenerado.Append("AlimentarCargarImagenesPorTablero(arregloImagenesRecibido);");
            scriptJSGenerado.Append("});");
            scriptJSGenerado.Append("\n\n </script>");

            this.Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "JSScriptDespliegaGaleriaImagenesTablero", scriptJSGenerado.ToString());
        }


        public override void ObtenerInformacionTodosJSON()
        {
            contenedorJSONTableros.Value = controladorTableros.ObtenerTodosRegistrosTableros();
        }
    }
}