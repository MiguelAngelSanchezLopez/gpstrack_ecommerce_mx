﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="ConfigUsuarios.aspx.cs" Inherits="Track_Web.ConfigUsuarios" %>

<asp:Content ID="Content3" ContentPlaceHolderID="Title" runat="server">
    AltoTrack Platform 
</asp:Content>

<asp:Content ID="Content4" ContentPlaceHolderID="MainContent" runat="server">

    <script src="Scripts/TopMenu.js" type="text/javascript"></script>

    <script type="text/javascript">

        var perfil = 0;

        Ext.onReady(function () {

            Ext.QuickTips.init();
            Ext.Ajax.timeout = 3600000;
            Ext.override(Ext.form.Basic, { timeout: Ext.Ajax.timeout / 1000 });
            Ext.override(Ext.data.proxy.Server, { timeout: Ext.Ajax.timeout });
            Ext.override(Ext.data.Connection, { timeout: Ext.Ajax.timeout });

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=getTopMenu',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);
                        var i;
                        for (i = 0; i < data.length; i++) {
                            if (data[i].MenuPadre == 0) {
                                /* toolbarMenu.items.get(data[i].IdJavaScript).show();
                                 toolbarMenu.items.get(data[i].IdPipeLine).show();*/
                            }
                            else {
                                var listmenu = Ext.getCmp(data[i].JsPadre).menu;
                                listmenu.items.get(data[i].IdJavaScript).show();
                            }
                        }
                    }
                }
            });

            //Verifica si se debe controlar tiempo de expiración de sesión
            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetSessionExpirationTimeout',
                success: function (data, success) {
                    if (data != null) {
                        data = Ext.decode(data.responseText);

                        if (data > 0) {
                            Ext.ns('App');

                            //Session timeout in secons     
                            App.SESSION_TIMEOUT = data;

                            // Helper that converts minutes to milliseconds.
                            App.toMilliseconds = function (minutes) {
                                return minutes * 60 * 1000;
                            }

                            // Notifies user that her session has timed out.
                            App.sessionTimedOut = new Ext.util.DelayedTask(function () {
                                Ext.Msg.alert('Sesión expirada.', 'Su sesión ha expirado.');

                                Ext.MessageBox.show({
                                    title: "Sesión expirada.",
                                    msg: "Su sesión ha expirado.",
                                    icon: Ext.MessageBox.WARNING,
                                    buttons: Ext.MessageBox.OK,
                                    fn: function () {
                                        window.location = "Login.aspx";
                                    }
                                });

                            });

                            // Starts the session timeout workflow after an AJAX request completes.
                            Ext.Ajax.on('requestcomplete', function (conn, response, options) {

                                // Reset the client-side session timeout timers.
                                App.sessionTimedOut.delay(App.toMilliseconds(App.SESSION_TIMEOUT));

                            });

                        }
                    }
                }
            })

            var storeUsuarios = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['IdUsuario',
                    'Nombre',
                    'Apellidos',
                    'EMail',
                    'UserName',
                    'Password',
                    'IdRol',
                    'Agrupacion',
                    'Activo',
                    { name: 'FechaCreacion', type: 'date', dateFormat: 'c' }

                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetUsuarios',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelUsuarios = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelUsuarios',
                store: storeUsuarios,
                anchor: '100% 100%',
                columnLines: true,
                scroll: false,
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                    { text: 'Id', width: 55, sortable: true, dataIndex: 'IdUsuario' },
                    { text: 'Nombre', flex: 1, sortable: true, dataIndex: 'Nombre' },
                    { text: 'Apellidos', flex: 1, sortable: true, dataIndex: 'Apellidos' },
                    { text: 'Usuario', flex: 1, sortable: true, dataIndex: 'UserName' },
                    { text: 'Agrupación', flex: 1, sortable: true, dataIndex: 'Agrupacion' },
                    { text: 'Activo', width: 55, sortable: true, dataIndex: 'Activo' },
                    {
                        xtype: 'actioncolumn',
                        width: 25,

                        items: [{
                            icon: 'Images/edit.png',
                            tooltip: 'Editar usuario',
                            handler: function (grid, rowIndex, colIndex) {
                                var row = grid.getStore().getAt(rowIndex);
                                var idUsuario = row.data.IdUsuario;
                                var userName = row.data.UserName;
                                var password = row.data.Password;
                                var nombre = row.data.Nombre;
                                var apellidos = row.data.Apellidos;
                                var email = row.data.EMail;
                                var agrupacion = row.data.Agrupacion;
                                var activo = row.data.Activo;

                                Ext.getCmp("txtIdUsuario").setValue(idUsuario);
                                Ext.getCmp("txtUserName").setDisabled(true);
                                Ext.getCmp("txtUserName").setValue(userName);
                                Ext.getCmp("txtPassword").setValue(password);
                                Ext.getCmp("txtNombre").setValue(nombre);
                                Ext.getCmp("txtApellidos").setValue(apellidos);
                                Ext.getCmp("txtEMail").setValue(email);
                                Ext.getCmp("comboFiltroAgrupacion").setValue(agrupacion);
                                Ext.getCmp("chkUsuarioActivo").setValue(activo);

                                var storeCedisAsociados = Ext.getCmp('gridPanelCEDISAsociados').store;
                                storeCedisAsociados.load({
                                    params: {
                                        idUsuario: idUsuario
                                    }
                                });
                            }

                        }]
                    }/*,
                            {
                                icon: 'Images/delete.png',
                                tooltip: 'Eliminar usuario',
                                handler: function (grid, rowIndex, colIndex) {
                                    if (confirm("El usuario se eliminará permanentemente.¿Desea continuar?")) {
                                        DeleteUsuario(row.data.IdUsuario);
                                    }
                                }
                                }*/
                ]
            });

            var txtIdUsuario = new Ext.form.TextField({
                id: 'txtIdUsuario',
                fieldLabel: 'Id Usuario',
                labelWidth: 120,
                readOnly: true,
                anchor: '99%',
                disabled: true
            });

            var txtUserName = new Ext.form.TextField({
                id: 'txtUserName',
                fieldLabel: 'Usuario',
                allowBlank: false,
                labelWidth: 120,
                anchor: '99%'
            });

            var txtPassword = new Ext.form.TextField({
                id: 'txtPassword',
                fieldLabel: 'Contraseña',
                allowBlank: false,
                labelWidth: 120,
                inputType: 'password',
                anchor: '99%',
                maxLength: 10
            });

            var txtNombre = new Ext.form.TextField({
                id: 'txtNombre',
                fieldLabel: 'Nombre',
                allowBlank: false,
                labelWidth: 120,
                anchor: '99%'
            });

            var txtApellidos = new Ext.form.TextField({
                id: 'txtApellidos',
                fieldLabel: 'Apellidos',
                labelWidth: 120,
                anchor: '99%',
                maxLength: 25
            });

            var txtEMail = new Ext.form.TextField({
                id: 'txtEMail',
                fieldLabel: 'E-Mail',
                labelWidth: 120,
                anchor: '99%'
            });

            var storeFiltroAgrupacion = new Ext.data.JsonStore({
                autoLoad: true,
                fields: ['NombreAgrupacion'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxReportes.aspx?Metodo=GetAgrupacionUsuarios&Todos=True',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboFiltroAgrupacion = new Ext.form.field.ComboBox({
                id: 'comboFiltroAgrupacion',
                fieldLabel: 'Agrupación',
                allowBlank: false,
                store: storeFiltroAgrupacion,
                valueField: 'NombreAgrupacion',
                displayField: 'NombreAgrupacion',
                queryMode: 'local',
                anchor: '99%',
                forceSelection: true,
                enableKeyEvents: true,
                editable: true,
                labelWidth: 120,
                emptyText: 'Seleccione...'
            });

            var chkUsuarioActivo = new Ext.form.field.Checkbox({
                id: 'chkUsuarioActivo',
                fieldLabel: 'Usuario Activo',
                labelWidth: 120
            });

            var btnGuardar = {
                id: 'btnGuardar',
                xtype: 'button',
                iconAlign: 'left',
                icon: 'Images/save_black_20x20.png',
                text: 'Guardar',
                width: 100,
                height: 27,
                disabled: true,

                handler: function () {
                    GuardarUsuario();
                }
            };

            var btnCancelar = {
                id: 'btnCancelar',
                xtype: 'button',
                width: 100,
                height: 27,
                iconAlign: 'left',
                icon: 'Images/back_black_20x20.png',
                text: 'Cancelar',

                style: {
                    marginLeft: '5px',
                    marginBottom: '10px'
                },

                handler: function () {
                    Cancelar();
                }
            };

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetPerfilUsuarioConectado',
                success: function (data, success) {
                    perfil = data.responseText;
                    if (perfil < 3) {
                        Ext.getCmp("btnGuardar").setDisabled(false);
                    }
                    else {
                        Ext.getCmp("btnGuardar").setDisabled(true)
                    }
                }
            });

            var storeCEDIS = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['IdZona', 'NombreZona', 'IdTipoZona', 'NombreTipoZona', 'Latitud', 'Longitud'],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxZonas.aspx?Metodo=GetZonas&Todas=True',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var comboCEDIS = new Ext.form.field.ComboBox({
                id: 'comboCEDIS',
                fieldLabel: 'Determinante',
                store: storeCEDIS,
                valueField: 'IdZona',
                displayField: 'NombreZona',
                queryMode: 'local',
                anchor: '99%',
                allowEmpty: false,
                forceSelection: true,
                enableKeyEvents: true,
                editable: true,
                style: {
                    marginTop: '5px'
                },
                labelWidth: 100,
                emptyText: 'Seleccione...',
                listConfig: {
                    loadingText: 'Buscando...',
                    getInnerTpl: function () {
                        return '<a class="search-item">' +
                            '<span>Determinante: {IdZona}</span><br />' +
                            '<span>Nombre: {NombreZona}</span>' +
                            '</a>';
                    }
                },
                listeners: {
                    select: function () {
                        Ext.getCmp("btnAddDestino").setDisabled(false);
                    }
                }

            });

            storeCEDIS.load({
                params: {
                    idTipoZona: 1,
                    nombreZona: ''
                },
                callback: function (r, options, success) {
                    if (success) {
                        Ext.getCmp("comboCEDIS").store.insert(0, [{ IdZona: -1, NombreZona: 'Todos', IdTipoZona: 1, NombreTipoZona: 'CEDIS' }])
                    }
                }

            });

            var storeCEDISAsociados = new Ext.data.JsonStore({
                autoLoad: false,
                fields: ['DeterminanteCEDIS',
                    'NombreZona'
                ],
                proxy: new Ext.data.HttpProxy({
                    url: 'AjaxPages/AjaxLogin.aspx?Metodo=GetDeterminantesAsociados',
                    reader: { type: 'json', root: 'Zonas' },
                    headers: {
                        'Content-type': 'application/json'
                    }
                })
            });

            var gridPanelCEDISAsociados = Ext.create('Ext.grid.Panel', {
                id: 'gridPanelCEDISAsociados',
                store: storeCEDISAsociados,
                //width: 405,
                //height: 110,
                anchor: '98%',
                height: 200,
                columnLines: true,
                scroll: false,
                style: {
                    marginTop: '5px'
                },
                viewConfig: {
                    style: { overflow: 'auto', overflowX: 'hidden' }
                },
                columns: [
                    { text: 'Determinante', sortable: true, width: 100, dataIndex: 'DeterminanteCEDIS' },
                    { text: 'Nombre', sortable: true, flex: 1, dataIndex: 'NombreZona' },
                    {
                        xtype: 'actioncolumn',
                        width: 25,
                        editor: false,
                        items: [
                            {
                                icon: 'Images/delete.png',
                                tooltip: 'Eliminar',
                                handler: function (grid, rowIndex, colIndex) {
                                    var row = grid.getStore().getAt(rowIndex);
                                    grid.getStore().remove(row);
                                }
                            }]
                    }
                ]
            });

            var btnAddDestino = {
                id: 'btnAddDestino',
                xtype: 'button',
                iconAlign: 'left',
                text: 'Agregar',
                icon: 'Images/add_blue_16x15.png',
                width: 70,
                height: 23,
                style: {
                    marginTop: '5px'
                },
                disabled: true,
                handler: function () {

                    var determinante = Ext.getCmp("comboCEDIS").getValue();
                    var nombreCEDIS = Ext.getCmp("comboCEDIS").getRawValue();

                    var record = storeCEDISAsociados.findRecord('DeterminanteCEDIS', determinante);

                    if (record == null) {

                        storeCEDISAsociados.add({
                            DeterminanteCEDIS: determinante,
                            NombreZona: nombreCEDIS
                        });
                    }
                }
            };

            var panelConfiguracion = new Ext.FormPanel({
                id: 'panelConfiguracion',
                title: 'Datos de Usuario',
                height: '100%',
                border: false,
                bodyStyle: 'padding: 5px;',
                items: [
                    {
                        xtype: 'fieldset',
                        title: 'Datos de Usuario',
                        style: {
                            marginLeft: '5px',
                            marginRight: '5px'
                        },
                        layout: 'column',
                        anchor: '100% 30%',
                        items: [
                            {
                                xtype: 'container',
                                layout: 'anchor',
                                columnWidth: 0.5,
                                items: [txtUserName, txtNombre, txtEMail, chkUsuarioActivo]
                            },
                            {
                                xtype: 'container',
                                layout: 'anchor',
                                columnWidth: 0.5,
                                items: [txtPassword, txtApellidos, comboFiltroAgrupacion]
                            }
                        ]
                    },

                    {
                        xtype: 'fieldset',
                        title: 'Determinantes asociados',
                        style: {
                            marginLeft: '5px'
                        },
                        anchor: '100% 72%',
                        layout: 'column',
                        items: [
                            {
                                xtype: 'container',
                                layout: 'anchor',
                                columnWidth: 0.85,
                                items: [comboCEDIS]
                            },
                            {
                                xtype: 'container',
                                layout: 'anchor',
                                columnWidth: 0.15,
                                items: [btnAddDestino]
                            },
                            {
                                xtype: 'container',
                                layout: 'anchor',
                                columnWidth: 1,
                                items: [gridPanelCEDISAsociados]
                            }

                        ]
                    }

                ],
                buttons: [btnGuardar, btnCancelar]
            });

            var viewWidth = Ext.getBody().getViewSize().width;

            var leftPanel = new Ext.FormPanel({
                id: 'leftPanel',
                title: 'Alertas',
                border: false,
                margins: '1 0 3 3',
                region: 'west',
                layout: 'anchor',
                width: viewWidth / 2,
                minWidth: viewWidth / 4,
                maxWidth: viewWidth / 1.5,
                split: true,
                items: [gridPanelUsuarios]
            });

            var centerPanel = new Ext.FormPanel({
                id: 'centerPanel',
                region: 'center',
                border: true,
                margins: '0 3 3 0',
                anchor: '100% 100%',
                items: [panelConfiguracion]

            });

            var viewport = Ext.create('Ext.container.Viewport', {
                layout: 'border',
                items: [topMenu, leftPanel, centerPanel]
            });

        });

        function GuardarUsuario() {



            if (!Ext.getCmp('panelConfiguracion').getForm().isValid()) {
                return;
            }

            var idUsuario = Ext.getCmp('txtIdUsuario').getValue();
            var userName = Ext.getCmp('txtUserName').getValue();
            var password = Ext.getCmp('txtPassword').getValue();
            var nombre = Ext.getCmp('txtNombre').getValue();
            var apellidos = Ext.getCmp('txtApellidos').getValue();
            var eMail = Ext.getCmp('txtEMail').getValue();
            var agrupacion = Ext.getCmp('comboFiltroAgrupacion').getValue();
            var activo = Ext.getCmp('chkUsuarioActivo').getValue();

            var listaCEDISAsociados = "";

            var store = Ext.getCmp('gridPanelCEDISAsociados').getStore();

            for (var i = 0; i < store.count(); i++) {
                listaCEDISAsociados = listaCEDISAsociados + store.data.items[i].data.DeterminanteCEDIS

                if (i < store.count() - 1) {
                    listaCEDISAsociados = listaCEDISAsociados + ";"
                }
            }

            Ext.Ajax.request({
                url: 'AjaxPages/AjaxLogin.aspx?Metodo=GuardarUsuario',
                params: {
                    'idUsuario': idUsuario,
                    'userName': userName,
                    'password': password,
                    'nombre': nombre,
                    'apellidos': apellidos,
                    'eMail': eMail,
                    'agrupacion': agrupacion,
                    'activo': activo,
                    'listCEDISAsociados': listaCEDISAsociados
                },
                success: function (msg, success) {
                    alert(msg.responseText);

                    Ext.getCmp("gridPanelUsuarios").getStore().load();
                    Cancelar();
                },
                failure: function (msg) {
                    alert('Se ha producido un error.');
                }
            });

        }

        function DeleteUsuario(IdUsuario) {

            Cancelar();
        }

        function Cancelar() {

            Ext.getCmp('txtIdUsuario').reset();
            Ext.getCmp('txtUserName').reset();
            Ext.getCmp('txtUserName').setDisabled(false);
            Ext.getCmp('txtPassword').reset();
            Ext.getCmp('txtNombre').reset();
            Ext.getCmp('txtApellidos').reset();
            Ext.getCmp('txtEMail').reset();
            Ext.getCmp('comboFiltroAgrupacion').reset();
            Ext.getCmp('chkUsuarioActivo').reset();

            Ext.getCmp('comboCEDIS').reset();
            Ext.getCmp('gridPanelCEDISAsociados').getStore().removeAll();
            Ext.getCmp("btnAddDestino").setDisabled(true);
        }

    </script>

</asp:Content>
