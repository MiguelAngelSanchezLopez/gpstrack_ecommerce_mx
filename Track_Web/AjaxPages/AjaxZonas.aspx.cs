﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;

using System.IO;
using System.Globalization;

namespace Track_Web.AjaxPages
{
    public partial class AjaxZonas : System.Web.UI.Page
    {
        IFormatProvider culture = new CultureInfo("en-US", true);
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Request.QueryString["Metodo"])
            {
                case "GetTipoZonas":
                    GetTipoZonas();
                    break;
                case "GetZonas":
                    GetZonas();
                    break;
                case "EliminaZona":
                    EliminaZona(Request.Form["idZona"].ToString());
                    break;
                case "GetVerticesZona":
                    GetVerticesZona(Request.Form["IdZona"].ToString());
                    break;
                case "GetAllVerticesZona":
                    GetAllVerticesZona(Request.Form["IdZona"].ToString());
                    break;
                case "ValidarIdZona":
                    ValidarIdZona(Request.Form["IdZona"].ToString());
                    break;
                case "NuevaZona":
                    NuevaZona(Request.Form["IdZona"].ToString(), Request.Form["NombreZona"].ToString(), Request.Form["IdTipoZona"].ToString(), Request.Form["Vertices"].Split(','));
                    break;
                case "EditarZona":
                    EditarZona(Request.Form["IdZona"].ToString(), Request.Form["NombreZona"].ToString(), Request.Form["IdTipoZona"].ToString(), Request.Form["Vertices"].Split(','));
                    break;
                case "GetZonasToDraw":
                    GetZonasToDraw();
                    break;
                case "GetLocales":
                    GetLocales();
                    break;
                case "GetZonesInsideZona":
                    GetZonesInsideZona(Request.Form["IdZona"].ToString());
                    break;
                case "GetLocalesBackhaul":
                    GetLocalesBackhaul();
                    break;
                case "getStatesMexico":
                    getStatesMexico();
                    break;
            }
        }

        public void getStatesMexico()
        {

            Methods_Zonas _obj = new Methods_Zonas();
            string _response = JsonConvert.SerializeObject(_obj.GetEstadosMexico());
            Response.Write(_response);
        }

        public void GetLocalesBackhaul()
        {
            string idTipoZona = "" + Request.QueryString["idTipoZona"];
            string nombreZona = "" + Request.QueryString["nombreZona"];
            int _idTipoZona;

            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_obj.GetLocalesBackhaul(_idTipoZona, nombreZona));

            Response.Write(_response);
        }


        public void GetTipoZonas()
        {
            string _todos = "" + Request.QueryString["Todos"];
            string _result = "";
            Methods_Zonas _obj = new Methods_Zonas();

            if (_todos == "True")
            {
                var _list = (from i in _obj.GetTipoZonas(true)
                             select new
                             {
                                 i.IdTipoZona,
                                 i.NombreTipoZona
                             }).ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetTipoZonas()
                             select new
                             {
                                 i.IdTipoZona,
                                 i.NombreTipoZona
                             }).ToList();
                _result = JsonConvert.SerializeObject(_list);
            }


            Response.Write(_result);
        }

        public void GetZonas()
        {
            string idTipoZona = "" + Request.QueryString["idTipoZona"];
            string nombreZona = "" + Request.QueryString["nombreZona"];
            int _idTipoZona;

            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_obj.GetZonas(_idTipoZona, nombreZona));

            Response.Write(_response);
        }

        public void EliminaZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            string _resp = _obj.EliminaZona(_idZona);

            Response.Write(_resp);
        }

        public void GetVerticesZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Track_Zonas _zona = _obj.GetZonaById(_idZona);

            var json = new
            {
                IdZona = _zona.IdZona,
                NombreZona = _zona.NombreZona,
                Latitud = _zona.Latitud,
                Longitud = _zona.Longitud,
                idTipoZona = _zona.IdTipoZona,
                Vertices = _obj.GetVerticesZona(_idZona)
            };

            string _response = JsonConvert.SerializeObject(json);

            Response.Write(_response);
        }

        public void GetAllVerticesZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Track_Zonas _zona = _obj.GetZonaById(_idZona);

            var json = new
            {
                IdZona = _zona.IdZona,
                NombreZona = _zona.NombreZona,
                Latitud = _zona.Latitud,
                Longitud = _zona.Longitud,
                idTipoZona = _zona.IdTipoZona,
                Vertices = _obj.GetAllVerticesZona(_idZona)
            };

            string _response = JsonConvert.SerializeObject(json);

            Response.Write(_response);
        }

        public void ValidarIdZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Response.Write(_obj.ValidarIdZona(_idZona).ToString());
        }

        public void NuevaZona(string idZona, string nombreZona, string idTipoZona, string[] vertices)
        {
            int _idZona;
            int _idTipoZona;
            int.TryParse(idZona, out _idZona);
            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();
            try
            {
                List<Tuple<decimal, decimal>> _vertices = new List<Tuple<decimal, decimal>>();
                for (int i = 0; i < vertices.Length; i++)
                {
                    string[] temp = vertices[i].Split(';');
                    Tuple<decimal, decimal> _ver = new Tuple<decimal, decimal>(decimal.Parse(temp[0].Replace('.', ',')), decimal.Parse(temp[1].Replace('.', ',')));
                    _vertices.Add(_ver);
                }
                string _resp = _obj.NuevaZona(_idZona, nombreZona, _idTipoZona, _vertices);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void EditarZona(string idZona, string nombreZona, string idTipoZona, string[] vertices)
        {
            int _idZona;
            int _idTipoZona;
            int.TryParse(idZona, out _idZona);
            int.TryParse(idTipoZona, out _idTipoZona);

            Methods_Zonas _obj = new Methods_Zonas();
            try
            {
                List<Tuple<decimal, decimal>> _vertices = new List<Tuple<decimal, decimal>>();
                for (int i = 0; i < vertices.Length; i++)
                {
                    string[] temp = vertices[i].Split(';');
                    Tuple<decimal, decimal> _ver = new Tuple<decimal, decimal>(decimal.Parse(temp[0].Replace('.', ',')), decimal.Parse(temp[1].Replace('.', ',')));
                    _vertices.Add(_ver);
                }
                string _resp = _obj.EditarZona(_idZona, nombreZona, _idTipoZona, _vertices);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");
            }
        }

        public void GetZonasToDraw()
        {
            string fechaDesde = "" + Request.QueryString["fechaDesde"];
            string fechaHasta = "" + Request.QueryString["fechaHasta"];
            string patente1 = "" + Request.QueryString["patente1"];
            string patente2 = "" + Request.QueryString["patente2"];

            DateTime _fechaDesde = new DateTime();
            DateTime _fechaHasta = new DateTime();

            try
            {
                //_fechaDesde = Convert.ToDateTime(fechaDesde);
                //_fechaHasta = Convert.ToDateTime(fechaHasta);
                if (DateTime.TryParseExact(fechaDesde.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaDesde))
                {
                }
                else
                {
                    DateTime.TryParseExact(fechaDesde.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaDesde);
                }

                if (DateTime.TryParseExact(fechaHasta.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaHasta))
                {
                }
                else
                {
                    DateTime.TryParseExact(fechaHasta.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaHasta);
                }
            }
            catch (Exception ex) {
            }

            

            Methods_Zonas _objMethosZonas = new Methods_Zonas();

            string _response = JsonConvert.SerializeObject(_objMethosZonas.GetZonasToDraw(_fechaDesde, _fechaHasta, patente1, patente2));

            Response.Write(_response);
        }

        public void GetLocales()
        {
            string username = Session["userName"].ToString();

            string _result = "";

            Methods_Zonas _obj = new Methods_Zonas();

            var _list = (from i in _obj.GetLocales()
                         where i.Usuario == username || i.CodLocal == 0
                         orderby (i.CodLocal)
                         select new
                         {
                             Usuario = username,
                             CodLocal = i.CodLocal,
                             NombreLocal = i.NombreLocal
                         }).ToList();
            _result = JsonConvert.SerializeObject(_list);

            Response.Write(_result);
        }

        public void GetZonesInsideZona(string idZona)
        {
            int _idZona;
            int.TryParse(idZona, out _idZona);

            Methods_Zonas _obj = new Methods_Zonas();

            Response.Write(_obj.ZonesInsideZone(_idZona).ToString());
        }

    }
}