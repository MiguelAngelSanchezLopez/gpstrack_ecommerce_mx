﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;
using System.Reflection;

using System.IO;

namespace Track_Web.AjaxPages
{
    public partial class AjaxLogin : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Request.QueryString["Metodo"])
            {
                case "ValidarUsuario":
                    ValidarUsuario(Request.Form["Usuario"].ToString(), Request.Form["Password"].ToString());
                    break;
                case "ValidarUsuarioLocal":
                    ValidarUsuarioLocal(Request.Form["Local"].ToString(), Request.Form["Usuario"].ToString(), Request.Form["Password"].ToString());
                    break;
                case "GetPerfil":
                    GetPerfil(Request.Form["Usuario"].ToString());
                    break;
                case "GetVersion":
                    GetVersion();
                    break;
                case "GetSessionExpirationStatus":
                    GetSessionExpirationStatus();
                    break;
                case "GetUsuarios":
                    GetUsuarios();
                    break;
                case "GuardarUsuario":
                    GuardarUsuario(Request.Form["idUsuario"].ToString(), Request.Form["userName"].ToString(), Request.Form["password"].ToString(), Request.Form["nombre"].ToString(), Request.Form["apellidos"].ToString(), Request.Form["eMail"].ToString(), Request.Form["agrupacion"].ToString(), Request.Form["activo"].ToString(), Request.Form["listCEDISAsociados"].ToString());
                    break;
            }
        }

        public void GuardarUsuario(string idUsuario, string userName, string password, string nombre, string apellidos, string eMail, string agrupacion, string activo, string listCEDISAsociados)
        {
            string idusuarioConectado = Utilities.GetIdUsuarioSession(Session);
            int _idUsuarioConectado = 0;

            int.TryParse(idusuarioConectado, out _idUsuarioConectado);

            int _idUsuario;
            int _activo;

            int.TryParse(idUsuario, out _idUsuario);

            if (activo == "true")
            {
                _activo = 1;
            }
            else
            {
                _activo = 0;
            }


            Methods_User _obj = new Methods_User();
            try
            {
                string _resp = _obj.GuardarUsuario(_idUsuario, userName, password, nombre, apellidos, eMail, agrupacion, _activo, listCEDISAsociados, _idUsuarioConectado);

                Response.Write(_resp);
            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void GetUsuarios()
        {
            string userName = Utilities.GetUsuarioSession(Session);

            Methods_User _objMethodsUser = new Methods_User();

            string _response = JsonConvert.SerializeObject(_objMethodsUser.GetUsuarios(userName));

            Response.Write(_response);
        }

        /*
            public void ValidarUsuario(string usuario, string password)
            {
              Methods_User _obj = new Methods_User();
              try
              {
                string respuesta = _obj.ValidarUsuario(usuario, password).ToString();

                if (respuesta == "Ok")
                {
                  Session["userName"] = usuario;

                }

                Response.Write(respuesta);

              }
              catch (Exception)
              {
                Response.Write("Se ha producido un error.");
              }
            }
        */
        public void ValidarUsuario(string usuario, string password)
        {
            Methods_User _obj = new Methods_User();
            try
            {
                string respuesta = _obj.ValidarUsuario(usuario, password).ToString();

                Session.Timeout = 1440;

                if (respuesta == "Ok")
                {
                    Track_Usuarios userlogado = _obj.getUsuarioConectado(usuario);

                    Session["userName"] = usuario;
                    Session["idusuario"] = userlogado.IdUsuario.ToString();

                    string transportista = _obj.getTransportista(usuario).ToString();

                    Session["transportista"] = transportista;

                    string perfilUsuarioConectado = _obj.getPerfilUsuarioConectado(usuario).ToString();
                    string nameUsuarioConectado = _obj.getNameUsuarioConectado(usuario).ToString();

                    Session["perfilUsuarioConectado"] = perfilUsuarioConectado;
                    Session["nameUsuarioConectado"] = nameUsuarioConectado;

                    int idRol = int.Parse(userlogado.IdRol.ToString()); //_obj.getidRol(usuario);
                    Session["idpefil"] = idRol.ToString();
                    Session["menu"] = JsonConvert.SerializeObject(_obj.getModulos(idRol));

                    _obj.guardarlog(userlogado.IdUsuario, "GPSTrack. Login");
                }

                Response.Write(respuesta);

            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void ValidarUsuarioLocal(string local, string usuario, string password)
        {
            Methods_User _obj = new Methods_User();
            try
            {
                string respuesta = _obj.ValidarUsuarioLocal(local, usuario, password).ToString();

                if (respuesta == "Ok")
                {
                    Session["local"] = local;
                    Session["userName"] = usuario;
                }

                Response.Write(respuesta);

            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void GetPerfil(string usuario)
        {
            Methods_User _obj = new Methods_User();
            try
            {
                Track_Usuarios _usuario = _obj.GetUsuario(usuario);

                if (_usuario.IdRol == 6)
                {
                    Session["cliente"] = _usuario.Transportista;
                }

                Response.Write(_usuario.IdRol);

            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

        public void GetVersion()
        {
            string assemblyVersion = Assembly.GetExecutingAssembly().GetName().Version.Major.ToString() + '.' + Assembly.GetExecutingAssembly().GetName().Version.Minor.ToString() + '.' + Assembly.GetExecutingAssembly().GetName().Version.Build.ToString();

            Response.Write(assemblyVersion);
        }

        public void GetSessionExpirationStatus()
        {
            Methods_Utilities _obj = new Methods_Utilities();
            try
            {
                string respuesta = _obj.SessionExpirationStatus().ToString();

                Response.Write(respuesta);

            }
            catch (Exception)
            {
                Response.Write("Se ha producido un error.");

            }
        }

    }
}