﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;

using System.IO;

using System.Globalization;
using System.Data.SqlClient;
using System.Configuration;
using System.Data;
using Track_Web.ModelosBD;

namespace Track_Web.AjaxPages
{
    public partial class AjaxViajes : System.Web.UI.Page
    {
        IFormatProvider culture = new CultureInfo("en-US", true);
        protected void Page_Load(object sender, EventArgs e)
        {
            switch (Request.QueryString["Metodo"])
            {
                case "GetTransportesRuta":
                    GetTransportesRuta();
                    break;
                case "GetPatentesRuta":
                    GetPatentesRuta();
                    break;
                case "GetViajesRuta":
                    GetViajesRuta();
                    break;
                case "GetTransportistasRuta":
                    GetTransportistasRuta();
                    break;
                case "GetPosicionesRuta":
                    GetPosicionesRuta();
                    break;
                case "GetAllPatentes":
                    GetAllPatentes();
                    break;
                case "GetAllTransportistas":
                    GetAllTransportistas();
                    break;
                case "GetViajesControlLocal":
                    GetViajesControlLocal();
                    break;
                case "GetPosicionesGPS":
                    GetPosicionesGPS();
                    break;
                case "GetPosicionesGPS_Ruta":
                    GetPosicionesGPS_Ruta();
                    break;
                case "GetDetalleTrayecto":
                    GetDetalleTrayecto();
                    break;
                case "GetNroTransportes":
                    GetNroTransportes();
                    break;
                case "GetTipoEtis":
                    GetTipoEtis();
                    break;
                case "GetClientes":
                    GetClientes();
                    break;
                case "GetClientesUsuario":
                    GetClientesUsuario();
                    break;
                case "GetFotografiasViaje":
                    GetFotografiasViaje();
                    break;
                case "GetViajesControlLocalCliente":
                    GetViajesControlLocalCliente();
                    break;
                case "GetFlotaOnline":
                    GetFlotaOnline();
                    break;
                case "GetDashboardControl":
                    GetDashboardControl();
                    break;
                case "GetMonitoreoOnline":
                    GetMonitoreoOnline();
                    break;
                case "GetProveedoresGPS":
                    GetProveedoresGPS();
                    break;
                case "GetCamionesCercanosBackhaul":
                    GetCamionesCercanosBackhaul();
                    break;
                case "GetAllCedisPool":
                    GetAllCedisPool();
                    break;
                case "GetAllPatentesPool":
                    GetAllPatentesPool();
                    break;
                case "GetAllCarrierPool":
                    GetAllCarrierPool();
                    break;
                case "GetAllPatentesPoolRemolques":
                    GetAllPatentesPoolRemolques();
                    break;
                case "GetInformeViajes":
                    GetInformeViajes();
                    break;
                case "GetTransporistasPool":
                    GetTransporistasPool();
                    break;
                case "GetinfoWindow":
                    GetinfoWindow(Request.Form["nrotransporte"].ToString());
                    break;
                case "GetViajesHistoricos":
                    GetViajesHistoricos();
                    break;
            }
        }

        public void GetViajesHistoricos()
        {
            Track_Usuarios _users = new Track_Usuarios();
            string userName = Utilities.GetUsuarioSession(Session);

            string desde = "" + Request.QueryString["desde"];
            string hasta = "" + Request.QueryString["hasta"];
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string patente = "" + Request.QueryString["patente"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];
            string transportista = "" + Request.QueryString["transportista"];
            string codLocal = "" + Request.QueryString["codLocal"];
            string tipoViaje = "" + Request.QueryString["tipoViaje"];

            DateTime _desde = Utilities.convertDateIc(desde);
            DateTime _hasta = Utilities.convertDateIc(hasta);
            long _nroTransporte;
            int _codLocal;

            //DateTime.TryParse(desde, out _desde);
            //DateTime.TryParse(hasta, out _hasta);

            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                long.TryParse(nroTransporte, out _nroTransporte);
            }

            int.TryParse(codLocal, out _codLocal);

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetViajesHistoricos(_desde, _hasta, _nroTransporte, patente, estadoViaje, transportista, _codLocal, tipoViaje, userName));

            Response.Write(_response);
        }


        public void GetinfoWindow(string nroTransporte)
        {

            Methods_Viajes _obj = new Methods_Viajes();
            string _response = JsonConvert.SerializeObject(_obj.InfoWindows(long.Parse(nroTransporte)));
            Response.Write(_response);
        }

        public void GetTransporistasPool()
        {
            string _cedis = "" + Request.QueryString["cedis"];
            string _result = "";

            //consulta a las placas de track_patentes_ecommerce de trasnporte_walmart_mx
            List<Transportista> listaTransportistasEcommerce = new List<Transportista>();

            try
            {
                InicializarConexion_TRANSPORTE_ECOMMERCE_MX();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetTransportistasEcommerce");

                //EstablecerParametroProcedimientoAlmacenado(Transportista, "string", "@Transportista");

                EjecutarProcedimientoAlmacenado();

                LlenarLectorDatosSQL();

                listaTransportistasEcommerce.AddRange(LlenarListadoTransportistasEcommerce());

                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            //if (_todas == "True")
            //listaTransportistasEcommerce.Insert(0, new Transportista { Transporte = "Todas" });


            List<string> listaCadenasTransportistasEcommerce = (from resultado in listaTransportistasEcommerce
                                                          select
                                                   resultado.Transporte
                             ).Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

            var listadoTransportistasEcommerceFormateado = (from Transporte in listaCadenasTransportistasEcommerce
                                                            select new
                                                            {
                                                                Transporte
                                                            }).ToList();

            _result = JsonConvert.SerializeObject(listadoTransportistasEcommerceFormateado);

            /*
            Methods_Viajes _obj = new Methods_Viajes();

            var _list = (from i in _obj.GetTransportistabyCedis(_cedis)
                         select new
                         {
                             i.Transporte
                         }).Distinct().ToList();
            _result = JsonConvert.SerializeObject(_list);
            */

            Response.Write(_result);
        }

        public void GetAllPatentesPoolRemolques()
        {
            string _transportista = "" + Request.QueryString["transportista"];
            string _todas = "" + Request.QueryString["Todas"];
            string _result = "";
            Methods_Viajes _obj = new Methods_Viajes();

            if (_todas == "True")
            {
                var _list = (from i in _obj.GetAllPatentesPoolRemolques(_transportista, true)
                             select new
                             {
                                 Patente = i.Placa,
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetAllPatentesPoolRemolques(_transportista)
                             select new
                             {
                                 Patente = i.Placa
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }

            Response.Write(_result);
        }


        public void GetAllCarrierPool()
        {
            string _todos = "" + Request.QueryString["Todos"];
            string _result = "";
            Methods_Viajes _obj = new Methods_Viajes();

            if (_todos == "True")
            {
                var _list = (from i in _obj.GetAllCarrierPool(true)
                             select new
                             {
                                 i.Carrier
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetAllCarrierPool()
                             select new
                             {
                                 i.Carrier
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }

            Response.Write(_result);
        }

        #region nuevocodigopruebasp030718
        //codigo para probar nuevo sp //030718

        protected SqlConnection conexionSQL;
        protected SqlCommand comandoSQL;
        protected SqlDataReader lectorDatosSQL;

        //public string CADENA_CONEXION = ConfigurationManager.ConnectionStrings["Conexion_Track_Patentes_Ecommerce"].ConnectionString;

        public string CADENA_CONEXION_TRANSPORTE_ECOMMERCE_MX = ConfigurationManager.ConnectionStrings["ConexionPerfilamiento"].ConnectionString;
        /*
        public void InicializarConexion()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        */
        public void InicializarConexion_TRANSPORTE_ECOMMERCE_MX()
        {
            try
            {
                conexionSQL = new SqlConnection(CADENA_CONEXION_TRANSPORTE_ECOMMERCE_MX);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EstablecerProcedimientoAlmacenado(string nombreProcedimientoAlmacenado)
        {
            try
            {
                comandoSQL = new SqlCommand(nombreProcedimientoAlmacenado, conexionSQL);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void EjecutarProcedimientoAlmacenado()
        {
            comandoSQL.ExecuteNonQuery();
        }

        public void EstablecerParametroProcedimientoAlmacenado(Object valorParametro, string descripcionTipo, string nombreParametro)
        {
            comandoSQL.CommandType = CommandType.StoredProcedure;
            comandoSQL.Parameters.Add(nombreParametro, ObtenerTipoDatoParaConsultaSQL(descripcionTipo)).Value = valorParametro;
        }

        public SqlDbType ObtenerTipoDatoParaConsultaSQL(string descripcionTipo)
        {
            SqlDbType tipoDatoDevolver = new SqlDbType();
            switch (descripcionTipo)
            {
                case "int":
                    tipoDatoDevolver = SqlDbType.Int;
                    break;
                case "string":
                    tipoDatoDevolver = SqlDbType.NVarChar;
                    break;
                case "bool":
                    tipoDatoDevolver = SqlDbType.Bit;
                    break;
                case "datetime":
                    tipoDatoDevolver = SqlDbType.DateTime;
                    break;
            }
            return tipoDatoDevolver;
        }

        public void LlenarLectorDatosSQL()
        {
            try
            {
                lectorDatosSQL = comandoSQL.ExecuteReader();
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public List<Transportista> LlenarListadoTransportistasEcommerce() {
            List<Transportista> listadoTransportistasEcommerce = new List<Transportista>();

            while (lectorDatosSQL.Read())
                listadoTransportistasEcommerce.Add(LlenarElementoTransportistaEcommerce());

            return listadoTransportistasEcommerce;
        }

        public Transportista LlenarElementoTransportistaEcommerce()
        {
            Transportista elementoTransportistaEcommerce = new Transportista();

            elementoTransportistaEcommerce.Transporte = lectorDatosSQL["Transportista"].ToString();

            return elementoTransportistaEcommerce;
        }

        public List<Patente_Ecommerce> LlenarListadoPatentesEcommerce()
        {
            List<Patente_Ecommerce> listadoPatentesEcommerce = new List<Patente_Ecommerce>();

            while (lectorDatosSQL.Read())
                listadoPatentesEcommerce.Add(LlenarElementoPatenteEcommerce());

            return listadoPatentesEcommerce;
        }

        
        public Patente_Ecommerce LlenarElementoPatenteEcommerce()
        {
            Patente_Ecommerce elementoPatenteEcommerce = new Patente_Ecommerce();
            
            elementoPatenteEcommerce.Patente = lectorDatosSQL["Patente"].ToString();
            
            return elementoPatenteEcommerce;
        }


        //fin de codigo para porbar nuevo sp //030718
        #endregion nuevocodigopruebasp030718

        public string ConsultaPatentesEcommerce() {

            string _todas = "" + Request.QueryString["Todas"];
            string _result = "";

            //consulta a las placas de track_patentes_ecommerce de trasnporte_walmart_mx
            List<Patente_Ecommerce> listaPatentesEcommerce = new List<Patente_Ecommerce>();

            try
            {
                InicializarConexion_TRANSPORTE_ECOMMERCE_MX();
                conexionSQL.Open();

                EstablecerProcedimientoAlmacenado("Track_GetPatentesEcommerce");

                //EstablecerParametroProcedimientoAlmacenado(Transportista, "string", "@Transportista");

                EjecutarProcedimientoAlmacenado();

                LlenarLectorDatosSQL();

                listaPatentesEcommerce.AddRange(LlenarListadoPatentesEcommerce());

                conexionSQL.Close();
            }
            catch (Exception ex)
            {
                throw;
            }

            if (_todas == "True")
                listaPatentesEcommerce.Insert(0, new Patente_Ecommerce { Patente = "Todas" });


            List<string> listaCadenasPatentesEcommerce = (from resultado in listaPatentesEcommerce
                                                          select
                                                   resultado.Patente
                             ).Distinct(StringComparer.CurrentCultureIgnoreCase).ToList();

            var listadoPatentesEcommerceFormateado = (from Patente in listaCadenasPatentesEcommerce
                                                      select new
                                                      {
                                                          Patente
                                                      }).ToList();

            _result = JsonConvert.SerializeObject(listadoPatentesEcommerceFormateado);

            return _result;
        }

        public void GetAllPatentesPool()
        {
            string _transportista = "" + Request.QueryString["transportista"];
            string _todas = "" + Request.QueryString["Todas"];
            string _result = "";
            
            /*
            Methods_Viajes _obj = new Methods_Viajes();

            if (_todas == "True")
            {
                var _list = (from i in _obj.GetAllPatentesPool(_transportista, true)
                             select new
                             {
                                 Patente = i.Placa,
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetAllPatentesPool(_transportista)
                             select new
                             {
                                 Patente = i.Placa
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            */


            //Response.Write(_result);

            Response.Write(ConsultaPatentesEcommerce());
        }


        public void GetAllCedisPool()
        {
            string _transportista = "" + Request.QueryString["transportista"];
            string _todas = "" + Request.QueryString["Todas"];
            string _result = "";
            Methods_Viajes _obj = new Methods_Viajes();

            string userName = Utilities.GetUsuarioSession(Session);

            if (_todas == "True")
            {
                /*var _list = (from i in _obj.GetAllCedisPool(_transportista, userName, true)
                             select new
                             {
                               i.Cedis
                             }).Union
                             (from i in _obj.GetAllCedisPoolRemolque(_transportista, userName, true)
                              select new
                              {
                                  i.Cedis
                              }).Distinct().ToList();*/
                var _list = (from i in _obj.GetAllCedisPool(_transportista, userName, true)
                             select new
                             {
                                 i.Cedis
                             }).Distinct().ToList();

                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetAllCedisPool(_transportista, userName)
                             select new
                             {
                                 i.Cedis
                             }).Distinct().ToList();

                _result = JsonConvert.SerializeObject(_list);
            }

            Response.Write(_result);
        }


        public void GetCamionesCercanosBackhaul()
        {
            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetCamionesCercanosBackhaul());

            Response.Write(_response);
        }


        public void GetProveedoresGPS()
        {
            string _todos = "" + Request.QueryString["Todos"];
            string _rutTransportista = "" + Request.QueryString["Transportista"];

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetProveedoresGPS(true, _rutTransportista));

            Response.Write(_response);

        }

        public void GetMonitoreoOnline()
        {
            string userName = Utilities.GetUsuarioSession(Session);

            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];
            string estadoGPS = "" + Request.QueryString["estadoGPS"];
            string transportista = "" + Request.QueryString["transportista"];
            string proveedorGPS = "" + Request.QueryString["proveedorGPS"];
            string patente = "" + Request.QueryString["patente"];

            long _nroTransporte;

            long.TryParse(nroTransporte, out _nroTransporte);

            int _ignicion = -1;

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetMonitoreoOnline(patente, transportista, _ignicion, estadoViaje, estadoGPS, proveedorGPS, _nroTransporte, userName));
            //string _response = JsonConvert.SerializeObject(_objMethosViajes.GetMonitoreoOnline(patente, transportista, _ignicion, "Todos", estadoGPS, proveedorGPS, _nroTransporte, userName));

            Response.Write(_response);
        }


        public void GetDashboardControl()
        {
            Track_Usuarios _users = new Track_Usuarios();
            string userName = Utilities.GetUsuarioSession(Session);

            string desde = "" + Request.QueryString["desde"];
            string hasta = "" + Request.QueryString["hasta"];
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string patenteTracto = "" + Request.QueryString["patenteTracto"];
            string patenteTrailer = "" + Request.QueryString["patenteTrailer"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];
            string alertas = "" + Request.QueryString["alertas"];
            string transportista = "" + Request.QueryString["transportista"];

            DateTime _desde = Utilities.convertDateIc(desde);
            DateTime _hasta = Utilities.convertDateIc(hasta);
            long _nroTransporte;

            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                long.TryParse(nroTransporte, out _nroTransporte);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetViajesDashboardControl(_desde, _hasta, _nroTransporte, patenteTracto, patenteTrailer, estadoViaje, transportista, alertas, userName));

            Response.Write(_response);
        }

        public void GetFlotaOnline()
        {
            string patente = "" + Request.QueryString["patente"];
            string transportista = "" + Request.QueryString["transportista"];
            //string ignicion = "" + Request.QueryString["ignicion"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];
            string estadoGPS = "" + Request.QueryString["estadoGPS"];
            string proveedorGPS = "" + Request.QueryString["proveedorGPS"];

            int _ignicion = -1;

            //int.TryParse(ignicion, out _ignicion);

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetFlotaOnline(patente, transportista, _ignicion, estadoViaje, estadoGPS, proveedorGPS));

            Response.Write(_response);
        }

        public void GetTransportesRuta()
        {
            string _todos = "" + Request.QueryString["Todos"];

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetNroTransportesRuta());

            Response.Write(_response);

        }

        public void GetPatentesRuta()
        {
            string _todos = "" + Request.QueryString["Todos"];

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetPatentesRuta());

            Response.Write(_response);

        }

        public void GetTransportistasRuta()
        {
            string _todos = "" + Request.QueryString["Todos"];

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetTransportistasRuta());

            Response.Write(_response);

        }

        public void GetViajesRuta()
        {
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string patente = "" + Request.QueryString["patente"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];
            string transportista = "" + Request.QueryString["transportista"];

            int _nroTransporte;
            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                int.TryParse(nroTransporte, out _nroTransporte);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetViajesRuta(_nroTransporte, patente, estadoViaje, transportista));

            Response.Write(_response);
        }

        public void GetPosicionesRuta()
        {
            string patenteTracto = "" + Request.QueryString["patenteTracto"];
            string patenteTrailer = "" + Request.QueryString["patenteTrailer"];
            string fechaCreacion = "" + Request.QueryString["fechaHoraCreacion"];
            string fechaSalidaOrigen = "" + Request.QueryString["fechaHoraSalidaOrigen"];
            string fechaLlegadaDestino = "" + Request.QueryString["fechaHoraLlegadaDestino"];
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string idEmbarque = "" + Request.QueryString["idEmbarque"];
            string destino = "" + Request.QueryString["destino"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];

            int _nroTransporte;
            int _idEmbarque;
            int _destino;

            int.TryParse(nroTransporte, out _nroTransporte);
            int.TryParse(idEmbarque, out _idEmbarque);
            int.TryParse(destino, out _destino);

            DateTime _fechaCreacion;
            DateTime _fechaSalidaOrigen;
            DateTime _fechaLlegadaDestino;

            if (fechaCreacion == "")
            {
                fechaCreacion = DateTime.UtcNow.AddHours(-3).ToString("yyyy-MM-ddTHH\\:mm");
            }

            if (fechaSalidaOrigen == "")
            {
                fechaSalidaOrigen = DateTime.UtcNow.AddHours(-3).ToString("yyyy-MM-ddTHH\\:mm");
            }

            if (fechaLlegadaDestino == "")
            {
                fechaLlegadaDestino = DateTime.UtcNow.AddHours(-3).ToString("yyyy-MM-ddTHH\\:mm");
            }

            if (DateTime.TryParseExact(fechaCreacion.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaCreacion))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaCreacion.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaCreacion);
            }

            if (DateTime.TryParseExact(fechaSalidaOrigen.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaSalidaOrigen))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaSalidaOrigen.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaSalidaOrigen);
            }

            if (DateTime.TryParseExact(fechaLlegadaDestino.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaLlegadaDestino))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaLlegadaDestino.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaLlegadaDestino);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetPosicionesRuta(patenteTracto, patenteTrailer, _fechaCreacion, _fechaSalidaOrigen, _fechaLlegadaDestino, _nroTransporte, _idEmbarque, _destino, estadoViaje));

            Response.Write(_response);
        }

        public void GetAllPatentes()
        {
            string _transportista = "" + Request.QueryString["transportista"];
            string _todas = "" + Request.QueryString["Todas"];
            string _result = "";
           
            /*
            Methods_Viajes _obj = new Methods_Viajes();

            if (_todas == "True")
            {
                var _list = (from i in _obj.GetAllPatentes(_transportista, true)
                             select new
                             {
                                 i.Patente,
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetAllPatentes(_transportista)
                             select new
                             {
                                 i.Patente
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            */
            //Response.Write(_result);
            Response.Write(ConsultaPatentesEcommerce());
        }

        public void GetAllTransportistas()
        {
            string _todos = "" + Request.QueryString["Todos"];
            string _result = "";
            Methods_Viajes _obj = new Methods_Viajes();

            if (_todos == "True")
            {
                var _list = (from i in _obj.GetAllTransportistas(true)
                             where i.Transportista != null
                             select new
                             {
                                 i.Transportista,
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }
            else
            {
                var _list = (from i in _obj.GetAllTransportistas()
                             select new
                             {
                                 i.Transportista
                             }).Distinct().ToList();
                _result = JsonConvert.SerializeObject(_list);
            }

            Response.Write(_result);
        }
        /*
        public void GetViajesLocal()
        {
          string desde = "" + Request.QueryString["desde"];
          string hasta = "" + Request.QueryString["hasta"];
          string rutCliente = Request.QueryString["rutCliente"];
          string nroTransporte = "" + Request.QueryString["nroTransporte"];
          string transportista = "" + Request.QueryString["transportista"];
          string tipoEtis = "" + Request.QueryString["tipoEtis"];
          string username = Session["userName"].ToString();
          string estadoViaje = "" + Request.QueryString["estadoViaje"];

          int _nroTransporte;
          if (nroTransporte == "Todos" || nroTransporte == "")
          {
            _nroTransporte = 0;
          }
          else
          {
            int.TryParse(nroTransporte, out _nroTransporte);
          }

          DateTime _desde;
          DateTime _hasta;

          DateTime.TryParse(desde, out _desde);
          DateTime.TryParse(hasta, out _hasta);

          Methods_Viajes _objMethosViajes = new Methods_Viajes();

          string _response = JsonConvert.SerializeObject(_objMethosViajes.GetViajesLocal(_desde, _hasta, rutCliente, _nroTransporte, transportista, username, tipoEtis, estadoViaje));

          Response.Write(_response);
        }*/

        public void GetViajesControlLocal()
        {
            string desde = "" + Request.QueryString["desde"];
            string hasta = "" + Request.QueryString["hasta"];

            string rutCliente = string.Empty;
            if (Request.QueryString["rutCliente"].Contains("Todos"))
                rutCliente = "Todos";
            else
                rutCliente = Request.QueryString["rutCliente"];
            
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string transportista = "" + Request.QueryString["transportista"];
            string tipoEtis = "" + Request.QueryString["tipoEtis"];
            string estadoViaje = "" + Request.QueryString["estadoViaje"];
            string tipoCarga = "" + Request.QueryString["tipoCarga"];
            string pod = "" + Request.QueryString["pod"];
            string otif = "" + Request.QueryString["otif"];
            string nroContenedor = "" + Request.QueryString["nroContenedor"];
            string problema = "" + Request.QueryString["problema"];
            bool FiltroFH = bool.Parse( Request.QueryString["FilFH"]);


            string username = Session["userName"].ToString();

            int _nroTransporte;
            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                int.TryParse(nroTransporte, out _nroTransporte);
            }

            DateTime _desde;
            DateTime _hasta;

            //DateTime.TryParse(desde, out _desde);
            //DateTime.TryParse(hasta, out _hasta);
            if (DateTime.TryParseExact(desde.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _desde))
            {
            }
            else
            {
                DateTime.TryParseExact(desde.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _desde);
            }

            if (DateTime.TryParseExact(hasta.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _hasta))
            {
            }
            else
            {
                DateTime.TryParseExact(hasta.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _hasta);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetViajesControlLocal(_desde, _hasta, rutCliente, _nroTransporte, transportista, username, tipoEtis, estadoViaje, tipoCarga, pod, otif, nroContenedor, problema, FiltroFH));

            Response.Write(_response);
        }
        public void GetPosicionesGPS()
        {
            string fechaDesde = "" + Request.QueryString["fechaDesde"];
            string fechaHasta = "" + Request.QueryString["fechaHasta"];

            string horaDesde = "" + Request.QueryString["HoraDesde"];
            string horaHasta = "" + Request.QueryString["HoraHasta"];

            string patente = "" + Request.QueryString["patente"];

            if (horaDesde.Trim() != "")
                fechaDesde = fechaDesde.Replace("T00:00:00", "T" + horaDesde + ":00");

            if (horaHasta.Trim() != "")
                fechaHasta = fechaHasta.Replace("T00:00:00", "T" + horaHasta + ":00");

            DateTime _fechaDesde;
            DateTime _fechaHasta;

            _fechaDesde = Utilities.convertDateIc(fechaDesde); // Convert.ToDateTime(fechaDesde);
            _fechaHasta = Utilities.convertDateIc(fechaHasta); // Convert.ToDateTime(fechaHasta);

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetPosicionesGPS(_fechaDesde, _fechaHasta, patente));

            Response.Write(_response);
        }
        /*
        public void GetPosicionesGPS()
        {
            string fechaDesde = "" + Request.QueryString["fechaDesde"];
            string fechaHasta = "" + Request.QueryString["fechaHasta"];
            string patente = "" + Request.QueryString["patente"];

            DateTime _fechaDesde;
            DateTime _fechaHasta;

            //_fechaDesde = Convert.ToDateTime(fechaDesde);
            //_fechaHasta = Convert.ToDateTime(fechaHasta);
            if (DateTime.TryParseExact(fechaDesde.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaDesde))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaDesde.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaDesde);
            }

            if (DateTime.TryParseExact(fechaHasta.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaHasta))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaHasta.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaHasta);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetPosicionesGPS(_fechaDesde, _fechaHasta, patente));

            Response.Write(_response);
        }
        */
        public void GetPosicionesGPS_Ruta()
        {
            string fechaDesde = "" + Request.QueryString["fechaDesde"];
            string fechaHasta = "" + Request.QueryString["fechaHasta"];
            string patente = "" + Request.QueryString["patente"];

            DateTime _fechaDesde;
            DateTime _fechaHasta;

            //_fechaDesde = Convert.ToDateTime(fechaDesde);
            //_fechaHasta = Convert.ToDateTime(fechaHasta);
            if (DateTime.TryParseExact(fechaDesde.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaDesde))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaDesde.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaDesde);
            }

            if (DateTime.TryParseExact(fechaHasta.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaHasta))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaHasta.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaHasta);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetPosicionesGPS_Ruta(_fechaDesde, _fechaHasta, patente));

            Response.Write(_response);
        }

        /*public void GetInformeViajes()
        {
          string desde = "" + Request.QueryString["desde"];
          string hasta = "" + Request.QueryString["hasta"];
          string nroTransporte = "" + Request.QueryString["nroTransporte"];
          string patente = "" + Request.QueryString["patente"];
          string transportista = "" + Request.QueryString["transportista"];

          DateTime _desde;
          DateTime _hasta;
          int _nroTransporte;

          DateTime.TryParse(desde, out _desde);
          DateTime.TryParse(hasta, out _hasta);

          if (nroTransporte == "Todos" || nroTransporte == "")
          {
            _nroTransporte = 0;
          }
          else
          {
            int.TryParse(nroTransporte, out _nroTransporte);
          }

          Methods_Viajes _objMethosViajes = new Methods_Viajes();

          string _response = JsonConvert.SerializeObject(_objMethosViajes.GetInformeViajes(_desde, _hasta, _nroTransporte, patente, transportista));

          Response.Write(_response);
        }*/

        public void GetInformeViajes()
        {
            Track_Usuarios _users = new Track_Usuarios();
            string userName = Utilities.GetUsuarioSession(Session);

            string desde = "" + Request.QueryString["desde"];
            string hasta = "" + Request.QueryString["hasta"];
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string patente = "" + Request.QueryString["patente"];
            string transportista = "" + Request.QueryString["transportista"];

            DateTime _desde = Utilities.convertDateIc(desde);
            DateTime _hasta = Utilities.convertDateIc(hasta);
            long _nroTransporte;

            if (nroTransporte == "Todos" || nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                long.TryParse(nroTransporte, out _nroTransporte);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetInformeViajes(_desde, _hasta, _nroTransporte, patente, transportista, userName));

            Response.Write(_response);
        }

        public void GetDetalleTrayecto()
        {
            string nroTransporte = "" + Request.QueryString["nroTransporte"];

            int _nroTransporte;

            int.TryParse(nroTransporte, out _nroTransporte);


            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetDetalleTrayecto(_nroTransporte));

            Response.Write(_response);
        }

        public void GetNroTransportes()
        {
            string _todos = "" + Request.QueryString["Todos"];

            string fechaDesde = "" + Request.QueryString["desde"];
            string fechaHasta = "" + Request.QueryString["hasta"];

            DateTime _fechaDesde;
            DateTime _fechaHasta;

            //DateTime.TryParse(fechaDesde, out _fechaDesde);
            //DateTime.TryParse(fechaHasta, out _fechaHasta);
            if (DateTime.TryParseExact(fechaDesde.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaDesde))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaDesde.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaDesde);
            }

            if (DateTime.TryParseExact(fechaHasta.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _fechaHasta))
            {
            }
            else
            {
                DateTime.TryParseExact(fechaHasta.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _fechaHasta);
            }

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetNroTransportes(_fechaDesde, _fechaHasta));

            Response.Write(_response);

        }

        public void GetTipoEtis()
        {
            string _todos = "" + Request.QueryString["Todos"];

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetTipoEtis(true));

            Response.Write(_response);

        }

        public void GetClientes()
        {
            string _todos = "" + Request.QueryString["Todos"];

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetClientes(true));

            Response.Write(_response);

        }

        public void GetClientesUsuario()
        {
            string username = Session["userName"].ToString();
            string _todos = "" + Request.QueryString["Todos"];
            bool todos = true;

            if (_todos == "False")
            {
                todos = false;
            }

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetClientesUsuario(todos, username));

            Response.Write(_response);

        }

        public void GetFotografiasViaje()
        {
            string nroTransporte = "" + Request.QueryString["nroTransporte"];

            int _nroTransporte;

            int.TryParse(nroTransporte, out _nroTransporte);

            Methods_Viajes _objMethodsViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethodsViajes.GetFotografiasViaje(_nroTransporte));

            Response.Write(_response);

        }

        public void GetViajesControlLocalCliente()
        {
            string desde = "" + Request.QueryString["desde"];
            string hasta = "" + Request.QueryString["hasta"];
            string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string codOrigen = "" + Request.QueryString["codOrigen"];
            string patente = "" + Request.QueryString["patente"];
            string tipoViaje = "" + Request.QueryString["tipoViaje"];

            string username = Session["userName"].ToString();
            int _codOrigen;
            long _nroTransporte;

            if (nroTransporte == "")
            {
                _nroTransporte = 0;
            }
            else
            {
                long.TryParse(nroTransporte, out _nroTransporte);
            }

            int.TryParse(codOrigen, out _codOrigen);

            DateTime _desde;
            DateTime _hasta;

            if (DateTime.TryParseExact(desde.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _desde))
            {
            }
            else
            {
                DateTime.TryParseExact(desde.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _desde);
            }

            if (DateTime.TryParseExact(hasta.Replace("-", "/").Substring(0, 16), "dd/MM/yyyy HH:mm", culture, DateTimeStyles.None, out _hasta))
            {
            }
            else
            {
                DateTime.TryParseExact(hasta.Replace("T", " ").Substring(0, 16), "yyyy-MM-dd HH:mm", culture, DateTimeStyles.None, out _hasta);
            }

            Methods_Viajes _objMethosViajes = new Methods_Viajes();

            string _response = JsonConvert.SerializeObject(_objMethosViajes.GetViajesControlLocalCliente(_desde, _hasta,  _nroTransporte, _codOrigen, patente, tipoViaje, username));

            Response.Write(_response);
        }



    }
}