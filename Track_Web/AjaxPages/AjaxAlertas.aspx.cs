﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using BusinessEntities;
using BusinessLayer;
using Newtonsoft.Json;
using UtilitiesLayer;

using System.IO;

namespace Track_Web.AjaxPages
{
  public partial class AjaxAlertas : System.Web.UI.Page
  {
    protected void Page_Load(object sender, EventArgs e)
    {
      switch (Request.QueryString["Metodo"])
      {
        case "GetAlertasRuta":
          GetAlertasRuta();
          break;
        case "GetTipoAlertas":
          GetTipoAlertas();
          break;
        case "GuardarCongifAlerta":
          GuardarCongifAlerta(Request.Form["IdTipoAlerta"].ToString(), Request.Form["Control1"].ToString(), Request.Form["Control2"].ToString(), Request.Form["Control3"].ToString(), Request.Form["Control4"].ToString(), Request.Form["alertaActiva"].ToString());
          break;
        case "GetAlertasInformeViaje":
          GetAlertasInformeViaje();
          break;
        case "UpdateObservacionesInformeViaje":
          UpdateObservacionesInformeViaje(Request.Form["idAlerta"].ToString(), Request.Form["nroTransporte"].ToString(), Request.Form["observaciones"].ToString());
          break;
        case "GetGestionCallCenter":
          GetGestionCallCenter();
          break;
        case "GetAlertasPorGestionarCallCenter":
          GetAlertasPorGestionarCallCenter();
          break;
        case "GetGestionView":
          GetGestionView();
          break;
        case "SearchIdentifiers":
          SearchIdentifiers(Request.Form["nroTransporte"].ToString(), Request.Form["destino"].ToString(), Request.Form["opcion"].ToString());
          break;
      }
    }

    public void GetAlertasRuta()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string idEmbarque = "" + Request.QueryString["idEmbarque"];
            string destino = "" + Request.QueryString["destino"];
      string estadoViaje = "" + Request.QueryString["estadoViaje"];

      long _nroTransporte;
    long _idEmbarque;
    int _destino;

      long.TryParse(nroTransporte, out _nroTransporte);
            long.TryParse(idEmbarque, out _idEmbarque);
            int.TryParse(destino, out _destino);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();

      string _response = JsonConvert.SerializeObject(_objMethodsAlertas.GetAlertasRuta(_nroTransporte, _idEmbarque, _destino, estadoViaje));

      Response.Write(_response);
    }

    public void GetTipoAlertas()
    {

      string _result = "";
      Methods_Alertas _obj = new Methods_Alertas();

        var _list = (from i in _obj.GetTipoAlertas()
                     select new
                     {
                       i.IdTipoAlerta,
                       i.NombreTipoAlerta,
                       i.AlertaActiva,
                       i.DescripcionAlerta,
                       i.Control1,
                       i.Control2,
                       i.Control3,
                       i.Control4,
                       i.InfoAlerta
                     }).ToList();
        _result = JsonConvert.SerializeObject(_list);

      Response.Write(_result);
    }

    public void GuardarCongifAlerta(string idTipoAlerta, string control1, string control2, string control3, string control4, string alertaActiva)
    {
      int _idTipoAlerta;
      int _control1;
      int _control2;
      int _control3;
      int _control4;
      bool _alertaActiva;

      int.TryParse(idTipoAlerta, out _idTipoAlerta);
      int.TryParse(control1, out _control1);
      int.TryParse(control2, out _control2);
      int.TryParse(control3, out _control3);
      int.TryParse(control4, out _control4);
      bool.TryParse(alertaActiva, out _alertaActiva);

      Methods_Alertas _obj = new Methods_Alertas();

      string _result = _obj.GuardarConfigAlerta(_idTipoAlerta, _control1, _control2, _control3, _control4, _alertaActiva);

      Response.Write(_result);
    }

    public void GetAlertasInformeViaje()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];

      int _nroTransporte;

      int.TryParse(nroTransporte, out _nroTransporte);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();

      string _response = JsonConvert.SerializeObject(_objMethodsAlertas.GetAlertasInformeViaje(_nroTransporte));

      Response.Write(_response);
    }

    public void UpdateObservacionesInformeViaje(string idAlerta, string nroTransporte, string observaciones)
    {
      int _idAlerta;
      int _nroTransporte;

      int.TryParse(idAlerta, out _idAlerta);
      int.TryParse(nroTransporte, out _nroTransporte);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();

      string _response = JsonConvert.SerializeObject(_objMethodsAlertas.UpdateObservacionesInformeViaje(_idAlerta, _nroTransporte, observaciones));

      Response.Write(_response);

    }

    public void GetGestionCallCenter()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string idEmbarque = "" + Request.QueryString["idEmbarque"];
            string codLocal = "" + Request.QueryString["codLocal"];

      long _nroTransporte;
            long _idEmbarque;
            int _codLocal;

      long.TryParse(nroTransporte, out _nroTransporte);
            long.TryParse(idEmbarque, out _idEmbarque);
            int.TryParse(codLocal, out _codLocal);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();

      string _response = JsonConvert.SerializeObject(_objMethodsAlertas.GetGestionCallCenter(_nroTransporte, _idEmbarque, _codLocal));

      Response.Write(_response);
    }

    public void GetAlertasPorGestionarCallCenter()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];
            string idEmbarque = "" + Request.QueryString["idEmbarque"];
            string codLocal = "" + Request.QueryString["codLocal"];

      long _nroTransporte;
            long _idEmbarque;
            int _codLocal;

      long.TryParse(nroTransporte, out _nroTransporte);
            long.TryParse(idEmbarque, out _idEmbarque);
            int.TryParse(codLocal, out _codLocal);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();

      string _response = JsonConvert.SerializeObject(_objMethodsAlertas.GetAlertasPorGestionarCallCenter(_nroTransporte, _idEmbarque, _codLocal));

      Response.Write(_response);
    }

    public void GetGestionView()
    {
      string nroTransporte = "" + Request.QueryString["nroTransporte"];
      string codLocal = "" + Request.QueryString["codLocal"];

      int _nroTransporte;
      int _codLocal;

      int.TryParse(nroTransporte, out _nroTransporte);
      int.TryParse(codLocal, out _codLocal);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();

      string _response = JsonConvert.SerializeObject(_objMethodsAlertas.GetGestionView(_nroTransporte, _codLocal));

      Response.Write(_response);
    }

    public void SearchIdentifiers(string nroTransporte, string destino, string opcion)
    {
      int _opcion;

      int.TryParse(opcion, out _opcion);

      Methods_Alertas _objMethodsAlertas = new Methods_Alertas();
      try
      {
        string respuesta = _objMethodsAlertas.SearchIdentifier(nroTransporte, destino, _opcion).ToString();

        Response.Write(respuesta);

      }
      catch (Exception)
      {
        Response.Write("Se ha producido un error.");

      }
    }

  }
}