//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    using System;
    
    public partial class Track_GetRpt_Alertas_Result
    {
        public Nullable<System.DateTime> FechaInicioAlerta { get; set; }
        public Nullable<System.DateTime> FechaHoraCreacion { get; set; }
        public string TextFechaCreacion { get; set; }
        public long NroTransporte { get; set; }
        public long IdEmbarque { get; set; }
        public Nullable<int> CodigoDestino { get; set; }
        public string NombreDestino { get; set; }
        public string TipoAlerta { get; set; }
        public string DescripcionAlerta { get; set; }
        public string PatenteTracto { get; set; }
        public string PatenteTrailer { get; set; }
        public string RutConductor { get; set; }
        public string NombreConductor { get; set; }
        public string ScoreConductor { get; set; }
        public string Transportista { get; set; }
        public string Velocidad { get; set; }
        public string Latitud { get; set; }
        public string Longitud { get; set; }
        public Nullable<int> Ocurrencia { get; set; }
        public string Temp1 { get; set; }
        public string Permiso { get; set; }
        public string Formato { get; set; }
        public string EstadoViaje { get; set; }
        public string ProveedorGPS { get; set; }
    }
}
