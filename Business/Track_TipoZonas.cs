//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    
    using System;
    using System.Collections.Generic;
    
    using System;
    using System.Collections.Generic;
    
    public partial class Track_TipoZonas
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Track_TipoZonas()
        {
            this.Track_Zonas = new HashSet<Track_Zonas>();
        }
    
        public int IdTipoZona { get; set; }
        public string NombreTipoZona { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Track_Zonas> Track_Zonas { get; set; }
    }
}
