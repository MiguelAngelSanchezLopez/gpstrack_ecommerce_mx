//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    using System;
    
    public partial class Track_GetRpt_MonitoreoDiario_Result
    {
        public string Carga { get; set; }
        public Nullable<int> NroTransporte { get; set; }
        public Nullable<int> CodigoInterno { get; set; }
        public string NombreLocal { get; set; }
        public Nullable<int> OrdenEntrega { get; set; }
        public string NombreTransportista { get; set; }
        public string HoraDespacho { get; set; }
        public string VentanaHoraria { get; set; }
        public string Plataforma { get; set; }
        public string Visualizacion { get; set; }
        public string FechaDespacho { get; set; }
        public string HoraDespachoReal { get; set; }
        public string FechaEntregaLocal { get; set; }
        public string HoraEntregaLocal { get; set; }
        public string CumpleVentana { get; set; }
        public string CumpleDespacho { get; set; }
    }
}
