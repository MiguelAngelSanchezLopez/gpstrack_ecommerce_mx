//------------------------------------------------------------------------------
// <auto-generated>
//     Este código se generó a partir de una plantilla.
//
//     Los cambios manuales en este archivo pueden causar un comportamiento inesperado de la aplicación.
//     Los cambios manuales en este archivo se sobrescribirán si se regenera el código.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BusinessEntities
{
    
    using System;
    using System.Collections.Generic;
    
    using System;
    using System.Collections.Generic;
    
    public partial class PoolPlaca
    {
        public int Id { get; set; }
        public string Placa { get; set; }
        public string TipoVehiculo { get; set; }
        public string Transporte { get; set; }
        public string Cedis { get; set; }
        public Nullable<int> Determinante { get; set; }
        public Nullable<int> IdUsuarioTransportista { get; set; }
        public Nullable<int> IdCentroDistribucion { get; set; }
        public Nullable<int> IdUsuarioCreacion { get; set; }
        public Nullable<System.DateTime> FechaCarga { get; set; }
        public string NumeroEconomico { get; set; }
    
        public virtual Usuario Usuario { get; set; }
        public virtual Usuario Usuario1 { get; set; }
    }
}
