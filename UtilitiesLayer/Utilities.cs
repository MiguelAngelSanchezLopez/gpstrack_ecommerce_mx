﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web.SessionState;
using System.Web;
using Amazon.S3;
using Amazon;
using Amazon.S3.Model;
using System.Configuration;
using System.Globalization;

namespace UtilitiesLayer
{
    public class Utilities
    {
        public static void VerifyLoginStatus(HttpSessionState _session, HttpResponse _response)
        {
            if (_session["userName"] == null)
            {
                _response.Redirect("Login.aspx");
            }
        }

        public static void ResetSession(HttpSessionState _session)
        {
            _session["userName"] = null;
        }

        public static string GetExePath()
        {
            return (System.AppDomain.CurrentDomain.BaseDirectory);
        }

        public void LimpiarCache()
        {            
            HttpContext.Current.Response.AppendHeader("CACHE-CONTROL", "NO-CACHE");
            HttpContext.Current.Response.AppendHeader("Pragma", "NO-CACHE");
            HttpContext.Current.Response.Expires = 0;
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);       
        }

        public static string DevolverURLAmazon(int intIDEntrega, string strIDImagen)
        {
            string strUrl = string.Empty;
            string strBucket = string.Empty;
            string strPais = ConfigurationManager.AppSettings["PAIS"];
            string strAplicacion = ConfigurationManager.AppSettings["APLICACION"];
            string strEntidad = ConfigurationManager.AppSettings["ENTIDAD"];

            IAmazonS3 amazonCliente = new AmazonS3Client(RegionEndpoint.USEast1);

            strBucket = "grupo-altos-rds-baks/Altotrack/" + strPais + "/" + strAplicacion + "/Repositorio/" + strEntidad + "/" + intIDEntrega + "/" + strIDImagen;

            GetPreSignedUrlRequest requestImagen = new GetPreSignedUrlRequest
            {
                BucketName = strBucket,
                Expires = DateTime.Now.AddHours(10)
            };

            strUrl = amazonCliente.GetPreSignedURL(requestImagen);

            return strUrl;
        }

        //        ''' <summary>
        //''' limpia el cache del navegador
        //''' </summary>
        //''' <remarks></remarks>
        //Public Sub LimpiarCache()
        //  Context.Response.AppendHeader("CACHE-CONTROL", "NO-CACHE")
        //  Context.Response.AppendHeader("Pragma", "NO-CACHE")
        //  Context.Response.Expires = 0
        //  Context.Response.Cache.SetCacheability(HttpCacheability.NoCache)
        //End Sub

        public static string GetTransportistaSession(HttpSessionState _session)
        {
            try
            {
                return _session["transportista"].ToString();
            }
            catch
            {
                return "";
            }

        }

        public static string GetIdUsuarioSession(HttpSessionState _session)
        {
            try
            {
                return _session["idusuario"].ToString();
            }
            catch
            {
                return "";
            }

        }

        public static string GetUsuarioSession(HttpSessionState _session)
        {
            try
            {
                return _session["userName"].ToString();
            }
            catch
            {
                return "";
            }

        }

        /// <summary>
        /// Transforma un string en un formato Datetime, dependiendo de la globalization de la aplicación 
        /// </summary>
        /// <param name="pfecha">Fecha en string a convertir</param>
        /// <returns>Fecha convertida según la cultura</returns>
        public static DateTime convertDateIc(string pfecha)
        {
            DateTime _fecrtn;
            try
            {
                // Obtengo la cultura desde el webconfig
                CultureInfo _culture = CultureInfo.CurrentCulture;
                IFormatProvider formatCulture = _culture;

                // Formateo la fecha de acuerdo a la cultura habilitada 
                DateTime.TryParse(pfecha, formatCulture, DateTimeStyles.None, out _fecrtn);

                // Si no se logro dar formato a la fecha
                if (_fecrtn.Year.ToString() == "1")
                {
                    // Habilito la cultura español, para dar un formato estandar a la fecha
                    IFormatProvider tmpformat = new CultureInfo("es-cl");
                    DateTime.TryParse(pfecha, tmpformat, DateTimeStyles.None, out _fecrtn);

                    // Si el formateo estandar da resultado
                    if (_fecrtn.Year.ToString() != "1")
                    {
                        // Vuelvo a intentar dar el formato de acuerdo a la cultura habilitada
                        DateTime.TryParse(_fecrtn.ToString(), formatCulture, DateTimeStyles.None, out _fecrtn);
                    }
                }
            }
            catch (Exception ex)
            {
                string err = ex.Message.ToString();

                // Si existe un error se entrega la fecha de hoy
                _fecrtn = DateTime.Now;
            }
            return _fecrtn;
        }
    }
}
